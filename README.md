[![NuGet Badge](https://buildstats.info/nuget/K3Cloud.WebApi.Core.IoC)](https://www.nuget.org/packages/K3Cloud.WebApi.Core.IoC)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg?style=flat-square&logo=Apache)](LICENSE)
# k3cloud_webapi_sample
项目特色：

1、支持多账套切换

2、支持自定义缓存(默认内存缓存)，缓存故障切换回内存缓存

3、表单实体类生成(C#、Java)

4、表单查询链式调用(支持分页、排序、缓存、汇总、计数)

5、集成json转Lambda表达式进行表单查询

6、支持金蝶云星空所有WebApi接口

7、附公有云代理SQL操作示例


[文档阅读](https://gitee.com/connwap135/k3cloud_webapi_sample/wikis)

[更新记录](https://gitee.com/connwap135/k3cloud_webapi_sample/graph/master)

2023-08-10新增自定义缓存

2023-07-21链式查询支持Select,只输出需要查询的字段

2023-06-09新增 完成[@GTGuang](https://gitee.com/gtguang)的Issues[#I7BSGM](https://gitee.com/connwap135/k3cloud_webapi_sample/issues/I7BSGM)星空账套切换

需要实现多账套切换请appsettings.json配置修改为：
```
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
  "AllowedHosts": "*",
  "K3CLOUDCONFIG": {
    "config1": { // 键值自己随便命名都可以
      "ServerHost": "http://localhost:1200/k3cloud",
      "AcctId": "61271b176f110c",
      "UserName": "宋小康",
      "Password": "123456",
      "AppID": "",
      "AppSecret": "",
      "LCID": 2052,
      "ConnectionValidTime": 10
    },
    "config2": {
      "ServerHost": "https://yyds.test.ik3cloud.com/k3cloud",
      "AcctId": "20186311217100",
      "UserName": "宋小康",
      "AppID": "204614_12cVu21XNUbC0YDoyuG/TZTJ6Lx9RoOF",
      "AppSecret": "b4f16e8a797b85326fc5b86407190772",
      "LCID": 2052,
      "ConnectionValidTime": 10
    }
  }
}
```
使用方法：
```
// 在Program.cs注入
services.AddK3Sugar();

// Controller使用
var jsonDocument = JsonDocument.Parse(jsonStr);
var u = ExpressionBuilder.ParseExpressionOf<SAL_SaleOrder>(jsonDocument);
// 默认第一个配置（config1）
Refasync<int> total = 0;
var res = await K3Scoped.Client.Queryable(u)
    .OrderBy(u => new { u.FId, u.FQty }, new[] { OrderType.Descending, OrderType.Ascending })
    //.WithCache()
    .ToPageListAsync(page, limit, t => t.FMaterialId_FNumber, total);
var sumData = await K3Scoped.Client.Queryable(u).SumAsync(t => new { t.FQty, t.FStockOutQty, t.FRemainOutQty });

// 切换配置
var res = await K3Scoped.GetClient("config2").Queryable(u) // 重点在这里
    .OrderBy(u => new { u.FId, u.FQty }, new[] { OrderType.Descending, OrderType.Ascending })
    //.WithCache() //同样的参数去除缓存才能看到效果
    .ToPageListAsync(page, limit, t => t.FMaterialId_FNumber, total);
var sumData = await K3Scoped.GetClient("config2").Queryable(u).SumAsync(t => new { t.FQty, t.FStockOutQty, t.FRemainOutQty });

```
**完整使用示例请[查看HomeController.cs文件](https://gitee.com/connwap135/k3cloud_webapi_sample/blob/master/k3cloud_webapi_sample/WebDemo/Controllers/HomeController.cs)**

2022-9-22新增
```
1、数字类型字段的IN NotIN支持 如：FQty In(1,2)
2、过滤字符中 大括号包裹 如：FQty greater_than {{fStockOutQty}} 转换为sql语句 FQty>FStockOutQty
```
#### 介绍
金蝶云星空webapi C# Demo 
#####    一个基于 金蝶K3Cloud(星空云) webapi 封装库


#### 测试环境

1. Visual Studio Community 2022
2. .net 7.0+

#### 配置
控制台配置使用
```
using K3Cloud.WebApi.Core.IoC;
using K3Cloud.WebApi.Core.IoC.Types;

K3Services.AddK3Sugar(new K3Config
{
    ServerHost = "http://192.168.0.19/k3cloud",
    AcctId = "6115be3f79b045",
    UserName = "宋小康",
    Password = "123456",
    LCID = "2052"
});
Refasync<int> total = 0;
var res = await K3Scoped.Client.Queryable(u).WithCache(60).ToPageListAsync(1, 10, total);
var sumData = await K3Scoped.Client.Queryable(u).WithCache(60).SumAsync(t => t.FBaseQty);
return Json(new
{
    code = 0,
    count = total.Value,
    totalRow = sumData,
    data = res
});
```
WebMvc 或 WebApi 配置使用
```
appsettings.json
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
  "AllowedHosts": "*",
  "K3CLOUDCONFIG": {
    "ServerHost": "http://192.168.0.19/k3cloud",
    "AcctId": "6115be3f79b045",
    "UserName": "宋小康",
    "Password": "123456",
    "AppID": "",
    "AppSecret": "",
    "LCID": 2052
  }
}

public class Startup
{
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddControllersWithViews();
        var options = Configuration.GetValue<K3Config>("K3CLOUDCONFIG");
        services.AddK3Sugar(options);
    }
}

public class HomeController : Controller
{
    public async Task<IActionResult> IndexAsync()
    {
        Expression<Func<SAL_SaleOrder2, bool>> u = null;// u => u.FBillNo == "TM2205";
        Refasync<int> totalNum = 0;
        var res = await K3Scoped.Client.Queryable(u).Where(t => t.FBillNo == "TM2205").OrderBy(u => u.FId, new[] { OrderType.Descending }).WithCache().ToPageListAsync(1, 10, totalNum);
        var sumData = await K3Scoped.Client.Queryable(u).WithCache().SumAsync(u => new { u.FQty, u.FRemainOutQty, u.FStockOutQty });
        return Json(new
        {
            code = 0,
            totalRow = sumData,
            count = totalNum.Value,
            data = res
        });
    }
}
```
演示单据保存
```
private async Task<decimal> HuilvAsync()
{
    // 从某Api获取当月第一个美元汇率
    return 6.3012;
}
DateTime fDate = DateTime.Now;
var begDate = new DateTime(fDate.Year, fDate.Month, 1);
var endDate = begDate.AddMonths(1).AddDays(-1);
var rateList = await K3Scoped.Client.ExecuteBillQueryAsync<BD_Rate>(t => t.FBegDate == begDate && t.FEndDate == endDate);
// 查询当月美元固定汇率 无则创建 有则更新
if (rateList.Any())
{
    var fid = rateList.First().FRateID;
    await K3Scoped.Client.UnAuditAsync("BD_Rate", fid);
    var curRate = await HuilvAsync();
    var bill = new K3SaveParam<BD_RateModel>
    {
        NeedUpDateFields = new string[] { "FExchangeRate", "FBegDate", "FEndDate" },
        Model = new BD_RateModel
        {
            FRateID = fid,
            FRATETYPEID = K3Number.Parse("HLTX01_SYS"),
            FCyForID = K3Number.Parse("PRE007"),
            FCyToID = K3Number.Parse("PRE001"),
            FExchangeRate = curRate,
            FBegDate = begDate,
            FEndDate = endDate,
            FUseOrgId = K3Number.Parse("100"),
            FCreateOrgId = K3Number.Parse("100"),
        }
    };
    _ = await K3Scoped.Client.SaveAsync(bill, true);
}
else
{
    var curRate = await HuilvAsync();
    if (curRate == 0) return;
    var bill = new K3SaveParam<BD_RateModel>
    {
        Model = new BD_RateModel
        {
            FRateID = 0,
            FRATETYPEID = K3Number.Parse("HLTX01_SYS"),
            FCyForID = K3Number.Parse("PRE007"),
            FCyToID = K3Number.Parse("PRE001"),
            FExchangeRate = curRate,
            FBegDate = begDate,
            FEndDate = endDate,
            FUseOrgId = K3Number.Parse("100"),
            FCreateOrgId = K3Number.Parse("100"),
        }
    };
    _ = await K3Scoped.Client.SaveAsync(bill, true);
}
```
#### 实现方法

1. 建一个与业务表单对象ID(FormId)同名的class(如:销售订单类 SAL_SaleOrder.cs)
2. 在SAL_SaleOrder.cs创建 Field的对应属性(如:单据编号 FBillNo)
3. __ (双横线)替代 .   返回解析数组后 . 再替代 __
4. 利用 SQLBuilder 生成 SQL 

前端提交查询条件:
```
[{"logicalOperator": "and",
 "conditionFieldVal": "FMaterialId__FNumber",
 "conditionOptionVal": "in",
 "conditionValueVal": {"value": "CH4441,CH4443","text": "CH4441,CH4443"}, "conditionValueLeftVal": {"value": "", "text": ""},
 "conditionValueRightVal": {"value": "", "text": ""}
}]
```
后端生成json发送至webapi
```
[{"FormId":"STK_Inventory","FieldKeys":"FOwnerId.FNumber,FOwnerId.FName,FKeeperId.FNumber,FKeeperId.FName,FMaterialId.FNumber,FMaterialName,FModel,FLot.FNumber,FStockId.FNumber,FStockName,FStockStatusId.FNumber,FStockStatusId.FName,FStockUnitId.FNumber,FStockUnitId.FName,FBaseQty",
"FilterString":"FMaterialId.FNumber IN ('CH4441','CH4443')",
"OrderString":"",
"TopRowCount":0,
"StartRow":0,
"Limit":10
}]
```
webapi服务器返回数组
```
[["100","测试账套","100","测试账套","CH4441","测试物料1"," ",null,"CK001","配件仓","KCZT01_SYS","可用","Pcs","Pcs",1280.0000000000],
["100","测试账套","100","测试账套","CH4443","子项物料1"," ",null,"CK003","赠品仓","KCZT002","不参与核算","Pcs","Pcs",98.0000000000],
["100","测试账套","100","测试账套","CH4443","子项物料1"," ",null,"CK001","配件仓","KCZT01_SYS","可用","Pcs","Pcs",10.0000000000],
["100","测试账套","100","测试账套","CH4443","子项物料1"," ",null,"CK002","成品仓","KCZT01_SYS","可用","Pcs","Pcs",0.0],
["100","测试账套","100","测试账套","CH4441","测试物料1"," ",null,"CK002","成品仓","KCZT01_SYS","可用","Pcs","Pcs",0.0]]
```
再将数组翻译成Json
```
[
    {"FOwnerId__FNumber":"100","FOwnerId__FName":"测试账套","FKeeperId__FNumber":"100","FKeeperId__FName":"测试账套","FMaterialId__FNumber":"CH4441","FMaterialName":"测试物料1","FModel":" ","FStockId__FNumber":"CK001","FStockName":"配件仓","FStockStatusId__FNumber":"KCZT01_SYS","FStockStatusId__FName":"可用","FStockUnitId__FNumber":"Pcs","FStockUnitId__FName":"Pcs","FBaseQty":1280.0},
    {"FOwnerId__FNumber":"100","FOwnerId__FName":"测试账套","FKeeperId__FNumber":"100","FKeeperId__FName":"测试账套","FMaterialId__FNumber":"CH4443","FMaterialName":"子项物料1","FModel":" ","FStockId__FNumber":"CK003","FStockName":"赠品仓","FStockStatusId__FNumber":"KCZT002","FStockStatusId__FName":"不参与核算","FStockUnitId__FNumber":"Pcs","FStockUnitId__FName":"Pcs","FBaseQty":98.0},
    {"FOwnerId__FNumber":"100","FOwnerId__FName":"测试账套","FKeeperId__FNumber":"100","FKeeperId__FName":"测试账套","FMaterialId__FNumber":"CH4443","FMaterialName":"子项物料1","FModel":" ","FStockId__FNumber":"CK001","FStockName":"配件仓","FStockStatusId__FNumber":"KCZT01_SYS","FStockStatusId__FName":"可用","FStockUnitId__FNumber":"Pcs","FStockUnitId__FName":"Pcs","FBaseQty":10.0},
    {"FOwnerId__FNumber":"100","FOwnerId__FName":"测试账套","FKeeperId__FNumber":"100","FKeeperId__FName":"测试账套","FMaterialId__FNumber":"CH4443","FMaterialName":"子项物料1","FModel":" ","FStockId__FNumber":"CK002","FStockName":"成品仓","FStockStatusId__FNumber":"KCZT01_SYS","FStockStatusId__FName":"可用","FStockUnitId__FNumber":"Pcs","FStockUnitId__FName":"Pcs","FBaseQty":0.0},
    {"FOwnerId__FNumber":"100","FOwnerId__FName":"测试账套","FKeeperId__FNumber":"100","FKeeperId__FName":"测试账套","FMaterialId__FNumber":"CH4441","FMaterialName":"测试物料1","FModel":" ","FStockId__FNumber":"CK002","FStockName":"成品仓","FStockStatusId__FNumber":"KCZT01_SYS","FStockStatusId__FName":"可用","FStockUnitId__FNumber":"Pcs","FStockUnitId__FName":"Pcs","FBaseQty":0.0}
]
```
### 截图
![](Screenshot/2.png)
![](Screenshot/3.png)