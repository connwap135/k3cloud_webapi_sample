﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebDemo.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using WebDemo.Models;

namespace WebDemo.Helpers.Tests
{
    [TestClass()]
    public class EnumToStringConverterTests
    {
        private readonly JsonSerializerOptions _options;
        public EnumToStringConverterTests()
        {
            _options = new JsonSerializerOptions
            {
                Converters = { new EnumToStringConverter() }
            };
        }
        [TestMethod()]
        public void ReadTest()
        {
            var json = "\"审核\"";
            var status = JsonSerializer.Deserialize<KFDocumentStatus>(json, _options);
            Assert.AreEqual(KFDocumentStatus.C, status);
        }
        [TestMethod]
        public void ReadTest2()
        {
            var json = "\"C\"";
            var status = JsonSerializer.Deserialize<KFDocumentStatus>(json, _options);
            Assert.AreEqual(KFDocumentStatus.C, status);
        }
        [TestMethod]
        public void ReadTest3()
        {
            var json = "\"未知状态\"";
            Assert.ThrowsException<ArgumentException>(() => JsonSerializer.Deserialize<KFDocumentStatus>(json, _options));
        }

        [TestMethod()]
        public void WriteTest()
        {

        }
    }
}