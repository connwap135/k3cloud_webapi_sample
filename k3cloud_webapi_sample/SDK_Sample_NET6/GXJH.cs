﻿namespace SDK_Sample_NET6
{
    public class GXJH
    {
        /// <summary>
        /// 自动审核
        /// </summary>
        public bool AutoAudit { get; set; } = false;
        public List<GXDatas> Datas { get; set; }


        public class GXDetailIds
        {
            /// <summary>
            /// 工序计划序列Id
            /// </summary>
            public int EntryId { get; set; }
            /// <summary>
            /// 工序计划工序Id
            /// </summary>
            public int DetailId { get; set; }
            /// <summary>
            /// 合格数量，不需要改数量可以不填
            /// </summary>
            public int QuaQty { get; set; }
            /// <summary>
            /// 工废数量，不需要改数量可以不填
            /// </summary>
            public int ProcessFailQty { get; set; }
            /// <summary>
            /// 料废数量，不需要改数量可以不填
            /// </summary>
            public int MaterialFailQty { get; set; }
            /// <summary>
            /// 完工数量
            /// </summary>
            public int FinishQty { get; set; }
            /// <summary>
            /// 班次，不需要改可以不填
            /// </summary>
            public int ShiftSliceId { get; set; }
            /// <summary>
            /// 班组，不需要改可以不填
            /// </summary>
            public int ShiftGroupId { get; set; }
            /// <summary>
            /// 资源，不需要改可以不填
            /// </summary>
            public int ResourceId { get; set; }
            /// <summary>
            /// 设备，不需要改可以不填
            /// </summary>
            public int EquipmentId { get; set; }
            /// <summary>
            /// 检验员，不需要改可以不填
            /// </summary>
            public int Inspector { get; set; }
            /// <summary>
            /// 加工开始时间
            /// </summary>
            public string ProcessStartTime { get; set; }
            /// <summary>
            /// 加工完成时间,不能大于当前时间
            /// </summary>
            public string ProcessEndTime { get; set; }

            public List<GXEpmIds> EpmIds { get; set; }


        }

        public class GXDatas

        {
            public int Id { get; set; }
            public List<GXDetailIds> DetailIds { get; set; }
        }
        public class GXEpmIds
        {
            /// <summary>
            /// 操作工，不需要改可以不填
            /// </summary>
            public int EpmId { get; set; }
        }
    }
}
