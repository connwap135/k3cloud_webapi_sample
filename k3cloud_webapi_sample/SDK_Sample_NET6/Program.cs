﻿using Kingdee.CDP.WebApi.SDK;

namespace SDK_Sample_NET6
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //Console.WriteLine("Hello, World!");
            K3CloudApi clienter = new();
            //var param = new QueryParam()
            //{
            //    FormId = "BD_Empinfo",
            //    FieldKeys = "FName,FStaffNumber,FJoinDate",
            //};
            ////调用单据查询接口
            //var returnInfo = clienter.ExecuteBillQuery(param.ToJson());
            //if (returnInfo.Count == 1)
            //{
            //    var resultJObject = JArray.Parse(JsonConvert.SerializeObject(returnInfo[0]));
            //    Console.WriteLine(resultJObject);
            //}
            GXJH datas = new()
            {
                AutoAudit = true,
                Datas = new List<GXJH.GXDatas>{
                    new() {
                        Id = 104550,
                        DetailIds=new List<GXJH.GXDetailIds> {
                            new() {
                                EntryId = 104551,
                                DetailId=113153,
                                QuaQty=1,
                                FinishQty=1,
                                EpmIds=new List<GXJH.GXEpmIds>
                                {
                                    new() {
                                        EpmId=100616
                                    }
                                }
                            }
                        }
                    }
                }
            };
            string str = clienter.Execute<string>("Kingdee.K3.MFG.WebApi.ServicesStub.OptPlanOptRtpApiService.Push", new object[] { datas });
            Console.WriteLine(str);



        }

    }
}