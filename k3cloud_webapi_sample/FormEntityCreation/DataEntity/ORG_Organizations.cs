using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormEntityCreation.DataEntity
{
	/// <summary>
	/// 组织机构
	/// </summary>
	public class ORG_Organizations
	{
		/// <summary>
		/// FOrgID
		/// </summary>
		public System.Int32 FOrgID { get; set; }
		#region 组织机构
		/// <summary>
		/// 数据状态
		/// </summary>
		public string? FDocumentStatus { get; set; }
		/// <summary>
		/// 禁用状态
		/// </summary>
		public string? FForbidStatus { get; set; }
		/// <summary>
		/// 名称
		/// </summary>
		public string FName { get; set; }
		/// <summary>
		/// 编码
		/// </summary>
		public string FNumber { get; set; }
		/// <summary>
		/// 修改人
		/// </summary>
		public long? FModifierId { get; set; }
		/// <summary>
		/// 修改人用户名称
		/// </summary>
		public string FModifierId__FName { get; set; }
		/// <summary>
		/// 创建人
		/// </summary>
		public long? FCreatorId { get; set; }
		/// <summary>
		/// 创建人用户名称
		/// </summary>
		public string FCreatorId__FName { get; set; }
		/// <summary>
		/// 创建日期
		/// </summary>
		public DateTime? FCreateDate { get; set; }
		/// <summary>
		/// 修改日期
		/// </summary>
		public DateTime? FModifyDate { get; set; }
		/// <summary>
		/// 描述
		/// </summary>
		public string? FDescription { get; set; }
		/// <summary>
		/// 联系人
		/// </summary>
		public string? FContact { get; set; }
		/// <summary>
		/// 形态
		/// </summary>
		public string FOrgFormID { get; set; }
		/// <summary>
		/// 地址
		/// </summary>
		public string? FADDRESS { get; set; }
		/// <summary>
		/// 联系电话
		/// </summary>
		public string? FTel { get; set; }
		/// <summary>
		/// 核算组织类型
		/// </summary>
		public string? FAcctOrgType { get; set; }
		/// <summary>
		/// 所属法人
		/// </summary>
		public long? FParentID { get; set; }
		/// <summary>
		/// 所属法人名称
		/// </summary>
		public string FParentID__FName { get; set; }
		/// <summary>
		/// 所属法人编码
		/// </summary>
		public string FParentID__FNumber { get; set; }
		/// <summary>
		/// 禁用人
		/// </summary>
		public long? FFORBIDORID { get; set; }
		/// <summary>
		/// 禁用人用户名称
		/// </summary>
		public string FFORBIDORID__FName { get; set; }
		/// <summary>
		/// 禁用日期
		/// </summary>
		public DateTime? FForbidDate { get; set; }
		/// <summary>
		/// 业务组织
		/// </summary>
		public string? FIsBusinessOrg { get; set; }
		/// <summary>
		/// 核算组织
		/// </summary>
		public string? FIsAccountOrg { get; set; }
		/// <summary>
		/// 审核日期
		/// </summary>
		public DateTime? FAUDITDATE { get; set; }
		/// <summary>
		/// 审核人
		/// </summary>
		public long? FAUDITORID { get; set; }
		/// <summary>
		/// 审核人用户名称
		/// </summary>
		public string FAUDITORID__FName { get; set; }
		/// <summary>
		/// 邮编
		/// </summary>
		public string? FPostCode { get; set; }
		/// <summary>
		/// 组织职能
		/// </summary>
		public string? FOrgFunctions { get; set; }
		/// <summary>
		/// 同步管易货主
		/// </summary>
		public string? FIsSynCERPOwer { get; set; }
		/// <summary>
		/// 时区
		/// </summary>
		public long? FTimeZone { get; set; }
		/// <summary>
		/// 时区名称
		/// </summary>
		public string FTimeZone__FName { get; set; }
		/// <summary>
		/// 时区编码
		/// </summary>
		public string FTimeZone__FNumber { get; set; }
		#endregion
	}
}