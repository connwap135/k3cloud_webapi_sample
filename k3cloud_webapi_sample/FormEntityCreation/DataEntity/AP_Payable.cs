using System;

namespace FormEntityCreation.DataEntity
{
    /// <summary>
    /// 应付单
    /// </summary>
    public class AP_Payable
    {
        /// <summary>
        /// FID
        /// </summary>
        public int FID { get; set; }
        #region 单据头
        /// <summary>
        /// 单据编号
        /// </summary>
        public string? FBillNo { get; set; }
        /// <summary>
        /// 单据状态
        /// </summary>
        public string FDOCUMENTSTATUS { get; set; }
        /// <summary>
        /// 表头基本 - 付（退）款单的关联金额（作废）
        /// </summary>
        public decimal? FRelateHadPayAmount { get; set; }
        /// <summary>
        /// 业务日期
        /// </summary>
        public DateTime FDATE { get; set; }
        /// <summary>
        /// 结算组织
        /// </summary>
        public long FSETTLEORGID { get; set; }
        /// <summary>
        /// 结算组织名称
        /// </summary>
        public string FSETTLEORGID__FName { get; set; }
        /// <summary>
        /// 结算组织编码
        /// </summary>
        public string FSETTLEORGID__FNumber { get; set; }
        /// <summary>
        /// 币别
        /// </summary>
        public long FCURRENCYID { get; set; }
        /// <summary>
        /// 币别名称
        /// </summary>
        public string FCURRENCYID__FName { get; set; }
        /// <summary>
        /// 币别编码
        /// </summary>
        public string FCURRENCYID__FNumber { get; set; }
        /// <summary>
        /// 单据类型
        /// </summary>
        public string FBillTypeID { get; set; }
        /// <summary>
        /// 单据类型名称
        /// </summary>
        public string FBillTypeID__FName { get; set; }
        /// <summary>
        /// 单据类型编码
        /// </summary>
        public string FBillTypeID__FNumber { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        public long? FModifierId { get; set; }
        /// <summary>
        /// 修改人用户名称
        /// </summary>
        public string FModifierId__FName { get; set; }
        /// <summary>
        /// 审核日期
        /// </summary>
        public DateTime? FAPPROVEDATE { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime? FCreateDate { get; set; }
        /// <summary>
        /// 修改日期
        /// </summary>
        public DateTime? FModifyDate { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        public long? FAPPROVERID { get; set; }
        /// <summary>
        /// 审核人用户名称
        /// </summary>
        public string FAPPROVERID__FName { get; set; }
        /// <summary>
        /// 表头基本 - 付款核销状态
        /// </summary>
        public string? FWRITTENOFFSTATUS { get; set; }
        /// <summary>
        /// 表头基本 - 开票核销状态
        /// </summary>
        public string? FOPENSTATUS { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public long? FCreatorId { get; set; }
        /// <summary>
        /// 创建人用户名称
        /// </summary>
        public string FCreatorId__FName { get; set; }
        /// <summary>
        /// 价税合计
        /// </summary>
        public decimal? FALLAMOUNTFOR { get; set; }
        /// <summary>
        /// 到期日
        /// </summary>
        public DateTime FENDDATE_H { get; set; }
        /// <summary>
        /// 会计核算体系
        /// </summary>
        public long? FACCOUNTSYSTEM { get; set; }
        /// <summary>
        /// 会计核算体系名称
        /// </summary>
        public string FACCOUNTSYSTEM__FName { get; set; }
        /// <summary>
        /// 会计核算体系编码
        /// </summary>
        public string FACCOUNTSYSTEM__FNumber { get; set; }
        /// <summary>
        /// 按含税单价录入
        /// </summary>
        public string? FISTAX { get; set; }
        /// <summary>
        /// 作废人
        /// </summary>
        public long? FCancellerId { get; set; }
        /// <summary>
        /// 作废人用户名称
        /// </summary>
        public string FCancellerId__FName { get; set; }
        /// <summary>
        /// 作废日期
        /// </summary>
        public DateTime? FCancelDate { get; set; }
        /// <summary>
        /// 作废状态
        /// </summary>
        public string FCancelStatus { get; set; }
        /// <summary>
        /// 付款条件
        /// </summary>
        public long? FPayConditon { get; set; }
        /// <summary>
        /// 付款条件名称
        /// </summary>
        public string FPayConditon__FName { get; set; }
        /// <summary>
        /// 付款条件编码
        /// </summary>
        public string FPayConditon__FNumber { get; set; }
        /// <summary>
        /// 供应商
        /// </summary>
        public long FSUPPLIERID { get; set; }
        /// <summary>
        /// 供应商名称
        /// </summary>
        public string FSUPPLIERID__FName { get; set; }
        /// <summary>
        /// 供应商编码
        /// </summary>
        public string FSUPPLIERID__FNumber { get; set; }
        /// <summary>
        /// 供应商简称
        /// </summary>
        public string FSUPPLIERID__FShortName { get; set; }
        /// <summary>
        /// 采购组织
        /// </summary>
        public long? FPURCHASEORGID { get; set; }
        /// <summary>
        /// 采购组织名称
        /// </summary>
        public string FPURCHASEORGID__FName { get; set; }
        /// <summary>
        /// 采购组织编码
        /// </summary>
        public string FPURCHASEORGID__FNumber { get; set; }
        /// <summary>
        /// 业务类型
        /// </summary>
        public string FBUSINESSTYPE { get; set; }
        /// <summary>
        /// 采购部门
        /// </summary>
        public long? FPURCHASEDEPTID { get; set; }
        /// <summary>
        /// 采购部门名称
        /// </summary>
        public string FPURCHASEDEPTID__FName { get; set; }
        /// <summary>
        /// 采购部门编码
        /// </summary>
        public string FPURCHASEDEPTID__FNumber { get; set; }
        /// <summary>
        /// 采购组
        /// </summary>
        public long? FPURCHASERGROUPID { get; set; }
        /// <summary>
        /// 采购组业务组名称
        /// </summary>
        public string FPURCHASERGROUPID__FName { get; set; }
        /// <summary>
        /// 采购组业务组编码
        /// </summary>
        public string FPURCHASERGROUPID__FNumber { get; set; }
        /// <summary>
        /// 采购员
        /// </summary>
        public long? FPURCHASERID { get; set; }
        /// <summary>
        /// 采购员名称
        /// </summary>
        public string FPURCHASERID__FName { get; set; }
        /// <summary>
        /// 采购员职员编码
        /// </summary>
        public string FPURCHASERID__FNumber { get; set; }
        /// <summary>
        /// 源单类型
        /// </summary>
        public string? FSourceBillType { get; set; }
        /// <summary>
        /// 是否期初单据
        /// </summary>
        public string? FISINIT { get; set; }
        /// <summary>
        /// 入库单业务类型
        /// </summary>
        public string? FINSTOCKBUSTYPE { get; set; }
        /// <summary>
        /// 退料单业务类型
        /// </summary>
        public string? FMRBBUSTYPE { get; set; }
        /// <summary>
        /// 付款组织
        /// </summary>
        public long FPAYORGID { get; set; }
        /// <summary>
        /// 付款组织名称
        /// </summary>
        public string FPAYORGID__FName { get; set; }
        /// <summary>
        /// 付款组织编码
        /// </summary>
        public string FPAYORGID__FNumber { get; set; }
        /// <summary>
        /// 是发票审核自动生成
        /// </summary>
        public string? FISBYIV { get; set; }
        /// <summary>
        /// 是否需要成本调整
        /// </summary>
        public string? FISGENHSADJ { get; set; }
        /// <summary>
        /// 税额计入成本
        /// </summary>
        public string? FISTAXINCOST { get; set; }
        /// <summary>
        /// 核销方式
        /// </summary>
        public int? FMatchMethodID { get; set; }
        /// <summary>
        /// 价外税
        /// </summary>
        public string? FISPRICEEXCLUDETAX { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string? FAP_Remark { get; set; }
        /// <summary>
        /// 立账类型
        /// </summary>
        public string? FSetAccountType { get; set; }
        /// <summary>
        /// 参与暂估应付核销
        /// </summary>
        public string? FISHookMatch { get; set; }
        /// <summary>
        /// 转销
        /// </summary>
        public string? FIsWriteOff { get; set; }
        /// <summary>
        /// 先到票后入库
        /// </summary>
        public string? FISINVOICEARLIER { get; set; }
        /// <summary>
        /// 红蓝字
        /// </summary>
        public string? FRedBlue { get; set; }
        /// <summary>
        /// 生成方式
        /// </summary>
        public string? FByVerify { get; set; }
        /// <summary>
        /// 扫描点
        /// </summary>
        public long? FScanPoint { get; set; }
        /// <summary>
        /// 扫描点名称
        /// </summary>
        public string FScanPoint__FName { get; set; }
        /// <summary>
        /// 扫描点编码
        /// </summary>
        public string FScanPoint__FNumber { get; set; }
        /// <summary>
        /// 现购
        /// </summary>
        public string? FISCASHPURCHASE { get; set; }
        /// <summary>
        /// 转出供应商
        /// </summary>
        public long? FOUTSUPPLIERID { get; set; }
        /// <summary>
        /// 转出供应商名称
        /// </summary>
        public string FOUTSUPPLIERID__FName { get; set; }
        /// <summary>
        /// 转出供应商编码
        /// </summary>
        public string FOUTSUPPLIERID__FNumber { get; set; }
        /// <summary>
        /// 转出供应商简称
        /// </summary>
        public string FOUTSUPPLIERID__FShortName { get; set; }
        /// <summary>
        /// 外部单据
        /// </summary>
        public string? FISINSIDEBILL { get; set; }
        /// <summary>
        /// 开票核销记录序号
        /// </summary>
        public int? FBILLMATCHLOGID { get; set; }
        /// <summary>
        /// 反写开票数量
        /// </summary>
        public string? FWBOPENQTY { get; set; }
        /// <summary>
        /// 预设基础资料字段1
        /// </summary>
        public long? FPRESETBASE1 { get; set; }
        /// <summary>
        /// 预设基础资料字段1名称
        /// </summary>
        public string FPRESETBASE1__FName { get; set; }
        /// <summary>
        /// 预设基础资料字段1编码
        /// </summary>
        public string FPRESETBASE1__FNumber { get; set; }
        /// <summary>
        /// 预设基础资料字段2
        /// </summary>
        public long? FPRESETBASE2 { get; set; }
        /// <summary>
        /// 预设基础资料字段2名称
        /// </summary>
        public string FPRESETBASE2__FName { get; set; }
        /// <summary>
        /// 预设基础资料字段2编码
        /// </summary>
        public string FPRESETBASE2__FNumber { get; set; }
        /// <summary>
        /// 预设文本字段1
        /// </summary>
        public string? FPRESETTEXT1 { get; set; }
        /// <summary>
        /// 预设文本字段2
        /// </summary>
        public string? FPRESETTEXT2 { get; set; }
        /// <summary>
        /// 预设辅助资料字段2
        /// </summary>
        public string? FPRESETASSISTANT2 { get; set; }
        /// <summary>
        /// 预设辅助资料字段2编码
        /// </summary>
        public string FPRESETASSISTANT2__FNumber { get; set; }
        /// <summary>
        /// 预设辅助资料字段2名称
        /// </summary>
        public string FPRESETASSISTANT2__FDataValue { get; set; }
        /// <summary>
        /// 预设辅助资料字段1
        /// </summary>
        public string? FPRESETASSISTANT1 { get; set; }
        /// <summary>
        /// 预设辅助资料字段1编码
        /// </summary>
        public string FPRESETASSISTANT1__FNumber { get; set; }
        /// <summary>
        /// 预设辅助资料字段1名称
        /// </summary>
        public string FPRESETASSISTANT1__FDataValue { get; set; }
        /// <summary>
        /// 按费用项目生成计划
        /// </summary>
        public string? FIsGeneratePlanByCostItem { get; set; }
        /// <summary>
        /// 供应商协同平台确认人
        /// </summary>
        public long? FSCPCONFIRMERID { get; set; }
        /// <summary>
        /// 供应商协同平台确认人用户名称
        /// </summary>
        public string FSCPCONFIRMERID__FName { get; set; }
        /// <summary>
        /// 供应商协同平台确认日期
        /// </summary>
        public DateTime? FSCPCONFIRMDATE { get; set; }
        /// <summary>
        /// 供应商协同平台确认状态
        /// </summary>
        public string? FSCPCONFIRMSTATUS { get; set; }
        /// <summary>
        /// 整单折扣金额
        /// </summary>
        public decimal? FOrderDiscountAmountFor { get; set; }
        /// <summary>
        /// 新直运业务
        /// </summary>
        public string? FISZY { get; set; }
        #endregion
        #region 明细
        /// <summary>
        /// 明细主键FEntryID
        /// </summary>
        public long FEntityDetail_FEntryID { get; set; }
        /// <summary>
        /// 明细序列
        /// </summary>
        public int FEntityDetail_FSeq { get; set; }
        /// <summary>
        /// 物料编码
        /// </summary>
        public long? FMATERIALID { get; set; }
        /// <summary>
        /// 物料编码名称
        /// </summary>
        public string FMATERIALID__FName { get; set; }
        /// <summary>
        /// 物料编码编码
        /// </summary>
        public string FMATERIALID__FNumber { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        public decimal? FPrice { get; set; }
        /// <summary>
        /// 含税单价
        /// </summary>
        public decimal? FTaxPrice { get; set; }
        /// <summary>
        /// 价格系数
        /// </summary>
        public decimal? FPriceCoefficient { get; set; }
        /// <summary>
        /// 计价数量
        /// </summary>
        public decimal? FPriceQty { get; set; }
        /// <summary>
        /// 折扣额
        /// </summary>
        public decimal? FDISCOUNTAMOUNTFOR { get; set; }
        /// <summary>
        /// 折扣率(%)
        /// </summary>
        public decimal? FEntryDiscountRate { get; set; }
        /// <summary>
        /// 不含税金额
        /// </summary>
        public decimal? FNoTaxAmountFor_D { get; set; }
        /// <summary>
        /// 税率(%)
        /// </summary>
        public decimal? FEntryTaxRate { get; set; }
        /// <summary>
        /// 税额
        /// </summary>
        public decimal? FTAXAMOUNTFOR_D { get; set; }
        /// <summary>
        /// 价税合计
        /// </summary>
        public decimal? FALLAMOUNTFOR_D { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string? FComment { get; set; }
        /// <summary>
        /// 采购发票价税合计
        /// </summary>
        public decimal? FIVALLAMOUNTFOR { get; set; }
        /// <summary>
        /// 折扣额本位币
        /// </summary>
        public decimal? FDISCOUNTAMOUNT { get; set; }
        /// <summary>
        /// 税额本位币
        /// </summary>
        public decimal? FTAXAMOUNT_D { get; set; }
        /// <summary>
        /// 价税合计本位币
        /// </summary>
        public decimal? FALLAMOUNT_D { get; set; }
        /// <summary>
        /// 不含税金额本位币
        /// </summary>
        public decimal? FNOTAXAMOUNT_D { get; set; }
        /// <summary>
        /// 表体明细 - 开票核销状态
        /// </summary>
        public string? FOPENSTATUS_D { get; set; }
        /// <summary>
        /// 表体明细 - 已开票核销金额
        /// </summary>
        public decimal? FOPENAMOUNTFOR_D { get; set; }
        /// <summary>
        /// 采购订单号
        /// </summary>
        public string? FORDERNUMBER { get; set; }
        /// <summary>
        /// 计价单位
        /// </summary>
        public long? FPRICEUNITID { get; set; }
        /// <summary>
        /// 计价单位名称
        /// </summary>
        public string FPRICEUNITID__FName { get; set; }
        /// <summary>
        /// 计价单位编码
        /// </summary>
        public string FPRICEUNITID__FNumber { get; set; }
        /// <summary>
        /// 基本单位
        /// </summary>
        public long? FBASICUNITID { get; set; }
        /// <summary>
        /// 基本单位名称
        /// </summary>
        public string FBASICUNITID__FName { get; set; }
        /// <summary>
        /// 基本单位编码
        /// </summary>
        public string FBASICUNITID__FNumber { get; set; }
        /// <summary>
        /// 计价基本数量
        /// </summary>
        public decimal? FBASICUNITQTY { get; set; }
        /// <summary>
        /// 含税净价
        /// </summary>
        public decimal? FPriceWithTax { get; set; }
        /// <summary>
        /// 基本单位关联数量
        /// </summary>
        public decimal? FBaseJoinQty { get; set; }
        /// <summary>
        /// 源单类型
        /// </summary>
        public string? FSOURCETYPE { get; set; }
        /// <summary>
        /// 源单编号
        /// </summary>
        public string? FSourceBillNo { get; set; }
        /// <summary>
        /// 税组合
        /// </summary>
        public long? FTaxCombination { get; set; }
        /// <summary>
        /// 税组合名称
        /// </summary>
        public string FTaxCombination__FName { get; set; }
        /// <summary>
        /// 税组合编码
        /// </summary>
        public string FTaxCombination__FNumber { get; set; }
        /// <summary>
        /// 采购发票基本单位数量
        /// </summary>
        public decimal? FBUYIVBASICQTY { get; set; }
        /// <summary>
        /// 采购发票数量
        /// </summary>
        public decimal? FBUYIVQTY { get; set; }
        /// <summary>
        /// 已开票核销数量
        /// </summary>
        public decimal? FOPENQTY_D { get; set; }
        /// <summary>
        /// 卖家代扣增值税税率
        /// </summary>
        public decimal? FTEMPTAXRATE { get; set; }
        /// <summary>
        /// 已核销不含税金额
        /// </summary>
        public decimal? FMatchNotTaxAmtFor { get; set; }
        /// <summary>
        /// 已开票金额（下推审核反写、核销反写）
        /// </summary>
        public decimal? FHadMatchAmountFor { get; set; }
        /// <summary>
        /// 结算清单调拨类型
        /// </summary>
        public string? FSettleTran { get; set; }
        /// <summary>
        /// 特殊核销数量
        /// </summary>
        public decimal? FSpecialQTY { get; set; }
        /// <summary>
        /// 特殊核销金额
        /// </summary>
        public decimal? FSpecialAmountFor { get; set; }
        /// <summary>
        /// 业务单据分录内码
        /// </summary>
        public int? FBizEntryId { get; set; }
        /// <summary>
        /// 业务单据内码
        /// </summary>
        public int? FBizID { get; set; }
        /// <summary>
        /// 业务单据类型
        /// </summary>
        public long? FBizBillTypeId { get; set; }
        /// <summary>
        /// 业务单据类型名称
        /// </summary>
        public string FBizBillTypeId__FName { get; set; }
        /// <summary>
        /// 业务单据类型编码
        /// </summary>
        public string FBizBillTypeId__FNumber { get; set; }
        /// <summary>
        /// 业务单据编号
        /// </summary>
        public string? FBizBillNo { get; set; }
        /// <summary>
        /// 业务单据名称
        /// </summary>
        public long? FBizFormId { get; set; }
        /// <summary>
        /// 业务单据名称名称
        /// </summary>
        public string FBizFormId__FName { get; set; }
        /// <summary>
        /// 业务单据名称编码
        /// </summary>
        public string FBizFormId__FNumber { get; set; }
        /// <summary>
        /// 物料说明
        /// </summary>
        public string? FMaterialDesc { get; set; }
        /// <summary>
        /// 分配状态
        /// </summary>
        public string? FALLOCATESTATUS { get; set; }
        /// <summary>
        /// 勾稽状态
        /// </summary>
        public string? FHOOKSTATUS { get; set; }
        /// <summary>
        /// 费用项目编码
        /// </summary>
        public long? FCOSTID { get; set; }
        /// <summary>
        /// 费用项目编码名称
        /// </summary>
        public string FCOSTID__FName { get; set; }
        /// <summary>
        /// 费用项目编码编码
        /// </summary>
        public string FCOSTID__FNumber { get; set; }
        /// <summary>
        /// 计入采购成本
        /// </summary>
        public string? FINCLUDECOST { get; set; }
        /// <summary>
        /// 入库单号
        /// </summary>
        public string? FINSTOCKID { get; set; }
        /// <summary>
        /// 入库单号名称
        /// </summary>
        public string FINSTOCKID__FName { get; set; }
        /// <summary>
        /// 入库单号编码
        /// </summary>
        public string FINSTOCKID__FNumber { get; set; }
        /// <summary>
        /// 出库单号
        /// </summary>
        public string? FOUTSTOCKID { get; set; }
        /// <summary>
        /// 出库单号名称
        /// </summary>
        public string FOUTSTOCKID__FName { get; set; }
        /// <summary>
        /// 出库单号编码
        /// </summary>
        public string FOUTSTOCKID__FNumber { get; set; }
        /// <summary>
        /// 是否有出库单号
        /// </summary>
        public string? FISOUTSTOCK { get; set; }
        /// <summary>
        /// 费用承担部门
        /// </summary>
        public long? FCOSTDEPARTMENTID { get; set; }
        /// <summary>
        /// 费用承担部门名称
        /// </summary>
        public string FCOSTDEPARTMENTID__FName { get; set; }
        /// <summary>
        /// 费用承担部门编码
        /// </summary>
        public string FCOSTDEPARTMENTID__FNumber { get; set; }
        /// <summary>
        /// 辅助属性
        /// </summary>
        public long? FAUXPROPID { get; set; }
        /// <summary>
        /// 批号
        /// </summary>
        public long? FLot { get; set; }
        /// <summary>
        /// 批号名称
        /// </summary>
        public string FLot__FName { get; set; }
        /// <summary>
        /// 批号批号
        /// </summary>
        public string FLot__FNumber { get; set; }
        /// <summary>
        /// 卡片编码
        /// </summary>
        public long? FASSETID { get; set; }
        /// <summary>
        /// 卡片编码资产名称
        /// </summary>
        public string FASSETID__FName { get; set; }
        /// <summary>
        /// 卡片编码卡片编码
        /// </summary>
        public string FASSETID__FNumber { get; set; }
        /// <summary>
        /// 生产订单编号
        /// </summary>
        public string? FMONUMBER { get; set; }
        /// <summary>
        /// 生产订单行号
        /// </summary>
        public int? FMOENTRYSEQ { get; set; }
        /// <summary>
        /// 工序计划号
        /// </summary>
        public string? FOPNO { get; set; }
        /// <summary>
        /// 序列号
        /// </summary>
        public string? FSEQNUMBER { get; set; }
        /// <summary>
        /// 工序号
        /// </summary>
        public int? FOPERNUMBER { get; set; }
        /// <summary>
        /// 作业
        /// </summary>
        public long? FPROCESSID { get; set; }
        /// <summary>
        /// 作业名称
        /// </summary>
        public string FPROCESSID__FName { get; set; }
        /// <summary>
        /// 作业编码
        /// </summary>
        public string FPROCESSID__FNumber { get; set; }
        /// <summary>
        /// 生产车间
        /// </summary>
        public long? FFPRODEPARTMENTID { get; set; }
        /// <summary>
        /// 生产车间名称
        /// </summary>
        public string FFPRODEPARTMENTID__FName { get; set; }
        /// <summary>
        /// 生产车间编码
        /// </summary>
        public string FFPRODEPARTMENTID__FNumber { get; set; }
        /// <summary>
        /// 入库类型
        /// </summary>
        public string? FWWINTYPE { get; set; }
        /// <summary>
        /// 是否赠品
        /// </summary>
        public string? FIsFree { get; set; }
        /// <summary>
        /// 库存单位
        /// </summary>
        public long? FStockUnitId { get; set; }
        /// <summary>
        /// 库存单位名称
        /// </summary>
        public string FStockUnitId__FName { get; set; }
        /// <summary>
        /// 库存单位编码
        /// </summary>
        public string FStockUnitId__FNumber { get; set; }
        /// <summary>
        /// 库存基本数量
        /// </summary>
        public decimal? FStockBaseQty { get; set; }
        /// <summary>
        /// 库存数量
        /// </summary>
        public decimal? FStockQty { get; set; }
        /// <summary>
        /// 计价基本分母
        /// </summary>
        public decimal? FPriceBaseDen { get; set; }
        /// <summary>
        /// 库存基本分子
        /// </summary>
        public decimal? FStockBaseNum { get; set; }
        /// <summary>
        /// 采购订单明细内码
        /// </summary>
        public int? FOrderEntryID { get; set; }
        /// <summary>
        /// 采购订单行号
        /// </summary>
        public int? FORDERENTRYSEQ { get; set; }
        /// <summary>
        /// 已结算金额
        /// </summary>
        public decimal? FPAYMENTAMOUNT { get; set; }
        /// <summary>
        /// 采购发票数量期初
        /// </summary>
        public decimal? FBUYIVINIQTY { get; set; }
        /// <summary>
        /// 采购发票基本单位数量期初
        /// </summary>
        public decimal? FBUYIVINIBASICQTY { get; set; }
        /// <summary>
        /// 采购发票价税合计期初
        /// </summary>
        public decimal? FIVINIALLAMOUNTFOR { get; set; }
        /// <summary>
        /// 已下推负数财务应付数量
        /// </summary>
        public decimal? FPushRedQty { get; set; }
        /// <summary>
        /// 源单行内码
        /// </summary>
        public int? FSRCROWID { get; set; }
        /// <summary>
        /// 已完全到票
        /// </summary>
        public string? FISINVOICEALLARRIVED { get; set; }
        /// <summary>
        /// 零售条形码
        /// </summary>
        public string? FBarcode { get; set; }
        /// <summary>
        /// 成本差异金额
        /// </summary>
        public decimal? FDIFFAMOUNT { get; set; }
        /// <summary>
        /// 价税合计差异金额
        /// </summary>
        public decimal? FDIFFALLAMOUNT { get; set; }
        /// <summary>
        /// 成本汇率差异金额
        /// </summary>
        public decimal? FDIFFAMOUNTEXRATE { get; set; }
        /// <summary>
        /// 价税合计汇率差异金额
        /// </summary>
        public decimal? FDIFFALLAMOUNTEXRATE { get; set; }
        /// <summary>
        /// 未结算金额
        /// </summary>
        public decimal? FNORECEIVEAMOUNT { get; set; }
        /// <summary>
        /// 未开票核销金额
        /// </summary>
        public decimal? FNOINVOICEAMOUNT { get; set; }
        /// <summary>
        /// 未开票核销数量
        /// </summary>
        public decimal? FNOINVOICEQTY { get; set; }
        /// <summary>
        /// 源立账类型（用于反写控制，请勿改动）
        /// </summary>
        public string? FROOTSETACCOUNTTYPE { get; set; }
        /// <summary>
        /// 源应付单源单类型（用于反写控制，请勿改动）
        /// </summary>
        public string? FROOTSOURCETYPE { get; set; }
        /// <summary>
        /// 尾差处理标识
        /// </summary>
        public string? FTAILDIFFFLAG { get; set; }
        /// <summary>
        /// 已核销金额本位币
        /// </summary>
        public decimal? FOPENAMOUNT { get; set; }
        /// <summary>
        /// 已核销不含税金额本位币
        /// </summary>
        public decimal? Fmatchnottaxamt { get; set; }
        /// <summary>
        /// 计入合同履约成本
        /// </summary>
        public string? FINCLUDECONTRACTCOMPCOST { get; set; }
        #endregion
        #region 表头供应商
        /// <summary>
        /// 供货方
        /// </summary>
        public long? FTRANSFERID { get; set; }
        /// <summary>
        /// 供货方名称
        /// </summary>
        public string FTRANSFERID__FName { get; set; }
        /// <summary>
        /// 供货方编码
        /// </summary>
        public string FTRANSFERID__FNumber { get; set; }
        /// <summary>
        /// 供货方简称
        /// </summary>
        public string FTRANSFERID__FShortName { get; set; }
        /// <summary>
        /// 收款方
        /// </summary>
        public long? FChargeId { get; set; }
        /// <summary>
        /// 收款方名称
        /// </summary>
        public string FChargeId__FName { get; set; }
        /// <summary>
        /// 收款方编码
        /// </summary>
        public string FChargeId__FNumber { get; set; }
        /// <summary>
        /// 收款方简称
        /// </summary>
        public string FChargeId__FShortName { get; set; }
        /// <summary>
        /// 订货方
        /// </summary>
        public long? FORDERID { get; set; }
        /// <summary>
        /// 订货方名称
        /// </summary>
        public string FORDERID__FName { get; set; }
        /// <summary>
        /// 订货方编码
        /// </summary>
        public string FORDERID__FNumber { get; set; }
        /// <summary>
        /// 订货方简称
        /// </summary>
        public string FORDERID__FShortName { get; set; }
        #endregion
        #region 表头财务
        /// <summary>
        /// 结算方式
        /// </summary>
        public long? FSettleTypeID { get; set; }
        /// <summary>
        /// 结算方式名称
        /// </summary>
        public string FSettleTypeID__FName { get; set; }
        /// <summary>
        /// 结算方式编码
        /// </summary>
        public string FSettleTypeID__FNumber { get; set; }
        /// <summary>
        /// 本位币
        /// </summary>
        public long FMAINBOOKSTDCURRID { get; set; }
        /// <summary>
        /// 本位币名称
        /// </summary>
        public string FMAINBOOKSTDCURRID__FName { get; set; }
        /// <summary>
        /// 本位币编码
        /// </summary>
        public string FMAINBOOKSTDCURRID__FNumber { get; set; }
        /// <summary>
        /// 税额
        /// </summary>
        public decimal? FTaxAmountFor { get; set; }
        /// <summary>
        /// 不含税金额
        /// </summary>
        public decimal? FNoTaxAmountFor { get; set; }
        /// <summary>
        /// 税额本位币
        /// </summary>
        public decimal? FTaxAmount { get; set; }
        /// <summary>
        /// 不含税金额本位币
        /// </summary>
        public decimal? FNoTaxAmount { get; set; }
        /// <summary>
        /// 价税合计本位币
        /// </summary>
        public decimal? FALLAMOUNT { get; set; }
        /// <summary>
        /// 汇率类型
        /// </summary>
        public long FEXCHANGETYPE { get; set; }
        /// <summary>
        /// 汇率类型名称
        /// </summary>
        public string FEXCHANGETYPE__FName { get; set; }
        /// <summary>
        /// 汇率类型编码
        /// </summary>
        public string FEXCHANGETYPE__FNumber { get; set; }
        /// <summary>
        /// 汇率
        /// </summary>
        public decimal? FExchangeRate { get; set; }
        /// <summary>
        /// 到期日计算日期
        /// </summary>
        public DateTime FACCNTTIMEJUDGETIME { get; set; }
        /// <summary>
        /// 到期日计算日期携带标识
        /// </summary>
        public string? FISCARRIEDDATE { get; set; }
        #endregion
        #region 付款计划
        /// <summary>
        /// 付款计划主键FEntryID
        /// </summary>
        public long FEntityPlan_FEntryID { get; set; }
        /// <summary>
        /// 付款计划序列
        /// </summary>
        public int FEntityPlan_FSeq { get; set; }
        /// <summary>
        /// 表头计划 - 到期日
        /// </summary>
        public DateTime? FENDDATE { get; set; }
        /// <summary>
        /// 应付金额
        /// </summary>
        public decimal? FPAYAMOUNTFOR { get; set; }
        /// <summary>
        /// 应付金额本位币
        /// </summary>
        public decimal? FPAYAMOUNT { get; set; }
        /// <summary>
        /// 表头计划 - 核销状态
        /// </summary>
        public string? FWRITTENOFFSTATUS_P { get; set; }
        /// <summary>
        /// 表头计划 - 未核销金额（作废）
        /// </summary>
        public decimal? FNOTWRITTENOFFAMOUNTFOR_P { get; set; }
        /// <summary>
        /// 表头计划 - 付（退）款单关联金额
        /// </summary>
        public decimal? FRELATEHADPAYAMOUNT_P { get; set; }
        /// <summary>
        /// 应付比例(%)
        /// </summary>
        public decimal? FPAYRATE { get; set; }
        /// <summary>
        /// 已核销金额
        /// </summary>
        public decimal? FWRITTENOFFAMOUNTFOR_P { get; set; }
        /// <summary>
        /// 付款申请关联金额
        /// </summary>
        public decimal? FAPPLYAMOUNT { get; set; }
        /// <summary>
        /// 已付款关联申请金额
        /// </summary>
        public decimal? FPAYREAPPLYAMT { get; set; }
        /// <summary>
        /// 采购订单号
        /// </summary>
        public string? FPURCHASEORDERNO { get; set; }
        /// <summary>
        /// 采购订单ID
        /// </summary>
        public int? FPURCHASEORDERID { get; set; }
        /// <summary>
        /// 采购订单明细内码
        /// </summary>
        public int? FPAYABLEENTRYID { get; set; }
        /// <summary>
        /// 物料编码
        /// </summary>
        public long? FMATERIALID_P { get; set; }
        /// <summary>
        /// 物料编码名称
        /// </summary>
        public string FMATERIALID_P__FName { get; set; }
        /// <summary>
        /// 物料编码编码
        /// </summary>
        public string FMATERIALID_P__FNumber { get; set; }
        /// <summary>
        /// 订单行号
        /// </summary>
        public int? FMATERIALSEQ { get; set; }
        /// <summary>
        /// 含税单价
        /// </summary>
        public decimal? FPRICE_P { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public decimal? FQTY_P { get; set; }
        /// <summary>
        /// 计价单位
        /// </summary>
        public long? FPRICEUNITID_P { get; set; }
        /// <summary>
        /// 计价单位名称
        /// </summary>
        public string FPRICEUNITID_P__FName { get; set; }
        /// <summary>
        /// 计价单位编码
        /// </summary>
        public string FPRICEUNITID_P__FNumber { get; set; }
        /// <summary>
        /// 付款申请关联数量
        /// </summary>
        public decimal? FAPPLYQTY { get; set; }
        /// <summary>
        /// 付(退)款关联数量
        /// </summary>
        public decimal? FRELATEHADPAYQTY { get; set; }
        /// <summary>
        /// 付（退）款关联金额本位币
        /// </summary>
        public decimal? FWRITTENOFFAMOUNT { get; set; }
        /// <summary>
        /// 未核销金额
        /// </summary>
        public decimal? FNOTVERIFICATEAMOUNT { get; set; }
        /// <summary>
        /// 费用项目编码
        /// </summary>
        public long? FCOSTID_P { get; set; }
        /// <summary>
        /// 费用项目编码名称
        /// </summary>
        public string FCOSTID_P__FName { get; set; }
        /// <summary>
        /// 费用项目编码编码
        /// </summary>
        public string FCOSTID_P__FNumber { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string? FREMARK { get; set; }
        #endregion
        #region 税务明细
        /// <summary>
        /// 税务明细主键FDetailID
        /// </summary>
        public long FTaxDetailSubEntity_FDetailID { get; set; }
        /// <summary>
        /// 税务明细序列
        /// </summary>
        public int FTaxDetailSubEntity_FSeq { get; set; }
        /// <summary>
        /// 税率名称
        /// </summary>
        public long? FTaxRateId { get; set; }
        /// <summary>
        /// 税率名称名称
        /// </summary>
        public string FTaxRateId__FName { get; set; }
        /// <summary>
        /// 税率名称编码
        /// </summary>
        public string FTaxRateId__FNumber { get; set; }
        /// <summary>
        /// 税率(%)
        /// </summary>
        public decimal? FTaxRate { get; set; }
        /// <summary>
        /// 税额
        /// </summary>
        public decimal? FTaxAmount_T { get; set; }
        /// <summary>
        /// 计入成本比例
        /// </summary>
        public decimal? FCostPercent { get; set; }
        /// <summary>
        /// 计入成本金额
        /// </summary>
        public decimal? FCostAmount { get; set; }
        /// <summary>
        /// 增值税
        /// </summary>
        public string? FVAT { get; set; }
        /// <summary>
        /// 卖方代扣代缴
        /// </summary>
        public string? FSellerWithholding { get; set; }
        /// <summary>
        /// 买方代扣代缴
        /// </summary>
        public string? FBuyerWithholding { get; set; }
        #endregion
        #region 收票信息
        /// <summary>
        /// 收票信息主键FEntryID
        /// </summary>
        public long FRecInvInfo_FEntryID { get; set; }
        /// <summary>
        /// 收票信息序列
        /// </summary>
        public int FRecInvInfo_FSEQ { get; set; }
        /// <summary>
        /// 收票单
        /// </summary>
        public long? FRecInv { get; set; }
        /// <summary>
        /// 从源单携带
        /// </summary>
        public string? FISFROMSRCBILL { get; set; }
        #endregion
    }
}