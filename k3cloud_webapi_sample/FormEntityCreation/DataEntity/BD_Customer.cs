using System;

namespace FormEntityCreation.DataEntity
{
    /// <summary>
    /// 客户
    /// </summary>
    public class BD_Customer
    {
        /// <summary>
        /// FCUSTID
        /// </summary>
        public int FCUSTID { get; set; }
        #region 基本信息
        /// <summary>
        /// 单据状态
        /// </summary>
        public string? FDocumentStatus { get; set; }
        /// <summary>
        /// 禁用状态
        /// </summary>
        public string? FForbidStatus { get; set; }
        /// <summary>
        /// 客户名称
        /// </summary>
        public string FName { get; set; }
        /// <summary>
        /// 客户编码
        /// </summary>
        public string? FNumber { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string? FDescription { get; set; }
        /// <summary>
        /// 创建组织
        /// </summary>
        public long FCreateOrgId { get; set; }
        /// <summary>
        /// 创建组织名称
        /// </summary>
        public string FCreateOrgId__FName { get; set; }
        /// <summary>
        /// 创建组织编码
        /// </summary>
        public string FCreateOrgId__FNumber { get; set; }
        /// <summary>
        /// 使用组织
        /// </summary>
        public long? FUseOrgId { get; set; }
        /// <summary>
        /// 使用组织名称
        /// </summary>
        public string FUseOrgId__FName { get; set; }
        /// <summary>
        /// 使用组织编码
        /// </summary>
        public string FUseOrgId__FNumber { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public long? FCreatorId { get; set; }
        /// <summary>
        /// 创建人用户名称
        /// </summary>
        public string FCreatorId__FName { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        public long? FModifierId { get; set; }
        /// <summary>
        /// 修改人用户名称
        /// </summary>
        public string FModifierId__FName { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime? FCreateDate { get; set; }
        /// <summary>
        /// 修改日期
        /// </summary>
        public DateTime? FModifyDate { get; set; }
        /// <summary>
        /// 简称
        /// </summary>
        public string? FShortName { get; set; }
        /// <summary>
        /// 国家
        /// </summary>
        public string? FCOUNTRY { get; set; }
        /// <summary>
        /// 国家编码
        /// </summary>
        public string FCOUNTRY__FNumber { get; set; }
        /// <summary>
        /// 地区
        /// </summary>
        public string? FPROVINCIAL { get; set; }
        /// <summary>
        /// 地区编码
        /// </summary>
        public string FPROVINCIAL__FNumber { get; set; }
        /// <summary>
        /// 邮政编码
        /// </summary>
        public string? FZIP { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        public string? FTEL { get; set; }
        /// <summary>
        /// 纳税登记号
        /// </summary>
        public string? FTAXREGISTERCODE { get; set; }
        /// <summary>
        /// 传真
        /// </summary>
        public string? FFAX { get; set; }
        /// <summary>
        /// 对应集团客户
        /// </summary>
        public long? FGROUPCUSTID { get; set; }
        /// <summary>
        /// 对应集团客户客户名称
        /// </summary>
        public string FGROUPCUSTID__FName { get; set; }
        /// <summary>
        /// 对应集团客户客户编码
        /// </summary>
        public string FGROUPCUSTID__FNumber { get; set; }
        /// <summary>
        /// 对应集团客户简称
        /// </summary>
        public string FGROUPCUSTID__FShortName { get; set; }
        /// <summary>
        /// 对应供应商
        /// </summary>
        public long? FSUPPLIERID { get; set; }
        /// <summary>
        /// 对应供应商名称
        /// </summary>
        public string FSUPPLIERID__FName { get; set; }
        /// <summary>
        /// 对应供应商编码
        /// </summary>
        public string FSUPPLIERID__FNumber { get; set; }
        /// <summary>
        /// 对应供应商简称
        /// </summary>
        public string FSUPPLIERID__FShortName { get; set; }
        /// <summary>
        /// 结算币别
        /// </summary>
        public long FTRADINGCURRID { get; set; }
        /// <summary>
        /// 结算币别名称
        /// </summary>
        public string FTRADINGCURRID__FName { get; set; }
        /// <summary>
        /// 结算币别编码
        /// </summary>
        public string FTRADINGCURRID__FNumber { get; set; }
        /// <summary>
        /// 销售部门
        /// </summary>
        public long? FSALDEPTID { get; set; }
        /// <summary>
        /// 销售部门名称
        /// </summary>
        public string FSALDEPTID__FName { get; set; }
        /// <summary>
        /// 销售部门编码
        /// </summary>
        public string FSALDEPTID__FNumber { get; set; }
        /// <summary>
        /// 销售组
        /// </summary>
        public long? FSALGROUPID { get; set; }
        /// <summary>
        /// 销售组业务组名称
        /// </summary>
        public string FSALGROUPID__FName { get; set; }
        /// <summary>
        /// 销售组业务组编码
        /// </summary>
        public string FSALGROUPID__FNumber { get; set; }
        /// <summary>
        /// 销售员
        /// </summary>
        public long? FSELLER { get; set; }
        /// <summary>
        /// 销售员名称
        /// </summary>
        public string FSELLER__FName { get; set; }
        /// <summary>
        /// 销售员业务员编码
        /// </summary>
        public string FSELLER__FNumber { get; set; }
        /// <summary>
        /// 运输提前期
        /// </summary>
        public int? FTRANSLEADTIME { get; set; }
        /// <summary>
        /// 价目表
        /// </summary>
        public long? FPRICELISTID { get; set; }
        /// <summary>
        /// 价目表名称
        /// </summary>
        public string FPRICELISTID__FName { get; set; }
        /// <summary>
        /// 价目表编号
        /// </summary>
        public string FPRICELISTID__FNumber { get; set; }
        /// <summary>
        /// 折扣表
        /// </summary>
        public long? FDISCOUNTLISTID { get; set; }
        /// <summary>
        /// 折扣表名称
        /// </summary>
        public string FDISCOUNTLISTID__FName { get; set; }
        /// <summary>
        /// 折扣表编码
        /// </summary>
        public string FDISCOUNTLISTID__FNumber { get; set; }
        /// <summary>
        /// 结算方式
        /// </summary>
        public long? FSETTLETYPEID { get; set; }
        /// <summary>
        /// 结算方式名称
        /// </summary>
        public string FSETTLETYPEID__FName { get; set; }
        /// <summary>
        /// 结算方式编码
        /// </summary>
        public string FSETTLETYPEID__FNumber { get; set; }
        /// <summary>
        /// 收款币别
        /// </summary>
        public long? FRECEIVECURRID { get; set; }
        /// <summary>
        /// 收款币别名称
        /// </summary>
        public string FRECEIVECURRID__FName { get; set; }
        /// <summary>
        /// 收款币别编码
        /// </summary>
        public string FRECEIVECURRID__FNumber { get; set; }
        /// <summary>
        /// 收款条件
        /// </summary>
        public long? FRECCONDITIONID { get; set; }
        /// <summary>
        /// 收款条件名称
        /// </summary>
        public string FRECCONDITIONID__FName { get; set; }
        /// <summary>
        /// 收款条件编码
        /// </summary>
        public string FRECCONDITIONID__FNumber { get; set; }
        /// <summary>
        /// 启用信用管理
        /// </summary>
        public string? FISCREDITCHECK { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        public long? FAPPROVERID { get; set; }
        /// <summary>
        /// 审核人用户名称
        /// </summary>
        public string FAPPROVERID__FName { get; set; }
        /// <summary>
        /// 审核日期
        /// </summary>
        public DateTime? FAPPROVEDATE { get; set; }
        /// <summary>
        /// 禁用人
        /// </summary>
        public long? FFORBIDDERID { get; set; }
        /// <summary>
        /// 禁用人用户名称
        /// </summary>
        public string FFORBIDDERID__FName { get; set; }
        /// <summary>
        /// 禁用日期
        /// </summary>
        public DateTime? FFORBIDDATE { get; set; }
        /// <summary>
        /// 税分类
        /// </summary>
        public string? FTaxType { get; set; }
        /// <summary>
        /// 税分类编码
        /// </summary>
        public string FTaxType__FNumber { get; set; }
        /// <summary>
        /// 客户类别
        /// </summary>
        public string? FCustTypeId { get; set; }
        /// <summary>
        /// 客户类别编码
        /// </summary>
        public string FCustTypeId__FNumber { get; set; }
        /// <summary>
        /// 通讯地址
        /// </summary>
        public string? FADDRESS { get; set; }
        /// <summary>
        /// 公司网址
        /// </summary>
        public string? FWEBSITE { get; set; }
        /// <summary>
        /// 客户分组
        /// </summary>
        public long? FGroup { get; set; }
        /// <summary>
        /// 公司规模
        /// </summary>
        public string? FCompanyScale { get; set; }
        /// <summary>
        /// 公司规模编码
        /// </summary>
        public string FCompanyScale__FNumber { get; set; }
        /// <summary>
        /// 公司类别
        /// </summary>
        public string? FCompanyClassify { get; set; }
        /// <summary>
        /// 公司类别编码
        /// </summary>
        public string FCompanyClassify__FNumber { get; set; }
        /// <summary>
        /// 公司性质
        /// </summary>
        public string? FCompanyNature { get; set; }
        /// <summary>
        /// 公司性质编码
        /// </summary>
        public string FCompanyNature__FNumber { get; set; }
        /// <summary>
        /// 对应组织
        /// </summary>
        public long? FCorrespondOrgId { get; set; }
        /// <summary>
        /// 对应组织名称
        /// </summary>
        public string FCorrespondOrgId__FName { get; set; }
        /// <summary>
        /// 对应组织编码
        /// </summary>
        public string FCorrespondOrgId__FNumber { get; set; }
        /// <summary>
        /// 客户优先级
        /// </summary>
        public int? FPriority { get; set; }
        /// <summary>
        /// 发票类型
        /// </summary>
        public string? FInvoiceType { get; set; }
        /// <summary>
        /// 默认税率
        /// </summary>
        public long? FTaxRate { get; set; }
        /// <summary>
        /// 默认税率名称
        /// </summary>
        public string FTaxRate__FName { get; set; }
        /// <summary>
        /// 默认税率编码
        /// </summary>
        public string FTaxRate__FNumber { get; set; }
        /// <summary>
        /// 默认付款方
        /// </summary>
        public string? FIsDefPayer { get; set; }
        /// <summary>
        /// 订货平台管理员
        /// </summary>
        public string? FCPAdminCode { get; set; }
        /// <summary>
        /// 集团客户
        /// </summary>
        public string? FIsGroup { get; set; }
        /// <summary>
        /// 发票抬头
        /// </summary>
        public string? FINVOICETITLE { get; set; }
        /// <summary>
        /// 开户银行
        /// </summary>
        public string? FINVOICEBANKNAME { get; set; }
        /// <summary>
        /// 银行账号
        /// </summary>
        public string? FINVOICEBANKACCOUNT { get; set; }
        /// <summary>
        /// 开票联系电话
        /// </summary>
        public string? FINVOICETEL { get; set; }
        /// <summary>
        /// 开票通讯地址
        /// </summary>
        public string? FINVOICEADDRESS { get; set; }
        /// <summary>
        /// 是否交易客户
        /// </summary>
        public string? FIsTrade { get; set; }
        /// <summary>
        /// 不校验可发量
        /// </summary>
        public string? FUncheckExpectQty { get; set; }
        /// <summary>
        /// 集团客户标识(仅用于审核操作功能的一个判断标识)
        /// </summary>
        public string? FIsGroupTag { get; set; }
        /// <summary>
        /// 法人代表
        /// </summary>
        public string? FLegalPerson { get; set; }
        /// <summary>
        /// 注册资本
        /// </summary>
        public string? FRegisterFund { get; set; }
        /// <summary>
        /// 创立日期
        /// </summary>
        public string? FFoundDate { get; set; }
        /// <summary>
        /// 行业
        /// </summary>
        public string? FDomains { get; set; }
        /// <summary>
        /// 统一社会信用代码
        /// </summary>
        public string? FSOCIALCRECODE { get; set; }
        /// <summary>
        /// 注册地址
        /// </summary>
        public string? FRegisterAddress { get; set; }
        #endregion
        #region 银行信息
        /// <summary>
        /// 银行账号
        /// </summary>
        public string? FBANKCODE { get; set; }
        /// <summary>
        /// 币别
        /// </summary>
        public long? FCURRENCYID { get; set; }
        /// <summary>
        /// 币别名称
        /// </summary>
        public string FCURRENCYID__FName { get; set; }
        /// <summary>
        /// 币别编码
        /// </summary>
        public string FCURRENCYID__FNumber { get; set; }
        /// <summary>
        /// 账户名称
        /// </summary>
        public string? FACCOUNTNAME { get; set; }
        /// <summary>
        /// 默认
        /// </summary>
        public string? FISDEFAULT1 { get; set; }
        /// <summary>
        /// 开户国家
        /// </summary>
        public string? FCOUNTRY1 { get; set; }
        /// <summary>
        /// 开户国家编码
        /// </summary>
        public string FCOUNTRY1__FNumber { get; set; }
        /// <summary>
        /// 开户银行
        /// </summary>
        public string? FOPENBANKNAME { get; set; }
        /// <summary>
        /// 收款银行
        /// </summary>
        public long? FBankTypeRec { get; set; }
        /// <summary>
        /// 收款银行名称
        /// </summary>
        public string FBankTypeRec__FName { get; set; }
        /// <summary>
        /// 收款银行编码
        /// </summary>
        public string FBankTypeRec__FNumber { get; set; }
        /// <summary>
        /// 开户行地址
        /// </summary>
        public string? FOpenAddressRec { get; set; }
        /// <summary>
        /// 联行号
        /// </summary>
        public string? FCNAPS { get; set; }
        /// <summary>
        /// 网点名称
        /// </summary>
        public string? FTextBankDetail { get; set; }
        /// <summary>
        /// 银行网点
        /// </summary>
        public long? FBankDetail { get; set; }
        /// <summary>
        /// 银行网点银行名称
        /// </summary>
        public string FBankDetail__FName { get; set; }
        /// <summary>
        /// 银行网点联行号
        /// </summary>
        public string FBankDetail__FNumber { get; set; }
        #endregion
        #region 地址信息
        /// <summary>
        /// 地点编码
        /// </summary>
        public string? FNUMBER1 { get; set; }
        /// <summary>
        /// 地点名称
        /// </summary>
        public string? FNAME1 { get; set; }
        /// <summary>
        /// 详细地址
        /// </summary>
        public string? FADDRESS1 { get; set; }
        /// <summary>
        /// 运输提前期
        /// </summary>
        public int? FTRANSLEADTIME1 { get; set; }
        /// <summary>
        /// 税率
        /// </summary>
        public decimal? FTAXRATE1 { get; set; }
        /// <summary>
        /// 默认收货地址
        /// </summary>
        public string? FIsDefaultConsignee { get; set; }
        /// <summary>
        /// 默认开票地址
        /// </summary>
        public string? FIsDefaultSettle { get; set; }
        /// <summary>
        /// 默认付款地址
        /// </summary>
        public string? FIsDefaultPayer { get; set; }
        /// <summary>
        /// 固定电话
        /// </summary>
        public string? FTTel { get; set; }
        /// <summary>
        /// 电子邮箱
        /// </summary>
        public string? FEMail { get; set; }
        /// <summary>
        /// 启用
        /// </summary>
        public string? FIsUsed { get; set; }
        /// <summary>
        /// 联系人
        /// </summary>
        public long? FTContact { get; set; }
        /// <summary>
        /// 联系人编码
        /// </summary>
        public string FTContact__FNumber { get; set; }
        /// <summary>
        /// 联系人姓名
        /// </summary>
        public string FTContact__FName { get; set; }
        /// <summary>
        /// 移动电话
        /// </summary>
        public string? FMOBILE { get; set; }
        /// <summary>
        /// 地址禁用状态
        /// </summary>
        public string? FLocationStatus { get; set; }
        #endregion
        #region 订货组织
        /// <summary>
        /// 订货组织
        /// </summary>
        public long? FOrderOrgId { get; set; }
        /// <summary>
        /// 订货组织名称
        /// </summary>
        public string FOrderOrgId__FName { get; set; }
        /// <summary>
        /// 订货组织编码
        /// </summary>
        public string FOrderOrgId__FNumber { get; set; }
        /// <summary>
        /// 默认
        /// </summary>
        public string? FIsDefaultOrderOrg { get; set; }
        #endregion
        #region 商务信息
        /// <summary>
        /// 启用商联在线
        /// </summary>
        public string? FEnableSL { get; set; }
        /// <summary>
        /// 冻结状态
        /// </summary>
        public string? FFreezeStatus { get; set; }
        /// <summary>
        /// 冻结范围
        /// </summary>
        public string? FFreezeLimit { get; set; }
        /// <summary>
        /// 冻结人
        /// </summary>
        public long? FFreezeOperator { get; set; }
        /// <summary>
        /// 冻结人用户名称
        /// </summary>
        public string FFreezeOperator__FName { get; set; }
        /// <summary>
        /// 冻结日期
        /// </summary>
        public DateTime? FFreezeDate { get; set; }
        /// <summary>
        /// 省份
        /// </summary>
        public string? FPROVINCE { get; set; }
        /// <summary>
        /// 省份编码
        /// </summary>
        public string FPROVINCE__FNumber { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public string? FCITY { get; set; }
        /// <summary>
        /// 城市编码
        /// </summary>
        public string FCITY__FNumber { get; set; }
        /// <summary>
        /// 默认收货地点
        /// </summary>
        public long? FDefaultConsiLoc { get; set; }
        /// <summary>
        /// 默认收货地点地点名称
        /// </summary>
        public string FDefaultConsiLoc__FName { get; set; }
        /// <summary>
        /// 默认开票地点
        /// </summary>
        public long? FDefaultSettleLoc { get; set; }
        /// <summary>
        /// 默认开票地点地点名称
        /// </summary>
        public string FDefaultSettleLoc__FName { get; set; }
        /// <summary>
        /// 默认付款地点
        /// </summary>
        public long? FDefaultPayerLoc { get; set; }
        /// <summary>
        /// 默认付款地点地点名称
        /// </summary>
        public string FDefaultPayerLoc__FName { get; set; }
        /// <summary>
        /// 默认联系人
        /// </summary>
        public long? FDefaultContact { get; set; }
        /// <summary>
        /// 默认联系人姓名
        /// </summary>
        public string FDefaultContact__FName { get; set; }
        /// <summary>
        /// 默认联系人编码
        /// </summary>
        public string FDefaultContact__FNumber { get; set; }
        /// <summary>
        /// 保证金比例（%）
        /// </summary>
        public decimal? FMarginLevel { get; set; }
        /// <summary>
        /// 结算卡
        /// </summary>
        public string? FDebitCard { get; set; }
        /// <summary>
        /// 结算方
        /// </summary>
        public long? FSettleId { get; set; }
        /// <summary>
        /// 结算方客户名称
        /// </summary>
        public string FSettleId__FName { get; set; }
        /// <summary>
        /// 结算方客户编码
        /// </summary>
        public string FSettleId__FNumber { get; set; }
        /// <summary>
        /// 结算方简称
        /// </summary>
        public string FSettleId__FShortName { get; set; }
        /// <summary>
        /// 付款方
        /// </summary>
        public long? FChargeId { get; set; }
        /// <summary>
        /// 付款方客户名称
        /// </summary>
        public string FChargeId__FName { get; set; }
        /// <summary>
        /// 付款方客户编码
        /// </summary>
        public string FChargeId__FNumber { get; set; }
        /// <summary>
        /// 付款方简称
        /// </summary>
        public string FChargeId__FShortName { get; set; }
        /// <summary>
        /// 允许对接智慧订货
        /// </summary>
        public string? FALLOWJOINZHJ { get; set; }
        /// <summary>
        /// 联系人必录
        /// </summary>
        public string? FIsContractMustInPut { get; set; }
        /// <summary>
        /// 联系人数量
        /// </summary>
        public int? FContractCount { get; set; }
        #endregion
        #region 对应子账户信息
        /// <summary>
        /// 对应子账户信息主键FEntryID
        /// </summary>
        public long FT_BD_CUSTSUBACCOUNT_FEntryID { get; set; }
        /// <summary>
        /// 对应子账户信息序列
        /// </summary>
        public int FT_BD_CUSTSUBACCOUNT_FSEQ { get; set; }
        /// <summary>
        /// 子账户类型
        /// </summary>
        public string? FSUBACCOUNTTYPE { get; set; }
        /// <summary>
        /// 子账户号
        /// </summary>
        public string? FSUBACCOUNT { get; set; }
        #endregion
    }
}