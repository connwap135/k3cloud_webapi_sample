using System;

namespace FormEntityCreation.DataEntity
{
    /// <summary>
    /// 物料清单
    /// </summary>
    public class ENG_BOM
    {
        /// <summary>
        /// FID
        /// </summary>
        public int FID { get; set; }
        #region 单据头
        /// <summary>
        /// 数据状态
        /// </summary>
        public string? FDocumentStatus { get; set; }
        /// <summary>
        /// 禁用状态
        /// </summary>
        public string? FForbidStatus { get; set; }
        /// <summary>
        /// BOM简称
        /// </summary>
        public string? FName { get; set; }
        /// <summary>
        /// BOM版本
        /// </summary>
        public string? FNumber { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string? FDescription { get; set; }
        /// <summary>
        /// 创建组织
        /// </summary>
        public long FCreateOrgId { get; set; }
        /// <summary>
        /// 创建组织名称
        /// </summary>
        public string FCreateOrgId__FName { get; set; }
        /// <summary>
        /// 创建组织编码
        /// </summary>
        public string FCreateOrgId__FNumber { get; set; }
        /// <summary>
        /// 使用组织
        /// </summary>
        public long FUseOrgId { get; set; }
        /// <summary>
        /// 使用组织名称
        /// </summary>
        public string FUseOrgId__FName { get; set; }
        /// <summary>
        /// 使用组织编码
        /// </summary>
        public string FUseOrgId__FNumber { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public long? FCreatorId { get; set; }
        /// <summary>
        /// 创建人用户名称
        /// </summary>
        public string FCreatorId__FName { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        public long? FModifierId { get; set; }
        /// <summary>
        /// 修改人用户名称
        /// </summary>
        public string FModifierId__FName { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime? FCreateDate { get; set; }
        /// <summary>
        /// 修改日期
        /// </summary>
        public DateTime? FModifyDate { get; set; }
        /// <summary>
        /// 禁用人
        /// </summary>
        public long? FForbidderId { get; set; }
        /// <summary>
        /// 禁用人用户名称
        /// </summary>
        public string FForbidderId__FName { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        public long? FApproverId { get; set; }
        /// <summary>
        /// 审核人用户名称
        /// </summary>
        public string FApproverId__FName { get; set; }
        /// <summary>
        /// 禁用日期
        /// </summary>
        public DateTime? FForbidDate { get; set; }
        /// <summary>
        /// 审核日期
        /// </summary>
        public DateTime? FApproveDate { get; set; }
        /// <summary>
        /// BOM分类
        /// </summary>
        public string FBOMCATEGORY { get; set; }
        /// <summary>
        /// BOM用途
        /// </summary>
        public string FBOMUSE { get; set; }
        /// <summary>
        /// 父项物料编码
        /// </summary>
        public long FMATERIALID { get; set; }
        /// <summary>
        /// 父项物料编码名称
        /// </summary>
        public string FMATERIALID__FName { get; set; }
        /// <summary>
        /// 父项物料编码编码
        /// </summary>
        public string FMATERIALID__FNumber { get; set; }
        /// <summary>
        /// 成品率%
        /// </summary>
        public decimal? FYIELDRATE { get; set; }
        /// <summary>
        /// 单据类型
        /// </summary>
        public string FBILLTYPE { get; set; }
        /// <summary>
        /// 单据类型名称
        /// </summary>
        public string FBILLTYPE__FName { get; set; }
        /// <summary>
        /// 单据类型编码
        /// </summary>
        public string FBILLTYPE__FNumber { get; set; }
        /// <summary>
        /// 父项物料单位
        /// </summary>
        public long FUNITID { get; set; }
        /// <summary>
        /// 父项物料单位名称
        /// </summary>
        public string FUNITID__FName { get; set; }
        /// <summary>
        /// 父项物料单位编码
        /// </summary>
        public string FUNITID__FNumber { get; set; }
        /// <summary>
        /// 父项基本单位
        /// </summary>
        public long? FBaseUnitId { get; set; }
        /// <summary>
        /// 父项基本单位名称
        /// </summary>
        public string FBaseUnitId__FName { get; set; }
        /// <summary>
        /// 父项基本单位编码
        /// </summary>
        public string FBaseUnitId__FNumber { get; set; }
        /// <summary>
        /// 是否新版本
        /// </summary>
        public string? FISDEFAULT { get; set; }
        /// <summary>
        /// 配置BOM
        /// </summary>
        public int? FCfgBomId { get; set; }
        /// <summary>
        /// 批量
        /// </summary>
        public decimal? FQty { get; set; }
        /// <summary>
        /// 基本单位批量
        /// </summary>
        public decimal? FBaseQty { get; set; }
        /// <summary>
        /// BOM分组
        /// </summary>
        public long? FGroup { get; set; }
        /// <summary>
        /// PLMBOM内码
        /// </summary>
        public string? FPLMBOMId { get; set; }
        /// <summary>
        /// BOM来源
        /// </summary>
        public string? FBOMSRC { get; set; }
        /// <summary>
        /// 辅助属性
        /// </summary>
        public long? FParentAuxPropId { get; set; }
        /// <summary>
        /// 产品模型
        /// </summary>
        public long? FMDLID { get; set; }
        /// <summary>
        /// 产品模型模型名称
        /// </summary>
        public string FMDLID__FName { get; set; }
        /// <summary>
        /// 产品模型模型编码
        /// </summary>
        public string FMDLID__FNumber { get; set; }
        /// <summary>
        /// 禁用原因
        /// </summary>
        public string? FForbidReson { get; set; }
        /// <summary>
        /// 是否变更中
        /// </summary>
        public string? FIsChange { get; set; }
        #endregion
        #region 子项明细
        /// <summary>
        /// 子项明细主键FENTRYID
        /// </summary>
        public long FTreeEntity_FENTRYID { get; set; }
        /// <summary>
        /// 子项明细序列
        /// </summary>
        public int FTreeEntity_FSEQ { get; set; }
        /// <summary>
        /// 父级行主键
        /// </summary>
        public string? FParentRowId { get; set; }
        /// <summary>
        /// 行展开类型
        /// </summary>
        public int? FRowExpandType { get; set; }
        /// <summary>
        /// 行标识
        /// </summary>
        public string? FRowId { get; set; }
        /// <summary>
        /// 子项类型
        /// </summary>
        public string FMATERIALTYPE { get; set; }
        /// <summary>
        /// 用量类型
        /// </summary>
        public string FDOSAGETYPE { get; set; }
        /// <summary>
        /// 作业
        /// </summary>
        public long? FPROCESSID { get; set; }
        /// <summary>
        /// 作业名称
        /// </summary>
        public string FPROCESSID__FName { get; set; }
        /// <summary>
        /// 作业编码
        /// </summary>
        public string FPROCESSID__FNumber { get; set; }
        /// <summary>
        /// 固定损耗
        /// </summary>
        public decimal? FFIXSCRAPQTY { get; set; }
        /// <summary>
        /// 生效日期
        /// </summary>
        public DateTime FEFFECTDATE { get; set; }
        /// <summary>
        /// 失效日期
        /// </summary>
        public DateTime FEXPIREDATE { get; set; }
        /// <summary>
        /// 发料方式
        /// </summary>
        public string FISSUETYPE { get; set; }
        /// <summary>
        /// 发料组织
        /// </summary>
        public long? FSUPPLYORG { get; set; }
        /// <summary>
        /// 发料组织名称
        /// </summary>
        public string FSUPPLYORG__FName { get; set; }
        /// <summary>
        /// 发料组织编码
        /// </summary>
        public string FSUPPLYORG__FNumber { get; set; }
        /// <summary>
        /// 默认发料仓库
        /// </summary>
        public long? FSTOCKID { get; set; }
        /// <summary>
        /// 默认发料仓库名称
        /// </summary>
        public string FSTOCKID__FName { get; set; }
        /// <summary>
        /// 默认发料仓库编码
        /// </summary>
        public string FSTOCKID__FNumber { get; set; }
        /// <summary>
        /// 默认发料仓位
        /// </summary>
        public long? FSTOCKLOCID { get; set; }
        /// <summary>
        /// 允许超发
        /// </summary>
        public string? FALLOWOVER { get; set; }
        /// <summary>
        /// 倒冲时机
        /// </summary>
        public string? FBACKFLUSHTYPE { get; set; }
        /// <summary>
        /// 时间单位
        /// </summary>
        public string FTIMEUNIT { get; set; }
        /// <summary>
        /// 是否关键件
        /// </summary>
        public string? FISKEYCOMPONENT { get; set; }
        /// <summary>
        /// 子项物料编码
        /// </summary>
        public long FMATERIALIDCHILD { get; set; }
        /// <summary>
        /// 子项物料编码名称
        /// </summary>
        public string FMATERIALIDCHILD__FName { get; set; }
        /// <summary>
        /// 子项物料编码编码
        /// </summary>
        public string FMATERIALIDCHILD__FNumber { get; set; }
        /// <summary>
        /// 位置号
        /// </summary>
        public string? FPOSITIONNO { get; set; }
        /// <summary>
        /// 变动损耗率%
        /// </summary>
        public decimal? FSCRAPRATE { get; set; }
        /// <summary>
        /// 拆卸成本拆分比例
        /// </summary>
        public decimal? FDISASSMBLERATE { get; set; }
        /// <summary>
        /// 偏置时间
        /// </summary>
        public int? FOFFSETTIME { get; set; }
        /// <summary>
        /// 是否发损耗
        /// </summary>
        public string? FISGETSCRAP { get; set; }
        /// <summary>
        /// 子项BOM版本
        /// </summary>
        public long? FBOMID { get; set; }
        /// <summary>
        /// 子项BOM版本BOM简称
        /// </summary>
        public string FBOMID__FName { get; set; }
        /// <summary>
        /// 子项BOM版本BOM版本
        /// </summary>
        public string FBOMID__FNumber { get; set; }
        /// <summary>
        /// 用量:分子
        /// </summary>
        public decimal? FNUMERATOR { get; set; }
        /// <summary>
        /// 用量:分母
        /// </summary>
        public decimal? FDENOMINATOR { get; set; }
        /// <summary>
        /// 货主类型
        /// </summary>
        public string FOWNERTYPEID { get; set; }
        /// <summary>
        /// 货主
        /// </summary>
        public int? FOWNERID { get; set; }
        /// <summary>
        /// 货主名称
        /// </summary>
        public string FOWNERID__FName { get; set; }
        /// <summary>
        /// 货主编码
        /// </summary>
        public string FOWNERID__FNumber { get; set; }
        /// <summary>
        /// 工序
        /// </summary>
        public int? FOPERID { get; set; }
        /// <summary>
        /// 辅助属性
        /// </summary>
        public long? FAuxPropId { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string? FMEMO { get; set; }
        /// <summary>
        /// 子项单位
        /// </summary>
        public long FCHILDUNITID { get; set; }
        /// <summary>
        /// 子项单位名称
        /// </summary>
        public string FCHILDUNITID__FName { get; set; }
        /// <summary>
        /// 子项单位编码
        /// </summary>
        public string FCHILDUNITID__FNumber { get; set; }
        /// <summary>
        /// 子项标识
        /// </summary>
        public string? FEntryRowId { get; set; }
        /// <summary>
        /// 子项基本单位
        /// </summary>
        public long? FChildBaseUnitID { get; set; }
        /// <summary>
        /// 子项基本单位名称
        /// </summary>
        public string FChildBaseUnitID__FName { get; set; }
        /// <summary>
        /// 子项基本单位编码
        /// </summary>
        public string FChildBaseUnitID__FNumber { get; set; }
        /// <summary>
        /// 基本单位分子
        /// </summary>
        public decimal? FBaseNumerator { get; set; }
        /// <summary>
        /// 基本单位固定损耗
        /// </summary>
        public decimal? FBaseFixscrapQty { get; set; }
        /// <summary>
        /// 基本单位分母
        /// </summary>
        public decimal? FBaseDenominator { get; set; }
        /// <summary>
        /// 标准用量
        /// </summary>
        public decimal? FQty2 { get; set; }
        /// <summary>
        /// 实际用量
        /// </summary>
        public decimal? FActualQty { get; set; }
        /// <summary>
        /// 超发控制方式
        /// </summary>
        public string FOverControlMode { get; set; }
        /// <summary>
        /// 项次
        /// </summary>
        public int? FReplaceGroup { get; set; }
        /// <summary>
        /// 替代策略
        /// </summary>
        public string? FReplacePolicy { get; set; }
        /// <summary>
        /// 替代方式
        /// </summary>
        public string? FReplaceType { get; set; }
        /// <summary>
        /// 替代优先级
        /// </summary>
        public int? FReplacePriority { get; set; }
        /// <summary>
        /// 动态优先级
        /// </summary>
        public int? FMRPPriority { get; set; }
        /// <summary>
        /// 替代主料
        /// </summary>
        public string? FIskeyItem { get; set; }
        /// <summary>
        /// 可选择
        /// </summary>
        public string? FIsCanChoose { get; set; }
        /// <summary>
        /// 可修改
        /// </summary>
        public string? FIsCanEdit { get; set; }
        /// <summary>
        /// 可替换
        /// </summary>
        public string? FIsCanReplace { get; set; }
        /// <summary>
        /// 配置BOM分录内码
        /// </summary>
        public int? FCFGBOMENTRYID { get; set; }
        /// <summary>
        /// 父项特征件分录内码
        /// </summary>
        public int? FCfgFeatureEntryId { get; set; }
        /// <summary>
        /// 供应组织
        /// </summary>
        public long? FChildSupplyOrgId { get; set; }
        /// <summary>
        /// 供应组织名称
        /// </summary>
        public string FChildSupplyOrgId__FName { get; set; }
        /// <summary>
        /// 供应组织编码
        /// </summary>
        public string FChildSupplyOrgId__FNumber { get; set; }
        /// <summary>
        /// 工序序列
        /// </summary>
        public string? FOptQueue { get; set; }
        /// <summary>
        /// PLMBOM分录内码
        /// </summary>
        public string? FPLMBOMEntryId { get; set; }
        /// <summary>
        /// BOM分录来源
        /// </summary>
        public string? FBOMEntrySRC { get; set; }
        /// <summary>
        /// 跳层
        /// </summary>
        public string? FISSkip { get; set; }
        /// <summary>
        /// 子项明细Id备份(引入时与BOP关联)
        /// </summary>
        public int? FTreeEntryIdBak { get; set; }
        /// <summary>
        /// 领料考虑最小发料批量
        /// </summary>
        public string? FISMinIssueQty { get; set; }
        /// <summary>
        /// 变更类型
        /// </summary>
        public string? FChangeType { get; set; }
        /// <summary>
        /// 变更日期
        /// </summary>
        public DateTime? FChangeTime { get; set; }
        /// <summary>
        /// 变更单号
        /// </summary>
        public string? FECNBillNo { get; set; }
        /// <summary>
        /// 变更类型
        /// </summary>
        public string? FECNChgType { get; set; }
        /// <summary>
        /// 变更日期
        /// </summary>
        public DateTime? FECNChgDate { get; set; }
        /// <summary>
        /// 供料方式
        /// </summary>
        public string? FSupplyMode { get; set; }
        /// <summary>
        /// ECN行类型
        /// </summary>
        public string? FECNRowType { get; set; }
        /// <summary>
        /// 记录字段
        /// </summary>
        public string? FRecordData { get; set; }
        /// <summary>
        /// CloudPLMBOM分录内码
        /// </summary>
        public string? FPLMBOMENTRYROWID { get; set; }
        /// <summary>
        /// MRP运算
        /// </summary>
        public string? FIsMrpRun { get; set; }
        /// <summary>
        /// 替代方案编码
        /// </summary>
        public long? FSubstitutionId { get; set; }
        /// <summary>
        /// 替代方案编码替代名称
        /// </summary>
        public string FSubstitutionId__FName { get; set; }
        /// <summary>
        /// 替代方案编码替代编码
        /// </summary>
        public string FSubstitutionId__FNumber { get; set; }
        /// <summary>
        /// 替代方案分录内码
        /// </summary>
        public int? FSTEntryId { get; set; }
        /// <summary>
        /// 供应类型
        /// </summary>
        public string? FSupplyType { get; set; }
        /// <summary>
        /// 净需求比例(%)
        /// </summary>
        public decimal? FNETDEMANDRATE { get; set; }
        /// <summary>
        /// 变更单分录内码
        /// </summary>
        public int? FEcnEntryId { get; set; }
        #endregion
        #region 阶梯用量
        /// <summary>
        /// 阶梯用量主键FDETAILID
        /// </summary>
        public long FBOMCHILDLOTBASEDQTY_FDETAILID { get; set; }
        /// <summary>
        /// 阶梯用量序列
        /// </summary>
        public int FBOMCHILDLOTBASEDQTY_FSeq { get; set; }
        /// <summary>
        /// 子项物料编码
        /// </summary>
        public long FMATERIALIDLOTBASED { get; set; }
        /// <summary>
        /// 子项物料编码名称
        /// </summary>
        public string FMATERIALIDLOTBASED__FName { get; set; }
        /// <summary>
        /// 子项物料编码编码
        /// </summary>
        public string FMATERIALIDLOTBASED__FNumber { get; set; }
        /// <summary>
        /// 起始数量（含）
        /// </summary>
        public decimal? FSTARTQTY { get; set; }
        /// <summary>
        /// 截止数量
        /// </summary>
        public decimal? FENDQTY { get; set; }
        /// <summary>
        /// 固定损耗
        /// </summary>
        public decimal? FFIXSCRAPQTYLOT { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string? FNOTELOT { get; set; }
        /// <summary>
        /// 变动损耗率
        /// </summary>
        public decimal? FSCRAPRATELOT { get; set; }
        /// <summary>
        /// 用量:分子
        /// </summary>
        public decimal? FNUMERATORLOT { get; set; }
        /// <summary>
        /// 用量:分母
        /// </summary>
        public decimal? FDENOMINATORLOT { get; set; }
        /// <summary>
        /// 子项单位
        /// </summary>
        public long FUNITIDLOT { get; set; }
        /// <summary>
        /// 子项单位名称
        /// </summary>
        public string FUNITIDLOT__FName { get; set; }
        /// <summary>
        /// 子项单位编码
        /// </summary>
        public string FUNITIDLOT__FNumber { get; set; }
        /// <summary>
        /// 子项阶梯基本单位
        /// </summary>
        public long? FBASEUNITIDLOT { get; set; }
        /// <summary>
        /// 子项阶梯基本单位名称
        /// </summary>
        public string FBASEUNITIDLOT__FName { get; set; }
        /// <summary>
        /// 子项阶梯基本单位编码
        /// </summary>
        public string FBASEUNITIDLOT__FNumber { get; set; }
        /// <summary>
        /// 子项基本单位分子
        /// </summary>
        public decimal? FBASENUMERATORLOT { get; set; }
        /// <summary>
        /// 基本单位起始数量
        /// </summary>
        public decimal? FBASESTARTQTY { get; set; }
        /// <summary>
        /// 基本单位截止数量
        /// </summary>
        public decimal? FBASEENDQTY { get; set; }
        /// <summary>
        /// 基本单位固定损耗数量
        /// </summary>
        public decimal? FBASEFIXSCRAPQTYLOT { get; set; }
        /// <summary>
        /// 子项基本单位分母
        /// </summary>
        public decimal? FBASEDENOMINATORLOT { get; set; }
        #endregion
        #region 联副产品
        /// <summary>
        /// 联副产品主键FENTRYID
        /// </summary>
        public long FEntryBOMCOBY_FENTRYID { get; set; }
        /// <summary>
        /// 联副产品序列
        /// </summary>
        public int FEntryBOMCOBY_FSEQ { get; set; }
        /// <summary>
        /// 联副产品类型
        /// </summary>
        public string FCOBYTYPE { get; set; }
        /// <summary>
        /// 联副产品物料编码
        /// </summary>
        public long FMATERIALIDCOBY { get; set; }
        /// <summary>
        /// 联副产品物料编码名称
        /// </summary>
        public string FMATERIALIDCOBY__FName { get; set; }
        /// <summary>
        /// 联副产品物料编码编码
        /// </summary>
        public string FMATERIALIDCOBY__FNumber { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public decimal? FQTYCOBY { get; set; }
        /// <summary>
        /// 成本权重
        /// </summary>
        public decimal? FCOSTRATECOBY { get; set; }
        /// <summary>
        /// 产出工序
        /// </summary>
        public string? FPPROCESSID { get; set; }
        /// <summary>
        /// 生效日期
        /// </summary>
        public DateTime? FEFFECTDATECOBY { get; set; }
        /// <summary>
        /// 失效日期
        /// </summary>
        public DateTime? FEXPIREDATECOBY { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string? FNOTECOBY { get; set; }
        /// <summary>
        /// 联副产品基本数量
        /// </summary>
        public decimal? FBaseQtyCoby { get; set; }
        /// <summary>
        /// 联副产品基本单位
        /// </summary>
        public long? FBaseUnitIdCoby { get; set; }
        /// <summary>
        /// 联副产品基本单位名称
        /// </summary>
        public string FBaseUnitIdCoby__FName { get; set; }
        /// <summary>
        /// 联副产品基本单位编码
        /// </summary>
        public string FBaseUnitIdCoby__FNumber { get; set; }
        /// <summary>
        /// 产出作业编码
        /// </summary>
        public long? FTASKIDCOBY { get; set; }
        /// <summary>
        /// 产出作业编码名称
        /// </summary>
        public string FTASKIDCOBY__FName { get; set; }
        /// <summary>
        /// 产出作业编码编码
        /// </summary>
        public string FTASKIDCOBY__FNumber { get; set; }
        /// <summary>
        /// 单位
        /// </summary>
        public long FUNITIDCOBY { get; set; }
        /// <summary>
        /// 单位名称
        /// </summary>
        public string FUNITIDCOBY__FName { get; set; }
        /// <summary>
        /// 单位编码
        /// </summary>
        public string FUNITIDCOBY__FNumber { get; set; }
        /// <summary>
        /// 辅助属性
        /// </summary>
        public long? FAuxPropIdCoby { get; set; }
        /// <summary>
        /// 产出工序序列
        /// </summary>
        public string? FOutPutOptQueue { get; set; }
        /// <summary>
        /// BOM版本
        /// </summary>
        public long? FBOMIDCoby { get; set; }
        /// <summary>
        /// BOM版本BOM简称
        /// </summary>
        public string FBOMIDCoby__FName { get; set; }
        /// <summary>
        /// BOM版本BOM版本
        /// </summary>
        public string FBOMIDCoby__FNumber { get; set; }
        /// <summary>
        /// 倒冲领料
        /// </summary>
        public string? FISBACKFLUSH { get; set; }
        #endregion
        #region BOP
        /// <summary>
        /// BOP主键FEntryID
        /// </summary>
        public long FBopEntity_FEntryID { get; set; }
        /// <summary>
        /// BOP序列
        /// </summary>
        public int FBopEntity_FSeq { get; set; }
        /// <summary>
        /// 项次
        /// </summary>
        public int? FReplaceGroupBop { get; set; }
        /// <summary>
        /// 生产线编码
        /// </summary>
        public long? FProductLineId { get; set; }
        /// <summary>
        /// 生产线编码名称
        /// </summary>
        public string FProductLineId__FName { get; set; }
        /// <summary>
        /// 生产线编码编码
        /// </summary>
        public string FProductLineId__FNumber { get; set; }
        /// <summary>
        /// 工位/工序
        /// </summary>
        public long? FPrdLineLocId { get; set; }
        /// <summary>
        /// 子项物料编码
        /// </summary>
        public long? FBopMaterialId { get; set; }
        /// <summary>
        /// 子项物料编码名称
        /// </summary>
        public string FBopMaterialId__FName { get; set; }
        /// <summary>
        /// 子项物料编码编码
        /// </summary>
        public string FBopMaterialId__FNumber { get; set; }
        /// <summary>
        /// 子项单位
        /// </summary>
        public long? FBopUnitId { get; set; }
        /// <summary>
        /// 子项单位名称
        /// </summary>
        public string FBopUnitId__FName { get; set; }
        /// <summary>
        /// 子项单位编码
        /// </summary>
        public string FBopUnitId__FNumber { get; set; }
        /// <summary>
        /// 用量类型
        /// </summary>
        public string? FBopDosageType { get; set; }
        /// <summary>
        /// 用量:分子
        /// </summary>
        public decimal? FBopNumerator { get; set; }
        /// <summary>
        /// 用量:分母
        /// </summary>
        public decimal? FBopDenominator { get; set; }
        /// <summary>
        /// 子项基本单位
        /// </summary>
        public long? FBopBaseUnitID { get; set; }
        /// <summary>
        /// 子项基本单位名称
        /// </summary>
        public string FBopBaseUnitID__FName { get; set; }
        /// <summary>
        /// 子项基本单位编码
        /// </summary>
        public string FBopBaseUnitID__FNumber { get; set; }
        /// <summary>
        /// BOP基本单位分子
        /// </summary>
        public decimal? FBaseBopNumerator { get; set; }
        /// <summary>
        /// BOP基本单位分母
        /// </summary>
        public decimal? FBaseBopDenominator { get; set; }
        /// <summary>
        /// 子项明细Id
        /// </summary>
        public int? FTreeEntryId { get; set; }
        #endregion
    }
}