using System;

namespace FormEntityCreation.DataEntity
{
    /// <summary>
    /// 物料
    /// </summary>
    public class BD_MATERIAL
    {
        /// <summary>
        /// FMATERIALID
        /// </summary>
        public int FMATERIALID { get; set; }
        #region 物料
        /// <summary>
        /// 数据状态
        /// </summary>
        public string? FDocumentStatus { get; set; }
        /// <summary>
        /// 禁用状态
        /// </summary>
        public string? FForbidStatus { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string FName { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        public string? FNumber { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string? FDescription { get; set; }
        /// <summary>
        /// 创建组织
        /// </summary>
        public long FCreateOrgId { get; set; }
        /// <summary>
        /// 创建组织名称
        /// </summary>
        public string FCreateOrgId__FName { get; set; }
        /// <summary>
        /// 创建组织编码
        /// </summary>
        public string FCreateOrgId__FNumber { get; set; }
        /// <summary>
        /// 使用组织
        /// </summary>
        public long FUseOrgId { get; set; }
        /// <summary>
        /// 使用组织名称
        /// </summary>
        public string FUseOrgId__FName { get; set; }
        /// <summary>
        /// 使用组织编码
        /// </summary>
        public string FUseOrgId__FNumber { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public long? FCreatorId { get; set; }
        /// <summary>
        /// 创建人用户名称
        /// </summary>
        public string FCreatorId__FName { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        public long? FModifierId { get; set; }
        /// <summary>
        /// 修改人用户名称
        /// </summary>
        public string FModifierId__FName { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime? FCreateDate { get; set; }
        /// <summary>
        /// 修改日期
        /// </summary>
        public DateTime? FModifyDate { get; set; }
        /// <summary>
        /// 助记码
        /// </summary>
        public string? FMnemonicCode { get; set; }
        /// <summary>
        /// 规格型号
        /// </summary>
        public string? FSpecification { get; set; }
        /// <summary>
        /// 禁用人
        /// </summary>
        public long? FForbidderId { get; set; }
        /// <summary>
        /// 禁用人用户名称
        /// </summary>
        public string FForbidderId__FName { get; set; }
        /// <summary>
        /// 禁用日期
        /// </summary>
        public DateTime? FForbidDate { get; set; }
        /// <summary>
        /// 审核日期
        /// </summary>
        public DateTime? FApproveDate { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        public long? FApproverId { get; set; }
        /// <summary>
        /// 审核人用户名称
        /// </summary>
        public string FApproverId__FName { get; set; }
        /// <summary>
        /// 图片(数据库)
        /// </summary>
        public byte[]? FImage1 { get; set; }
        /// <summary>
        /// 旧物料编码
        /// </summary>
        public string? FOldNumber { get; set; }
        /// <summary>
        /// 物料分组
        /// </summary>
        public long? FMaterialGroup { get; set; }
        /// <summary>
        /// PLM物料内码
        /// </summary>
        public string? FPLMMaterialId { get; set; }
        /// <summary>
        /// 物料来源
        /// </summary>
        public string? FMaterialSRC { get; set; }
        /// <summary>
        /// 图片(文件服务器)
        /// </summary>
        public string? FImageFileServer { get; set; }
        /// <summary>
        /// 图片存储类型
        /// </summary>
        public string? FImgStorageType { get; set; }
        /// <summary>
        /// 是否网销
        /// </summary>
        public string? FIsSalseByNet { get; set; }
        /// <summary>
        /// 自动分配
        /// </summary>
        public string? FIsAutoAllocate { get; set; }
        /// <summary>
        /// SPU信息
        /// </summary>
        public long? FSPUID { get; set; }
        /// <summary>
        /// SPU信息名称
        /// </summary>
        public string FSPUID__FName { get; set; }
        /// <summary>
        /// SPU信息编码
        /// </summary>
        public string FSPUID__FNumber { get; set; }
        /// <summary>
        /// SPU信息短名称
        /// </summary>
        public string FSPUID__FShortName { get; set; }
        /// <summary>
        /// 拼音
        /// </summary>
        public string? FPinYin { get; set; }
        /// <summary>
        /// 按批号匹配供需
        /// </summary>
        public string? FDSMatchByLot { get; set; }
        /// <summary>
        /// 禁用原因
        /// </summary>
        public string? FForbidReson { get; set; }
        /// <summary>
        /// 已使用
        /// </summary>
        public string? FRefStatus { get; set; }
        /// <summary>
        /// 可手工预留
        /// </summary>
        public string? FIsHandleReserve { get; set; }
        /// <summary>
        /// 销售出库替代物料
        /// </summary>
        public string? FMulReplaceMaterialId { get; set; }
        /// <summary>
        /// 销售出库替代物料名称
        /// </summary>
        public string FMulReplaceMaterialId__FName { get; set; }
        /// <summary>
        /// 销售出库替代物料编码
        /// </summary>
        public string FMulReplaceMaterialId__FNumber { get; set; }
        /// <summary>
        /// 物料产品类别
        /// </summary>
        public string? F_BP_ProTypes { get; set; }
        /// <summary>
        /// 物料产品类别编码
        /// </summary>
        public string F_BP_ProTypes__FNumber { get; set; }
        /// <summary>
        /// 物料产品类别名称
        /// </summary>
        public string F_BP_ProTypes__FDataValue { get; set; }
        #endregion
        #region 零售特性
        /// <summary>
        /// 商品类型
        /// </summary>
        public long? FComTypeId_CMK { get; set; }
        /// <summary>
        /// 商品类型名称
        /// </summary>
        public string FComTypeId_CMK__FName { get; set; }
        /// <summary>
        /// 商品类型编码
        /// </summary>
        public string FComTypeId_CMK__FNumber { get; set; }
        /// <summary>
        /// 条码前缀
        /// </summary>
        public string? FBarCodeHeader_CMK { get; set; }
        /// <summary>
        /// 主条形码
        /// </summary>
        public string? FGoodBarCode_CMK { get; set; }
        /// <summary>
        /// 品牌
        /// </summary>
        public long? FComBrandId_CMK { get; set; }
        /// <summary>
        /// 品牌名称
        /// </summary>
        public string FComBrandId_CMK__FName { get; set; }
        /// <summary>
        /// 品牌编码
        /// </summary>
        public string FComBrandId_CMK__FNumber { get; set; }
        /// <summary>
        /// 经营方式
        /// </summary>
        public string? FBusinessType_CMK { get; set; }
        /// <summary>
        /// 经营方式编码
        /// </summary>
        public string FBusinessType_CMK__FNumber { get; set; }
        /// <summary>
        /// 经营方式名称
        /// </summary>
        public string FBusinessType_CMK__FDataValue { get; set; }
        /// <summary>
        /// 售卖方式
        /// </summary>
        public string? FSellMethod_CMK { get; set; }
        /// <summary>
        /// 售卖方式编码
        /// </summary>
        public string FSellMethod_CMK__FNumber { get; set; }
        /// <summary>
        /// 售卖方式名称
        /// </summary>
        public string FSellMethod_CMK__FDataValue { get; set; }
        /// <summary>
        /// 本位币
        /// </summary>
        public long? FCurrencyId_CMK { get; set; }
        /// <summary>
        /// 本位币名称
        /// </summary>
        public string FCurrencyId_CMK__FName { get; set; }
        /// <summary>
        /// 本位币编码
        /// </summary>
        public string FCurrencyId_CMK__FNumber { get; set; }
        /// <summary>
        /// 零售状态
        /// </summary>
        public string? FSaleStatus_CMK { get; set; }
        /// <summary>
        /// 采购状态
        /// </summary>
        public string? FPurStatus_CMK { get; set; }
        /// <summary>
        /// 采购单价
        /// </summary>
        public decimal? FPurPrice_CMK { get; set; }
        /// <summary>
        /// 零售价
        /// </summary>
        public decimal? FSalePrice_CMK { get; set; }
        /// <summary>
        /// 会员价
        /// </summary>
        public decimal? FVIPPrice_CMK { get; set; }
        /// <summary>
        /// 积分比率
        /// </summary>
        public decimal? FPointsRate_CMK { get; set; }
        /// <summary>
        /// 图片(文件服务器)
        /// </summary>
        public string? FImgFile_CMK { get; set; }
        /// <summary>
        /// 所属专柜
        /// </summary>
        public long? FShoppeID_CMK { get; set; }
        /// <summary>
        /// 所属专柜名称
        /// </summary>
        public string FShoppeID_CMK__FName { get; set; }
        /// <summary>
        /// 所属专柜编码
        /// </summary>
        public string FShoppeID_CMK__FNumber { get; set; }
        /// <summary>
        /// 门店供货价
        /// </summary>
        public decimal? FLSProPrice { get; set; }
        /// <summary>
        /// 商品来源
        /// </summary>
        public long? FMaterialSource { get; set; }
        /// <summary>
        /// 商品来源名称
        /// </summary>
        public string FMaterialSource__FName { get; set; }
        /// <summary>
        /// 商品来源编码
        /// </summary>
        public string FMaterialSource__FNumber { get; set; }
        /// <summary>
        /// 是否按区间价控制销售
        /// </summary>
        public string? FIsControlSal { get; set; }
        /// <summary>
        /// 上调百分比
        /// </summary>
        public decimal? FUpPercent { get; set; }
        /// <summary>
        /// 下调百分比
        /// </summary>
        public decimal? FLowerPercent { get; set; }
        /// <summary>
        /// 计算基数
        /// </summary>
        public string? FCalculateBase { get; set; }
        /// <summary>
        /// 最高销售价
        /// </summary>
        public decimal? FMaxSalPrice_CMK { get; set; }
        /// <summary>
        /// 最低销售价
        /// </summary>
        public decimal? FMinSalPrice_CMK { get; set; }
        /// <summary>
        /// 组装商品退货是否自动拆卸
        /// </summary>
        public string? FIsAutoRemove { get; set; }
        /// <summary>
        /// 快递费虚拟商品
        /// </summary>
        public string? FIsMailVirtual { get; set; }
        /// <summary>
        /// 是否包邮
        /// </summary>
        public string? FIsFreeSend { get; set; }
        /// <summary>
        /// 物流计价方式
        /// </summary>
        public string? FPriceType { get; set; }
        /// <summary>
        /// 物流计量
        /// </summary>
        public decimal? FLogisticsCount { get; set; }
        /// <summary>
        /// 零售最小包装量
        /// </summary>
        public decimal? FRequestMinPackQty { get; set; }
        /// <summary>
        /// 门店最低要货量
        /// </summary>
        public decimal? FMinRequestQty { get; set; }
        /// <summary>
        /// 零售单位
        /// </summary>
        public long? FRetailUnitID { get; set; }
        /// <summary>
        /// 零售单位名称
        /// </summary>
        public string FRetailUnitID__FName { get; set; }
        /// <summary>
        /// 零售单位编码
        /// </summary>
        public string FRetailUnitID__FNumber { get; set; }
        /// <summary>
        /// 是否打印
        /// </summary>
        public string? FIsPrinttAg { get; set; }
        /// <summary>
        /// 是否辅料
        /// </summary>
        public string? FIsAccessory { get; set; }
        /// <summary>
        /// 计时单位
        /// </summary>
        public string? FTimeUnit { get; set; }
        /// <summary>
        /// 最低租赁时长
        /// </summary>
        public decimal? FMinRentDura { get; set; }
        /// <summary>
        /// 租赁起步价
        /// </summary>
        public decimal? FRentBeginPrice { get; set; }
        /// <summary>
        /// 计价步长
        /// </summary>
        public decimal? FPricingStep { get; set; }
        /// <summary>
        /// 租赁步长单价
        /// </summary>
        public decimal? FRentStepPrice { get; set; }
        /// <summary>
        /// 免租时长
        /// </summary>
        public decimal? FRentFreeDura { get; set; }
        /// <summary>
        /// 押金金额
        /// </summary>
        public decimal? FDepositAmount { get; set; }
        /// <summary>
        /// 是否上传SKU图片
        /// </summary>
        public string? FUploadSkuImage { get; set; }
        /// <summary>
        /// 销售价格最低折扣率
        /// </summary>
        public decimal? FCMKMINDISCOUNTRATE { get; set; }
        #endregion
        #region 条形码
        /// <summary>
        /// 条形码主键FEntryID
        /// </summary>
        public long FBarCodeEntity_CMK_FEntryID { get; set; }
        /// <summary>
        /// 条形码序列
        /// </summary>
        public int FBarCodeEntity_CMK_FSeq { get; set; }
        /// <summary>
        /// 条形码类型
        /// </summary>
        public string FCodeType_CMK { get; set; }
        /// <summary>
        /// 条形码
        /// </summary>
        public string? FBarCode_CMK { get; set; }
        /// <summary>
        /// 计量单位
        /// </summary>
        public long FUnitId_CMK { get; set; }
        /// <summary>
        /// 计量单位名称
        /// </summary>
        public string FUnitId_CMK__FName { get; set; }
        /// <summary>
        /// 计量单位编码
        /// </summary>
        public string FUnitId_CMK__FNumber { get; set; }
        /// <summary>
        /// 会员卡级别
        /// </summary>
        public long? FVIPCardLevel_CMK { get; set; }
        /// <summary>
        /// 会员卡级别名称
        /// </summary>
        public string FVIPCardLevel_CMK__FName { get; set; }
        /// <summary>
        /// 会员卡级别编码
        /// </summary>
        public string FVIPCardLevel_CMK__FNumber { get; set; }
        /// <summary>
        /// 零售价
        /// </summary>
        public decimal? FPrice_CMK { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string? FRemarks_CMK { get; set; }
        /// <summary>
        /// 会员价
        /// </summary>
        public decimal? FVIPPrice { get; set; }
        /// <summary>
        /// 门店供货价
        /// </summary>
        public decimal? FProPrice { get; set; }
        #endregion
        #region 规格属性列表
        /// <summary>
        /// 规格属性列表主键FEntryID
        /// </summary>
        public long FSpecialAttributeEntity_FEntryID { get; set; }
        /// <summary>
        /// 规格属性列表序列
        /// </summary>
        public int FSpecialAttributeEntity_FSeq { get; set; }
        /// <summary>
        /// 规格属性类别
        /// </summary>
        public long? FSpecAttrCategoryID { get; set; }
        /// <summary>
        /// 规格属性类别名称
        /// </summary>
        public string FSpecAttrCategoryID__FName { get; set; }
        /// <summary>
        /// 规格属性类别编码
        /// </summary>
        public string FSpecAttrCategoryID__FNumber { get; set; }
        /// <summary>
        /// 规格属性
        /// </summary>
        public long? FSpecialAttributeID { get; set; }
        /// <summary>
        /// 规格属性名称
        /// </summary>
        public string FSpecialAttributeID__FName { get; set; }
        /// <summary>
        /// 规格属性编码
        /// </summary>
        public string FSpecialAttributeID__FNumber { get; set; }
        #endregion
        #region 基本
        /// <summary>
        /// 物料属性
        /// </summary>
        public string FErpClsID { get; set; }
        /// <summary>
        /// 允许库存
        /// </summary>
        public string? FIsInventory { get; set; }
        /// <summary>
        /// 允许销售
        /// </summary>
        public string? FIsSale { get; set; }
        /// <summary>
        /// 允许转资产
        /// </summary>
        public string? FIsAsset { get; set; }
        /// <summary>
        /// 允许委外
        /// </summary>
        public string? FIsSubContract { get; set; }
        /// <summary>
        /// 允许生产
        /// </summary>
        public string? FIsProduce { get; set; }
        /// <summary>
        /// 允许采购
        /// </summary>
        public string? FIsPurchase { get; set; }
        /// <summary>
        /// 即时核算
        /// </summary>
        public string? FIsRealTimeAccout { get; set; }
        /// <summary>
        /// 基本单位
        /// </summary>
        public long FBaseUnitId { get; set; }
        /// <summary>
        /// 基本单位名称
        /// </summary>
        public string FBaseUnitId__FName { get; set; }
        /// <summary>
        /// 基本单位编码
        /// </summary>
        public string FBaseUnitId__FNumber { get; set; }
        /// <summary>
        /// 税分类
        /// </summary>
        public string? FTaxType { get; set; }
        /// <summary>
        /// 税分类编码
        /// </summary>
        public string FTaxType__FNumber { get; set; }
        /// <summary>
        /// 税分类名称
        /// </summary>
        public string FTaxType__FDataValue { get; set; }
        /// <summary>
        /// 物料分类
        /// </summary>
        public long? FTypeID { get; set; }
        /// <summary>
        /// 存货类别
        /// </summary>
        public long FCategoryID { get; set; }
        /// <summary>
        /// 存货类别名称
        /// </summary>
        public string FCategoryID__FName { get; set; }
        /// <summary>
        /// 存货类别编码
        /// </summary>
        public string FCategoryID__FNumber { get; set; }
        /// <summary>
        /// 默认税率
        /// </summary>
        public long? FTaxRateId { get; set; }
        /// <summary>
        /// 默认税率名称
        /// </summary>
        public string FTaxRateId__FName { get; set; }
        /// <summary>
        /// 默认税率编码
        /// </summary>
        public string FTaxRateId__FNumber { get; set; }
        /// <summary>
        /// 条码
        /// </summary>
        public string? FBARCODE { get; set; }
        /// <summary>
        /// 重量单位
        /// </summary>
        public long? FWEIGHTUNITID { get; set; }
        /// <summary>
        /// 重量单位名称
        /// </summary>
        public string FWEIGHTUNITID__FName { get; set; }
        /// <summary>
        /// 重量单位编码
        /// </summary>
        public string FWEIGHTUNITID__FNumber { get; set; }
        /// <summary>
        /// 尺寸单位
        /// </summary>
        public long? FVOLUMEUNITID { get; set; }
        /// <summary>
        /// 尺寸单位名称
        /// </summary>
        public string FVOLUMEUNITID__FName { get; set; }
        /// <summary>
        /// 尺寸单位编码
        /// </summary>
        public string FVOLUMEUNITID__FNumber { get; set; }
        /// <summary>
        /// 毛重
        /// </summary>
        public decimal? FGROSSWEIGHT { get; set; }
        /// <summary>
        /// 净重
        /// </summary>
        public decimal? FNETWEIGHT { get; set; }
        /// <summary>
        /// 长
        /// </summary>
        public decimal? FLENGTH { get; set; }
        /// <summary>
        /// 体积
        /// </summary>
        public decimal? FVOLUME { get; set; }
        /// <summary>
        /// 宽
        /// </summary>
        public decimal? FWIDTH { get; set; }
        /// <summary>
        /// 高
        /// </summary>
        public decimal? FHEIGHT { get; set; }
        /// <summary>
        /// 配置生产
        /// </summary>
        public string? FCONFIGTYPE { get; set; }
        /// <summary>
        /// 套件
        /// </summary>
        public string FSuite { get; set; }
        /// <summary>
        /// 结算成本价加减价比例(%)
        /// </summary>
        public decimal? FCostPriceRate { get; set; }
        /// <summary>
        /// 英文名称
        /// </summary>
        public string? FNameEn { get; set; }
        /// <summary>
        /// 制式
        /// </summary>
        public string? FSysModel { get; set; }
        /// <summary>
        /// 颜色
        /// </summary>
        public string? FColor { get; set; }
        /// <summary>
        /// 传播名
        /// </summary>
        public string? FSpreadName { get; set; }
        /// <summary>
        /// 开票方
        /// </summary>
        public string? FMAKEINVOICEPARTY { get; set; }
        /// <summary>
        /// 特征件子项
        /// </summary>
        public string FFeatureItem { get; set; }
        /// <summary>
        /// 使用组织
        /// </summary>
        public long? FUseOrgId1 { get; set; }
        /// <summary>
        /// 使用组织名称
        /// </summary>
        public string FUseOrgId1__FName { get; set; }
        /// <summary>
        /// 使用组织编码
        /// </summary>
        public string FUseOrgId1__FNumber { get; set; }
        /// <summary>
        /// 是否变更中
        /// </summary>
        public string? FIsChange { get; set; }
        /// <summary>
        /// 适用产品
        /// </summary>
        public string? FC001 { get; set; }
        /// <summary>
        /// 适用产品名称
        /// </summary>
        public string FC001__FName { get; set; }
        /// <summary>
        /// 适用产品编码
        /// </summary>
        public string FC001__FNumber { get; set; }
        #endregion
        #region 库存
        /// <summary>
        /// 库存单位
        /// </summary>
        public int FStoreUnitID { get; set; }
        /// <summary>
        /// 库存单位名称
        /// </summary>
        public string FStoreUnitID__FName { get; set; }
        /// <summary>
        /// 库存单位编码
        /// </summary>
        public string FStoreUnitID__FNumber { get; set; }
        /// <summary>
        /// 辅助单位
        /// </summary>
        public int? FAuxUnitID { get; set; }
        /// <summary>
        /// 辅助单位名称
        /// </summary>
        public string FAuxUnitID__FName { get; set; }
        /// <summary>
        /// 辅助单位编码
        /// </summary>
        public string FAuxUnitID__FNumber { get; set; }
        /// <summary>
        /// 仓库
        /// </summary>
        public long? FStockId { get; set; }
        /// <summary>
        /// 仓库名称
        /// </summary>
        public string FStockId__FName { get; set; }
        /// <summary>
        /// 仓库编码
        /// </summary>
        public string FStockId__FNumber { get; set; }
        /// <summary>
        /// 可锁库
        /// </summary>
        public string? FIsLockStock { get; set; }
        /// <summary>
        /// 批号编码规则
        /// </summary>
        public int? FBatchRuleID { get; set; }
        /// <summary>
        /// 批号编码规则名称
        /// </summary>
        public string FBatchRuleID__FName { get; set; }
        /// <summary>
        /// 批号编码规则编码
        /// </summary>
        public string FBatchRuleID__FNumber { get; set; }
        /// <summary>
        /// 保质期单位
        /// </summary>
        public string? FExpUnit { get; set; }
        /// <summary>
        /// 仓位
        /// </summary>
        public string? FStockPlaceId { get; set; }
        /// <summary>
        /// 在架寿命期
        /// </summary>
        public int? FOnlineLife { get; set; }
        /// <summary>
        /// 保质期
        /// </summary>
        public int? FExpPeriod { get; set; }
        /// <summary>
        /// 库存单位换算率分子
        /// </summary>
        public decimal? FStoreURNum { get; set; }
        /// <summary>
        /// 库存单位换算率分母
        /// </summary>
        public decimal? FStoreURNom { get; set; }
        /// <summary>
        /// 启用批号管理
        /// </summary>
        public string? FIsBatchManage { get; set; }
        /// <summary>
        /// 启用保质期管理
        /// </summary>
        public string? FIsKFPeriod { get; set; }
        /// <summary>
        /// 批号附属信息
        /// </summary>
        public string? FIsExpParToFlot { get; set; }
        /// <summary>
        /// 启用盘点周期
        /// </summary>
        public string? FIsCycleCounting { get; set; }
        /// <summary>
        /// 必盘
        /// </summary>
        public string? FIsMustCounting { get; set; }
        /// <summary>
        /// 币别
        /// </summary>
        public long FCurrencyId { get; set; }
        /// <summary>
        /// 币别名称
        /// </summary>
        public string FCurrencyId__FName { get; set; }
        /// <summary>
        /// 币别编码
        /// </summary>
        public string FCurrencyId__FNumber { get; set; }
        /// <summary>
        /// 参考成本
        /// </summary>
        public decimal? FRefCost { get; set; }
        /// <summary>
        /// 盘点周期单位
        /// </summary>
        public string? FCountCycle { get; set; }
        /// <summary>
        /// 盘点周期
        /// </summary>
        public int? FCountDay { get; set; }
        /// <summary>
        /// 库存管理
        /// </summary>
        public string? FIsSNManage { get; set; }
        /// <summary>
        /// 序列号编码规则
        /// </summary>
        public long? FSNCodeRule { get; set; }
        /// <summary>
        /// 序列号编码规则名称
        /// </summary>
        public string FSNCodeRule__FName { get; set; }
        /// <summary>
        /// 序列号编码规则编码
        /// </summary>
        public string FSNCodeRule__FNumber { get; set; }
        /// <summary>
        /// 序列号单位
        /// </summary>
        public long? FSNUnit { get; set; }
        /// <summary>
        /// 序列号单位名称
        /// </summary>
        public string FSNUnit__FName { get; set; }
        /// <summary>
        /// 序列号单位编码
        /// </summary>
        public string FSNUnit__FNumber { get; set; }
        /// <summary>
        /// 安全库存
        /// </summary>
        public decimal? FSafeStock { get; set; }
        /// <summary>
        /// 再订货点
        /// </summary>
        public decimal? FReOrderGood { get; set; }
        /// <summary>
        /// 最小库存
        /// </summary>
        public decimal? FMinStock { get; set; }
        /// <summary>
        /// 最大库存
        /// </summary>
        public decimal? FMaxStock { get; set; }
        /// <summary>
        /// 换算方向
        /// </summary>
        public string FUnitConvertDir { get; set; }
        /// <summary>
        /// 启用最小库存
        /// </summary>
        public string? FIsEnableMinStock { get; set; }
        /// <summary>
        /// 启用安全库存
        /// </summary>
        public string? FIsEnableSafeStock { get; set; }
        /// <summary>
        /// 启用最大库存
        /// </summary>
        public string? FIsEnableMaxStock { get; set; }
        /// <summary>
        /// 启用再订货点
        /// </summary>
        public string? FIsEnableReOrder { get; set; }
        /// <summary>
        /// 经济订货批量
        /// </summary>
        public decimal? FEconReOrderQty { get; set; }
        /// <summary>
        /// 生产追溯
        /// </summary>
        public string? FIsSNPRDTracy { get; set; }
        /// <summary>
        /// 序列号生成时机
        /// </summary>
        public string FSNGenerateTime { get; set; }
        /// <summary>
        /// 业务范围
        /// </summary>
        public string FSNManageType { get; set; }
        /// <summary>
        /// 单箱标准数量
        /// </summary>
        public decimal? FBoxStandardQty { get; set; }
        /// <summary>
        /// 使用组织
        /// </summary>
        public long? FUseOrgId2 { get; set; }
        /// <summary>
        /// 使用组织名称
        /// </summary>
        public string FUseOrgId2__FName { get; set; }
        /// <summary>
        /// 使用组织编码
        /// </summary>
        public string FUseOrgId2__FNumber { get; set; }
        #endregion
        #region 销售
        /// <summary>
        /// ATP检查
        /// </summary>
        public string? FIsATPCheck { get; set; }
        /// <summary>
        /// 销售计价单位
        /// </summary>
        public int FSalePriceUnitId { get; set; }
        /// <summary>
        /// 销售计价单位名称
        /// </summary>
        public string FSalePriceUnitId__FName { get; set; }
        /// <summary>
        /// 销售计价单位编码
        /// </summary>
        public string FSalePriceUnitId__FNumber { get; set; }
        /// <summary>
        /// 销售单位
        /// </summary>
        public int FSaleUnitId { get; set; }
        /// <summary>
        /// 销售单位名称
        /// </summary>
        public string FSaleUnitId__FName { get; set; }
        /// <summary>
        /// 销售单位编码
        /// </summary>
        public string FSaleUnitId__FNumber { get; set; }
        /// <summary>
        /// 可开票
        /// </summary>
        public string? FIsInvoice { get; set; }
        /// <summary>
        /// 最大批量
        /// </summary>
        public decimal? FMaxQty { get; set; }
        /// <summary>
        /// 允许退货
        /// </summary>
        public string? FIsReturn { get; set; }
        /// <summary>
        /// 最小批量
        /// </summary>
        public decimal? FMinQty { get; set; }
        /// <summary>
        /// 部件可退
        /// </summary>
        public string? FIsReturnPart { get; set; }
        /// <summary>
        /// 起订量
        /// </summary>
        public decimal? FOrderQty { get; set; }
        /// <summary>
        /// 销售计价单位换算率分母
        /// </summary>
        public decimal? FSalePriceURNom { get; set; }
        /// <summary>
        /// 销售单位换算率分子
        /// </summary>
        public decimal? FSaleURNum { get; set; }
        /// <summary>
        /// 销售计价单位换算率分子
        /// </summary>
        public decimal? FSalePriceURNum { get; set; }
        /// <summary>
        /// 销售单位换算率分母
        /// </summary>
        public decimal? FSaleURNom { get; set; }
        /// <summary>
        /// 超发上限(%)
        /// </summary>
        public decimal? FOutStockLmtH { get; set; }
        /// <summary>
        /// 超发下限(%)
        /// </summary>
        public decimal? FOutStockLmtL { get; set; }
        /// <summary>
        /// 代理销售减价比例(%)
        /// </summary>
        public decimal? FAgentSalReduceRate { get; set; }
        /// <summary>
        /// 允许发布到订货平台
        /// </summary>
        public string? FAllowPublish { get; set; }
        /// <summary>
        /// 启用售后服务
        /// </summary>
        public string? FISAFTERSALE { get; set; }
        /// <summary>
        /// 生成产品档案
        /// </summary>
        public string? FISPRODUCTFILES { get; set; }
        /// <summary>
        /// 是否保修
        /// </summary>
        public string? FISWARRANTED { get; set; }
        /// <summary>
        /// 保修期
        /// </summary>
        public int? FWARRANTY { get; set; }
        /// <summary>
        /// 保修期单位
        /// </summary>
        public string? FWARRANTYUNITID { get; set; }
        /// <summary>
        /// 超发控制单位
        /// </summary>
        public string? FOutLmtUnit { get; set; }
        /// <summary>
        /// 税收分类编码
        /// </summary>
        public long? FTaxCategoryCodeId { get; set; }
        /// <summary>
        /// 税收分类编码名称
        /// </summary>
        public string FTaxCategoryCodeId__FName { get; set; }
        /// <summary>
        /// 税收分类编码编码
        /// </summary>
        public string FTaxCategoryCodeId__FNumber { get; set; }
        /// <summary>
        /// 销售分组
        /// </summary>
        public long? FSalGroup { get; set; }
        /// <summary>
        /// 税收优惠政策类型
        /// </summary>
        public string? FTaxDiscountsType { get; set; }
        /// <summary>
        /// 享受税收优惠政策
        /// </summary>
        public string? FIsTaxEnjoy { get; set; }
        /// <summary>
        /// 使用组织
        /// </summary>
        public long? FUseOrgId3 { get; set; }
        /// <summary>
        /// 使用组织名称
        /// </summary>
        public string FUseOrgId3__FName { get; set; }
        /// <summary>
        /// 使用组织编码
        /// </summary>
        public string FUseOrgId3__FNumber { get; set; }
        /// <summary>
        /// 不参与可发量统计
        /// </summary>
        public string? FUnValidateExpQty { get; set; }
        #endregion
        #region 采购
        /// <summary>
        /// 采购单位
        /// </summary>
        public int FPurchaseUnitId { get; set; }
        /// <summary>
        /// 采购单位名称
        /// </summary>
        public string FPurchaseUnitId__FName { get; set; }
        /// <summary>
        /// 采购单位编码
        /// </summary>
        public string FPurchaseUnitId__FNumber { get; set; }
        /// <summary>
        /// 采购员
        /// </summary>
        public long? FPurchaserId { get; set; }
        /// <summary>
        /// 采购员名称
        /// </summary>
        public string FPurchaserId__FName { get; set; }
        /// <summary>
        /// 采购员职员编码
        /// </summary>
        public string FPurchaserId__FNumber { get; set; }
        /// <summary>
        /// 默认供应商
        /// </summary>
        public long? FDefaultVendor { get; set; }
        /// <summary>
        /// 默认供应商名称
        /// </summary>
        public string FDefaultVendor__FName { get; set; }
        /// <summary>
        /// 默认供应商编码
        /// </summary>
        public string FDefaultVendor__FNumber { get; set; }
        /// <summary>
        /// 默认供应商简称
        /// </summary>
        public string FDefaultVendor__FShortName { get; set; }
        /// <summary>
        /// 货源控制
        /// </summary>
        public string? FIsSourceControl { get; set; }
        /// <summary>
        /// 需要请购
        /// </summary>
        public string? FIsPR { get; set; }
        /// <summary>
        /// 收货下限比例(%)
        /// </summary>
        public decimal? FReceiveMinScale { get; set; }
        /// <summary>
        /// 采购组
        /// </summary>
        public long? FPurchaseGroupId { get; set; }
        /// <summary>
        /// 采购组业务组名称
        /// </summary>
        public string FPurchaseGroupId__FName { get; set; }
        /// <summary>
        /// 采购组业务组编码
        /// </summary>
        public string FPurchaseGroupId__FNumber { get; set; }
        /// <summary>
        /// 收货上限比例(%)
        /// </summary>
        public decimal? FReceiveMaxScale { get; set; }
        /// <summary>
        /// 采购计价单位
        /// </summary>
        public long FPurchasePriceUnitId { get; set; }
        /// <summary>
        /// 采购计价单位名称
        /// </summary>
        public string FPurchasePriceUnitId__FName { get; set; }
        /// <summary>
        /// 采购计价单位编码
        /// </summary>
        public string FPurchasePriceUnitId__FNumber { get; set; }
        /// <summary>
        /// 供应商资质
        /// </summary>
        public string? FIsVendorQualification { get; set; }
        /// <summary>
        /// 收货提前天数
        /// </summary>
        public int? FReceiveAdvanceDays { get; set; }
        /// <summary>
        /// 收货延迟天数
        /// </summary>
        public int? FReceiveDelayDays { get; set; }
        /// <summary>
        /// 采购单位换算率分子
        /// </summary>
        public decimal? FPurURNum { get; set; }
        /// <summary>
        /// 采购计价单位换算率分子
        /// </summary>
        public decimal? FPurPriceURNum { get; set; }
        /// <summary>
        /// 采购单位换算率分母
        /// </summary>
        public decimal? FPurURNom { get; set; }
        /// <summary>
        /// 采购计价单位换算率分母
        /// </summary>
        public decimal? FPurPriceURNom { get; set; }
        /// <summary>
        /// 配额管理
        /// </summary>
        public string? FIsQuota { get; set; }
        /// <summary>
        /// 配额方式
        /// </summary>
        public string FQuotaType { get; set; }
        /// <summary>
        /// 代理采购加成比例
        /// </summary>
        public decimal? FAgentPurPlusRate { get; set; }
        /// <summary>
        /// 费用项目
        /// </summary>
        public long? FChargeID { get; set; }
        /// <summary>
        /// 费用项目名称
        /// </summary>
        public string FChargeID__FName { get; set; }
        /// <summary>
        /// 费用项目编码
        /// </summary>
        public string FChargeID__FNumber { get; set; }
        /// <summary>
        /// 最小拆分数量
        /// </summary>
        public decimal? FMinSplitQty { get; set; }
        /// <summary>
        /// 基本单位最小拆分数量
        /// </summary>
        public decimal? FBaseMinSplitQty { get; set; }
        /// <summary>
        /// VMI业务
        /// </summary>
        public string? FIsVmiBusiness { get; set; }
        /// <summary>
        /// 允许退料
        /// </summary>
        public string? FIsReturnMaterial { get; set; }
        /// <summary>
        /// 启用商联在线(6.1弃用)
        /// </summary>
        public string? FEnableSL { get; set; }
        /// <summary>
        /// 采购组织
        /// </summary>
        public long? FPurchaseOrgId { get; set; }
        /// <summary>
        /// 采购组织名称
        /// </summary>
        public string FPurchaseOrgId__FName { get; set; }
        /// <summary>
        /// 采购组织编码
        /// </summary>
        public string FPurchaseOrgId__FNumber { get; set; }
        /// <summary>
        /// 默认条码规则
        /// </summary>
        public long? FDefBarCodeRuleId { get; set; }
        /// <summary>
        /// 默认条码规则规则名称
        /// </summary>
        public string FDefBarCodeRuleId__FName { get; set; }
        /// <summary>
        /// 默认条码规则规则编码
        /// </summary>
        public string FDefBarCodeRuleId__FNumber { get; set; }
        /// <summary>
        /// 重复打印数
        /// </summary>
        public int? FPrintCount { get; set; }
        /// <summary>
        /// 采购类型
        /// </summary>
        public long? FPOBillTypeId { get; set; }
        /// <summary>
        /// 采购类型名称
        /// </summary>
        public string FPOBillTypeId__FName { get; set; }
        /// <summary>
        /// 采购类型编码
        /// </summary>
        public string FPOBillTypeId__FNumber { get; set; }
        /// <summary>
        /// 最小包装数
        /// </summary>
        public decimal? FMinPackCount { get; set; }
        /// <summary>
        /// 使用组织
        /// </summary>
        public long? FUseOrgId4 { get; set; }
        /// <summary>
        /// 使用组织名称
        /// </summary>
        public string FUseOrgId4__FName { get; set; }
        /// <summary>
        /// 使用组织编码
        /// </summary>
        public string FUseOrgId4__FNumber { get; set; }
        /// <summary>
        /// 日产量
        /// </summary>
        public decimal? FDailyOutQtySub { get; set; }
        /// <summary>
        /// 默认产线
        /// </summary>
        public long? FDefaultLineIdSub { get; set; }
        /// <summary>
        /// 默认产线产线名称
        /// </summary>
        public string FDefaultLineIdSub__FName { get; set; }
        /// <summary>
        /// 默认产线产线编码
        /// </summary>
        public string FDefaultLineIdSub__FNumber { get; set; }
        /// <summary>
        /// 启用日排产
        /// </summary>
        public string? FIsEnableScheduleSub { get; set; }
        #endregion
        #region 计划
        /// <summary>
        /// 计划员
        /// </summary>
        public int? FPlanerID { get; set; }
        /// <summary>
        /// 计划员名称
        /// </summary>
        public string FPlanerID__FName { get; set; }
        /// <summary>
        /// 计划员职员编码
        /// </summary>
        public string FPlanerID__FNumber { get; set; }
        /// <summary>
        /// 固定/经济批量
        /// </summary>
        public decimal? FEOQ { get; set; }
        /// <summary>
        /// 计划策略
        /// </summary>
        public string FPlanningStrategy { get; set; }
        /// <summary>
        /// 订货策略
        /// </summary>
        public string FOrderPolicy { get; set; }
        /// <summary>
        /// 计划区
        /// </summary>
        public long? FPlanWorkshop { get; set; }
        /// <summary>
        /// 计划区计划区名称
        /// </summary>
        public string FPlanWorkshop__FName { get; set; }
        /// <summary>
        /// 计划区计划区编码
        /// </summary>
        public string FPlanWorkshop__FNumber { get; set; }
        /// <summary>
        /// 固定提前期单位
        /// </summary>
        public string FFixLeadTimeType { get; set; }
        /// <summary>
        /// 固定提前期
        /// </summary>
        public int? FFixLeadTime { get; set; }
        /// <summary>
        /// 变动提前期单位
        /// </summary>
        public string FVarLeadTimeType { get; set; }
        /// <summary>
        /// 变动提前期
        /// </summary>
        public int? FVarLeadTime { get; set; }
        /// <summary>
        /// 检验提前期单位
        /// </summary>
        public string FCheckLeadTimeType { get; set; }
        /// <summary>
        /// 检验提前期
        /// </summary>
        public int? FCheckLeadTime { get; set; }
        /// <summary>
        /// 订货间隔期单位
        /// </summary>
        public string FOrderIntervalTimeType { get; set; }
        /// <summary>
        /// 订货间隔期
        /// </summary>
        public int? FOrderIntervalTime { get; set; }
        /// <summary>
        /// 批量拆分间隔天数
        /// </summary>
        public int? FPlanIntervalsDays { get; set; }
        /// <summary>
        /// 拆分批量
        /// </summary>
        public decimal? FPlanBatchSplitQty { get; set; }
        /// <summary>
        /// 计划时界
        /// </summary>
        public int? FPlanTimeZone { get; set; }
        /// <summary>
        /// 需求时界
        /// </summary>
        public int? FRequestTimeZone { get; set; }
        /// <summary>
        /// MRP计算是否合并需求
        /// </summary>
        public string? FIsMrpComReq { get; set; }
        /// <summary>
        /// 预留类型
        /// </summary>
        public string FReserveType { get; set; }
        /// <summary>
        /// 允许提前天数
        /// </summary>
        public int? FCanLeadDays { get; set; }
        /// <summary>
        /// 提前宽限期
        /// </summary>
        public int? FLeadExtendDay { get; set; }
        /// <summary>
        /// 延后宽限期
        /// </summary>
        public int? FDelayExtendDay { get; set; }
        /// <summary>
        /// 允许延后天数
        /// </summary>
        public int? FCanDelayDays { get; set; }
        /// <summary>
        /// 时间单位
        /// </summary>
        public string FPlanOffsetTimeType { get; set; }
        /// <summary>
        /// 偏置时间
        /// </summary>
        public int? FPlanOffsetTime { get; set; }
        /// <summary>
        /// 最小订货量
        /// </summary>
        public decimal? FMinPOQty { get; set; }
        /// <summary>
        /// 最小包装量
        /// </summary>
        public decimal? FIncreaseQty { get; set; }
        /// <summary>
        /// 最大订货量
        /// </summary>
        public decimal? FMaxPOQty { get; set; }
        /// <summary>
        /// 变动提前期批量
        /// </summary>
        public decimal? FVarLeadTimeLotSize { get; set; }
        /// <summary>
        /// 基本变动提前期批量
        /// </summary>
        public decimal? FBaseVarLeadTimeLotSize { get; set; }
        /// <summary>
        /// 计划组
        /// </summary>
        public long? FPlanGroupId { get; set; }
        /// <summary>
        /// 计划组业务组名称
        /// </summary>
        public string FPlanGroupId__FName { get; set; }
        /// <summary>
        /// 计划组业务组编码
        /// </summary>
        public string FPlanGroupId__FNumber { get; set; }
        /// <summary>
        /// 制造策略
        /// </summary>
        public long? FMfgPolicyId { get; set; }
        /// <summary>
        /// 制造策略名称
        /// </summary>
        public string FMfgPolicyId__FName { get; set; }
        /// <summary>
        /// 制造策略编码
        /// </summary>
        public string FMfgPolicyId__FNumber { get; set; }
        /// <summary>
        /// 供应来源
        /// </summary>
        public long? FSupplySourceId { get; set; }
        /// <summary>
        /// 供应来源名称
        /// </summary>
        public string FSupplySourceId__FName { get; set; }
        /// <summary>
        /// 供应来源编码
        /// </summary>
        public string FSupplySourceId__FNumber { get; set; }
        /// <summary>
        /// 时间紧迫系数
        /// </summary>
        public long? FTimeFactorId { get; set; }
        /// <summary>
        /// 时间紧迫系数名称
        /// </summary>
        public string FTimeFactorId__FName { get; set; }
        /// <summary>
        /// 时间紧迫系数编码
        /// </summary>
        public string FTimeFactorId__FNumber { get; set; }
        /// <summary>
        /// 数量负荷系数
        /// </summary>
        public long? FQtyFactorId { get; set; }
        /// <summary>
        /// 数量负荷系数名称
        /// </summary>
        public string FQtyFactorId__FName { get; set; }
        /// <summary>
        /// 数量负荷系数编码
        /// </summary>
        public string FQtyFactorId__FNumber { get; set; }
        /// <summary>
        /// 计划模式
        /// </summary>
        public string? FPlanMode { get; set; }
        /// <summary>
        /// 预计入库允许部分延后
        /// </summary>
        public string? FAllowPartDelay { get; set; }
        /// <summary>
        /// 预计入库允许部分提前
        /// </summary>
        public string? FAllowPartAhead { get; set; }
        /// <summary>
        /// 安全库存
        /// </summary>
        public decimal? FPlanSafeStockQty { get; set; }
        /// <summary>
        /// ATO预测冲销方案
        /// </summary>
        public int? FATOSchemeId { get; set; }
        /// <summary>
        /// ATO预测冲销方案方案名称
        /// </summary>
        public string FATOSchemeId__FName { get; set; }
        /// <summary>
        /// ATO预测冲销方案方案编码
        /// </summary>
        public string FATOSchemeId__FNumber { get; set; }
        /// <summary>
        /// 累计提前期
        /// </summary>
        public int? FAccuLeadTime { get; set; }
        /// <summary>
        /// 产品系列
        /// </summary>
        public long? FProductLine { get; set; }
        /// <summary>
        /// 产品系列名称
        /// </summary>
        public string FProductLine__FName { get; set; }
        /// <summary>
        /// 产品系列编码
        /// </summary>
        public string FProductLine__FNumber { get; set; }
        /// <summary>
        /// 冲销数量
        /// </summary>
        public decimal? FWriteOffQty { get; set; }
        /// <summary>
        /// 计划标识
        /// </summary>
        public string? FPlanIdent { get; set; }
        /// <summary>
        /// 计划标识编码
        /// </summary>
        public string FPlanIdent__FNumber { get; set; }
        /// <summary>
        /// 计划标识名称
        /// </summary>
        public string FPlanIdent__FDataValue { get; set; }
        /// <summary>
        /// 订单进度分组
        /// </summary>
        public string? FProScheTrackId { get; set; }
        /// <summary>
        /// 订单进度分组编码
        /// </summary>
        public string FProScheTrackId__FNumber { get; set; }
        /// <summary>
        /// 订单进度分组名称
        /// </summary>
        public string FProScheTrackId__FDataValue { get; set; }
        /// <summary>
        /// 日产量
        /// </summary>
        public decimal? FDailyOutQty { get; set; }
        /// <summary>
        /// MRP计算是否按单合并
        /// </summary>
        public string? FIsMrpComBill { get; set; }
        /// <summary>
        /// 使用组织
        /// </summary>
        public long? FUseOrgId7 { get; set; }
        /// <summary>
        /// 使用组织名称
        /// </summary>
        public string FUseOrgId7__FName { get; set; }
        /// <summary>
        /// 使用组织编码
        /// </summary>
        public string FUseOrgId7__FNumber { get; set; }
        #endregion
        #region 生产
        /// <summary>
        /// 发料仓库
        /// </summary>
        public int? FPickStockId { get; set; }
        /// <summary>
        /// 发料仓库名称
        /// </summary>
        public string FPickStockId__FName { get; set; }
        /// <summary>
        /// 发料仓库编码
        /// </summary>
        public string FPickStockId__FNumber { get; set; }
        /// <summary>
        /// 子项单位
        /// </summary>
        public int? FBOMUnitId { get; set; }
        /// <summary>
        /// 子项单位名称
        /// </summary>
        public string FBOMUnitId__FName { get; set; }
        /// <summary>
        /// 子项单位编码
        /// </summary>
        public string FBOMUnitId__FNumber { get; set; }
        /// <summary>
        /// 生产车间
        /// </summary>
        public int? FWorkShopId { get; set; }
        /// <summary>
        /// 生产车间名称
        /// </summary>
        public string FWorkShopId__FName { get; set; }
        /// <summary>
        /// 生产车间编码
        /// </summary>
        public string FWorkShopId__FNumber { get; set; }
        /// <summary>
        /// 发料方式
        /// </summary>
        public string FIssueType { get; set; }
        /// <summary>
        /// 生产单位
        /// </summary>
        public int? FProduceUnitId { get; set; }
        /// <summary>
        /// 生产单位名称
        /// </summary>
        public string FProduceUnitId__FName { get; set; }
        /// <summary>
        /// 生产单位编码
        /// </summary>
        public string FProduceUnitId__FNumber { get; set; }
        /// <summary>
        /// 是否关键件
        /// </summary>
        public string? FIsKitting { get; set; }
        /// <summary>
        /// 默认工艺路线
        /// </summary>
        public long? FDefaultRouting { get; set; }
        /// <summary>
        /// 默认工艺路线工艺路线名称
        /// </summary>
        public string FDefaultRouting__FName { get; set; }
        /// <summary>
        /// 默认工艺路线工艺路线编码
        /// </summary>
        public string FDefaultRouting__FNumber { get; set; }
        /// <summary>
        /// 可为联副产品
        /// </summary>
        public string? FIsCoby { get; set; }
        /// <summary>
        /// 标准工时
        /// </summary>
        public decimal? FPerUnitStandHour { get; set; }
        /// <summary>
        /// 倒冲时机
        /// </summary>
        public string? FBKFLTime { get; set; }
        /// <summary>
        /// 入库超收比例(%)
        /// </summary>
        public decimal? FFinishReceiptOverRate { get; set; }
        /// <summary>
        /// 入库欠收比例(%)
        /// </summary>
        public decimal? FFinishReceiptShortRate { get; set; }
        /// <summary>
        /// 发料仓位
        /// </summary>
        public string? FPickBinId { get; set; }
        /// <summary>
        /// 生产单位换算率分子
        /// </summary>
        public decimal? FPrdURNum { get; set; }
        /// <summary>
        /// 生产单位换算率分母
        /// </summary>
        public decimal? FPrdURNom { get; set; }
        /// <summary>
        /// BOM单位换算率分子
        /// </summary>
        public decimal? FBOMURNum { get; set; }
        /// <summary>
        /// BOM单位换算率分母
        /// </summary>
        public decimal? FBOMURNom { get; set; }
        /// <summary>
        /// 可为主产品
        /// </summary>
        public string? FIsMainPrd { get; set; }
        /// <summary>
        /// 是否齐套件
        /// </summary>
        public string? FIsCompleteSet { get; set; }
        /// <summary>
        /// 超发控制方式
        /// </summary>
        public string FOverControlMode { get; set; }
        /// <summary>
        /// 最小发料批量
        /// </summary>
        public decimal? FMinIssueQty { get; set; }
        /// <summary>
        /// 标准人员准备工时
        /// </summary>
        public decimal? FStdLaborPrePareTime { get; set; }
        /// <summary>
        /// 标准人员实作工时
        /// </summary>
        public decimal? FStdLaborProcessTime { get; set; }
        /// <summary>
        /// 标准机器准备工时
        /// </summary>
        public decimal? FStdMachinePrepareTime { get; set; }
        /// <summary>
        /// 标准机器实作工时
        /// </summary>
        public decimal? FStdMachineProcessTime { get; set; }
        /// <summary>
        /// 消耗波动(%)
        /// </summary>
        public decimal? FConsumVolatility { get; set; }
        /// <summary>
        /// 生产线生产
        /// </summary>
        public string? FIsProductLine { get; set; }
        /// <summary>
        /// 生产类型
        /// </summary>
        public long? FProduceBillType { get; set; }
        /// <summary>
        /// 生产类型名称
        /// </summary>
        public string FProduceBillType__FName { get; set; }
        /// <summary>
        /// 生产类型编码
        /// </summary>
        public string FProduceBillType__FNumber { get; set; }
        /// <summary>
        /// 组织间受托类型
        /// </summary>
        public long? FOrgTrustBillType { get; set; }
        /// <summary>
        /// 组织间受托类型名称
        /// </summary>
        public string FOrgTrustBillType__FName { get; set; }
        /// <summary>
        /// 组织间受托类型编码
        /// </summary>
        public string FOrgTrustBillType__FNumber { get; set; }
        /// <summary>
        /// 领料考虑最小发料批量
        /// </summary>
        public string? FISMinIssueQty { get; set; }
        /// <summary>
        /// 启用ECN
        /// </summary>
        public string? FIsECN { get; set; }
        /// <summary>
        /// 最小发料批量单位
        /// </summary>
        public long FMinIssueUnitId { get; set; }
        /// <summary>
        /// 最小发料批量单位名称
        /// </summary>
        public string FMinIssueUnitId__FName { get; set; }
        /// <summary>
        /// 最小发料批量单位编码
        /// </summary>
        public string FMinIssueUnitId__FNumber { get; set; }
        /// <summary>
        /// 产品模型
        /// </summary>
        public long? FMdlId { get; set; }
        /// <summary>
        /// 产品模型模型名称
        /// </summary>
        public string FMdlId__FName { get; set; }
        /// <summary>
        /// 产品模型模型编码
        /// </summary>
        public string FMdlId__FNumber { get; set; }
        /// <summary>
        /// 模型物料
        /// </summary>
        public long? FMdlMaterialId { get; set; }
        /// <summary>
        /// 模型物料名称
        /// </summary>
        public string FMdlMaterialId__FName { get; set; }
        /// <summary>
        /// 模型物料编码
        /// </summary>
        public string FMdlMaterialId__FNumber { get; set; }
        /// <summary>
        /// 变动损耗率(%)
        /// </summary>
        public decimal? FLOSSPERCENT { get; set; }
        /// <summary>
        /// 序列号携带到父项
        /// </summary>
        public string? FIsSNCarryToParent { get; set; }
        /// <summary>
        /// 工时单位
        /// </summary>
        public string FStandHourUnitId { get; set; }
        /// <summary>
        /// 倒冲数量
        /// </summary>
        public string FBackFlushType { get; set; }
        /// <summary>
        /// 固定损耗
        /// </summary>
        public decimal? FFIXLOSS { get; set; }
        /// <summary>
        /// 使用组织
        /// </summary>
        public long? FUseOrgId6 { get; set; }
        /// <summary>
        /// 使用组织名称
        /// </summary>
        public string FUseOrgId6__FName { get; set; }
        /// <summary>
        /// 使用组织编码
        /// </summary>
        public string FUseOrgId6__FNumber { get; set; }
        /// <summary>
        /// 启用日排产
        /// </summary>
        public string? FIsEnableSchedule { get; set; }
        /// <summary>
        /// 默认产线
        /// </summary>
        public long? FDefaultLineId { get; set; }
        /// <summary>
        /// 默认产线产线名称
        /// </summary>
        public string FDefaultLineId__FName { get; set; }
        /// <summary>
        /// 默认产线产线编码
        /// </summary>
        public string FDefaultLineId__FNumber { get; set; }
        #endregion
        #region 辅助属性
        /// <summary>
        /// 启用
        /// </summary>
        public string? FIsEnable1 { get; set; }
        /// <summary>
        /// 必录
        /// </summary>
        public string? FIsMustInput { get; set; }
        /// <summary>
        /// 影响价格
        /// </summary>
        public string? FIsAffectPrice1 { get; set; }
        /// <summary>
        /// 影响计划
        /// </summary>
        public string? FIsAffectPlan1 { get; set; }
        /// <summary>
        /// 影响出库成本
        /// </summary>
        public string? FIsAffectCost1 { get; set; }
        /// <summary>
        /// 组合控制
        /// </summary>
        public string? FIsComControl { get; set; }
        /// <summary>
        /// 辅助属性
        /// </summary>
        public long? FAuxPropertyId { get; set; }
        /// <summary>
        /// 辅助属性名称
        /// </summary>
        public string FAuxPropertyId__FName { get; set; }
        /// <summary>
        /// 取值方式
        /// </summary>
        public string? FValueType { get; set; }
        /// <summary>
        /// 值设置状态
        /// </summary>
        public string? FValueSet { get; set; }
        /// <summary>
        /// 使用组织
        /// </summary>
        public long? FUseOrgId11 { get; set; }
        /// <summary>
        /// 使用组织名称
        /// </summary>
        public string FUseOrgId11__FName { get; set; }
        /// <summary>
        /// 使用组织编码
        /// </summary>
        public string FUseOrgId11__FNumber { get; set; }
        #endregion
        #region 库存属性
        /// <summary>
        /// 启用
        /// </summary>
        public string? FIsEnable { get; set; }
        /// <summary>
        /// 影响价格
        /// </summary>
        public string? FIsAffectPrice { get; set; }
        /// <summary>
        /// 影响计划
        /// </summary>
        public string? FIsAffectPlan { get; set; }
        /// <summary>
        /// 影响出库成本
        /// </summary>
        public string? FIsAffectCost { get; set; }
        /// <summary>
        /// 库存属性
        /// </summary>
        public long FInvPtyId { get; set; }
        /// <summary>
        /// 库存属性编码
        /// </summary>
        public string FInvPtyId__FNumber { get; set; }
        /// <summary>
        /// 库存属性名称
        /// </summary>
        public string FInvPtyId__FName { get; set; }
        /// <summary>
        /// 使用组织
        /// </summary>
        public long? FUseOrgId10 { get; set; }
        /// <summary>
        /// 使用组织名称
        /// </summary>
        public string FUseOrgId10__FName { get; set; }
        /// <summary>
        /// 使用组织编码
        /// </summary>
        public string FUseOrgId10__FNumber { get; set; }
        #endregion
        #region 委外
        /// <summary>
        /// 委外单位
        /// </summary>
        public long? FSubconUnitId { get; set; }
        /// <summary>
        /// 委外单位名称
        /// </summary>
        public string FSubconUnitId__FName { get; set; }
        /// <summary>
        /// 委外单位编码
        /// </summary>
        public string FSubconUnitId__FNumber { get; set; }
        /// <summary>
        /// 委外计价单位
        /// </summary>
        public long? FSubconPriceUnitId { get; set; }
        /// <summary>
        /// 委外计价单位名称
        /// </summary>
        public string FSubconPriceUnitId__FName { get; set; }
        /// <summary>
        /// 委外计价单位编码
        /// </summary>
        public string FSubconPriceUnitId__FNumber { get; set; }
        /// <summary>
        /// 委外单位换算率分子
        /// </summary>
        public decimal? FSUBCONURNUM { get; set; }
        /// <summary>
        /// 委外单位换算率分母
        /// </summary>
        public decimal? FSUBCONURNOM { get; set; }
        /// <summary>
        /// 委外计价单位换算率分子
        /// </summary>
        public decimal? FSUBCONPRICEURNUM { get; set; }
        /// <summary>
        /// 委外计价单位换算率分母
        /// </summary>
        public decimal? FSUBCONPRICEURNOM { get; set; }
        /// <summary>
        /// 委外类型
        /// </summary>
        public long? FSubBillType { get; set; }
        /// <summary>
        /// 委外类型名称
        /// </summary>
        public string FSubBillType__FName { get; set; }
        /// <summary>
        /// 委外类型编码
        /// </summary>
        public string FSubBillType__FNumber { get; set; }
        /// <summary>
        /// 使用组织
        /// </summary>
        public long? FUseOrgId8 { get; set; }
        /// <summary>
        /// 使用组织名称
        /// </summary>
        public string FUseOrgId8__FName { get; set; }
        /// <summary>
        /// 使用组织编码
        /// </summary>
        public string FUseOrgId8__FNumber { get; set; }
        #endregion
        #region 质量
        /// <summary>
        /// 产品检验
        /// </summary>
        public string? FCheckProduct { get; set; }
        /// <summary>
        /// 来料检验
        /// </summary>
        public string? FCheckIncoming { get; set; }
        /// <summary>
        /// 抽样方案
        /// </summary>
        public long? FIncSampSchemeId { get; set; }
        /// <summary>
        /// 抽样方案名称
        /// </summary>
        public string FIncSampSchemeId__FName { get; set; }
        /// <summary>
        /// 抽样方案编码
        /// </summary>
        public string FIncSampSchemeId__FNumber { get; set; }
        /// <summary>
        /// 质检方案
        /// </summary>
        public long? FIncQcSchemeId { get; set; }
        /// <summary>
        /// 质检方案名称
        /// </summary>
        public string FIncQcSchemeId__FName { get; set; }
        /// <summary>
        /// 质检方案编码
        /// </summary>
        public string FIncQcSchemeId__FNumber { get; set; }
        /// <summary>
        /// 库存检验
        /// </summary>
        public string? FCheckStock { get; set; }
        /// <summary>
        /// 启用库存周期复检
        /// </summary>
        public string? FEnableCyclistQCSTK { get; set; }
        /// <summary>
        /// 启用库存周期复检提醒
        /// </summary>
        public string? FEnableCyclistQCSTKEW { get; set; }
        /// <summary>
        /// 提醒提前期
        /// </summary>
        public int? FEWLeadDay { get; set; }
        /// <summary>
        /// 复检周期
        /// </summary>
        public int? FStockCycle { get; set; }
        /// <summary>
        /// 发货检验
        /// </summary>
        public string? FCheckDelivery { get; set; }
        /// <summary>
        /// 退货检验
        /// </summary>
        public string? FCheckReturn { get; set; }
        /// <summary>
        /// 质检组
        /// </summary>
        public long? FInspectGroupId { get; set; }
        /// <summary>
        /// 质检组业务组名称
        /// </summary>
        public string FInspectGroupId__FName { get; set; }
        /// <summary>
        /// 质检组业务组编码
        /// </summary>
        public string FInspectGroupId__FNumber { get; set; }
        /// <summary>
        /// 质检员
        /// </summary>
        public long? FInspectorId { get; set; }
        /// <summary>
        /// 质检员名称
        /// </summary>
        public string FInspectorId__FName { get; set; }
        /// <summary>
        /// 质检员职员编码
        /// </summary>
        public string FInspectorId__FNumber { get; set; }
        /// <summary>
        /// 受托材料检验
        /// </summary>
        public string? FCheckEntrusted { get; set; }
        /// <summary>
        /// 其他检验
        /// </summary>
        public string? FCheckOther { get; set; }
        /// <summary>
        /// 产品首检
        /// </summary>
        public string? FIsFirstInspect { get; set; }
        /// <summary>
        /// 使用组织
        /// </summary>
        public long? FUseOrgId5 { get; set; }
        /// <summary>
        /// 使用组织名称
        /// </summary>
        public string FUseOrgId5__FName { get; set; }
        /// <summary>
        /// 使用组织编码
        /// </summary>
        public string FUseOrgId5__FNumber { get; set; }
        /// <summary>
        /// 生产退料检验
        /// </summary>
        public string? FCheckReturnMtrl { get; set; }
        /// <summary>
        /// 委外退料检验
        /// </summary>
        public string? FCheckSubRtnMtrl { get; set; }
        /// <summary>
        /// 首检控制方式
        /// </summary>
        public string FFirstQCControlType { get; set; }
        #endregion
    }
}