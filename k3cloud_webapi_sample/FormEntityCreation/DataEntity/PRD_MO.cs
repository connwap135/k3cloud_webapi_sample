using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormEntityCreation.DataEntity
{
	/// <summary>
	/// 生产订单
	/// </summary>
	public class PRD_MO
	{
		/// <summary>
		/// FID
		/// </summary>
		public System.Int32 FID { get; set; }
		#region 单据头
		/// <summary>
		/// 单据编号
		/// </summary>
		public string? FBillNo { get; set; }
		/// <summary>
		/// 单据状态
		/// </summary>
		public string? FDocumentStatus { get; set; }
		/// <summary>
		/// 审核人
		/// </summary>
		public long? FApproverId { get; set; }
		/// <summary>
		/// 审核人用户名称
		/// </summary>
		public string FApproverId__FName { get; set; }
		/// <summary>
		/// 审核日期
		/// </summary>
		public DateTime? FApproveDate { get; set; }
		/// <summary>
		/// 修改人
		/// </summary>
		public long? FModifierId { get; set; }
		/// <summary>
		/// 修改人用户名称
		/// </summary>
		public string FModifierId__FName { get; set; }
		/// <summary>
		/// 创建日期
		/// </summary>
		public DateTime? FCreateDate { get; set; }
		/// <summary>
		/// 创建人
		/// </summary>
		public long? FCreatorId { get; set; }
		/// <summary>
		/// 创建人用户名称
		/// </summary>
		public string FCreatorId__FName { get; set; }
		/// <summary>
		/// 修改日期
		/// </summary>
		public DateTime? FModifyDate { get; set; }
		/// <summary>
		/// 作废日期
		/// </summary>
		public DateTime? FCancelDate { get; set; }
		/// <summary>
		/// 作废人
		/// </summary>
		public long? FCanceler { get; set; }
		/// <summary>
		/// 作废人用户名称
		/// </summary>
		public string FCanceler__FName { get; set; }
		/// <summary>
		/// 作废状态
		/// </summary>
		public string? FCancelStatus { get; set; }
		/// <summary>
		/// 备注
		/// </summary>
		public string? FDescription { get; set; }
		/// <summary>
		/// 单据类型
		/// </summary>
		public string FBillType { get; set; }
		/// <summary>
		/// 单据类型名称
		/// </summary>
		public string FBillType__FName { get; set; }
		/// <summary>
		/// 单据类型编码
		/// </summary>
		public string FBillType__FNumber { get; set; }
		/// <summary>
		/// 受托
		/// </summary>
		public string? FTrustteed { get; set; }
		/// <summary>
		/// 生产车间
		/// </summary>
		public long? FWorkShopID0 { get; set; }
		/// <summary>
		/// 生产车间名称
		/// </summary>
		public string FWorkShopID0__FName { get; set; }
		/// <summary>
		/// 生产车间编码
		/// </summary>
		public string FWorkShopID0__FNumber { get; set; }
		/// <summary>
		/// 生产组织
		/// </summary>
		public long FPrdOrgId { get; set; }
		/// <summary>
		/// 生产组织名称
		/// </summary>
		public string FPrdOrgId__FName { get; set; }
		/// <summary>
		/// 生产组织编码
		/// </summary>
		public string FPrdOrgId__FNumber { get; set; }
		/// <summary>
		/// 计划员
		/// </summary>
		public long? FPlannerID { get; set; }
		/// <summary>
		/// 计划员名称
		/// </summary>
		public string FPlannerID__FName { get; set; }
		/// <summary>
		/// 计划员职员编码
		/// </summary>
		public string FPlannerID__FNumber { get; set; }
		/// <summary>
		/// 单据日期
		/// </summary>
		public DateTime FDate { get; set; }
		/// <summary>
		/// 货主类型
		/// </summary>
		public string FOwnerTypeId { get; set; }
		/// <summary>
		/// 货主
		/// </summary>
		public int? FOwnerId { get; set; }
		/// <summary>
		/// 货主名称
		/// </summary>
		public string FOwnerId__FName { get; set; }
		/// <summary>
		/// 货主编码
		/// </summary>
		public string FOwnerId__FNumber { get; set; }
		/// <summary>
		/// 计划组
		/// </summary>
		public long? FWorkGroupId { get; set; }
		/// <summary>
		/// 计划组业务组名称
		/// </summary>
		public string FWorkGroupId__FName { get; set; }
		/// <summary>
		/// 计划组业务组编码
		/// </summary>
		public string FWorkGroupId__FNumber { get; set; }
		/// <summary>
		/// 销售业务类型
		/// </summary>
		public string? FBusinessType { get; set; }
		/// <summary>
		/// 是否返工
		/// </summary>
		public string? FIsRework { get; set; }
		/// <summary>
		/// 组织受托加工
		/// </summary>
		public string? FIsEntrust { get; set; }
		/// <summary>
		/// 委托组织
		/// </summary>
		public long? FEnTrustOrgId { get; set; }
		/// <summary>
		/// 委托组织名称
		/// </summary>
		public string FEnTrustOrgId__FName { get; set; }
		/// <summary>
		/// 委托组织编码
		/// </summary>
		public string FEnTrustOrgId__FNumber { get; set; }
		/// <summary>
		/// 用料清单展开
		/// </summary>
		public string FPPBOMType { get; set; }
		/// <summary>
		/// 生产发料
		/// </summary>
		public string? FIssueMtrl { get; set; }
		/// <summary>
		/// 期初生产订单
		/// </summary>
		public string? FIsQCMO { get; set; }
		#endregion
		#region 明细
		/// <summary>
		/// 明细主键FEntryId
		/// </summary>
		public System.Int64 FTreeEntity_FEntryId { get; set; }
		/// <summary>
		/// 明细序列
		/// </summary>
		public int FTreeEntity_FSeq { get; set; }
		/// <summary>
		/// 父级行主键
		/// </summary>
		public string? FParentRowId { get; set; }
		/// <summary>
		/// 行展开类型
		/// </summary>
		public int? FRowExpandType { get; set; }
		/// <summary>
		/// 行标识
		/// </summary>
		public string? FRowId { get; set; }
		/// <summary>
		/// 物料编码
		/// </summary>
		public long FMaterialId { get; set; }
		/// <summary>
		/// 物料编码名称
		/// </summary>
		public string FMaterialId__FName { get; set; }
		/// <summary>
		/// 物料编码编码
		/// </summary>
		public string FMaterialId__FNumber { get; set; }
		/// <summary>
		/// 产品类型
		/// </summary>
		public string FProductType { get; set; }
		/// <summary>
		/// 数量
		/// </summary>
		public decimal? FQty { get; set; }
		/// <summary>
		/// 计划完工时间
		/// </summary>
		public DateTime FPlanFinishDate { get; set; }
		/// <summary>
		/// 计划开工时间
		/// </summary>
		public DateTime FPlanStartDate { get; set; }
		/// <summary>
		/// 批号
		/// </summary>
		public long? FLot { get; set; }
		/// <summary>
		/// 批号名称
		/// </summary>
		public string FLot__FName { get; set; }
		/// <summary>
		/// 批号批号
		/// </summary>
		public string FLot__FNumber { get; set; }
		/// <summary>
		/// 计划跟踪号
		/// </summary>
		public string? FMTONO { get; set; }
		/// <summary>
		/// 项目编号
		/// </summary>
		public string? FProjectNo { get; set; }
		/// <summary>
		/// 基本单位数量
		/// </summary>
		public decimal? FBaseUnitQty { get; set; }
		/// <summary>
		/// 组别
		/// </summary>
		public int? FGroup { get; set; }
		/// <summary>
		/// BOM版本
		/// </summary>
		public long? FBomId { get; set; }
		/// <summary>
		/// BOM版本BOM简称
		/// </summary>
		public string FBomId__FName { get; set; }
		/// <summary>
		/// BOM版本BOM版本
		/// </summary>
		public string FBomId__FNumber { get; set; }
		/// <summary>
		/// 工艺路线
		/// </summary>
		public long? FRoutingId { get; set; }
		/// <summary>
		/// 工艺路线工艺路线名称
		/// </summary>
		public string FRoutingId__FName { get; set; }
		/// <summary>
		/// 工艺路线工艺路线编码
		/// </summary>
		public string FRoutingId__FNumber { get; set; }
		/// <summary>
		/// 成品率%
		/// </summary>
		public decimal? FYieldRate { get; set; }
		/// <summary>
		/// 入库上限
		/// </summary>
		public decimal? FStockInLimitH { get; set; }
		/// <summary>
		/// 入库下限
		/// </summary>
		public decimal? FStockInLimitL { get; set; }
		/// <summary>
		/// 合格品入库选单数量
		/// </summary>
		public decimal? FStockInQuaSelAuxQty { get; set; }
		/// <summary>
		/// 基本单位合格品入库选单数量
		/// </summary>
		public decimal? FStockInQuaSelQty { get; set; }
		/// <summary>
		/// 基本单位不合格品入库选单数量
		/// </summary>
		public decimal? FStockInFailSelQty { get; set; }
		/// <summary>
		/// 不合格品入库选单数量
		/// </summary>
		public decimal? FStockInFailSelAuxQty { get; set; }
		/// <summary>
		/// 基本单位合格品入库数量
		/// </summary>
		public decimal? FStockInQuaQty { get; set; }
		/// <summary>
		/// 合格品入库数量
		/// </summary>
		public decimal? FStockInQuaAuxQty { get; set; }
		/// <summary>
		/// 基本单位不合格品入库数量
		/// </summary>
		public decimal? FStockInFailQty { get; set; }
		/// <summary>
		/// 不合格品入库数量
		/// </summary>
		public decimal? FStockInFailAuxQty { get; set; }
		/// <summary>
		/// 入库组织
		/// </summary>
		public long FStockInOrgId { get; set; }
		/// <summary>
		/// 入库组织名称
		/// </summary>
		public string FStockInOrgId__FName { get; set; }
		/// <summary>
		/// 入库组织编码
		/// </summary>
		public string FStockInOrgId__FNumber { get; set; }
		/// <summary>
		/// 仓库
		/// </summary>
		public long? FStockId { get; set; }
		/// <summary>
		/// 仓库名称
		/// </summary>
		public string FStockId__FName { get; set; }
		/// <summary>
		/// 仓库编码
		/// </summary>
		public string FStockId__FNumber { get; set; }
		/// <summary>
		/// 仓位
		/// </summary>
		public long? FStockLocId { get; set; }
		/// <summary>
		/// 产出工序
		/// </summary>
		public int? FOperId { get; set; }
		/// <summary>
		/// 产出作业编码
		/// </summary>
		public long? FProcessId { get; set; }
		/// <summary>
		/// 产出作业编码名称
		/// </summary>
		public string FProcessId__FName { get; set; }
		/// <summary>
		/// 产出作业编码编码
		/// </summary>
		public string FProcessId__FNumber { get; set; }
		/// <summary>
		/// 成本权重
		/// </summary>
		public decimal? FCostRate { get; set; }
		/// <summary>
		/// 备注
		/// </summary>
		public string? FMemoItem { get; set; }
		/// <summary>
		/// 计划确认日期
		/// </summary>
		public DateTime? FPlanConfirmDate { get; set; }
		/// <summary>
		/// 排程日期
		/// </summary>
		public DateTime? FScheduleDate { get; set; }
		/// <summary>
		/// 下达日期
		/// </summary>
		public DateTime? FConveyDate { get; set; }
		/// <summary>
		/// 开工日期
		/// </summary>
		public DateTime? FStartDate { get; set; }
		/// <summary>
		/// 完工日期
		/// </summary>
		public DateTime? FFinishDate { get; set; }
		/// <summary>
		/// 结案日期
		/// </summary>
		public DateTime? FCloseDate { get; set; }
		/// <summary>
		/// 结算日期
		/// </summary>
		public DateTime? FCostDate { get; set; }
		/// <summary>
		/// 生成方式
		/// </summary>
		public string FCreateType { get; set; }
		/// <summary>
		/// 来源单据ID
		/// </summary>
		public int? FSrcBillId { get; set; }
		/// <summary>
		/// 来源单据分录内码
		/// </summary>
		public int? FSrcBillEntryId { get; set; }
		/// <summary>
		/// 销售订单ID
		/// </summary>
		public int? FSaleOrderId { get; set; }
		/// <summary>
		/// 需求单据
		/// </summary>
		public string? FSaleOrderNo { get; set; }
		/// <summary>
		/// 销售订单分录内码
		/// </summary>
		public int? FSaleOrderEntryId { get; set; }
		/// <summary>
		/// 需求组织
		/// </summary>
		public long? FRequestOrgId { get; set; }
		/// <summary>
		/// 需求组织名称
		/// </summary>
		public string FRequestOrgId__FName { get; set; }
		/// <summary>
		/// 需求组织编码
		/// </summary>
		public string FRequestOrgId__FNumber { get; set; }
		/// <summary>
		/// 基本单位汇报选单数量
		/// </summary>
		public decimal? FRepQuaSelQty { get; set; }
		/// <summary>
		/// 基本单位不合格数量
		/// </summary>
		public decimal? FRepFailQty { get; set; }
		/// <summary>
		/// 汇报选单数量
		/// </summary>
		public decimal? FRepQuaSelAuxQty { get; set; }
		/// <summary>
		/// 合格数量
		/// </summary>
		public decimal? FRepQuaAuxQty { get; set; }
		/// <summary>
		/// 不合格数量
		/// </summary>
		public decimal? FRepFailAuxQty { get; set; }
		/// <summary>
		/// 基本单位合格数量
		/// </summary>
		public decimal? FRepQuaQty { get; set; }
		/// <summary>
		/// 业务状态
		/// </summary>
		public string? FStatus { get; set; }
		/// <summary>
		/// 单位
		/// </summary>
		public long FUnitId { get; set; }
		/// <summary>
		/// 单位名称
		/// </summary>
		public string FUnitId__FName { get; set; }
		/// <summary>
		/// 单位编码
		/// </summary>
		public string FUnitId__FNumber { get; set; }
		/// <summary>
		/// 基本单位
		/// </summary>
		public long? FBaseUnitId { get; set; }
		/// <summary>
		/// 基本单位名称
		/// </summary>
		public string FBaseUnitId__FName { get; set; }
		/// <summary>
		/// 基本单位编码
		/// </summary>
		public string FBaseUnitId__FNumber { get; set; }
		/// <summary>
		/// 辅助属性
		/// </summary>
		public long? FAuxPropId { get; set; }
		/// <summary>
		/// 基本单位入库上限
		/// </summary>
		public decimal? FBaseStockInLimitH { get; set; }
		/// <summary>
		/// 基本单位入库下限
		/// </summary>
		public decimal? FBaseStockInLimitL { get; set; }
		/// <summary>
		/// 入库上限比例
		/// </summary>
		public decimal? FStockInUlRatio { get; set; }
		/// <summary>
		/// 源单类型
		/// </summary>
		public string? FSrcBillType { get; set; }
		/// <summary>
		/// 源单编号
		/// </summary>
		public string? FSrcBillNo { get; set; }
		/// <summary>
		/// 源单分录行号
		/// </summary>
		public int? FSrcBillEntrySeq { get; set; }
		/// <summary>
		/// 需求单据行号
		/// </summary>
		public int? FSaleOrderEntrySeq { get; set; }
		/// <summary>
		/// 基本单位成品数量
		/// </summary>
		public decimal? FBaseYieldQty { get; set; }
		/// <summary>
		/// 联副产品分录内码
		/// </summary>
		public int? FCopyEntryId { get; set; }
		/// <summary>
		/// 挂起状态
		/// </summary>
		public string? FIsSuspend { get; set; }
		/// <summary>
		/// 入库下限比例
		/// </summary>
		public decimal? FStockInLlRatio { get; set; }
		/// <summary>
		/// 业务流程
		/// </summary>
		public string? FBFLowId { get; set; }
		/// <summary>
		/// 业务流程名称
		/// </summary>
		public string FBFLowId__FName { get; set; }
		/// <summary>
		/// 基本单位汇报不合格选单数量
		/// </summary>
		public decimal? FRepFailSelQty { get; set; }
		/// <summary>
		/// 汇报不合格选单数量
		/// </summary>
		public decimal? FRepFailSelAuxQty { get; set; }
		/// <summary>
		/// 生产车间
		/// </summary>
		public long? FWorkShopID { get; set; }
		/// <summary>
		/// 生产车间名称
		/// </summary>
		public string FWorkShopID__FName { get; set; }
		/// <summary>
		/// 生产车间编码
		/// </summary>
		public string FWorkShopID__FNumber { get; set; }
		/// <summary>
		/// 需求类型
		/// </summary>
		public string FReqType { get; set; }
		/// <summary>
		/// 需求优先级
		/// </summary>
		public int? FPriority { get; set; }
		/// <summary>
		/// 备料套数
		/// </summary>
		public decimal? FSTOCKREADY { get; set; }
		/// <summary>
		/// 基本单位备料数量
		/// </summary>
		public decimal? FBaseStockReady { get; set; }
		/// <summary>
		/// 基本单位报废数量
		/// </summary>
		public decimal? FBaseScrapQty { get; set; }
		/// <summary>
		/// 报废数量
		/// </summary>
		public decimal? FScrapQty { get; set; }
		/// <summary>
		/// 基本单位返修数量
		/// </summary>
		public decimal? FBaseRepairQty { get; set; }
		/// <summary>
		/// 返修数量
		/// </summary>
		public decimal? FRepairQty { get; set; }
		/// <summary>
		/// 基本单位报废品入库选单数量
		/// </summary>
		public decimal? FBaseStockInScrapSelQty { get; set; }
		/// <summary>
		/// 报废品入库选单数量
		/// </summary>
		public decimal? FStockInScrapSelQty { get; set; }
		/// <summary>
		/// 基本单位报废品入库数量
		/// </summary>
		public decimal? FBaseStockInScrapQty { get; set; }
		/// <summary>
		/// 报废品入库数量
		/// </summary>
		public decimal? FStockInScrapQty { get; set; }
		/// <summary>
		/// 入库货主类型
		/// </summary>
		public string? FInStockOwnerTypeId { get; set; }
		/// <summary>
		/// 入库货主
		/// </summary>
		public int? FInStockOwnerId { get; set; }
		/// <summary>
		/// 入库货主名称
		/// </summary>
		public string FInStockOwnerId__FName { get; set; }
		/// <summary>
		/// 入库货主编码
		/// </summary>
		public string FInStockOwnerId__FNumber { get; set; }
		/// <summary>
		/// 入库类型-推入库单用
		/// </summary>
		public string? FInStockType { get; set; }
		/// <summary>
		/// 产品检验
		/// </summary>
		public string? FCheckProduct { get; set; }
		/// <summary>
		/// 产出序列
		/// </summary>
		public string? FOutPutOptQueue { get; set; }
		/// <summary>
		/// 基本单位汇报完成数量
		/// </summary>
		public decimal? FBaseRptFinishQty { get; set; }
		/// <summary>
		/// 汇报完成数量
		/// </summary>
		public decimal? FRptFinishQty { get; set; }
		/// <summary>
		/// 紧急放行
		/// </summary>
		public string? FQAIP { get; set; }
		/// <summary>
		/// 成品数量
		/// </summary>
		public decimal? FYieldQty { get; set; }
		/// <summary>
		/// 倒冲领料
		/// </summary>
		public string? FISBACKFLUSH { get; set; }
		/// <summary>
		/// 未入库数量
		/// </summary>
		public decimal? FNoStockInQty { get; set; }
		/// <summary>
		/// 基本单位未入库数量
		/// </summary>
		public decimal? FBaseNoStockInQty { get; set; }
		/// <summary>
		/// 需求来源
		/// </summary>
		public string? FReqSrc { get; set; }
		/// <summary>
		/// 结案人
		/// </summary>
		public long? FForceCloserId { get; set; }
		/// <summary>
		/// 结案人用户名称
		/// </summary>
		public string FForceCloserId__FName { get; set; }
		/// <summary>
		/// 产线
		/// </summary>
		public long? FREMWorkShopId { get; set; }
		/// <summary>
		/// 产线名称
		/// </summary>
		public string FREMWorkShopId__FName { get; set; }
		/// <summary>
		/// 产线编码
		/// </summary>
		public string FREMWorkShopId__FNumber { get; set; }
		/// <summary>
		/// 排产序号
		/// </summary>
		public decimal? FScheduleSeq { get; set; }
		/// <summary>
		/// 排程开工时间
		/// </summary>
		public DateTime? FScheduleStartTime { get; set; }
		/// <summary>
		/// 排程完工时间
		/// </summary>
		public DateTime? FScheduleFinishTime { get; set; }
		/// <summary>
		/// 结案类型
		/// </summary>
		public string? FCloseType { get; set; }
		/// <summary>
		/// 排程工序拆分数
		/// </summary>
		public int? FScheduleProcSplit { get; set; }
		/// <summary>
		/// 序列号单位
		/// </summary>
		public long? FSNUnitID { get; set; }
		/// <summary>
		/// 序列号单位名称
		/// </summary>
		public string FSNUnitID__FName { get; set; }
		/// <summary>
		/// 序列号单位编码
		/// </summary>
		public string FSNUnitID__FNumber { get; set; }
		/// <summary>
		/// 序列号单位数量
		/// </summary>
		public decimal? FSNQty { get; set; }
		/// <summary>
		/// 退库数量
		/// </summary>
		public decimal? FReStkQty { get; set; }
		/// <summary>
		/// 基本单位退库数量
		/// </summary>
		public decimal? FBaseReStkQty { get; set; }
		/// <summary>
		/// 合格品退库数量
		/// </summary>
		public decimal? FReStkQuaQty { get; set; }
		/// <summary>
		/// 基本合格品退库数量
		/// </summary>
		public decimal? FBaseReStkQuaQty { get; set; }
		/// <summary>
		/// 不合格品退库数量
		/// </summary>
		public decimal? FReStkFailQty { get; set; }
		/// <summary>
		/// 基本不合格品退库数量
		/// </summary>
		public decimal? FBaseReStkFailQty { get; set; }
		/// <summary>
		/// 报废品退库数量
		/// </summary>
		public decimal? FReStkScrapQty { get; set; }
		/// <summary>
		/// 基本报废品退库数量
		/// </summary>
		public decimal? FBaseReStkScrapQty { get; set; }
		/// <summary>
		/// 领补套数
		/// </summary>
		public decimal? FPickMtlQty { get; set; }
		/// <summary>
		/// 基本单位领补套数
		/// </summary>
		public decimal? FBasePickMtlQty { get; set; }
		/// <summary>
		/// 是否手工新增联副产品
		/// </summary>
		public int? FISNEWLC { get; set; }
		/// <summary>
		/// 领料状态
		/// </summary>
		public string? FPickMtrlStatus { get; set; }
		/// <summary>
		/// 基本单位发料通知套数
		/// </summary>
		public decimal? FBaseIssueQty { get; set; }
		/// <summary>
		/// 发料通知套数
		/// </summary>
		public decimal? FIssueQty { get; set; }
		/// <summary>
		/// 源拆分订单编号
		/// </summary>
		public string? FSrcSplitBillNo { get; set; }
		/// <summary>
		/// 源拆分订单行号
		/// </summary>
		public int? FSrcSplitSeq { get; set; }
		/// <summary>
		/// 源拆分订单分录内码
		/// </summary>
		public int? FSrcSplitEntryId { get; set; }
		/// <summary>
		/// 源拆分生产订单内码
		/// </summary>
		public int? FSrcSplitId { get; set; }
		/// <summary>
		/// 变更标志
		/// </summary>
		public string? FMOChangeFlag { get; set; }
		/// <summary>
		/// 上级订单BOM分录内码
		/// </summary>
		public int? FSRCBOMENTRYID { get; set; }
		/// <summary>
		/// 返工品退库数量
		/// </summary>
		public decimal? FReStkReMadeQty { get; set; }
		/// <summary>
		/// 基本单位返工品退库数量
		/// </summary>
		public decimal? FBaseReStkReMadeQty { get; set; }
		/// <summary>
		/// 基本单位返工数量
		/// </summary>
		public decimal? FBaseReMadeQty { get; set; }
		/// <summary>
		/// 返工数量
		/// </summary>
		public decimal? FReMadeQty { get; set; }
		/// <summary>
		/// 基本单位返工品入库数量
		/// </summary>
		public decimal? FBaseStockInReMadeQty { get; set; }
		/// <summary>
		/// 返工品入库数量
		/// </summary>
		public decimal? FStockInReMadeQty { get; set; }
		/// <summary>
		/// 基本单位返工品入库选单数量
		/// </summary>
		public decimal? FBaseStockInReMadeSelQty { get; set; }
		/// <summary>
		/// 返工品入库选单数量
		/// </summary>
		public decimal? FStockInReMadeSelQty { get; set; }
		/// <summary>
		/// 强制结案原因
		/// </summary>
		public string? FCloseReason { get; set; }
		/// <summary>
		/// 基本单位已排产数量
		/// </summary>
		public decimal? FBaseScheduledQtySum { get; set; }
		/// <summary>
		/// 已排产数量
		/// </summary>
		public decimal? FScheduledQtySum { get; set; }
		/// <summary>
		/// 排产状态
		/// </summary>
		public string FScheduleStatus { get; set; }
		/// <summary>
		/// 首检
		/// </summary>
		public string? FIsFirstInspect { get; set; }
		/// <summary>
		/// 首检状态
		/// </summary>
		public string FFirstInspectStatus { get; set; }
		/// <summary>
		/// 计划确认人
		/// </summary>
		public long? FConfirmId { get; set; }
		/// <summary>
		/// 计划确认人用户名称
		/// </summary>
		public string FConfirmId__FName { get; set; }
		/// <summary>
		/// 下达人
		/// </summary>
		public long? FReleaseId { get; set; }
		/// <summary>
		/// 下达人用户名称
		/// </summary>
		public string FReleaseId__FName { get; set; }
		/// <summary>
		/// 开工人
		/// </summary>
		public long? FStartID { get; set; }
		/// <summary>
		/// 开工人用户名称
		/// </summary>
		public string FStartID__FName { get; set; }
		/// <summary>
		/// 完工人
		/// </summary>
		public long? FFinishId { get; set; }
		/// <summary>
		/// 完工人用户名称
		/// </summary>
		public string FFinishId__FName { get; set; }
		/// <summary>
		/// 已预留
		/// </summary>
		public string? FIsMRP { get; set; }
		/// <summary>
		/// 基本单位样本破坏数
		/// </summary>
		public decimal? FBaseSampleDamageQty { get; set; }
		/// <summary>
		/// 样本破坏数
		/// </summary>
		public decimal? FSampleDamageQty { get; set; }
		/// <summary>
		/// 启用日排产
		/// </summary>
		public string? FISENABLESCHEDULE { get; set; }
		/// <summary>
		/// BOM展开路径
		/// </summary>
		public string? FPathEntryId { get; set; }
		/// <summary>
		/// 用料清单分录内码
		/// </summary>
		public int? FPPBOMENTRYID { get; set; }
		/// <summary>
		/// BOM分录内码
		/// </summary>
		public int? FBOMENTRYID { get; set; }
		/// <summary>
		/// 用料清单类型
		/// </summary>
		public string? FSrcFormID { get; set; }
		/// <summary>
		/// 预计齐套数量
		/// </summary>
		public decimal? FMatchQty { get; set; }
		/// <summary>
		/// 库存齐套数
		/// </summary>
		public decimal? FInvMatchQty { get; set; }
		/// <summary>
		/// 齐套状况
		/// </summary>
		public string? FCompleteCon { get; set; }
		/// <summary>
		/// 跟进备注
		/// </summary>
		public string? FRemarks { get; set; }
		/// <summary>
		/// 预计齐套日期
		/// </summary>
		public DateTime? FMatchDate { get; set; }
		/// <summary>
		/// 联副产品备注
		/// </summary>
		public string? FNOTECOBY { get; set; }
		/// <summary>
		/// 直送合格品入库数量
		/// </summary>
		public decimal? FDirectStockInQuaQty { get; set; }
		/// <summary>
		/// 直送领料选单数
		/// </summary>
		public decimal? FDirectPickMtrlSelQty { get; set; }
		/// <summary>
		/// 直送基本合格品入库数量
		/// </summary>
		public decimal? FBaseDirectStockInQuaQty { get; set; }
		/// <summary>
		/// 直送基本单位领料选单数
		/// </summary>
		public decimal? FBaseDirectPickMtrlSelQty { get; set; }
		/// <summary>
		/// 首检控制方式
		/// </summary>
		public string FFirstQCControlType { get; set; }
		/// <summary>
		/// 已计划运算
		/// </summary>
		public string? FISMRPCAL { get; set; }
		/// <summary>
		/// 生成下级订单标志
		/// </summary>
		public string? FIsGenerateOrder { get; set; }
		/// <summary>
		/// 齐套优先级
		/// </summary>
		public int? FANALYSEPRIORITY { get; set; }
		/// <summary>
		/// 销售订单产品名称
		/// </summary>
		public int? F_ProductName { get; set; }
		/// <summary>
		/// 销售订单产品名称名称
		/// </summary>
		public string F_ProductName__FName { get; set; }
		/// <summary>
		/// 销售订单产品名称编码
		/// </summary>
		public string F_ProductName__FNumber { get; set; }
		#endregion
		#region 序列号子单据体
		/// <summary>
		/// 序列号子单据体主键FDetailID
		/// </summary>
		public System.Int64 FSerialSubEntity_FDetailID { get; set; }
		/// <summary>
		/// 序列号子单据体序列
		/// </summary>
		public int FSerialSubEntity_FSEQ { get; set; }
		/// <summary>
		/// 数量
		/// </summary>
		public decimal? FSNQty1 { get; set; }
		/// <summary>
		/// 序列号
		/// </summary>
		public string? FSerialNo { get; set; }
		/// <summary>
		/// 序列号
		/// </summary>
		public long? FSerialId { get; set; }
		/// <summary>
		/// 序列号名称
		/// </summary>
		public string FSerialId__FName { get; set; }
		/// <summary>
		/// 序列号序列号
		/// </summary>
		public string FSerialId__FNumber { get; set; }
		/// <summary>
		/// 入库选单数量
		/// </summary>
		public decimal? FSNStockInSelQty { get; set; }
		/// <summary>
		/// 备注
		/// </summary>
		public string? FSerialNote { get; set; }
		/// <summary>
		/// 基本单位数量
		/// </summary>
		public decimal? FBaseSNQty { get; set; }
		/// <summary>
		/// 基本单位入库选单数量
		/// </summary>
		public decimal? FBaseSNStockInSelQty { get; set; }
		/// <summary>
		/// 汇报选单数量
		/// </summary>
		public decimal? FSNRptSelQty { get; set; }
		/// <summary>
		/// 基本单位汇报选单数量
		/// </summary>
		public decimal? FBaseSNRptSelQty { get; set; }
		#endregion
		#region 排产明细表体
		/// <summary>
		/// 排产明细表体主键FEntryID
		/// </summary>
		public System.Int64 FScheduledEntity_FEntryID { get; set; }
		/// <summary>
		/// 排产明细表体序列
		/// </summary>
		public int FScheduledEntity_FSEQ { get; set; }
		/// <summary>
		/// 生产订单分录内码
		/// </summary>
		public int? FMOENTRYID { get; set; }
		/// <summary>
		/// 生产订单行号
		/// </summary>
		public int? FMOSEQ { get; set; }
		/// <summary>
		/// 物料编码
		/// </summary>
		public long? FDetailMaterialId { get; set; }
		/// <summary>
		/// 物料编码名称
		/// </summary>
		public string FDetailMaterialId__FName { get; set; }
		/// <summary>
		/// 物料编码编码
		/// </summary>
		public string FDetailMaterialId__FNumber { get; set; }
		/// <summary>
		/// 日产量
		/// </summary>
		public decimal? FDAILYOUTPUTQTY { get; set; }
		/// <summary>
		/// 基本单位日产量
		/// </summary>
		public decimal? FBASEDAILYOUTPUTQTY { get; set; }
		/// <summary>
		/// 单位
		/// </summary>
		public long? FDetailUnitID { get; set; }
		/// <summary>
		/// 单位名称
		/// </summary>
		public string FDetailUnitID__FName { get; set; }
		/// <summary>
		/// 单位编码
		/// </summary>
		public string FDetailUnitID__FNumber { get; set; }
		/// <summary>
		/// 基本单位
		/// </summary>
		public long? FBaseUnitIdDetail { get; set; }
		/// <summary>
		/// 基本单位名称
		/// </summary>
		public string FBaseUnitIdDetail__FName { get; set; }
		/// <summary>
		/// 基本单位编码
		/// </summary>
		public string FBaseUnitIdDetail__FNumber { get; set; }
		/// <summary>
		/// 计划数量
		/// </summary>
		public decimal? FPLANQTY { get; set; }
		/// <summary>
		/// 基本单位计划数量
		/// </summary>
		public decimal? FBASEPLANQTY { get; set; }
		/// <summary>
		/// 基本单位排产数量
		/// </summary>
		public decimal? FBASESCHEDULEDQTY { get; set; }
		/// <summary>
		/// 排产数量
		/// </summary>
		public decimal? FSCHEDULEDQTY { get; set; }
		/// <summary>
		/// 产线
		/// </summary>
		public long? FPRODUCTIONLINEID { get; set; }
		/// <summary>
		/// 产线产线名称
		/// </summary>
		public string FPRODUCTIONLINEID__FName { get; set; }
		/// <summary>
		/// 产线产线编码
		/// </summary>
		public string FPRODUCTIONLINEID__FNumber { get; set; }
		/// <summary>
		/// 排产日期
		/// </summary>
		public DateTime? FPlanStartDateD { get; set; }
		/// <summary>
		/// 排产结束
		/// </summary>
		public DateTime? FPlanFinishDateD { get; set; }
		/// <summary>
		/// 锁定
		/// </summary>
		public string? FISLOCKED { get; set; }
		/// <summary>
		/// 排产标识
		/// </summary>
		public string? FSchDtlRowId { get; set; }
		/// <summary>
		/// 基本单位差异数
		/// </summary>
		public decimal? FBASEDIFFERQTY { get; set; }
		/// <summary>
		/// 差异数
		/// </summary>
		public decimal? FDIFFERQTY { get; set; }
		/// <summary>
		/// 基本单位上线数量
		/// </summary>
		public decimal? FBASEFINISHEDQTY { get; set; }
		/// <summary>
		/// 上线数量
		/// </summary>
		public decimal? FFINISHEDQTY { get; set; }
		/// <summary>
		/// 发料数量
		/// </summary>
		public decimal? FDIssueQty { get; set; }
		/// <summary>
		/// 基本单位发料数量
		/// </summary>
		public decimal? FBaseDIssueQty { get; set; }
		/// <summary>
		/// 齐套数量1
		/// </summary>
		public decimal? FMatchQty1 { get; set; }
		/// <summary>
		/// 齐套数量2
		/// </summary>
		public decimal? FMatchQty2 { get; set; }
		#endregion
	}
}