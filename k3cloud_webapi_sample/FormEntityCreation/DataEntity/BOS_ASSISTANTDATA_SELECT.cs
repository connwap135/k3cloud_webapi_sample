namespace FormEntityCreation.DataEntity
{
    /// <summary>
    /// 辅助资料选择
    /// </summary>
    public class BOS_ASSISTANTDATA_SELECT
    {
        /// <summary>
        /// FEntryID
        /// </summary>
        public string FEntryID { get; set; }
        #region 单据头
        /// <summary>
        /// 编码
        /// </summary>
        public string? FNumber { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string? FDataValue { get; set; }
        /// <summary>
        /// 辅助资料类别
        /// </summary>
        public int? FId { get; set; }
        /// <summary>
        /// 辅助资料类别类别编码
        /// </summary>
        public string FId__FNumber { get; set; }
        /// <summary>
        /// 辅助资料类别类别名称
        /// </summary>
        public string FId__FName { get; set; }
        /// <summary>
        /// 上级资料
        /// </summary>
        public int? FParentId { get; set; }
        /// <summary>
        /// 上级资料编码
        /// </summary>
        public string FParentId__FNumber { get; set; }
        /// <summary>
        /// 显示顺序
        /// </summary>
        public int? FSeq { get; set; }
        /// <summary>
        /// 禁用状态
        /// </summary>
        public string? FForbidStatus { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string? FDescription { get; set; }
        #endregion
    }
}