using System;

namespace FormEntityCreation.DataEntity
{
    /// <summary>
    /// 销售订单
    /// </summary>
    public class SAL_SaleOrder
    {
        /// <summary>
        /// FID
        /// </summary>
        public int FID { get; set; }
        #region 基本信息
        /// <summary>
        /// 单据编号
        /// </summary>
        public string? FBillNo { get; set; }
        /// <summary>
        /// 单据状态
        /// </summary>
        public string? FDocumentStatus { get; set; }
        /// <summary>
        /// 销售组织
        /// </summary>
        public long FSaleOrgId { get; set; }
        /// <summary>
        /// 销售组织名称
        /// </summary>
        public string FSaleOrgId__FName { get; set; }
        /// <summary>
        /// 销售组织编码
        /// </summary>
        public string FSaleOrgId__FNumber { get; set; }
        /// <summary>
        /// 日期
        /// </summary>
        public DateTime FDate { get; set; }
        /// <summary>
        /// 客户
        /// </summary>
        public long FCustId { get; set; }
        /// <summary>
        /// 客户客户名称
        /// </summary>
        public string FCustId__FName { get; set; }
        /// <summary>
        /// 客户客户编码
        /// </summary>
        public string FCustId__FNumber { get; set; }
        /// <summary>
        /// 客户简称
        /// </summary>
        public string FCustId__FShortName { get; set; }
        /// <summary>
        /// 销售部门
        /// </summary>
        public long? FSaleDeptId { get; set; }
        /// <summary>
        /// 销售部门名称
        /// </summary>
        public string FSaleDeptId__FName { get; set; }
        /// <summary>
        /// 销售部门编码
        /// </summary>
        public string FSaleDeptId__FNumber { get; set; }
        /// <summary>
        /// 销售组
        /// </summary>
        public long? FSaleGroupId { get; set; }
        /// <summary>
        /// 销售组业务组名称
        /// </summary>
        public string FSaleGroupId__FName { get; set; }
        /// <summary>
        /// 销售组业务组编码
        /// </summary>
        public string FSaleGroupId__FNumber { get; set; }
        /// <summary>
        /// 销售员
        /// </summary>
        public long FSalerId { get; set; }
        /// <summary>
        /// 销售员名称
        /// </summary>
        public string FSalerId__FName { get; set; }
        /// <summary>
        /// 销售员业务员编码
        /// </summary>
        public string FSalerId__FNumber { get; set; }
        /// <summary>
        /// 收货方
        /// </summary>
        public long? FReceiveId { get; set; }
        /// <summary>
        /// 收货方客户名称
        /// </summary>
        public string FReceiveId__FName { get; set; }
        /// <summary>
        /// 收货方客户编码
        /// </summary>
        public string FReceiveId__FNumber { get; set; }
        /// <summary>
        /// 收货方简称
        /// </summary>
        public string FReceiveId__FShortName { get; set; }
        /// <summary>
        /// 结算方
        /// </summary>
        public long? FSettleId { get; set; }
        /// <summary>
        /// 结算方客户名称
        /// </summary>
        public string FSettleId__FName { get; set; }
        /// <summary>
        /// 结算方客户编码
        /// </summary>
        public string FSettleId__FNumber { get; set; }
        /// <summary>
        /// 结算方简称
        /// </summary>
        public string FSettleId__FShortName { get; set; }
        /// <summary>
        /// 付款方
        /// </summary>
        public long? FChargeId { get; set; }
        /// <summary>
        /// 付款方客户名称
        /// </summary>
        public string FChargeId__FName { get; set; }
        /// <summary>
        /// 付款方客户编码
        /// </summary>
        public string FChargeId__FNumber { get; set; }
        /// <summary>
        /// 付款方简称
        /// </summary>
        public string FChargeId__FShortName { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public long? FCreatorId { get; set; }
        /// <summary>
        /// 创建人用户名称
        /// </summary>
        public string FCreatorId__FName { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime? FCreateDate { get; set; }
        /// <summary>
        /// 最后修改人
        /// </summary>
        public long? FModifierId { get; set; }
        /// <summary>
        /// 最后修改人用户名称
        /// </summary>
        public string FModifierId__FName { get; set; }
        /// <summary>
        /// 最后修改日期
        /// </summary>
        public DateTime? FModifyDate { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        public long? FApproverId { get; set; }
        /// <summary>
        /// 审核人用户名称
        /// </summary>
        public string FApproverId__FName { get; set; }
        /// <summary>
        /// 审核日期
        /// </summary>
        public DateTime? FApproveDate { get; set; }
        /// <summary>
        /// 关闭状态
        /// </summary>
        public string? FCloseStatus { get; set; }
        /// <summary>
        /// 关闭人
        /// </summary>
        public long? FCloserId { get; set; }
        /// <summary>
        /// 关闭人用户名称
        /// </summary>
        public string FCloserId__FName { get; set; }
        /// <summary>
        /// 关闭日期
        /// </summary>
        public DateTime? FCloseDate { get; set; }
        /// <summary>
        /// 作废状态
        /// </summary>
        public string? FCancelStatus { get; set; }
        /// <summary>
        /// 作废人
        /// </summary>
        public long? FCancellerId { get; set; }
        /// <summary>
        /// 作废人用户名称
        /// </summary>
        public string FCancellerId__FName { get; set; }
        /// <summary>
        /// 作废日期
        /// </summary>
        public DateTime? FCancelDate { get; set; }
        /// <summary>
        /// 版本号
        /// </summary>
        public string? FVersionNo { get; set; }
        /// <summary>
        /// 变更人
        /// </summary>
        public long? FChangerId { get; set; }
        /// <summary>
        /// 变更人用户名称
        /// </summary>
        public string FChangerId__FName { get; set; }
        /// <summary>
        /// 变更日期
        /// </summary>
        public DateTime? FChangeDate { get; set; }
        /// <summary>
        /// 变更原因
        /// </summary>
        public string? FChangeReason { get; set; }
        /// <summary>
        /// 单据类型
        /// </summary>
        public string FBillTypeID { get; set; }
        /// <summary>
        /// 单据类型名称
        /// </summary>
        public string FBillTypeID__FName { get; set; }
        /// <summary>
        /// 单据类型编码
        /// </summary>
        public string FBillTypeID__FNumber { get; set; }
        /// <summary>
        /// 业务类型
        /// </summary>
        public string? FBusinessType { get; set; }
        /// <summary>
        /// 交货方式
        /// </summary>
        public string? FHeadDeliveryWay { get; set; }
        /// <summary>
        /// 交货方式编码
        /// </summary>
        public string FHeadDeliveryWay__FNumber { get; set; }
        /// <summary>
        /// 收货方地址
        /// </summary>
        public string? FReceiveAddress { get; set; }
        /// <summary>
        /// 交货地点
        /// </summary>
        public long? FHEADLOCID { get; set; }
        /// <summary>
        /// 交货地点地点名称
        /// </summary>
        public string FHEADLOCID__FName { get; set; }
        /// <summary>
        /// 信用检查结果
        /// </summary>
        public string? FCreditCheckResult { get; set; }
        /// <summary>
        /// 对应组织
        /// </summary>
        public long? FCorrespondOrgId { get; set; }
        /// <summary>
        /// 对应组织名称
        /// </summary>
        public string FCorrespondOrgId__FName { get; set; }
        /// <summary>
        /// 对应组织编码
        /// </summary>
        public string FCorrespondOrgId__FNumber { get; set; }
        /// <summary>
        /// 收货方联系人
        /// </summary>
        public long? FReceiveContact { get; set; }
        /// <summary>
        /// 收货方联系人编码
        /// </summary>
        public string FReceiveContact__FNumber { get; set; }
        /// <summary>
        /// 收货方联系人姓名
        /// </summary>
        public string FReceiveContact__FName { get; set; }
        /// <summary>
        /// 移动销售订单编号
        /// </summary>
        public string? FNetOrderBillNo { get; set; }
        /// <summary>
        /// 移动销售订单ID
        /// </summary>
        public int? FNetOrderBillId { get; set; }
        /// <summary>
        /// 商机内码
        /// </summary>
        public int? FOppID { get; set; }
        /// <summary>
        /// 销售阶段
        /// </summary>
        public long? FSalePhaseID { get; set; }
        /// <summary>
        /// 销售阶段名称
        /// </summary>
        public string FSalePhaseID__FName { get; set; }
        /// <summary>
        /// 销售阶段编码
        /// </summary>
        public string FSalePhaseID__FNumber { get; set; }
        /// <summary>
        /// 是否期初单据
        /// </summary>
        public string? FISINIT { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string? FNote { get; set; }
        /// <summary>
        /// 来自移动（弃用）
        /// </summary>
        public string? FIsMobile { get; set; }
        /// <summary>
        /// 签收状态
        /// </summary>
        public string? FSignStatus { get; set; }
        /// <summary>
        /// 是否手工关闭
        /// </summary>
        public string? FManualClose { get; set; }
        /// <summary>
        /// 收货人姓名
        /// </summary>
        public string? FLinkMan { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        public string? FLinkPhone { get; set; }
        /// <summary>
        /// 订单来源
        /// </summary>
        public string? FSOFrom { get; set; }
        /// <summary>
        /// 合同类型
        /// </summary>
        public string? FContractType { get; set; }
        /// <summary>
        /// 销售合同内码
        /// </summary>
        public int? FContractId { get; set; }
        /// <summary>
        /// 生成受托加工材料清单
        /// </summary>
        public string? FIsUseOEMBomPush { get; set; }
        /// <summary>
        /// 变更单主键
        /// </summary>
        public int? FXPKID_H { get; set; }
        /// <summary>
        /// 关闭原因
        /// </summary>
        public string? FCloseReason { get; set; }
        /// <summary>
        /// 生成分销采购订单
        /// </summary>
        public string? FIsUseDrpSalePOPush { get; set; }
        /// <summary>
        /// 生成直运出入库
        /// </summary>
        public string? FIsCreateStraightOutIN { get; set; }
        /// <summary>
        /// 预设基础资料字段1
        /// </summary>
        public long? FPRESETBASE1 { get; set; }
        /// <summary>
        /// 预设基础资料字段1客户名称
        /// </summary>
        public string FPRESETBASE1__FName { get; set; }
        /// <summary>
        /// 预设基础资料字段1客户编码
        /// </summary>
        public string FPRESETBASE1__FNumber { get; set; }
        /// <summary>
        /// 预设基础资料字段1简称
        /// </summary>
        public string FPRESETBASE1__FShortName { get; set; }
        /// <summary>
        /// 预设辅助资料字段1
        /// </summary>
        public string? FPRESETASSISTANT1 { get; set; }
        /// <summary>
        /// 预设辅助资料字段1编码
        /// </summary>
        public string FPRESETASSISTANT1__FNumber { get; set; }
        /// <summary>
        /// 预设辅助资料字段2
        /// </summary>
        public string? FPRESETASSISTANT2 { get; set; }
        /// <summary>
        /// 预设辅助资料字段2编码
        /// </summary>
        public string FPRESETASSISTANT2__FNumber { get; set; }
        /// <summary>
        /// 预设基础资料字段2
        /// </summary>
        public long? FPRESETBASE2 { get; set; }
        /// <summary>
        /// 预设基础资料字段2客户名称
        /// </summary>
        public string FPRESETBASE2__FName { get; set; }
        /// <summary>
        /// 预设基础资料字段2客户编码
        /// </summary>
        public string FPRESETBASE2__FNumber { get; set; }
        /// <summary>
        /// 预设基础资料字段2简称
        /// </summary>
        public string FPRESETBASE2__FShortName { get; set; }
        #endregion
        #region 订单条款
        /// <summary>
        /// 订单条款主键FEntryID
        /// </summary>
        public long FSaleOrderClause_FEntryID { get; set; }
        /// <summary>
        /// 订单条款序列
        /// </summary>
        public int FSaleOrderClause_FSeq { get; set; }
        /// <summary>
        /// 条款编码
        /// </summary>
        public long? FClauseId { get; set; }
        /// <summary>
        /// 条款编码名称
        /// </summary>
        public string FClauseId__FName { get; set; }
        /// <summary>
        /// 条款编码编码
        /// </summary>
        public string FClauseId__FNumber { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string? FClauseDesc { get; set; }
        /// <summary>
        /// 变更单主键
        /// </summary>
        public int? FXPKID_C { get; set; }
        #endregion
        #region 订单明细
        /// <summary>
        /// 订单明细主键FEntryID
        /// </summary>
        public long FSaleOrderEntry_FEntryID { get; set; }
        /// <summary>
        /// 订单明细序列
        /// </summary>
        public int FSaleOrderEntry_FSeq { get; set; }
        /// <summary>
        /// 物料编码
        /// </summary>
        public long FMaterialId { get; set; }
        /// <summary>
        /// 物料编码名称
        /// </summary>
        public string FMaterialId__FName { get; set; }
        /// <summary>
        /// 物料编码编码
        /// </summary>
        public string FMaterialId__FNumber { get; set; }
        /// <summary>
        /// 销售单位
        /// </summary>
        public long FUnitID { get; set; }
        /// <summary>
        /// 销售单位名称
        /// </summary>
        public string FUnitID__FName { get; set; }
        /// <summary>
        /// 销售单位编码
        /// </summary>
        public string FUnitID__FNumber { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        public decimal? FPrice { get; set; }
        /// <summary>
        /// 含税单价
        /// </summary>
        public decimal? FTaxPrice { get; set; }
        /// <summary>
        /// BOM版本
        /// </summary>
        public long? FBomId { get; set; }
        /// <summary>
        /// BOM版本BOM简称
        /// </summary>
        public string FBomId__FName { get; set; }
        /// <summary>
        /// BOM版本BOM版本
        /// </summary>
        public string FBomId__FNumber { get; set; }
        /// <summary>
        /// 计价单位
        /// </summary>
        public long FPriceUnitId { get; set; }
        /// <summary>
        /// 计价单位名称
        /// </summary>
        public string FPriceUnitId__FName { get; set; }
        /// <summary>
        /// 计价单位编码
        /// </summary>
        public string FPriceUnitId__FNumber { get; set; }
        /// <summary>
        /// 计价数量
        /// </summary>
        public decimal? FPriceUnitQty { get; set; }
        /// <summary>
        /// 价格系数
        /// </summary>
        public decimal? FPriceCoefficient { get; set; }
        /// <summary>
        /// 折扣率%
        /// </summary>
        public decimal? FDiscountRate { get; set; }
        /// <summary>
        /// 折扣额
        /// </summary>
        public decimal? FDiscount { get; set; }
        /// <summary>
        /// 税率%
        /// </summary>
        public decimal? FEntryTaxRate { get; set; }
        /// <summary>
        /// 税额
        /// </summary>
        public decimal? FEntryTaxAmount { get; set; }
        /// <summary>
        /// 价税合计
        /// </summary>
        public decimal? FAllAmount { get; set; }
        /// <summary>
        /// 净价
        /// </summary>
        public decimal? FTaxNetPrice { get; set; }
        /// <summary>
        /// 销售基本数量
        /// </summary>
        public decimal? FBaseUnitQty { get; set; }
        /// <summary>
        /// 控制发货数量
        /// </summary>
        public string? FDeliveryControl { get; set; }
        /// <summary>
        /// 发货上限
        /// </summary>
        public decimal? FDeliveryMaxQty { get; set; }
        /// <summary>
        /// 发货下限
        /// </summary>
        public decimal? FDeliveryMinQty { get; set; }
        /// <summary>
        /// 运输提前期
        /// </summary>
        public int? FTransportLeadTime1 { get; set; }
        /// <summary>
        /// 折前价税合计(作废)
        /// </summary>
        public decimal? FBefDisAllAmt { get; set; }
        /// <summary>
        /// 折前金额(作废)
        /// </summary>
        public decimal? FBefDisAmt { get; set; }
        /// <summary>
        /// 税额（本位币）
        /// </summary>
        public decimal? FTaxAmount_LC { get; set; }
        /// <summary>
        /// 金额（本位币）
        /// </summary>
        public decimal? FAmount_LC { get; set; }
        /// <summary>
        /// 价税合计（本位币）
        /// </summary>
        public decimal? FAllAmount_LC { get; set; }
        /// <summary>
        /// 业务关闭
        /// </summary>
        public string? FMrpCloseStatus { get; set; }
        /// <summary>
        /// 业务冻结
        /// </summary>
        public string? FMrpFreezeStatus { get; set; }
        /// <summary>
        /// 冻结人
        /// </summary>
        public long? FFreezerId { get; set; }
        /// <summary>
        /// 冻结人用户名称
        /// </summary>
        public string FFreezerId__FName { get; set; }
        /// <summary>
        /// 冻结日期
        /// </summary>
        public DateTime? FFreezeDate { get; set; }
        /// <summary>
        /// 业务终止
        /// </summary>
        public string? FMrpTerminateStatus { get; set; }
        /// <summary>
        /// 终止人
        /// </summary>
        public long? FTerminaterId { get; set; }
        /// <summary>
        /// 终止人用户名称
        /// </summary>
        public string FTerminaterId__FName { get; set; }
        /// <summary>
        /// 终止日期
        /// </summary>
        public DateTime? FTerminateDate { get; set; }
        /// <summary>
        /// 关联发货通知数量（销售基本）（作废）
        /// </summary>
        public decimal? FBaseDeliJoinQty { get; set; }
        /// <summary>
        /// 累计发货通知数量
        /// </summary>
        public decimal? FDeliQty { get; set; }
        /// <summary>
        /// 累计出库数量
        /// </summary>
        public decimal? FStockOutQty { get; set; }
        /// <summary>
        /// 累计退货通知数量（销售）
        /// </summary>
        public decimal? FRetNoticeQty { get; set; }
        /// <summary>
        /// 累计退货数量（销售）
        /// </summary>
        public decimal? FReturnQty { get; set; }
        /// <summary>
        /// 剩余未出数量（销售）
        /// </summary>
        public decimal? FRemainOutQty { get; set; }
        /// <summary>
        /// 关联开票数量（基本）(作废)
        /// </summary>
        public decimal? FBaseInvoiceJoinQty { get; set; }
        /// <summary>
        /// 关联开票数量(作废)
        /// </summary>
        public decimal? FInvoiceJoinQty { get; set; }
        /// <summary>
        /// 累计开票数量(作废)
        /// </summary>
        public decimal? FInvoiceQty { get; set; }
        /// <summary>
        /// 累计开票金额(作废)
        /// </summary>
        public decimal? FInvoiceAmount { get; set; }
        /// <summary>
        /// 累计收款金额(作废)
        /// </summary>
        public decimal? FReceiveAmount { get; set; }
        /// <summary>
        /// 关联采购/生产数量（销售基本）
        /// </summary>
        public decimal? FBasePurJoinQty { get; set; }
        /// <summary>
        /// 关联采购/生产数量
        /// </summary>
        public decimal? FPurJoinQty { get; set; }
        /// <summary>
        /// 累计采购申请数量
        /// </summary>
        public decimal? FPurReqQty { get; set; }
        /// <summary>
        /// 累计采购订单数量
        /// </summary>
        public decimal? FPurOrderQty { get; set; }
        /// <summary>
        /// 收款组织
        /// </summary>
        public long? FReceiptOrgId { get; set; }
        /// <summary>
        /// 收款组织名称
        /// </summary>
        public string FReceiptOrgId__FName { get; set; }
        /// <summary>
        /// 收款组织编码
        /// </summary>
        public string FReceiptOrgId__FNumber { get; set; }
        /// <summary>
        /// 结算组织
        /// </summary>
        public long FSettleOrgIds { get; set; }
        /// <summary>
        /// 结算组织名称
        /// </summary>
        public string FSettleOrgIds__FName { get; set; }
        /// <summary>
        /// 结算组织编码
        /// </summary>
        public string FSettleOrgIds__FNumber { get; set; }
        /// <summary>
        /// 金额
        /// </summary>
        public decimal? FAmount { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string? FEntryNote { get; set; }
        /// <summary>
        /// 销售数量
        /// </summary>
        public decimal? FQty { get; set; }
        /// <summary>
        /// 最低限价
        /// </summary>
        public decimal? FLimitDownPrice { get; set; }
        /// <summary>
        /// 系统定价
        /// </summary>
        public decimal? FSysPrice { get; set; }
        /// <summary>
        /// 库存组织
        /// </summary>
        public long? FStockOrgId { get; set; }
        /// <summary>
        /// 库存组织名称
        /// </summary>
        public string FStockOrgId__FName { get; set; }
        /// <summary>
        /// 库存组织编码
        /// </summary>
        public string FStockOrgId__FNumber { get; set; }
        /// <summary>
        /// 累计出库数量（销售基本）
        /// </summary>
        public decimal? FBaseStockOutQty { get; set; }
        /// <summary>
        /// 累计发货通知数量（销售基本）
        /// </summary>
        public decimal? FBaseDeliQty { get; set; }
        /// <summary>
        /// 累计退货通知数量（销售基本）
        /// </summary>
        public decimal? FBaseRetNoticeQty { get; set; }
        /// <summary>
        /// 累计退货数量（销售基本）
        /// </summary>
        public decimal? FBaseReturnQty { get; set; }
        /// <summary>
        /// 累计采购申请数量（销售基本）
        /// </summary>
        public decimal? FBasePurReqQty { get; set; }
        /// <summary>
        /// 累计采购订单数量（销售基本）
        /// </summary>
        public decimal? FBasePurOrderQty { get; set; }
        /// <summary>
        /// 基本单位
        /// </summary>
        public long? FBaseUnitId { get; set; }
        /// <summary>
        /// 基本单位名称
        /// </summary>
        public string FBaseUnitId__FName { get; set; }
        /// <summary>
        /// 基本单位编码
        /// </summary>
        public string FBaseUnitId__FNumber { get; set; }
        /// <summary>
        /// 变更标志
        /// </summary>
        public string? FChangeFlag { get; set; }
        /// <summary>
        /// 客户物料编码
        /// </summary>
        public long? FMapId { get; set; }
        /// <summary>
        /// 客户物料编码客户物料名称
        /// </summary>
        public string FMapId__FName { get; set; }
        /// <summary>
        /// 客户物料编码客户物料编码
        /// </summary>
        public string FMapId__FNumber { get; set; }
        /// <summary>
        /// 货主类型
        /// </summary>
        public string? FOwnerTypeId { get; set; }
        /// <summary>
        /// 货主
        /// </summary>
        public int? FOwnerId { get; set; }
        /// <summary>
        /// 货主名称
        /// </summary>
        public string FOwnerId__FName { get; set; }
        /// <summary>
        /// 货主编码
        /// </summary>
        public string FOwnerId__FNumber { get; set; }
        /// <summary>
        /// 是否赠品
        /// </summary>
        public string? FIsFree { get; set; }
        /// <summary>
        /// 锁库/预留数量(库存单位)
        /// </summary>
        public decimal? FLOCKQTY { get; set; }
        /// <summary>
        /// 锁库/预留标识
        /// </summary>
        public string? FLOCKFLAG { get; set; }
        /// <summary>
        /// 生产日期
        /// </summary>
        public DateTime? FProduceDate { get; set; }
        /// <summary>
        /// 有效期至
        /// </summary>
        public DateTime? FExpiryDate { get; set; }
        /// <summary>
        /// 保质期单位
        /// </summary>
        public string? FExpUnit { get; set; }
        /// <summary>
        /// 保质期
        /// </summary>
        public int? FExpPeriod { get; set; }
        /// <summary>
        /// 税组合
        /// </summary>
        public long? FTaxCombination { get; set; }
        /// <summary>
        /// 税组合名称
        /// </summary>
        public string FTaxCombination__FName { get; set; }
        /// <summary>
        /// 税组合编码
        /// </summary>
        public string FTaxCombination__FNumber { get; set; }
        /// <summary>
        /// 批号
        /// </summary>
        public long? FLot { get; set; }
        /// <summary>
        /// 批号名称
        /// </summary>
        public string FLot__FName { get; set; }
        /// <summary>
        /// 批号批号
        /// </summary>
        public string FLot__FNumber { get; set; }
        /// <summary>
        /// 辅助属性
        /// </summary>
        public long? FAuxPropId { get; set; }
        /// <summary>
        /// 退补类型
        /// </summary>
        public string? FReturnType { get; set; }
        /// <summary>
        /// 要货日期
        /// </summary>
        public DateTime FDeliveryDate { get; set; }
        /// <summary>
        /// 累计调拨数量
        /// </summary>
        public decimal? FTransJoinQty { get; set; }
        /// <summary>
        /// 累计调拨数量(销售基本)
        /// </summary>
        public decimal? FBaseTransJoinQty { get; set; }
        /// <summary>
        /// 源单类型
        /// </summary>
        public string? FSrcType { get; set; }
        /// <summary>
        /// 源单编号
        /// </summary>
        public string? FSrcBillNo { get; set; }
        /// <summary>
        /// 发货上限（基本）
        /// </summary>
        public decimal? FBaseDeliveryMaxQty { get; set; }
        /// <summary>
        /// 发货下限（基本）
        /// </summary>
        public decimal? FBaseDeliveryMinQty { get; set; }
        /// <summary>
        /// 关联受托材料入库套数
        /// </summary>
        public decimal? FOEMInStockJoinQty { get; set; }
        /// <summary>
        /// 关联受托材料入库套数(库存基本)
        /// </summary>
        public decimal? FBaseOEMInStockJoinQty { get; set; }
        /// <summary>
        /// 关联应收数量（计价基本）
        /// </summary>
        public decimal? FBaseARJoinQty { get; set; }
        /// <summary>
        /// 业务流程
        /// </summary>
        public string? FBFLowId { get; set; }
        /// <summary>
        /// 业务流程名称
        /// </summary>
        public string FBFLowId__FName { get; set; }
        /// <summary>
        /// 累计应收数量（销售基本）
        /// </summary>
        public decimal? FBASEARQTY { get; set; }
        /// <summary>
        /// 关联应收金额
        /// </summary>
        public decimal? FARJOINAMOUNT { get; set; }
        /// <summary>
        /// 累计应收金额
        /// </summary>
        public decimal? FARAMOUNT { get; set; }
        /// <summary>
        /// 剩余未出数量（销售基本）
        /// </summary>
        public decimal? FBaseRemainOutQty { get; set; }
        /// <summary>
        /// 累计退货补货数量（销售）
        /// </summary>
        public decimal? FReBackQty { get; set; }
        /// <summary>
        /// 累计退货补货数量（销售基本）
        /// </summary>
        public decimal? FBaseReBackQty { get; set; }
        /// <summary>
        /// 累计应收数量（销售）
        /// </summary>
        public decimal? FARQTY { get; set; }
        /// <summary>
        /// 可出数量（销售）
        /// </summary>
        public decimal? FCanOutQty { get; set; }
        /// <summary>
        /// 可出数量（销售基本）
        /// </summary>
        public decimal? FBaseCanOutQty { get; set; }
        /// <summary>
        /// 可退数量（销售）
        /// </summary>
        public decimal? FCanReturnQty { get; set; }
        /// <summary>
        /// 可退数量（销售基本）
        /// </summary>
        public decimal? FBaseCanReturnQty { get; set; }
        /// <summary>
        /// 累计应付数量(基本单位)
        /// </summary>
        public decimal? FBASEAPQTY { get; set; }
        /// <summary>
        /// 累计应付金额
        /// </summary>
        public decimal? FAPAMOUNT { get; set; }
        /// <summary>
        /// 计划跟踪号
        /// </summary>
        public string? FMtoNo { get; set; }
        /// <summary>
        /// 需求优先级
        /// </summary>
        public int? FPriority { get; set; }
        /// <summary>
        /// 预留类型
        /// </summary>
        public string FReserveType { get; set; }
        /// <summary>
        /// 计划交货日期
        /// </summary>
        public DateTime? FMinPlanDeliveryDate { get; set; }
        /// <summary>
        /// 发货状态
        /// </summary>
        public string? FDeliveryStatus { get; set; }
        /// <summary>
        /// 原数量
        /// </summary>
        public decimal? FOldQty { get; set; }
        /// <summary>
        /// 促销匹配类型
        /// </summary>
        public string? FPromotionMatchType { get; set; }
        /// <summary>
        /// 行价目表
        /// </summary>
        public long? FPriceListEntry { get; set; }
        /// <summary>
        /// 行价目表名称
        /// </summary>
        public string FPriceListEntry__FName { get; set; }
        /// <summary>
        /// 行价目表编号
        /// </summary>
        public string FPriceListEntry__FNumber { get; set; }
        /// <summary>
        /// 供应组织
        /// </summary>
        public long? FSupplyOrgId { get; set; }
        /// <summary>
        /// 供应组织名称
        /// </summary>
        public string FSupplyOrgId__FName { get; set; }
        /// <summary>
        /// 供应组织编码
        /// </summary>
        public string FSupplyOrgId__FNumber { get; set; }
        /// <summary>
        /// 供应商协同平台订单分录ID
        /// </summary>
        public int? FNetOrderEntryId { get; set; }
        /// <summary>
        /// 计价基本数量
        /// </summary>
        public decimal? FPriceBaseQty { get; set; }
        /// <summary>
        /// 定价单位
        /// </summary>
        public long? FSetPriceUnitID { get; set; }
        /// <summary>
        /// 定价单位名称
        /// </summary>
        public string FSetPriceUnitID__FName { get; set; }
        /// <summary>
        /// 定价单位编码
        /// </summary>
        public string FSetPriceUnitID__FNumber { get; set; }
        /// <summary>
        /// 库存单位
        /// </summary>
        public long FStockUnitID { get; set; }
        /// <summary>
        /// 库存单位名称
        /// </summary>
        public string FStockUnitID__FName { get; set; }
        /// <summary>
        /// 库存单位编码
        /// </summary>
        public string FStockUnitID__FNumber { get; set; }
        /// <summary>
        /// 库存数量
        /// </summary>
        public decimal? FStockQty { get; set; }
        /// <summary>
        /// 库存基本数量
        /// </summary>
        public decimal? FStockBaseQty { get; set; }
        /// <summary>
        /// 可出数量（库存基本）
        /// </summary>
        public decimal? FStockBaseCanOutQty { get; set; }
        /// <summary>
        /// 可退数量（库存基本）
        /// </summary>
        public decimal? FStockBaseCanReturnQty { get; set; }
        /// <summary>
        /// 关联应收数量（库存基本）
        /// </summary>
        public decimal? FStockBaseARJoinQty { get; set; }
        /// <summary>
        /// 累计调拨数量（库存基本）
        /// </summary>
        public decimal? FStockBaseTransJoinQty { get; set; }
        /// <summary>
        /// 关联采购/生产数量（库存基本）
        /// </summary>
        public decimal? FStockBasePurJoinQty { get; set; }
        /// <summary>
        /// 源单基本分子
        /// </summary>
        public decimal? FSalBaseNum { get; set; }
        /// <summary>
        /// 库存基本分母
        /// </summary>
        public decimal? FStockBaseDen { get; set; }
        /// <summary>
        /// 携带主单位
        /// </summary>
        public long? FSRCBIZUNITID { get; set; }
        /// <summary>
        /// 携带主单位名称
        /// </summary>
        public string FSRCBIZUNITID__FName { get; set; }
        /// <summary>
        /// 携带主单位编码
        /// </summary>
        public string FSRCBIZUNITID__FNumber { get; set; }
        /// <summary>
        /// 采购基本数量
        /// </summary>
        public decimal? FPurBaseQty { get; set; }
        /// <summary>
        /// 采购单位
        /// </summary>
        public long? FPurUnitID { get; set; }
        /// <summary>
        /// 采购单位名称
        /// </summary>
        public string FPurUnitID__FName { get; set; }
        /// <summary>
        /// 采购单位编码
        /// </summary>
        public string FPurUnitID__FNumber { get; set; }
        /// <summary>
        /// 采购数量
        /// </summary>
        public decimal? FPurQty { get; set; }
        /// <summary>
        /// 关联应收数量（销售基本）
        /// </summary>
        public decimal? FSalBaseARJoinQty { get; set; }
        /// <summary>
        /// 累计出库数量（库存基本）
        /// </summary>
        public decimal? FSTOCKBASESTOCKOUTQTY { get; set; }
        /// <summary>
        /// 累计退货补货数量（库存基本）
        /// </summary>
        public decimal? FSTOCKBASEREBACKQTY { get; set; }
        /// <summary>
        /// 超发控制单位类型
        /// </summary>
        public string FOUTLMTUNIT { get; set; }
        /// <summary>
        /// 超发控制单位
        /// </summary>
        public long? FOutLmtUnitID { get; set; }
        /// <summary>
        /// 超发控制单位名称
        /// </summary>
        public string FOutLmtUnitID__FName { get; set; }
        /// <summary>
        /// 超发控制单位编码
        /// </summary>
        public string FOutLmtUnitID__FNumber { get; set; }
        /// <summary>
        /// 累计调拨退货数量
        /// </summary>
        public decimal? FTRANSRETURNQTY { get; set; }
        /// <summary>
        /// 累计调拨退货数量（销售基本）
        /// </summary>
        public decimal? FTRANSRETURNBASEQTY { get; set; }
        /// <summary>
        /// 寄售结算数量
        /// </summary>
        public decimal? FCONSIGNSETTQTY { get; set; }
        /// <summary>
        /// 寄售结算数量（销售基本）
        /// </summary>
        public decimal? FCONSIGNSETTBASEQTY { get; set; }
        /// <summary>
        /// 待锁库/待预留数量(库存单位)
        /// </summary>
        public decimal? FLeftQty { get; set; }
        /// <summary>
        /// 产品类型
        /// </summary>
        public string? FRowType { get; set; }
        /// <summary>
        /// 父项产品
        /// </summary>
        public long? FParentMatId { get; set; }
        /// <summary>
        /// 父项产品名称
        /// </summary>
        public string FParentMatId__FName { get; set; }
        /// <summary>
        /// 父项产品编码
        /// </summary>
        public string FParentMatId__FNumber { get; set; }
        /// <summary>
        /// 行标识
        /// </summary>
        public string? FRowId { get; set; }
        /// <summary>
        /// 父行标识
        /// </summary>
        public string? FParentRowId { get; set; }
        /// <summary>
        /// 最新采购入库价
        /// </summary>
        public decimal? FInStockPrice { get; set; }
        /// <summary>
        /// 仓库
        /// </summary>
        public long? FSOStockId { get; set; }
        /// <summary>
        /// 仓库名称
        /// </summary>
        public string FSOStockId__FName { get; set; }
        /// <summary>
        /// 仓库编码
        /// </summary>
        public string FSOStockId__FNumber { get; set; }
        /// <summary>
        /// 仓位
        /// </summary>
        public long? FSOStockLocalId { get; set; }
        /// <summary>
        /// 采购计价单位
        /// </summary>
        public long? FPurPriceUnitId { get; set; }
        /// <summary>
        /// 采购计价单位名称
        /// </summary>
        public string FPurPriceUnitId__FName { get; set; }
        /// <summary>
        /// 采购计价单位编码
        /// </summary>
        public string FPurPriceUnitId__FNumber { get; set; }
        /// <summary>
        /// 已预留
        /// </summary>
        public string? FISMRP { get; set; }
        /// <summary>
        /// 零售条形码
        /// </summary>
        public string? FBarcode { get; set; }
        /// <summary>
        /// 发货门店
        /// </summary>
        public long? FBranchId { get; set; }
        /// <summary>
        /// 发货门店名称
        /// </summary>
        public string FBranchId__FName { get; set; }
        /// <summary>
        /// 发货门店编码
        /// </summary>
        public string FBranchId__FNumber { get; set; }
        /// <summary>
        /// 是否零售促销
        /// </summary>
        public string? FRetailSaleProm { get; set; }
        /// <summary>
        /// 先开票数量（计价基本）
        /// </summary>
        public decimal? FBASEFINARQTY { get; set; }
        /// <summary>
        /// 先开票数量（销售基本）
        /// </summary>
        public decimal? FSALBASEFINARQTY { get; set; }
        /// <summary>
        /// 行折扣表
        /// </summary>
        public long? FEntryDiscountList { get; set; }
        /// <summary>
        /// 行折扣表名称
        /// </summary>
        public string FEntryDiscountList__FName { get; set; }
        /// <summary>
        /// 行折扣表编码
        /// </summary>
        public string FEntryDiscountList__FNumber { get; set; }
        /// <summary>
        /// 单价折扣
        /// </summary>
        public decimal? FPriceDiscount { get; set; }
        /// <summary>
        /// 智慧记上报库存
        /// </summary>
        public decimal? FZHJStockQty { get; set; }
        /// <summary>
        /// 促销政策ID
        /// </summary>
        public string? FSPMENTRYID { get; set; }
        /// <summary>
        /// 促销内容
        /// </summary>
        public string? FSPMANDRPMCONTENT { get; set; }
        /// <summary>
        /// 累计调拨退货数量（库存基本）
        /// </summary>
        public decimal? FTransReturnStockBaseQty { get; set; }
        /// <summary>
        /// 尾差处理标识
        /// </summary>
        public string? FTailDiffFlag { get; set; }
        /// <summary>
        /// 返利前价格
        /// </summary>
        public decimal? FOldTaxPrice { get; set; }
        /// <summary>
        /// 返利前金额
        /// </summary>
        public decimal? FOldAmount { get; set; }
        /// <summary>
        /// 返利前价税合计
        /// </summary>
        public decimal? FOldAllAmount { get; set; }
        /// <summary>
        /// 返利前折扣率
        /// </summary>
        public decimal? FOldDiscountRate { get; set; }
        /// <summary>
        /// 返利前折扣额
        /// </summary>
        public decimal? FOldDiscount { get; set; }
        /// <summary>
        /// 返利金额
        /// </summary>
        public decimal? FRPAmount { get; set; }
        /// <summary>
        /// 返利规则明细ID
        /// </summary>
        public int? FAccountBalanceId { get; set; }
        /// <summary>
        /// BOM分录内码
        /// </summary>
        public int? FBOMEntryId { get; set; }
        /// <summary>
        /// 返利折扣率
        /// </summary>
        public decimal? FRPDiscountRate { get; set; }
        /// <summary>
        /// 关联出库数量（库存基本）含跨级
        /// </summary>
        public decimal? FStockBaseOutJoinQty { get; set; }
        /// <summary>
        /// 价税合计（折前）
        /// </summary>
        public decimal? FAllAmountExceptDisCount { get; set; }
        /// <summary>
        /// 累计数量取价标识
        /// </summary>
        public string? FIsSumQtyTag { get; set; }
        /// <summary>
        /// 上级物料分组编码
        /// </summary>
        public long? FMaterialGroup { get; set; }
        /// <summary>
        /// 上级物料分组编码编码
        /// </summary>
        public string FMaterialGroup__FNumber { get; set; }
        /// <summary>
        /// 上级物料分组编码名称
        /// </summary>
        public string FMaterialGroup__FName { get; set; }
        /// <summary>
        /// 变更单主键
        /// </summary>
        public int? FXPKID { get; set; }
        /// <summary>
        /// 是否手工行关闭
        /// </summary>
        public string? FManualRowClose { get; set; }
        /// <summary>
        /// 物料分组编码
        /// </summary>
        public long? FMaterialGroupByMat { get; set; }
        /// <summary>
        /// 物料分组编码编码
        /// </summary>
        public string FMaterialGroupByMat__FNumber { get; set; }
        /// <summary>
        /// 物料分组编码名称
        /// </summary>
        public string FMaterialGroupByMat__FName { get; set; }
        /// <summary>
        /// 已计划运算
        /// </summary>
        public string? FISMRPCAL { get; set; }
        #endregion
        #region 交货明细
        /// <summary>
        /// 交货明细主键FDetailID
        /// </summary>
        public long FOrderEntryPlan_FDetailID { get; set; }
        /// <summary>
        /// 交货明细序列
        /// </summary>
        public int FOrderEntryPlan_FSeq { get; set; }
        /// <summary>
        /// 仓库
        /// </summary>
        public long? FStockId { get; set; }
        /// <summary>
        /// 仓库名称
        /// </summary>
        public string FStockId__FName { get; set; }
        /// <summary>
        /// 仓库编码
        /// </summary>
        public string FStockId__FNumber { get; set; }
        /// <summary>
        /// 销售单位
        /// </summary>
        public long? FPlanUnitId { get; set; }
        /// <summary>
        /// 销售单位名称
        /// </summary>
        public string FPlanUnitId__FName { get; set; }
        /// <summary>
        /// 销售单位编码
        /// </summary>
        public string FPlanUnitId__FNumber { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public decimal? FPlanQty { get; set; }
        /// <summary>
        /// 已出库数量
        /// </summary>
        public decimal? FDeliCommitQty { get; set; }
        /// <summary>
        /// 剩余未出数量
        /// </summary>
        public decimal? FDeliRemainQty { get; set; }
        /// <summary>
        /// 交货地点
        /// </summary>
        public long? FDetailLocId { get; set; }
        /// <summary>
        /// 交货地点地点名称
        /// </summary>
        public string FDetailLocId__FName { get; set; }
        /// <summary>
        /// 交货地址
        /// </summary>
        public string? FDetailLocAddress { get; set; }
        /// <summary>
        /// 要货日期
        /// </summary>
        public DateTime? FPlanDate { get; set; }
        /// <summary>
        /// 计划发货日期
        /// </summary>
        public DateTime? FPlanDeliveryDate { get; set; }
        /// <summary>
        /// 运输提前期
        /// </summary>
        public int? FTransportLeadTime { get; set; }
        /// <summary>
        /// 数量（基本单位）
        /// </summary>
        public decimal? FBasePlanQty { get; set; }
        /// <summary>
        /// 已出库数量计划（基本单位）
        /// </summary>
        public decimal? FBaseDeliCommitQty { get; set; }
        /// <summary>
        /// 剩余未出数量计划（基本单位）
        /// </summary>
        public decimal? FBaseDeliRemainQty { get; set; }
        /// <summary>
        /// 基本单位
        /// </summary>
        public long? FPlanBaseUnitId { get; set; }
        /// <summary>
        /// 基本单位名称
        /// </summary>
        public string FPlanBaseUnitId__FName { get; set; }
        /// <summary>
        /// 基本单位编码
        /// </summary>
        public string FPlanBaseUnitId__FNumber { get; set; }
        /// <summary>
        /// 已发货通知数量
        /// </summary>
        public decimal? FNOTICEQTY { get; set; }
        /// <summary>
        /// 剩余未通知数量
        /// </summary>
        public decimal? FNOTICEREMAINQTY { get; set; }
        /// <summary>
        /// 已发货通知数量（基本）
        /// </summary>
        public decimal? FNOTICEBASEQTY { get; set; }
        /// <summary>
        /// 剩余未通知数量（基本）
        /// </summary>
        public decimal? FNOTICEREMAINBASEQTY { get; set; }
        /// <summary>
        /// 变更单主键
        /// </summary>
        public int? FXPKID_D { get; set; }
        #endregion
        #region 财务信息
        /// <summary>
        /// 本位币
        /// </summary>
        public long? FLocalCurrId { get; set; }
        /// <summary>
        /// 本位币名称
        /// </summary>
        public string FLocalCurrId__FName { get; set; }
        /// <summary>
        /// 本位币编码
        /// </summary>
        public string FLocalCurrId__FNumber { get; set; }
        /// <summary>
        /// 汇率类型
        /// </summary>
        public long? FExchangeTypeId { get; set; }
        /// <summary>
        /// 汇率类型名称
        /// </summary>
        public string FExchangeTypeId__FName { get; set; }
        /// <summary>
        /// 汇率类型编码
        /// </summary>
        public string FExchangeTypeId__FNumber { get; set; }
        /// <summary>
        /// 汇率
        /// </summary>
        public decimal? FExchangeRate { get; set; }
        /// <summary>
        /// 预收比例%
        /// </summary>
        public decimal? FPayAdvanceRate { get; set; }
        /// <summary>
        /// 预收金额
        /// </summary>
        public decimal? FPayAdvanceAmount { get; set; }
        /// <summary>
        /// 折扣表
        /// </summary>
        public long? FDiscountListId { get; set; }
        /// <summary>
        /// 折扣表名称
        /// </summary>
        public string FDiscountListId__FName { get; set; }
        /// <summary>
        /// 折扣表编码
        /// </summary>
        public string FDiscountListId__FNumber { get; set; }
        /// <summary>
        /// 结算币别
        /// </summary>
        public long FSettleCurrId { get; set; }
        /// <summary>
        /// 结算币别名称
        /// </summary>
        public string FSettleCurrId__FName { get; set; }
        /// <summary>
        /// 结算币别编码
        /// </summary>
        public string FSettleCurrId__FNumber { get; set; }
        /// <summary>
        /// 税额（本位币）
        /// </summary>
        public decimal? FBillTaxAmount_LC { get; set; }
        /// <summary>
        /// 金额（本位币）
        /// </summary>
        public decimal? FBillAmount_LC { get; set; }
        /// <summary>
        /// 价税合计
        /// </summary>
        public decimal? FBillAllAmount { get; set; }
        /// <summary>
        /// 价税合计（本位币）
        /// </summary>
        public decimal? FBillAllAmount_LC { get; set; }
        /// <summary>
        /// 价目表
        /// </summary>
        public long? FPriceListId { get; set; }
        /// <summary>
        /// 价目表名称
        /// </summary>
        public string FPriceListId__FName { get; set; }
        /// <summary>
        /// 价目表编号
        /// </summary>
        public string FPriceListId__FNumber { get; set; }
        /// <summary>
        /// 税额
        /// </summary>
        public decimal? FBillTaxAmount { get; set; }
        /// <summary>
        /// 金额
        /// </summary>
        public decimal? FBillAmount { get; set; }
        /// <summary>
        /// 是否含税
        /// </summary>
        public string? FIsIncludedTax { get; set; }
        /// <summary>
        /// 需要预收
        /// </summary>
        public string? FNeedPayAdvance { get; set; }
        /// <summary>
        /// 收款单号
        /// </summary>
        public long? FRecBillId { get; set; }
        /// <summary>
        /// 收款单号收款单号
        /// </summary>
        public string FRecBillId__FNumber { get; set; }
        /// <summary>
        /// 收款单号名称
        /// </summary>
        public string FRecBillId__FName { get; set; }
        /// <summary>
        /// 收款条件
        /// </summary>
        public long? FRecConditionId { get; set; }
        /// <summary>
        /// 收款条件名称
        /// </summary>
        public string FRecConditionId__FName { get; set; }
        /// <summary>
        /// 收款条件编码
        /// </summary>
        public string FRecConditionId__FNumber { get; set; }
        /// <summary>
        /// 结算方式
        /// </summary>
        public long? FSettleModeId { get; set; }
        /// <summary>
        /// 结算方式名称
        /// </summary>
        public string FSettleModeId__FName { get; set; }
        /// <summary>
        /// 结算方式编码
        /// </summary>
        public string FSettleModeId__FNumber { get; set; }
        /// <summary>
        /// 关联应收金额（出库）
        /// </summary>
        public decimal? FJoinStockAmount { get; set; }
        /// <summary>
        /// 关联应收金额（订单）
        /// </summary>
        public decimal? FJoinOrderAmount { get; set; }
        /// <summary>
        /// 工作流信用检查状态
        /// </summary>
        public string? FCreChkStatus { get; set; }
        /// <summary>
        /// 工作流信用超标天数
        /// </summary>
        public int? FCreChkDays { get; set; }
        /// <summary>
        /// 工作流信用超标金额
        /// </summary>
        public decimal? FCreChkAmount { get; set; }
        /// <summary>
        /// 审批流信用压批月结检查
        /// </summary>
        public string? FCrePreBatAndMonStatus { get; set; }
        /// <summary>
        /// 信用压批超标
        /// </summary>
        public string? FCrePreBatchOver { get; set; }
        /// <summary>
        /// 信用月结超标
        /// </summary>
        public string? FCreMonControlOver { get; set; }
        /// <summary>
        /// 价外税
        /// </summary>
        public string? FIsPriceExcludeTax { get; set; }
        /// <summary>
        /// 保证金比例（%）
        /// </summary>
        public decimal? FMarginLevel { get; set; }
        /// <summary>
        /// 关联保证金
        /// </summary>
        public decimal? FAssociateMargin { get; set; }
        /// <summary>
        /// 保证金
        /// </summary>
        public decimal? FMargin { get; set; }
        /// <summary>
        /// 关联退款保证金
        /// </summary>
        public decimal? FAssRefundMargin { get; set; }
        /// <summary>
        /// 工作流信用逾期超标额度
        /// </summary>
        public decimal? FCreChkOverAmount { get; set; }
        /// <summary>
        /// 寄售生成跨组织调拨
        /// </summary>
        public string? FOverOrgTransDirect { get; set; }
        /// <summary>
        /// 收款通知单号
        /// </summary>
        public string? FRecNoticeNo { get; set; }
        /// <summary>
        /// 收款二维码链接
        /// </summary>
        public string? FRecBarcodeLink { get; set; }
        /// <summary>
        /// 整单折扣额
        /// </summary>
        public decimal? FAllDisCount { get; set; }
        /// <summary>
        /// 变更单主键
        /// </summary>
        public int? FXPKID_F { get; set; }
        #endregion
        #region 税务明细
        /// <summary>
        /// 税务明细主键FDetailID
        /// </summary>
        public long FTaxDetailSubEntity_FDetailID { get; set; }
        /// <summary>
        /// 税务明细序列
        /// </summary>
        public int FTaxDetailSubEntity_FSeq { get; set; }
        /// <summary>
        /// 税率名称
        /// </summary>
        public long? FTaxRateId { get; set; }
        /// <summary>
        /// 税率名称名称
        /// </summary>
        public string FTaxRateId__FName { get; set; }
        /// <summary>
        /// 税率名称编码
        /// </summary>
        public string FTaxRateId__FNumber { get; set; }
        /// <summary>
        /// 税率%
        /// </summary>
        public decimal? FTaxRate { get; set; }
        /// <summary>
        /// 税额
        /// </summary>
        public decimal? FTaxAmount { get; set; }
        /// <summary>
        /// 计入成本比例%
        /// </summary>
        public decimal? FCostPercent { get; set; }
        /// <summary>
        /// 计入成本金额
        /// </summary>
        public decimal? FCostAmount { get; set; }
        /// <summary>
        /// 增值税
        /// </summary>
        public string? FVAT { get; set; }
        /// <summary>
        /// 买方代扣代缴
        /// </summary>
        public string? FBuyerWithholding { get; set; }
        /// <summary>
        /// 卖方代扣代缴
        /// </summary>
        public string? FSellerWithholding { get; set; }
        #endregion
        #region 收款计划
        /// <summary>
        /// 收款计划主键FEntryID
        /// </summary>
        public long FSaleOrderPlan_FEntryID { get; set; }
        /// <summary>
        /// 收款计划序列
        /// </summary>
        public int FSaleOrderPlan_FSEQ { get; set; }
        /// <summary>
        /// 是否预收
        /// </summary>
        public string? FNeedRecAdvance { get; set; }
        /// <summary>
        /// 应收比例(%)
        /// </summary>
        public decimal? FRecAdvanceRate { get; set; }
        /// <summary>
        /// 到期日
        /// </summary>
        public DateTime? FMustDate { get; set; }
        /// <summary>
        /// 应收金额
        /// </summary>
        public decimal? FRecAdvanceAmount { get; set; }
        /// <summary>
        /// 实收金额
        /// </summary>
        public decimal? FRecAmount { get; set; }
        /// <summary>
        /// 关联单号
        /// </summary>
        public string? FRelBillNo { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string? FReMark { get; set; }
        /// <summary>
        /// 收款类型
        /// </summary>
        public string? FReceiveType { get; set; }
        /// <summary>
        /// 收款类型编码
        /// </summary>
        public string FReceiveType__FNumber { get; set; }
        /// <summary>
        /// 控制环节
        /// </summary>
        public string? FControlSend { get; set; }
        /// <summary>
        /// 物料编码
        /// </summary>
        public long? FPlanMaterialId { get; set; }
        /// <summary>
        /// 物料编码名称
        /// </summary>
        public string FPlanMaterialId__FName { get; set; }
        /// <summary>
        /// 物料编码编码
        /// </summary>
        public string FPlanMaterialId__FNumber { get; set; }
        /// <summary>
        /// 物料行号
        /// </summary>
        public int? FMaterialSeq { get; set; }
        /// <summary>
        /// 订单明细行内码
        /// </summary>
        public int? FOrderEntryId { get; set; }
        /// <summary>
        /// 关联金额
        /// </summary>
        public decimal? FASSAMOUNTFOR { get; set; }
        /// <summary>
        /// 关联扣减金额
        /// </summary>
        public decimal? FASSDEDAMOUNTFOR { get; set; }
        /// <summary>
        /// 按实际预收控制发货
        /// </summary>
        public string? FIsOutStockByRecamount { get; set; }
        /// <summary>
        /// 物料行标识
        /// </summary>
        public string? FMaterialRowID { get; set; }
        /// <summary>
        /// 聚合收款金额
        /// </summary>
        public decimal? FAGGRecAmount { get; set; }
        /// <summary>
        /// 含税单价
        /// </summary>
        public decimal? FMaterialTaxPrice { get; set; }
        /// <summary>
        /// 计价数量
        /// </summary>
        public decimal? FMaterialPriceUnitQty { get; set; }
        /// <summary>
        /// 计价单位
        /// </summary>
        public long? FMaterialPriceUnitID { get; set; }
        /// <summary>
        /// 计价单位名称
        /// </summary>
        public string FMaterialPriceUnitID__FName { get; set; }
        /// <summary>
        /// 计价单位编码
        /// </summary>
        public string FMaterialPriceUnitID__FNumber { get; set; }
        /// <summary>
        /// 预收超收金额
        /// </summary>
        public decimal? FOverRecAmount { get; set; }
        /// <summary>
        /// 变更单主键
        /// </summary>
        public int? FXPKID_R { get; set; }
        #endregion
        #region 收款计划子单据体
        /// <summary>
        /// 收款计划子单据体主键FDETAILID
        /// </summary>
        public long FSaleOrderPlanEntry_FDETAILID { get; set; }
        /// <summary>
        /// 收款计划子单据体序列
        /// </summary>
        public int FSaleOrderPlanEntry_FSEQ { get; set; }
        /// <summary>
        /// 实收金额
        /// </summary>
        public decimal? FActRecAmount { get; set; }
        /// <summary>
        /// 预收单号
        /// </summary>
        public string? FADVANCENO { get; set; }
        /// <summary>
        /// 预收单分录行号
        /// </summary>
        public string? FADVANCESEQ { get; set; }
        /// <summary>
        /// 预收单未关联金额
        /// </summary>
        public decimal? FRemainAmount { get; set; }
        /// <summary>
        /// 预收单分录内码
        /// </summary>
        public int? FADVANCEENTRYID { get; set; }
        /// <summary>
        /// 预收单内码
        /// </summary>
        public int? FADVANCEID { get; set; }
        /// <summary>
        /// 预收已核销金额
        /// </summary>
        public decimal? FPreMatchAmountFor { get; set; }
        /// <summary>
        /// 结算组织
        /// </summary>
        public long? FPESettleOrgId { get; set; }
        /// <summary>
        /// 结算组织名称
        /// </summary>
        public string FPESettleOrgId__FName { get; set; }
        /// <summary>
        /// 结算组织编码
        /// </summary>
        public string FPESettleOrgId__FNumber { get; set; }
        /// <summary>
        /// 预收超收金额
        /// </summary>
        public decimal? FOverRecAmount_D { get; set; }
        /// <summary>
        /// 变更单主键
        /// </summary>
        public int? FXPKID_RE { get; set; }
        #endregion
        #region 物流跟踪明细
        /// <summary>
        /// 物流跟踪明细主键FEntryID
        /// </summary>
        public long FSalOrderTrace_FEntryID { get; set; }
        /// <summary>
        /// 物流跟踪明细序列
        /// </summary>
        public int FSalOrderTrace_FSEQ { get; set; }
        /// <summary>
        /// 物流公司
        /// </summary>
        public long? FLogComId { get; set; }
        /// <summary>
        /// 物流公司快递公司名称
        /// </summary>
        public string FLogComId__FName { get; set; }
        /// <summary>
        /// 物流单号
        /// </summary>
        public string FCarryBillNo { get; set; }
        /// <summary>
        /// 物流状态
        /// </summary>
        public string? FTraceStatus { get; set; }
        /// <summary>
        /// 发货时间
        /// </summary>
        public DateTime? FDelTime { get; set; }
        /// <summary>
        /// 寄件人手机号码
        /// </summary>
        public string? FPhoneNumber { get; set; }
        /// <summary>
        /// 起始地点
        /// </summary>
        public string? FFrom { get; set; }
        /// <summary>
        /// 终止地点
        /// </summary>
        public string? FTo { get; set; }
        /// <summary>
        /// 签收时间
        /// </summary>
        public DateTime? FReceiptTime { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        public string FCarryBillNoType { get; set; }
        #endregion
        #region 物流详细信息
        /// <summary>
        /// 物流详细信息主键FDetailID
        /// </summary>
        public long FSalOrderTraceDetail_FDetailID { get; set; }
        /// <summary>
        /// 物流详细信息序列
        /// </summary>
        public int FSalOrderTraceDetail_FSEQ { get; set; }
        /// <summary>
        /// 时间
        /// </summary>
        public string? FTraceTime { get; set; }
        /// <summary>
        /// 物流详情
        /// </summary>
        public string? FTraceDetail { get; set; }
        #endregion
        #region 收款执行
        /// <summary>
        /// 未收款金额
        /// </summary>
        public decimal? FPLANNOTRECAMOUNT { get; set; }
        /// <summary>
        /// 收款金额（关联）
        /// </summary>
        public decimal? FPLANRECAMOUNT { get; set; }
        /// <summary>
        /// 累计收款金额
        /// </summary>
        public decimal? FPLANALLRECAMOUNT { get; set; }
        /// <summary>
        /// 收款单非关联核销金额
        /// </summary>
        public decimal? FMATCHRECAMOUNT { get; set; }
        /// <summary>
        /// 应收单非关联核销金额
        /// </summary>
        public decimal? FARMATCHRECAMOUNT { get; set; }
        /// <summary>
        /// 累计退款金额
        /// </summary>
        public decimal? FPLANREFUNDAMOUNT { get; set; }
        #endregion
        #region 收款执行明细
        /// <summary>
        /// 关联单据
        /// </summary>
        public long? FORMID { get; set; }
        /// <summary>
        /// 关联单据名称
        /// </summary>
        public string FORMID__FName { get; set; }
        /// <summary>
        /// 关联单据编码
        /// </summary>
        public string FORMID__FNumber { get; set; }
        /// <summary>
        /// 关联单据编号
        /// </summary>
        public string? FRELATBILLNO { get; set; }
        /// <summary>
        /// 业务日期
        /// </summary>
        public DateTime? FRELATDATE { get; set; }
        /// <summary>
        /// 收款金额
        /// </summary>
        public decimal? FENTRYRECAMOUNT { get; set; }
        /// <summary>
        /// 累计退款金额
        /// </summary>
        public decimal? FENTRYREFUNDAMT { get; set; }
        /// <summary>
        /// 核销记录的单据
        /// </summary>
        public string? FTYPE { get; set; }
        #endregion
    }
}