using System;

namespace FormEntityCreation.DataEntity
{
    /// <summary>
    /// 部门
    /// </summary>
    public class BD_Department
    {
        /// <summary>
        /// FDEPTID
        /// </summary>
        public int FDEPTID { get; set; }
        #region 单据头
        /// <summary>
        /// 数据状态
        /// </summary>
        public string? FDocumentStatus { get; set; }
        /// <summary>
        /// 禁用状态
        /// </summary>
        public string? FForbidStatus { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string FName { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        public string? FNumber { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string? FDescription { get; set; }
        /// <summary>
        /// 创建组织
        /// </summary>
        public long FCreateOrgId { get; set; }
        /// <summary>
        /// 创建组织名称
        /// </summary>
        public string FCreateOrgId__FName { get; set; }
        /// <summary>
        /// 创建组织编码
        /// </summary>
        public string FCreateOrgId__FNumber { get; set; }
        /// <summary>
        /// 使用组织
        /// </summary>
        public long FUseOrgId { get; set; }
        /// <summary>
        /// 使用组织名称
        /// </summary>
        public string FUseOrgId__FName { get; set; }
        /// <summary>
        /// 使用组织编码
        /// </summary>
        public string FUseOrgId__FNumber { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public long? FCreatorId { get; set; }
        /// <summary>
        /// 创建人用户名称
        /// </summary>
        public string FCreatorId__FName { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        public long? FModifierId { get; set; }
        /// <summary>
        /// 修改人用户名称
        /// </summary>
        public string FModifierId__FName { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime? FCreateDate { get; set; }
        /// <summary>
        /// 修改日期
        /// </summary>
        public DateTime? FModifyDate { get; set; }
        /// <summary>
        /// 助记码
        /// </summary>
        public string? FHelpCode { get; set; }
        /// <summary>
        /// 部门全称
        /// </summary>
        public string? FFullName { get; set; }
        /// <summary>
        /// 生效日期
        /// </summary>
        public DateTime? FEffectDate { get; set; }
        /// <summary>
        /// 失效日期
        /// </summary>
        public DateTime? FLapseDate { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        public long? FAuditorID { get; set; }
        /// <summary>
        /// 审核人用户名称
        /// </summary>
        public string FAuditorID__FName { get; set; }
        /// <summary>
        /// 审核日期
        /// </summary>
        public DateTime? FAuditDate { get; set; }
        /// <summary>
        /// 上级部门
        /// </summary>
        public long? FParentID { get; set; }
        /// <summary>
        /// 上级部门名称
        /// </summary>
        public string FParentID__FName { get; set; }
        /// <summary>
        /// 上级部门编码
        /// </summary>
        public string FParentID__FNumber { get; set; }
        /// <summary>
        /// 禁用人
        /// </summary>
        public long? FForbidderID { get; set; }
        /// <summary>
        /// 禁用人用户名称
        /// </summary>
        public string FForbidderID__FName { get; set; }
        /// <summary>
        /// 禁用日期
        /// </summary>
        public DateTime? FForbidDate { get; set; }
        /// <summary>
        /// HR部门
        /// </summary>
        public string? FIsHRDept { get; set; }
        /// <summary>
        /// 车间
        /// </summary>
        public string? FIsStock { get; set; }
        /// <summary>
        /// WIP仓库
        /// </summary>
        public long? FWIPStockID { get; set; }
        /// <summary>
        /// WIP仓库名称
        /// </summary>
        public string FWIPStockID__FName { get; set; }
        /// <summary>
        /// WIP仓库编码
        /// </summary>
        public string FWIPStockID__FNumber { get; set; }
        /// <summary>
        /// WIP仓位
        /// </summary>
        public long? FWIPLocationID { get; set; }
        /// <summary>
        /// 层级码
        /// </summary>
        public string? FLevelCode { get; set; }
        /// <summary>
        /// 深度
        /// </summary>
        public int? FDepth { get; set; }
        /// <summary>
        /// 是否根节点
        /// </summary>
        public string? FIsRoot { get; set; }
        /// <summary>
        /// 部门属性
        /// </summary>
        public string? FDeptProperty { get; set; }
        /// <summary>
        /// 部门属性编码
        /// </summary>
        public string FDeptProperty__FNumber { get; set; }
        /// <summary>
        /// 部门属性名称
        /// </summary>
        public string FDeptProperty__FDataValue { get; set; }
        /// <summary>
        /// 部门分组
        /// </summary>
        public long? FGroup { get; set; }
        /// <summary>
        /// 来源于s-HR
        /// </summary>
        public string? FIsSHR { get; set; }
        /// <summary>
        /// 副产品倒冲
        /// </summary>
        public string? FIsCopyFlush { get; set; }
        /// <summary>
        /// 更新已排
        /// </summary>
        public string? FFinishQtyDepend { get; set; }
        /// <summary>
        /// 是否明细部门
        /// </summary>
        public string? FIsDetailDpt { get; set; }
        #endregion
        #region SHR映射实体
        /// <summary>
        /// SHR内码
        /// </summary>
        public string? FShrId { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        public string? FSHRNumber { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string? FSHRName { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public string? FSHRStatus { get; set; }
        /// <summary>
        /// 所属ERP组织
        /// </summary>
        public string? FErpCode { get; set; }
        #endregion
    }
}