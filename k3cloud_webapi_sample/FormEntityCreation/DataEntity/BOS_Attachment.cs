using System;

namespace FormEntityCreation.DataEntity
{
    /// <summary>
    /// 附件明细
    /// </summary>
    public class BOS_Attachment
    {
        /// <summary>
        /// FID
        /// </summary>
        public int FID { get; set; }
        #region 单据头
        /// <summary>
        /// 单据体标识
        /// </summary>
        public string? FEntryKey { get; set; }
        /// <summary>
        /// 单据内码
        /// </summary>
        public string? FInterID { get; set; }
        /// <summary>
        /// 单据体内码
        /// </summary>
        public string? FEntryInterID { get; set; }
        /// <summary>
        /// 单据编号
        /// </summary>
        public string? FBillNo { get; set; }
        /// <summary>
        /// 单据类型
        /// </summary>
        public string? FBillType { get; set; }
        /// <summary>
        /// 文件名
        /// </summary>
        public string? FAttachmentName { get; set; }
        /// <summary>
        /// 大小(KB)
        /// </summary>
        public string? FAttachmentSize { get; set; }
        /// <summary>
        /// 说明
        /// </summary>
        public string? FAttachmentDes { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? FCreateTime { get; set; }
        /// <summary>
        /// 审核时间
        /// </summary>
        public DateTime? FAuditTime { get; set; }
        /// <summary>
        /// 单据状态
        /// </summary>
        public string? FBillStatus { get; set; }
        /// <summary>
        /// 附件内容
        /// </summary>
        public byte[]? FAttachment { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        public long? FAuditMen { get; set; }
        /// <summary>
        /// 审核人用户名称
        /// </summary>
        public string FAuditMen__FName { get; set; }
        /// <summary>
        /// 别名
        /// </summary>
        public string? FaliasFileName { get; set; }
        /// <summary>
        /// 文件类型
        /// </summary>
        public string? FExtName { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        public long? FModifyMen { get; set; }
        /// <summary>
        /// 修改人用户名称
        /// </summary>
        public string FModifyMen__FName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? FModifyTime { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public long? FCreateMen { get; set; }
        /// <summary>
        /// 创建人用户名称
        /// </summary>
        public string FCreateMen__FName { get; set; }
        /// <summary>
        /// 文件服务器文件信息标识
        /// </summary>
        public string? FFileId { get; set; }
        /// <summary>
        /// 存储位置
        /// </summary>
        public string? FFileStorage { get; set; }
        /// <summary>
        /// 禁止下载
        /// </summary>
        public string? FIsAllowDownLoad { get; set; }
        /// <summary>
        /// 缩略图编码
        /// </summary>
        public string? FThumbnailId { get; set; }
        /// <summary>
        /// 来源内码
        /// </summary>
        public int? FSourceId { get; set; }
        #endregion
    }
}