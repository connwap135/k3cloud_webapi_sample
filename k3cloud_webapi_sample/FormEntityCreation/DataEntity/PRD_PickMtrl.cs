using System;

namespace FormEntityCreation.DataEntity
{
    /// <summary>
    /// 生产领料单
    /// </summary>
    public class PRD_PickMtrl
    {
        /// <summary>
        /// FID
        /// </summary>
        public int FID { get; set; }
        #region 单据头
        /// <summary>
        /// 单据编号
        /// </summary>
        public string? FBillNo { get; set; }
        /// <summary>
        /// 单据状态
        /// </summary>
        public string? FDocumentStatus { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        public long? FApproverId { get; set; }
        /// <summary>
        /// 审核人用户名称
        /// </summary>
        public string FApproverId__FName { get; set; }
        /// <summary>
        /// 审核日期
        /// </summary>
        public DateTime? FApproveDate { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        public long? FModifierId { get; set; }
        /// <summary>
        /// 修改人用户名称
        /// </summary>
        public string FModifierId__FName { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime? FCreateDate { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public long? FCreatorId { get; set; }
        /// <summary>
        /// 创建人用户名称
        /// </summary>
        public string FCreatorId__FName { get; set; }
        /// <summary>
        /// 修改日期
        /// </summary>
        public DateTime? FModifyDate { get; set; }
        /// <summary>
        /// 作废日期
        /// </summary>
        public DateTime? FCancelDate { get; set; }
        /// <summary>
        /// 作废人
        /// </summary>
        public long? FCanceler { get; set; }
        /// <summary>
        /// 作废人用户名称
        /// </summary>
        public string FCanceler__FName { get; set; }
        /// <summary>
        /// 作废状态
        /// </summary>
        public string? FCancelStatus { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string? FDescription { get; set; }
        /// <summary>
        /// 日期
        /// </summary>
        public DateTime FDate { get; set; }
        /// <summary>
        /// 生产组织
        /// </summary>
        public long FPrdOrgId { get; set; }
        /// <summary>
        /// 生产组织名称
        /// </summary>
        public string FPrdOrgId__FName { get; set; }
        /// <summary>
        /// 生产组织编码
        /// </summary>
        public string FPrdOrgId__FNumber { get; set; }
        /// <summary>
        /// 发料组织
        /// </summary>
        public long FStockOrgId { get; set; }
        /// <summary>
        /// 发料组织名称
        /// </summary>
        public string FStockOrgId__FName { get; set; }
        /// <summary>
        /// 发料组织编码
        /// </summary>
        public string FStockOrgId__FNumber { get; set; }
        /// <summary>
        /// 单据类型
        /// </summary>
        public string FBillType { get; set; }
        /// <summary>
        /// 单据类型名称
        /// </summary>
        public string FBillType__FName { get; set; }
        /// <summary>
        /// 单据类型编码
        /// </summary>
        public string FBillType__FNumber { get; set; }
        /// <summary>
        /// 货主类型
        /// </summary>
        public string? FOwnerTypeId0 { get; set; }
        /// <summary>
        /// 货主
        /// </summary>
        public int? FOwnerId0 { get; set; }
        /// <summary>
        /// 货主名称
        /// </summary>
        public string FOwnerId0__FName { get; set; }
        /// <summary>
        /// 货主编码
        /// </summary>
        public string FOwnerId0__FNumber { get; set; }
        /// <summary>
        /// 本位币
        /// </summary>
        public long? FCurrId { get; set; }
        /// <summary>
        /// 本位币名称
        /// </summary>
        public string FCurrId__FName { get; set; }
        /// <summary>
        /// 本位币编码
        /// </summary>
        public string FCurrId__FNumber { get; set; }
        /// <summary>
        /// 跨组织业务类型
        /// </summary>
        public long? FTransferBizType { get; set; }
        /// <summary>
        /// 跨组织业务类型名称
        /// </summary>
        public string FTransferBizType__FName { get; set; }
        /// <summary>
        /// 跨组织业务类型编码
        /// </summary>
        public string FTransferBizType__FNumber { get; set; }
        /// <summary>
        /// 领料人
        /// </summary>
        public long? FPickerId { get; set; }
        /// <summary>
        /// 领料人员工编码
        /// </summary>
        public string FPickerId__FNumber { get; set; }
        /// <summary>
        /// 领料人员工名称
        /// </summary>
        public string FPickerId__FName { get; set; }
        /// <summary>
        /// 仓管员
        /// </summary>
        public long? FSTOCKERID { get; set; }
        /// <summary>
        /// 仓管员名称
        /// </summary>
        public string FSTOCKERID__FName { get; set; }
        /// <summary>
        /// 仓管员职员编码
        /// </summary>
        public string FSTOCKERID__FNumber { get; set; }
        /// <summary>
        /// 跨法人交易
        /// </summary>
        public string? FIsCrossTrade { get; set; }
        /// <summary>
        /// VMI业务
        /// </summary>
        public string? FVmiBusiness { get; set; }
        /// <summary>
        /// 倒冲来源
        /// </summary>
        public string? FSourceType { get; set; }
        /// <summary>
        /// 货主含组织
        /// </summary>
        public string? FIsOwnerTInclOrg { get; set; }
        /// <summary>
        /// 库存刷新日期
        /// </summary>
        public DateTime? FInventoryDate { get; set; }
        #endregion
        #region 明细
        /// <summary>
        /// 明细主键FEntryID
        /// </summary>
        public long FEntity_FEntryID { get; set; }
        /// <summary>
        /// 明细序列
        /// </summary>
        public int FEntity_FSeq { get; set; }
        /// <summary>
        /// 物料编码
        /// </summary>
        public long FMaterialId { get; set; }
        /// <summary>
        /// 物料编码名称
        /// </summary>
        public string FMaterialId__FName { get; set; }
        /// <summary>
        /// 物料编码编码
        /// </summary>
        public string FMaterialId__FNumber { get; set; }
        /// <summary>
        /// BOM版本
        /// </summary>
        public long? FBomId { get; set; }
        /// <summary>
        /// BOM版本BOM简称
        /// </summary>
        public string FBomId__FName { get; set; }
        /// <summary>
        /// BOM版本BOM版本
        /// </summary>
        public string FBomId__FNumber { get; set; }
        /// <summary>
        /// 仓库
        /// </summary>
        public long FStockId { get; set; }
        /// <summary>
        /// 仓库名称
        /// </summary>
        public string FStockId__FName { get; set; }
        /// <summary>
        /// 仓库编码
        /// </summary>
        public string FStockId__FNumber { get; set; }
        /// <summary>
        /// 仓位
        /// </summary>
        public long? FStockLocId { get; set; }
        /// <summary>
        /// 库存状态
        /// </summary>
        public long FStockStatusId { get; set; }
        /// <summary>
        /// 库存状态名称
        /// </summary>
        public string FStockStatusId__FName { get; set; }
        /// <summary>
        /// 库存状态编码
        /// </summary>
        public string FStockStatusId__FNumber { get; set; }
        /// <summary>
        /// 生产日期
        /// </summary>
        public DateTime? FProduceDate { get; set; }
        /// <summary>
        /// 计划跟踪号
        /// </summary>
        public string? FMtoNo { get; set; }
        /// <summary>
        /// 项目编号
        /// </summary>
        public string? FProjectNo { get; set; }
        /// <summary>
        /// 生产订单编号
        /// </summary>
        public string FMoBillNo { get; set; }
        /// <summary>
        /// 生产订单分录内码
        /// </summary>
        public int? FMoEntryId { get; set; }
        /// <summary>
        /// 用料清单分录内码
        /// </summary>
        public int? FPPBomEntryId { get; set; }
        /// <summary>
        /// 工序号
        /// </summary>
        public int? FOperId { get; set; }
        /// <summary>
        /// 作业
        /// </summary>
        public long? FProcessId { get; set; }
        /// <summary>
        /// 作业名称
        /// </summary>
        public string FProcessId__FName { get; set; }
        /// <summary>
        /// 作业编码
        /// </summary>
        public string FProcessId__FNumber { get; set; }
        /// <summary>
        /// 货主类型
        /// </summary>
        public string FOwnerTypeId { get; set; }
        /// <summary>
        /// 申请数量
        /// </summary>
        public decimal? FAppQty { get; set; }
        /// <summary>
        /// 实发数量
        /// </summary>
        public decimal? FActualQty { get; set; }
        /// <summary>
        /// 主库存单位申请数量
        /// </summary>
        public decimal? FStockAppQty { get; set; }
        /// <summary>
        /// 主库存单位实发数量
        /// </summary>
        public decimal? FStockActualQty { get; set; }
        /// <summary>
        /// 辅库存单位实发数量
        /// </summary>
        public decimal? FSecActualQty { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string? FEntrtyMemo { get; set; }
        /// <summary>
        /// 生产订单内码
        /// </summary>
        public int? FMoId { get; set; }
        /// <summary>
        /// 生产订单行号
        /// </summary>
        public int? FMoEntrySeq { get; set; }
        /// <summary>
        /// 基本单位申请数量
        /// </summary>
        public decimal? FBaseAppQty { get; set; }
        /// <summary>
        /// 可超发数量
        /// </summary>
        public decimal? FAllowOverQty { get; set; }
        /// <summary>
        /// 主库存单位可超发数量
        /// </summary>
        public decimal? FStockAllowOverQty { get; set; }
        /// <summary>
        /// 辅库存单位可超发数量
        /// </summary>
        public decimal? FSecAllowOverQty { get; set; }
        /// <summary>
        /// 基本单位可超发数量
        /// </summary>
        public decimal? FBaseAllowOverQty { get; set; }
        /// <summary>
        /// 用料清单编号
        /// </summary>
        public string? FPPBomBillNo { get; set; }
        /// <summary>
        /// 单位
        /// </summary>
        public long FUnitID { get; set; }
        /// <summary>
        /// 单位名称
        /// </summary>
        public string FUnitID__FName { get; set; }
        /// <summary>
        /// 单位编码
        /// </summary>
        public string FUnitID__FNumber { get; set; }
        /// <summary>
        /// 基本单位
        /// </summary>
        public long FBaseUnitId { get; set; }
        /// <summary>
        /// 基本单位名称
        /// </summary>
        public string FBaseUnitId__FName { get; set; }
        /// <summary>
        /// 基本单位编码
        /// </summary>
        public string FBaseUnitId__FNumber { get; set; }
        /// <summary>
        /// 主库存单位
        /// </summary>
        public long FStockUnitId { get; set; }
        /// <summary>
        /// 主库存单位名称
        /// </summary>
        public string FStockUnitId__FName { get; set; }
        /// <summary>
        /// 主库存单位编码
        /// </summary>
        public string FStockUnitId__FNumber { get; set; }
        /// <summary>
        /// 辅库存单位
        /// </summary>
        public long? FSecUnitId { get; set; }
        /// <summary>
        /// 辅库存单位名称
        /// </summary>
        public string FSecUnitId__FName { get; set; }
        /// <summary>
        /// 辅库存单位编码
        /// </summary>
        public string FSecUnitId__FNumber { get; set; }
        /// <summary>
        /// 车间
        /// </summary>
        public long? FEntryWorkShopId { get; set; }
        /// <summary>
        /// 车间名称
        /// </summary>
        public string FEntryWorkShopId__FName { get; set; }
        /// <summary>
        /// 车间编码
        /// </summary>
        public string FEntryWorkShopId__FNumber { get; set; }
        /// <summary>
        /// 退料选单数量
        /// </summary>
        public decimal? FSelPrcdReturnQty { get; set; }
        /// <summary>
        /// 基本单位退料选单数量
        /// </summary>
        public decimal? FBaseSelPrcdReturnQty { get; set; }
        /// <summary>
        /// 主库存单位退料选单数量
        /// </summary>
        public decimal? FStockSelPrcdReturnQty { get; set; }
        /// <summary>
        /// 辅库存单位退料选单数量
        /// </summary>
        public decimal? FSecSelPrcdReturnQty { get; set; }
        /// <summary>
        /// 基本单位实发数量
        /// </summary>
        public decimal? FBaseActualQty { get; set; }
        /// <summary>
        /// 辅助属性
        /// </summary>
        public long? FAuxPropId { get; set; }
        /// <summary>
        /// 保管者类型
        /// </summary>
        public string FKeeperTypeId { get; set; }
        /// <summary>
        /// 保管者
        /// </summary>
        public int FKeeperId { get; set; }
        /// <summary>
        /// 保管者名称
        /// </summary>
        public string FKeeperId__FName { get; set; }
        /// <summary>
        /// 保管者编码
        /// </summary>
        public string FKeeperId__FNumber { get; set; }
        /// <summary>
        /// 更新库存标志
        /// </summary>
        public string? FStockFlag { get; set; }
        /// <summary>
        /// 货主
        /// </summary>
        public int FOwnerId { get; set; }
        /// <summary>
        /// 货主名称
        /// </summary>
        public string FOwnerId__FName { get; set; }
        /// <summary>
        /// 货主编码
        /// </summary>
        public string FOwnerId__FNumber { get; set; }
        /// <summary>
        /// 有效期至
        /// </summary>
        public DateTime? FExpiryDate { get; set; }
        /// <summary>
        /// 系统源单类型
        /// </summary>
        public string? FSrcBillType { get; set; }
        /// <summary>
        /// 系统源单编号
        /// </summary>
        public string? FSrcBillNo { get; set; }
        /// <summary>
        /// 成本价
        /// </summary>
        public decimal? FPrice { get; set; }
        /// <summary>
        /// 总成本
        /// </summary>
        public decimal? FAmount { get; set; }
        /// <summary>
        /// 系统源单内码
        /// </summary>
        public int? FEntrySrcInterId { get; set; }
        /// <summary>
        /// 系统源单分录内码
        /// </summary>
        public int? FEntrySrcEnteryId { get; set; }
        /// <summary>
        /// 系统源单行号
        /// </summary>
        public int? FEntrySrcEntrySeq { get; set; }
        /// <summary>
        /// 批号
        /// </summary>
        public long? FLot { get; set; }
        /// <summary>
        /// 批号名称
        /// </summary>
        public string FLot__FName { get; set; }
        /// <summary>
        /// 批号批号
        /// </summary>
        public string FLot__FNumber { get; set; }
        /// <summary>
        /// 产品货主类型
        /// </summary>
        public string FParentOwnerTypeId { get; set; }
        /// <summary>
        /// 产品货主
        /// </summary>
        public int FParentOwnerId { get; set; }
        /// <summary>
        /// 产品货主名称
        /// </summary>
        public string FParentOwnerId__FName { get; set; }
        /// <summary>
        /// 产品货主编码
        /// </summary>
        public string FParentOwnerId__FNumber { get; set; }
        /// <summary>
        /// 业务源单类型
        /// </summary>
        public long? FSRCBIZBILLTYPE { get; set; }
        /// <summary>
        /// 业务源单类型名称
        /// </summary>
        public string FSRCBIZBILLTYPE__FName { get; set; }
        /// <summary>
        /// 业务源单编号
        /// </summary>
        public string? FSRCBIZBILLNO { get; set; }
        /// <summary>
        /// 业务源单内码
        /// </summary>
        public int? FSRCBIZINTERID { get; set; }
        /// <summary>
        /// 业务源单分录内码
        /// </summary>
        public int? FSRCBIZENTRYID { get; set; }
        /// <summary>
        /// 业务源单行号
        /// </summary>
        public int? FSRCBIZENTRYSEQ { get; set; }
        /// <summary>
        /// 拣货规则标识位
        /// </summary>
        public int? FPickingStatus { get; set; }
        /// <summary>
        /// 业务流程
        /// </summary>
        public string? FBFLowId { get; set; }
        /// <summary>
        /// 业务流程名称
        /// </summary>
        public string FBFLowId__FName { get; set; }
        /// <summary>
        /// 产品编码
        /// </summary>
        public long? FParentMaterialId { get; set; }
        /// <summary>
        /// 产品编码名称
        /// </summary>
        public string FParentMaterialId__FName { get; set; }
        /// <summary>
        /// 产品编码编码
        /// </summary>
        public string FParentMaterialId__FNumber { get; set; }
        /// <summary>
        /// 生产备料单据编号
        /// </summary>
        public string? FFPMBillNo { get; set; }
        /// <summary>
        /// 序列号单位
        /// </summary>
        public long? FSNUnitID { get; set; }
        /// <summary>
        /// 序列号单位名称
        /// </summary>
        public string FSNUnitID__FName { get; set; }
        /// <summary>
        /// 序列号单位编码
        /// </summary>
        public string FSNUnitID__FNumber { get; set; }
        /// <summary>
        /// 序列号单位数量
        /// </summary>
        public decimal? FSNQty { get; set; }
        /// <summary>
        /// 预留类型
        /// </summary>
        public string? FReserveType { get; set; }
        /// <summary>
        /// 主库存基本单位实发数量
        /// </summary>
        public decimal? FBaseStockActualQty { get; set; }
        /// <summary>
        /// 工序序列
        /// </summary>
        public string? FOptQueue { get; set; }
        /// <summary>
        /// 消耗汇总
        /// </summary>
        public string? FConsome { get; set; }
        /// <summary>
        /// VMI业务
        /// </summary>
        public string? FEntryVmiBusiness { get; set; }
        /// <summary>
        /// 工序计划单编号
        /// </summary>
        public string? FOptPlanBillNo { get; set; }
        /// <summary>
        /// 工序计划单内码
        /// </summary>
        public int? FOptPlanBillId { get; set; }
        /// <summary>
        /// 工作中心
        /// </summary>
        public long? FWorkCenterId { get; set; }
        /// <summary>
        /// 工作中心名称
        /// </summary>
        public string FWorkCenterId__FName { get; set; }
        /// <summary>
        /// 工作中心编码
        /// </summary>
        public string FWorkCenterId__FNumber { get; set; }
        /// <summary>
        /// 工序计划工序内码
        /// </summary>
        public int? FOptDetailId { get; set; }
        /// <summary>
        /// 联副产品BOM分录内码
        /// </summary>
        public int? FCobyBomEntryID { get; set; }
        /// <summary>
        /// 需求来源
        /// </summary>
        public string? FReqSrc { get; set; }
        /// <summary>
        /// 需求单据
        /// </summary>
        public string? FReqBillNo { get; set; }
        /// <summary>
        /// 需求单据内码
        /// </summary>
        public int? FReqBillId { get; set; }
        /// <summary>
        /// 需求单据行号
        /// </summary>
        public int? FReqEntrySeq { get; set; }
        /// <summary>
        /// 需求单据分录内码
        /// </summary>
        public int? FReqEntryId { get; set; }
        /// <summary>
        /// 组别
        /// </summary>
        public string? FGroupRow { get; set; }
        /// <summary>
        /// 业务来源类型
        /// </summary>
        public string? FSrcBusinessType { get; set; }
        /// <summary>
        /// 直送标识
        /// </summary>
        public string? FSendRowId { get; set; }
        /// <summary>
        /// 可用库存
        /// </summary>
        public decimal? FInventoryQty { get; set; }
        /// <summary>
        /// 基本单位可用库存数量
        /// </summary>
        public decimal? FBaseInventoryQty { get; set; }
        /// <summary>
        /// 转出退料单标识
        /// </summary>
        public string? FTransRetFormId { get; set; }
        /// <summary>
        /// 转出退料单号
        /// </summary>
        public string? FTransRetBillNo { get; set; }
        /// <summary>
        /// 转出退料单内码
        /// </summary>
        public int? FTransRetId { get; set; }
        /// <summary>
        /// 转出退料单分录内码
        /// </summary>
        public int? FTransRetEntryId { get; set; }
        /// <summary>
        /// 转出退料单行号
        /// </summary>
        public int? FTransRetEntrySeq { get; set; }
        /// <summary>
        /// 生产退料检验
        /// </summary>
        public string? FCheckReturnMtrl { get; set; }
        /// <summary>
        /// 基本单位生产退料请检选单数量
        /// </summary>
        public decimal? FBaseReturnAppSelQty { get; set; }
        /// <summary>
        /// 生产退料请检选单数量
        /// </summary>
        public decimal? FReturnAppSelQty { get; set; }
        /// <summary>
        /// 组织间结算跨法人标识
        /// </summary>
        public string? FIsOverLegalOrg { get; set; }
        /// <summary>
        /// 工序计划分录内码
        /// </summary>
        public int? FPlanEntryID { get; set; }
        /// <summary>
        /// 发料通知单订单明细分录内码
        /// </summary>
        public int? FISSUEINFOENTRYID { get; set; }
        #endregion
        #region 序列号子单据体
        /// <summary>
        /// 序列号子单据体主键FDetailID
        /// </summary>
        public long FSerialSubEntity_FDetailID { get; set; }
        /// <summary>
        /// 序列号子单据体序列
        /// </summary>
        public int FSerialSubEntity_FSEQ { get; set; }
        /// <summary>
        /// 序列号
        /// </summary>
        public string? FSerialNo { get; set; }
        /// <summary>
        /// 序列号
        /// </summary>
        public long? FSerialId { get; set; }
        /// <summary>
        /// 序列号序列号
        /// </summary>
        public string FSerialId__FNumber { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string? FSerialNote { get; set; }
        /// <summary>
        /// 请检关联标志
        /// </summary>
        public string FIsAppInspect { get; set; }
        #endregion
    }
}