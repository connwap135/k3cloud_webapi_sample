using System;

namespace FormEntityCreation.DataEntity
{
    /// <summary>
    /// 采购价目表
    /// </summary>
    public class PUR_PriceCategory
    {
        /// <summary>
        /// FID
        /// </summary>
        public int FID { get; set; }
        #region 基本信息
        /// <summary>
        /// 单据状态
        /// </summary>
        public string? FDocumentStatus { get; set; }
        /// <summary>
        /// 禁用
        /// </summary>
        public string? FForbidStatus { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string FName { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        public string? FNumber { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string? FDescription { get; set; }
        /// <summary>
        /// 采购组织
        /// </summary>
        public long FCreateOrgId { get; set; }
        /// <summary>
        /// 采购组织名称
        /// </summary>
        public string FCreateOrgId__FName { get; set; }
        /// <summary>
        /// 采购组织编码
        /// </summary>
        public string FCreateOrgId__FNumber { get; set; }
        /// <summary>
        /// 使用组织
        /// </summary>
        public long? FUseOrgId { get; set; }
        /// <summary>
        /// 使用组织名称
        /// </summary>
        public string FUseOrgId__FName { get; set; }
        /// <summary>
        /// 使用组织编码
        /// </summary>
        public string FUseOrgId__FNumber { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public long? FCreatorId { get; set; }
        /// <summary>
        /// 创建人用户名称
        /// </summary>
        public string FCreatorId__FName { get; set; }
        /// <summary>
        /// 最后修改人
        /// </summary>
        public long? FModifierId { get; set; }
        /// <summary>
        /// 最后修改人用户名称
        /// </summary>
        public string FModifierId__FName { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime? FCreateDate { get; set; }
        /// <summary>
        /// 最后修改日期
        /// </summary>
        public DateTime? FModifyDate { get; set; }
        /// <summary>
        /// 供应商
        /// </summary>
        public long? FSupplierID { get; set; }
        /// <summary>
        /// 供应商名称
        /// </summary>
        public string FSupplierID__FName { get; set; }
        /// <summary>
        /// 供应商编码
        /// </summary>
        public string FSupplierID__FNumber { get; set; }
        /// <summary>
        /// 供应商简称
        /// </summary>
        public string FSupplierID__FShortName { get; set; }
        /// <summary>
        /// 价格类型
        /// </summary>
        public string FPriceType { get; set; }
        /// <summary>
        /// 生效日（2.0废除）
        /// </summary>
        public DateTime? FEffectiveDate { get; set; }
        /// <summary>
        /// 失效日（2.0废除）
        /// </summary>
        public DateTime? FExpiryDate { get; set; }
        /// <summary>
        /// 含税
        /// </summary>
        public string? FIsIncludedTax { get; set; }
        /// <summary>
        /// 币别
        /// </summary>
        public long FCurrencyID { get; set; }
        /// <summary>
        /// 币别名称
        /// </summary>
        public string FCurrencyID__FName { get; set; }
        /// <summary>
        /// 币别编码
        /// </summary>
        public string FCurrencyID__FNumber { get; set; }
        /// <summary>
        /// 定价员
        /// </summary>
        public long? FPricer { get; set; }
        /// <summary>
        /// 定价员员工编码
        /// </summary>
        public string FPricer__FNumber { get; set; }
        /// <summary>
        /// 定价员员工名称
        /// </summary>
        public string FPricer__FName { get; set; }
        /// <summary>
        /// 禁用日期
        /// </summary>
        public DateTime? FForbidDate { get; set; }
        /// <summary>
        /// 禁用人
        /// </summary>
        public long? FForbiderId { get; set; }
        /// <summary>
        /// 禁用人用户名称
        /// </summary>
        public string FForbiderId__FName { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        public long? FApproverId { get; set; }
        /// <summary>
        /// 审核人用户名称
        /// </summary>
        public string FApproverId__FName { get; set; }
        /// <summary>
        /// 审核日期
        /// </summary>
        public DateTime? FApproveDate { get; set; }
        /// <summary>
        /// 供货方(2.0废除)
        /// </summary>
        public long? FSupplierLocId { get; set; }
        /// <summary>
        /// 供货方(2.0废除)名称
        /// </summary>
        public string FSupplierLocId__FName { get; set; }
        /// <summary>
        /// 供货方(2.0废除)编码
        /// </summary>
        public string FSupplierLocId__FNumber { get; set; }
        /// <summary>
        /// 价目表对象
        /// </summary>
        public string FPriceObject { get; set; }
        /// <summary>
        /// 默认价目表
        /// </summary>
        public string? FDefPriceListId { get; set; }
        /// <summary>
        /// 供应商Masterid
        /// </summary>
        public int? FSupplierMasterId { get; set; }
        /// <summary>
        /// 价外税
        /// </summary>
        public string? FIsPriceExcludeTax { get; set; }
        #endregion
        #region 价目表明细
        /// <summary>
        /// 价目表明细主键FEntryID
        /// </summary>
        public long FPriceListEntry_FEntryID { get; set; }
        /// <summary>
        /// 价目表明细序列
        /// </summary>
        public int FPriceListEntry_FSEQ { get; set; }
        /// <summary>
        /// 物料编码
        /// </summary>
        public long? FMaterialId { get; set; }
        /// <summary>
        /// 物料编码名称
        /// </summary>
        public string FMaterialId__FName { get; set; }
        /// <summary>
        /// 物料编码编码
        /// </summary>
        public string FMaterialId__FNumber { get; set; }
        /// <summary>
        /// 生效日期
        /// </summary>
        public DateTime FEntryEffectiveDate { get; set; }
        /// <summary>
        /// 失效日期
        /// </summary>
        public DateTime FEntryExpiryDate { get; set; }
        /// <summary>
        /// 至
        /// </summary>
        public decimal? FToQty { get; set; }
        /// <summary>
        /// 价格系数
        /// </summary>
        public decimal? FPriceCoefficient { get; set; }
        /// <summary>
        /// 来源单据号
        /// </summary>
        public string? FSrcBillNo { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string? FNote { get; set; }
        /// <summary>
        /// 业务禁用人
        /// </summary>
        public long? FDisablerId { get; set; }
        /// <summary>
        /// 业务禁用人用户名称
        /// </summary>
        public string FDisablerId__FName { get; set; }
        /// <summary>
        /// 业务禁用日期
        /// </summary>
        public DateTime? FDisableDate { get; set; }
        /// <summary>
        /// 行禁用
        /// </summary>
        public string? FDisableStatus { get; set; }
        /// <summary>
        /// 存货类别
        /// </summary>
        public long? FMaterialTypeId { get; set; }
        /// <summary>
        /// 存货类别名称
        /// </summary>
        public string FMaterialTypeId__FName { get; set; }
        /// <summary>
        /// 存货类别编码
        /// </summary>
        public string FMaterialTypeId__FNumber { get; set; }
        /// <summary>
        /// 价格上限
        /// </summary>
        public decimal? FUpPrice { get; set; }
        /// <summary>
        /// 价格下限
        /// </summary>
        public decimal? FDownPrice { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        public decimal FPrice { get; set; }
        /// <summary>
        /// 计价单位
        /// </summary>
        public long FUnitID { get; set; }
        /// <summary>
        /// 计价单位名称
        /// </summary>
        public string FUnitID__FName { get; set; }
        /// <summary>
        /// 计价单位编码
        /// </summary>
        public string FUnitID__FNumber { get; set; }
        /// <summary>
        /// 辅助属性
        /// </summary>
        public long? FAuxPropId { get; set; }
        /// <summary>
        /// 调价唯一性标示
        /// </summary>
        public string? FPATIdentity { get; set; }
        /// <summary>
        /// 采购目录编码
        /// </summary>
        public string? FCatalogNo { get; set; }
        /// <summary>
        /// 采购目录分录ID
        /// </summary>
        public int? FCatalogEntryId { get; set; }
        /// <summary>
        /// 含税单价
        /// </summary>
        public decimal FTaxPrice { get; set; }
        /// <summary>
        /// 税率%
        /// </summary>
        public decimal? FTaxRate { get; set; }
        /// <summary>
        /// 作业
        /// </summary>
        public long? FPROCESSID { get; set; }
        /// <summary>
        /// 作业名称
        /// </summary>
        public string FPROCESSID__FName { get; set; }
        /// <summary>
        /// 作业编码
        /// </summary>
        public string FPROCESSID__FNumber { get; set; }
        /// <summary>
        /// 工废单价
        /// </summary>
        public decimal? FPublicWastePrice { get; set; }
        /// <summary>
        /// 料废单价
        /// </summary>
        public decimal? FWastePrice { get; set; }
        /// <summary>
        /// 工废含税单价
        /// </summary>
        public decimal? FPublicWasteTaxPrice { get; set; }
        /// <summary>
        /// 料废含税单价
        /// </summary>
        public decimal? FWasteTaxPrice { get; set; }
        /// <summary>
        /// 需求组织
        /// </summary>
        public long? FProcessOrgId { get; set; }
        /// <summary>
        /// 需求组织名称
        /// </summary>
        public string FProcessOrgId__FName { get; set; }
        /// <summary>
        /// 需求组织编码
        /// </summary>
        public string FProcessOrgId__FNumber { get; set; }
        /// <summary>
        /// 物料masterid
        /// </summary>
        public int? FMMasterid { get; set; }
        /// <summary>
        /// 行审核状态
        /// </summary>
        public string? FRowAuditStatus { get; set; }
        /// <summary>
        /// 从
        /// </summary>
        public decimal? FFROMQTY { get; set; }
        /// <summary>
        /// 价格来源
        /// </summary>
        public string? FPRICEFROM { get; set; }
        /// <summary>
        /// 来源单号
        /// </summary>
        public string? FFROMBILLNO { get; set; }
        /// <summary>
        /// 自定义基础资料1
        /// </summary>
        public long? FDefBaseDataO { get; set; }
        /// <summary>
        /// 自定义基础资料1名称
        /// </summary>
        public string FDefBaseDataO__FName { get; set; }
        /// <summary>
        /// 自定义基础资料1编码
        /// </summary>
        public string FDefBaseDataO__FNumber { get; set; }
        /// <summary>
        /// 自定义基础资料2
        /// </summary>
        public long? FDefBaseDataT { get; set; }
        /// <summary>
        /// 自定义基础资料2名称
        /// </summary>
        public string FDefBaseDataT__FName { get; set; }
        /// <summary>
        /// 自定义基础资料2编码
        /// </summary>
        public string FDefBaseDataT__FNumber { get; set; }
        /// <summary>
        /// 自定义辅助资料1
        /// </summary>
        public string? FDefAssistantO { get; set; }
        /// <summary>
        /// 自定义辅助资料1编码
        /// </summary>
        public string FDefAssistantO__FNumber { get; set; }
        /// <summary>
        /// 自定义辅助资料1名称
        /// </summary>
        public string FDefAssistantO__FDataValue { get; set; }
        /// <summary>
        /// 自定义辅助资料2
        /// </summary>
        public string? FDefAssistantT { get; set; }
        /// <summary>
        /// 自定义辅助资料2编码
        /// </summary>
        public string FDefAssistantT__FNumber { get; set; }
        /// <summary>
        /// 自定义辅助资料2名称
        /// </summary>
        public string FDefAssistantT__FDataValue { get; set; }
        /// <summary>
        /// 自定义文本1
        /// </summary>
        public string? FDefTextO { get; set; }
        /// <summary>
        /// 自定义文本2
        /// </summary>
        public string? FDefTextT { get; set; }
        /// <summary>
        /// 自定义价格1
        /// </summary>
        public decimal? FDefaultPriceO { get; set; }
        /// <summary>
        /// 自定义价格2
        /// </summary>
        public decimal? FDefaultPriceT { get; set; }
        /// <summary>
        /// 物料分组编码
        /// </summary>
        public long? FMaterialGroupId { get; set; }
        /// <summary>
        /// 物料分组编码编码
        /// </summary>
        public string FMaterialGroupId__FNumber { get; set; }
        /// <summary>
        /// 物料分组编码名称
        /// </summary>
        public string FMaterialGroupId__FName { get; set; }
        /// <summary>
        /// 最近调价日期
        /// </summary>
        public DateTime? FRECENTDATE { get; set; }
        #endregion
    }
}