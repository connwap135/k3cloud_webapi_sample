using System;

namespace FormEntityCreation.DataEntity
{
    /// <summary>
    /// 仓库
    /// </summary>
    public class BD_STOCK
    {
        /// <summary>
        /// FStockId
        /// </summary>
        public int FStockId { get; set; }
        #region 基本信息
        /// <summary>
        /// 数据状态
        /// </summary>
        public string? FDocumentStatus { get; set; }
        /// <summary>
        /// 禁用状态
        /// </summary>
        public string? FForbidStatus { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string FName { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        public string? FNumber { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string? FDescription { get; set; }
        /// <summary>
        /// 创建组织
        /// </summary>
        public long FCreateOrgId { get; set; }
        /// <summary>
        /// 创建组织名称
        /// </summary>
        public string FCreateOrgId__FName { get; set; }
        /// <summary>
        /// 创建组织编码
        /// </summary>
        public string FCreateOrgId__FNumber { get; set; }
        /// <summary>
        /// 使用组织
        /// </summary>
        public long FUseOrgId { get; set; }
        /// <summary>
        /// 使用组织名称
        /// </summary>
        public string FUseOrgId__FName { get; set; }
        /// <summary>
        /// 使用组织编码
        /// </summary>
        public string FUseOrgId__FNumber { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public long? FCreatorId { get; set; }
        /// <summary>
        /// 创建人用户名称
        /// </summary>
        public string FCreatorId__FName { get; set; }
        /// <summary>
        /// 最后修改人
        /// </summary>
        public long? FModifierId { get; set; }
        /// <summary>
        /// 最后修改人用户名称
        /// </summary>
        public string FModifierId__FName { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime? FCreateDate { get; set; }
        /// <summary>
        /// 最后修改日期
        /// </summary>
        public DateTime? FModifyDate { get; set; }
        /// <summary>
        /// 仓库负责人
        /// </summary>
        public long? FPrincipal { get; set; }
        /// <summary>
        /// 仓库负责人员工编码
        /// </summary>
        public string FPrincipal__FNumber { get; set; }
        /// <summary>
        /// 仓库负责人员工名称
        /// </summary>
        public string FPrincipal__FName { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        public string? FTel { get; set; }
        /// <summary>
        /// 允许ATP检查
        /// </summary>
        public string? FAllowATPCheck { get; set; }
        /// <summary>
        /// 允许MRP计划
        /// </summary>
        public string? FAllowMRPPlan { get; set; }
        /// <summary>
        /// 启用仓位管理
        /// </summary>
        public string? FIsOpenLocation { get; set; }
        /// <summary>
        /// 允许锁库
        /// </summary>
        public string? FAllowLock { get; set; }
        /// <summary>
        /// 允许即时库存负库存
        /// </summary>
        public string? FAllowMinusQty { get; set; }
        /// <summary>
        /// 仓库地址
        /// </summary>
        public string? FAddress { get; set; }
        /// <summary>
        /// 禁用人
        /// </summary>
        public long? FForbiderId { get; set; }
        /// <summary>
        /// 禁用人用户名称
        /// </summary>
        public string FForbiderId__FName { get; set; }
        /// <summary>
        /// 禁用日期
        /// </summary>
        public DateTime? FForbidDate { get; set; }
        /// <summary>
        /// 系统预置
        /// </summary>
        public string? FSysDefault { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        public long? FAuditorId { get; set; }
        /// <summary>
        /// 审核人用户名称
        /// </summary>
        public string FAuditorId__FName { get; set; }
        /// <summary>
        /// 审核日期
        /// </summary>
        public DateTime? FAuditDate { get; set; }
        /// <summary>
        /// 分组
        /// </summary>
        public long? FGroup { get; set; }
        /// <summary>
        /// 仓库属性
        /// </summary>
        public string FStockProperty { get; set; }
        /// <summary>
        /// 供应商
        /// </summary>
        public long? FSupplierId { get; set; }
        /// <summary>
        /// 供应商名称
        /// </summary>
        public string FSupplierId__FName { get; set; }
        /// <summary>
        /// 供应商编码
        /// </summary>
        public string FSupplierId__FNumber { get; set; }
        /// <summary>
        /// 供应商简称
        /// </summary>
        public string FSupplierId__FShortName { get; set; }
        /// <summary>
        /// 客户
        /// </summary>
        public long? FCustomerId { get; set; }
        /// <summary>
        /// 客户客户名称
        /// </summary>
        public string FCustomerId__FName { get; set; }
        /// <summary>
        /// 客户客户编码
        /// </summary>
        public string FCustomerId__FNumber { get; set; }
        /// <summary>
        /// 客户简称
        /// </summary>
        public string FCustomerId__FShortName { get; set; }
        /// <summary>
        /// 库存状态类型
        /// </summary>
        public string FStockStatusType { get; set; }
        /// <summary>
        /// 默认库存状态
        /// </summary>
        public long? FDefStockStatusId { get; set; }
        /// <summary>
        /// 默认库存状态名称
        /// </summary>
        public string FDefStockStatusId__FName { get; set; }
        /// <summary>
        /// 默认库存状态编码
        /// </summary>
        public string FDefStockStatusId__FNumber { get; set; }
        /// <summary>
        /// 默认收料状态
        /// </summary>
        public long? FDefReceiveStatusId { get; set; }
        /// <summary>
        /// 默认收料状态名称
        /// </summary>
        public string FDefReceiveStatusId__FName { get; set; }
        /// <summary>
        /// 默认收料状态编码
        /// </summary>
        public string FDefReceiveStatusId__FNumber { get; set; }
        /// <summary>
        /// 第三方仓库编码
        /// </summary>
        public string? FTHIRDSTOCKNO { get; set; }
        /// <summary>
        /// 参与预警
        /// </summary>
        public string? FAvailableAlert { get; set; }
        /// <summary>
        /// 第三方仓储类型
        /// </summary>
        public string? FThirdStockType { get; set; }
        /// <summary>
        /// 参与拣货
        /// </summary>
        public string? FAvailablePicking { get; set; }
        /// <summary>
        /// 拣货优先级（1~9999）
        /// </summary>
        public int? FSortingPriority { get; set; }
        /// <summary>
        /// 管易仓
        /// </summary>
        public string? FIsGYStock { get; set; }
        /// <summary>
        /// 管易仓编码
        /// </summary>
        public string? FGYStockNumber { get; set; }
        /// <summary>
        /// 管易同步状态
        /// </summary>
        public string? FGYSynStatus { get; set; }
        /// <summary>
        /// 不参与可发量统计
        /// </summary>
        public string? FNotExpQty { get; set; }
        /// <summary>
        /// 仓位维度数据列表显示格式
        /// </summary>
        public string FLocListFormatter { get; set; }
        /// <summary>
        /// 部门
        /// </summary>
        public long? FDeptId { get; set; }
        /// <summary>
        /// 部门名称
        /// </summary>
        public string FDeptId__FName { get; set; }
        /// <summary>
        /// 部门编码
        /// </summary>
        public string FDeptId__FNumber { get; set; }
        #endregion
        #region 仓位值集
        /// <summary>
        /// 仓位值集主键FEntryID
        /// </summary>
        public long FStockFlexItem_FEntryID { get; set; }
        /// <summary>
        /// 仓位值集序列
        /// </summary>
        public int FStockFlexItem_FSeq { get; set; }
        /// <summary>
        /// 仓位值集编码
        /// </summary>
        public long? FFlexId { get; set; }
        /// <summary>
        /// 仓位值集编码代码
        /// </summary>
        public string FFlexId__FNumber { get; set; }
        /// <summary>
        /// 仓位值集编码名称
        /// </summary>
        public string FFlexId__FName { get; set; }
        /// <summary>
        /// 必录
        /// </summary>
        public string? FIsMustInput { get; set; }
        #endregion
        #region 仓位值
        /// <summary>
        /// 仓位值主键FDetailID
        /// </summary>
        public long FStockFlexDetail_FDetailID { get; set; }
        /// <summary>
        /// 仓位值序列
        /// </summary>
        public int FStockFlexDetail_FSEQ { get; set; }
        /// <summary>
        /// 仓位值编码
        /// </summary>
        public long? FFlexEntryId { get; set; }
        /// <summary>
        /// 仓位值编码编码
        /// </summary>
        public string FFlexEntryId__FNumber { get; set; }
        /// <summary>
        /// 仓位值编码名称
        /// </summary>
        public string FFlexEntryId__FName { get; set; }
        #endregion
    }
}