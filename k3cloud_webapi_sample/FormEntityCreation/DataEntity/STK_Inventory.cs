using System;

namespace FormEntityCreation.DataEntity
{
    /// <summary>
    /// 即时库存
    /// </summary>
    public class STK_Inventory
    {
        /// <summary>
        /// FID
        /// </summary>
        public string FID { get; set; }
        #region 即时库存
        /// <summary>
        /// 仓库编码
        /// </summary>
        public long? FStockId { get; set; }
        /// <summary>
        /// 仓库编码名称
        /// </summary>
        public string FStockId__FName { get; set; }
        /// <summary>
        /// 仓库编码编码
        /// </summary>
        public string FStockId__FNumber { get; set; }
        /// <summary>
        /// 保管者编码
        /// </summary>
        public int? FKeeperId { get; set; }
        /// <summary>
        /// 保管者编码名称
        /// </summary>
        public string FKeeperId__FName { get; set; }
        /// <summary>
        /// 保管者编码编码
        /// </summary>
        public string FKeeperId__FNumber { get; set; }
        /// <summary>
        /// 货主编码
        /// </summary>
        public int? FOwnerId { get; set; }
        /// <summary>
        /// 货主编码名称
        /// </summary>
        public string FOwnerId__FName { get; set; }
        /// <summary>
        /// 货主编码编码
        /// </summary>
        public string FOwnerId__FNumber { get; set; }
        /// <summary>
        /// 保管者类型
        /// </summary>
        public string? FKeeperTypeId { get; set; }
        /// <summary>
        /// 物料编码
        /// </summary>
        public long? FMaterialId { get; set; }
        /// <summary>
        /// 物料编码名称
        /// </summary>
        public string FMaterialId__FName { get; set; }
        /// <summary>
        /// 物料编码编码
        /// </summary>
        public string FMaterialId__FNumber { get; set; }
        /// <summary>
        /// 货主类型
        /// </summary>
        public string? FOwnerTypeId { get; set; }
        /// <summary>
        /// 仓位
        /// </summary>
        public long? FStockLocId { get; set; }
        /// <summary>
        /// 库存组织
        /// </summary>
        public long? FStockOrgId { get; set; }
        /// <summary>
        /// 库存组织名称
        /// </summary>
        public string FStockOrgId__FName { get; set; }
        /// <summary>
        /// 库存组织编码
        /// </summary>
        public string FStockOrgId__FNumber { get; set; }
        /// <summary>
        /// 基本单位
        /// </summary>
        public long? FBaseUnitId { get; set; }
        /// <summary>
        /// 基本单位名称
        /// </summary>
        public string FBaseUnitId__FName { get; set; }
        /// <summary>
        /// 基本单位编码
        /// </summary>
        public string FBaseUnitId__FNumber { get; set; }
        /// <summary>
        /// 库存辅单位
        /// </summary>
        public long? FSecUnitId { get; set; }
        /// <summary>
        /// 库存辅单位名称
        /// </summary>
        public string FSecUnitId__FName { get; set; }
        /// <summary>
        /// 库存辅单位编码
        /// </summary>
        public string FSecUnitId__FNumber { get; set; }
        /// <summary>
        /// 库存主单位
        /// </summary>
        public long? FStockUnitId { get; set; }
        /// <summary>
        /// 库存主单位名称
        /// </summary>
        public string FStockUnitId__FName { get; set; }
        /// <summary>
        /// 库存主单位编码
        /// </summary>
        public string FStockUnitId__FNumber { get; set; }
        /// <summary>
        /// 批号
        /// </summary>
        public long? FLot { get; set; }
        /// <summary>
        /// 批号名称
        /// </summary>
        public string FLot__FName { get; set; }
        /// <summary>
        /// 批号批号
        /// </summary>
        public string FLot__FNumber { get; set; }
        /// <summary>
        /// 项目编号
        /// </summary>
        public string? FProjectNo { get; set; }
        /// <summary>
        /// 预留量(主单位)
        /// </summary>
        public decimal? FLockQty { get; set; }
        /// <summary>
        /// 预留量(辅单位)
        /// </summary>
        public decimal? FSecLockQty { get; set; }
        /// <summary>
        /// 库存量(基本单位)
        /// </summary>
        public decimal? FBaseQty { get; set; }
        /// <summary>
        /// 库存量(主单位)
        /// </summary>
        public decimal? FQty { get; set; }
        /// <summary>
        /// 计划跟踪号
        /// </summary>
        public string? FMtoNo { get; set; }
        /// <summary>
        /// 预留量(基本单位)
        /// </summary>
        public decimal? FBaseLockQty { get; set; }
        /// <summary>
        /// 生产日期
        /// </summary>
        public DateTime? FProduceDate { get; set; }
        /// <summary>
        /// 辅助属性
        /// </summary>
        public long? FAuxPropId { get; set; }
        /// <summary>
        /// 库存状态
        /// </summary>
        public long? FStockStatusId { get; set; }
        /// <summary>
        /// 库存状态名称
        /// </summary>
        public string FStockStatusId__FName { get; set; }
        /// <summary>
        /// 库存状态编码
        /// </summary>
        public string FStockStatusId__FNumber { get; set; }
        /// <summary>
        /// 库存量(库存辅单位)
        /// </summary>
        public decimal? FSecQty { get; set; }
        /// <summary>
        /// 有效期至
        /// </summary>
        public DateTime? FExpiryDate { get; set; }
        /// <summary>
        /// BOM版本
        /// </summary>
        public long? FBomId { get; set; }
        /// <summary>
        /// BOM版本BOM简称
        /// </summary>
        public string FBomId__FName { get; set; }
        /// <summary>
        /// BOM版本BOM版本
        /// </summary>
        public string FBomId__FNumber { get; set; }
        /// <summary>
        /// 可用量(主单位)
        /// </summary>
        public decimal? FAVBQty { get; set; }
        /// <summary>
        /// 可用量(基本单位)
        /// </summary>
        public decimal? FBaseAVBQty { get; set; }
        /// <summary>
        /// 可用量(库存辅单位)
        /// </summary>
        public decimal? FSecAVBQty { get; set; }
        /// <summary>
        /// 最后更新日期
        /// </summary>
        public DateTime? FUpdateTime { get; set; }
        #endregion
    }
}