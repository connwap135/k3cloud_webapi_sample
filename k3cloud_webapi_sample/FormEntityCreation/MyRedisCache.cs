﻿using K3Cloud.WebApi.Core.IoC.Cache;
using MyRedisLibrary.Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;

namespace FormEntityCreation
{
    public class MyRedisCache : ICacheService
    {
        private readonly IDistributedCache cache;
        public MyRedisCache(IDistributedCache cache)
        {
            this.cache = cache;
        }
        public void Add<V>(string key, V value)
        {
            Add(key, value, 20 * 60);
        }

        public void Add<V>(string key, V value, int cacheDurationInSeconds)
        {
            cache.Add(key, value, cacheDurationInSeconds);
        }
        public bool ContainsKey<V>(string key)
        {
            return cache.ContainsKey(key);
        }

        public V Get<V>(string key)
        {
            return cache.Get<V>(key);
        }

        public IEnumerable<string> GetAllKey<V>()
        {
            return cache.GetAllKey<V>();
        }

        public V GetOrCreate<V>(string cacheKey, Func<V> create, int cacheDurationInSeconds = int.MaxValue)
        {
            return cache.GetOrCreate<V>(cacheKey, create, cacheDurationInSeconds);
        }

        public void Remove<V>(string key)
        {
            cache.Remove(key);
        }
        public void RemoveAll()
        {
            IEnumerable<string> keys = GetAllKey<object>();
            foreach (string key in keys)
            {
                cache.Remove(key);
            }
        }
    }
}