﻿using System.Collections.Concurrent;
using System.Threading;
namespace K3Cloud.WebApi.Core.IoC;

internal static class IocCallContext<T>
{
    private static readonly ConcurrentDictionary<string, AsyncLocal<T>> state = new();
    public static void SetData(string name, T data)
    {
        state.GetOrAdd(name, _ => new AsyncLocal<T>()).Value = data;
    }

    public static T? GetData(string name)
    {
        return state.TryGetValue(name, out AsyncLocal<T>? data) ? data.Value : default;
    }
}
