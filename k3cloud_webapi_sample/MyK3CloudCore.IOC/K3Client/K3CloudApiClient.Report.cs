﻿using K3Cloud.WebApi.Core.IoC.Attributes;
using K3Cloud.WebApi.Core.IoC.DataEntity.PresetEntity;
using K3Cloud.WebApi.Core.IoC.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace K3Cloud.WebApi.Core.IoC.K3Client;

public sealed partial class K3CloudApiClient
{
    /// <summary>
    /// 简单账表查询
    /// </summary>
    public async Task<string?> GetSysReportDataAsync(string formid, SysReportModel data, CancellationToken cancellationToken = default)
    {
        return await ExecuteAsync("Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.GetSysReportData", formid, data, cancellationToken);
    }
    /// <summary>
    /// 简单账表查询
    /// </summary>
    /// <returns></returns>
    public async Task<string?> GetSysReportDataAsync(string formid, string data, CancellationToken cancellationToken = default)
    {
        return await ExecuteAsync("Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.GetSysReportData", formid, data, cancellationToken);
    }
    /// <summary>
    /// 报表API接口;库存账龄分析报表;物料收发明细报表;物料收发汇总报表;
    /// <a href="https://vip.kingdee.com/article/301363894805890816?productLineId=1"/>
    /// </summary>
    public async Task<string?> GetSCMReportDataAsync(ReportParamModel data, CancellationToken cancellationToken = default)
    {
        return await ExecuteAsync("Kingdee.K3.SCM.WebApi.ServicesStub.StockReportQueryService.GetReportData,Kingdee.K3.SCM.WebApi.ServicesStub", data, cancellationToken);
    }

    public async Task<List<T>?> GetSysReportDataAsync<T>(SysReportModel data1, CancellationToken cancellationToken = default)
    {
        string formId = typeof(T).GetAlias();
        PropertyInfo[] fieldProperties = typeof(T).GetProperties().Where(t => t.GetCustomAttributes(typeof(ignoreAttribute), true).Length == 0).ToArray();
        string field = string.Join(",", fieldProperties.Select(x => x.GetCustomAttribute<AliasAttribute>()?.Name ?? x.Name).ToArray());
        data1.FieldKeys = field;
        string? result = await GetSysReportDataAsync(formId, data1, cancellationToken);
        if (result == null) return default;
        SysReportResult? obj = JsonConvert.DeserializeObject<SysReportResult>(result);
        if (obj == null) return default;
        if (!obj.Result.IsSuccess)
        {
            throw new Exception(JsonConvert.SerializeObject(obj.Result.ResponseStatus.Errors));
        }

        if (obj.Result.Rows.Count == 0)
        {
            return default;
        }

        List<List<object>> data = obj.Result.Rows;
        List<T> list = new(obj.Result.RowCount);
        for (int i = 0; i < data.Count; i++)
        {
            T item = Activator.CreateInstance<T>();
            if (item == null) { continue; }
            for (int j = 0; j < fieldProperties.Length; j++)
            {
                if (data[i][j] == null) { continue; }
                object? propertyValue = ConvertNullableType(data[i][j], item.GetType().GetProperty(fieldProperties[j].Name)?.PropertyType);
                SetPropertyValue(item, fieldProperties[j].Name, propertyValue);
            }
            list.Add(item);
        }
        return list;
    }

    public async Task<DataTable?> GetSCMReportDataTableAsync(ReportParamModel data1, CancellationToken cancellationToken = default)
    {
        string? result = await GetSCMReportDataAsync(data1, cancellationToken);
        if (result == null) return default;
        SCMReportResult? obj = JsonConvert.DeserializeObject<SCMReportResult>(result);
        return obj == null ? default : !obj.success ? throw new Exception(obj.message) : obj.data ?? default;
    }
}
