﻿using System;
using System.Diagnostics;

namespace K3Cloud.WebApi.Core.IoC.Attributes
{
    /// <summary>
    /// 别名
    /// </summary>
    [DebuggerStepThrough]
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Property | AttributeTargets.Field)]
    public class AliasAttribute(string name) : Attribute
    {
        /// <summary>
        /// 别名
        /// </summary>
        public string Name { get; set; } = name;
    }
}
