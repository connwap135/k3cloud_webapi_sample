﻿using System;

namespace K3Cloud.WebApi.Core.IoC.Attributes
{
    /// <summary>
    /// 忽略，不计算到查询的字段key集合
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
#pragma warning disable IDE1006
    public class ignoreAttribute : Attribute
#pragma warning restore IDE1006
    {
    }
}
