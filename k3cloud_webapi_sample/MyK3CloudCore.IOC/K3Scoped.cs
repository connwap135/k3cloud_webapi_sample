﻿using K3Cloud.WebApi.Core.IoC.DataEntity;
using K3Cloud.WebApi.Core.IoC.K3Client;
using System.Net;

namespace K3Cloud.WebApi.Core.IoC;
/// <summary>
/// 金蝶云星空SDK
/// </summary>
public class K3Scoped
{
    private K3Scoped() { }
    /// <summary>
    /// API客户端(默认客户端)
    /// </summary>
    public static K3CloudApiClient Client => GetK3CloudClient();
    /// <summary>
    /// 
    /// </summary>
    public static CookieContainer Cookie => K3IOCServiceCollectionExtensions._cookie;
    /// <summary>
    /// 
    /// </summary>
    public static LoginResult? LoginResult => K3IOCServiceCollectionExtensions.LoginResult;
    /// <summary>
    /// 
    /// </summary>
    public static K3HttpService HttpClient => K3IOCServiceCollectionExtensions.HttpClient;
    /// <summary>
    /// appsettings.json文件下键值为<paramref name="configKey"/>的客户端
    /// </summary>
    public static K3CloudApiClient GetClient(string configKey) => GetK3CloudClient(configKey);

    private static K3CloudApiClient GetK3CloudClient(string? configKey = default)
    {
        K3CloudApiClient? sugar = IocCallContext<K3CloudApiClient>.GetData(IocConst.CLIENT_KEY);
        if (sugar == null)
        {
            K3CloudApiClient result = K3CloudHelper.GetNewK3cloudClient(configKey);
            IocCallContext<K3CloudApiClient>.SetData(IocConst.CLIENT_KEY, result);
            return result;
        }
        return sugar;
    }
}
