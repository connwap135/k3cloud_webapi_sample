﻿namespace K3Cloud.WebApi.Core.IoC.Types
{
    public class Refasync<T>
    {
        public Refasync() { }
        public Refasync(T? value) { Value = value; }
        public T? Value { get; set; }
        public override string? ToString()
        {
            T? value = Value;
            return value == null ? "" : value.ToString();
        }
        public static implicit operator T?(Refasync<T?> r) { return r.Value; }
        public static implicit operator Refasync<T>(T value) { return new Refasync<T>(value); }
    }
}