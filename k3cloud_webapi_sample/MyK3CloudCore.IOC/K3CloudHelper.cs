﻿using K3Cloud.WebApi.Core.IoC.K3Client;
using Microsoft.Extensions.Configuration;
using System;

namespace K3Cloud.WebApi.Core.IoC;
/// <summary>
/// 
/// </summary>
public class K3CloudHelper
{
    /// <summary>
    /// 
    /// </summary>
    public static K3CloudApiClient GetNewK3cloudClient(string? configKey = default)
    {
        if (string.IsNullOrEmpty(configKey))
        {
            return new(K3IOCServiceCollectionExtensions.Configs);
        }

        IConfigurationRoot builder = new ConfigurationBuilder().AddInMemoryCollection().AddJsonFile("appsettings.json", false).Build();
        K3Config? k3Config = builder.GetSection($"K3CLOUDCONFIG:{configKey}").Get<K3Config>();
        return k3Config == null ? throw new ArgumentNullException(configKey) : new(k3Config);
    }
}
