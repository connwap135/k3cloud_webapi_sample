﻿using System;
using System.Collections.Generic;

namespace K3Cloud.WebApi.Core.IoC.Cache
{
    /// <summary>
    /// 缓存
    /// </summary>
    public interface ICacheService
    {
        /// <summary>
        /// 创建缓存
        /// </summary>
        void Add<V>(string key, V value);
        /// <summary>
        /// 创建缓存
        /// </summary>
        void Add<V>(string key, V value, int cacheDurationInSeconds);
        /// <summary>
        /// 检查缓存是否存在
        /// </summary>
        bool ContainsKey<V>(string key);
        /// <summary>
        /// 获取缓存
        /// </summary>
        V? Get<V>(string key);
        /// <summary>
        /// 获取所有缓存主键
        /// </summary>
        IEnumerable<string> GetAllKey<V>();
        /// <summary>
        /// 移除缓存
        /// </summary>
        void Remove<V>(string key);
        /// <summary>
        /// 获取或创建缓存
        /// </summary>
        V? GetOrCreate<V>(string cacheKey, Func<V> create, int cacheDurationInSeconds = int.MaxValue);
        /// <summary>
        /// 移除所有缓存
        /// </summary>
        void RemoveAll();
    }
}