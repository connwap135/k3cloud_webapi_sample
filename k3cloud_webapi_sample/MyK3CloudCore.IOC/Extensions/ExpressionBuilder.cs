﻿using K3Cloud.WebApi.Core.IoC.Attributes;
using SQLBuilder.Entry;
using SQLBuilder.Enums;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.Json;
using System.Text.RegularExpressions;

namespace K3Cloud.WebApi.Core.IoC.Extensions;

/// <summary>
/// 简单实现 根据Layui扩展组件dynamicCondition的Json生成表达式
/// </summary>
public static partial class ExpressionBuilder
{
    /// <summary>
    /// IN
    /// </summary>
    private static readonly string In = nameof(In).ToLower();
    /// <summary>
    /// Not IN
    /// </summary>
    private static readonly string NotIn = nameof(NotIn).ToLower();
    /// <summary>
    /// 等于
    /// </summary>
    private static readonly string Equal = nameof(Equal).ToLower();
    /// <summary>
    /// 不等于
    /// </summary>
    private static readonly string Unequal = nameof(Unequal).ToLower();
    /// <summary>
    /// 包含
    /// </summary>
    private static readonly string Like = nameof(Like).ToLower();
    /// <summary>
    /// 左包含
    /// </summary>
    private static readonly string Start = nameof(Start).ToLower();
    /// <summary>
    /// 右包含
    /// </summary>
    private static readonly string End = nameof(End).ToLower();
    /// <summary>
    /// 不包含
    /// </summary>
    private static readonly string NotLike = nameof(NotLike).ToLower();
    /// <summary>
    /// 为空
    /// </summary>
    private static readonly string Empty = nameof(Empty).ToLower();
    /// <summary>
    /// 不为空
    /// </summary>
    private static readonly string NotEmpty = nameof(NotEmpty).ToLower();
    /// <summary>
    /// 范围
    /// </summary>
    private static readonly string Between = nameof(Between).ToLower();
    /// <summary>
    /// 大于
    /// </summary>
    private static readonly string Greater_than = nameof(Greater_than).ToLower();
    /// <summary>
    /// 小于
    /// </summary>
    private static readonly string Less_than = nameof(Less_than).ToLower();
    /// <summary>
    /// 并且
    /// </summary>
    private static readonly string And = nameof(And).ToLower();
    /// <summary>
    /// 字符类型
    /// </summary>
    private static readonly Type StringType = typeof(string);
    private static readonly Type[] paramsType = [StringType];

    private delegate Expression Binder(Expression left, Expression right);
    /// <summary>
    /// 根据 <a href="https://github.com/JeremyLikness/ExpressionGenerator"/> 修改
    /// </summary>
    private static Expression? ParseTree<T>(JsonElement condition, ParameterExpression parm)
    {
        Expression? left = null;
        Type t = typeof(T);
        foreach (JsonElement rule in condition.EnumerateArray())
        {
            string? logicalOperator = rule.GetProperty(nameof(logicalOperator)).GetString();
            // https://gitee.com/dotnetchina/SqlSugar/issues/I4E3NU
            Binder binder = logicalOperator == And ? Expression.AndAlso : Expression.OrElse;
            Expression? bind(Expression? left, Expression right)
            {
                return left == null ? right : binder(left, right);
            }

            string? conditionFieldVal = rule.GetProperty(nameof(conditionFieldVal)).GetString();
            PropertyInfo? propertyAny = t.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(x => x.Name.Equals(conditionFieldVal, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            if (propertyAny == null) { continue; }
            // 比较操作符
            string? conditionOptionVal = rule.GetProperty(nameof(conditionOptionVal)).GetString();
            JsonElement conditionValueVal = rule.GetProperty(nameof(conditionValueVal));
            JsonElement conditionValueLeftVal = rule.GetProperty(nameof(conditionValueLeftVal));
            JsonElement conditionValueRightVal = rule.GetProperty(nameof(conditionValueRightVal));
            _ = conditionValueVal.TryGetProperty("value", out JsonElement value);
            _ = conditionValueLeftVal.TryGetProperty("value", out JsonElement leftValue);
            _ = conditionValueRightVal.TryGetProperty("value", out JsonElement rightValue);
            //var property = Expression.Property(parm, propertyAny);
            MemberExpression property = Expression.PropertyOrField(parm, propertyAny.Name);
            if (conditionOptionVal == In)
            {
                dynamic? tmpField = ExpTmpFieldDynamicCall(propertyAny.PropertyType, value.GetString());
                if (tmpField == null) { continue; }
                MethodInfo MethodContains = typeof(Enumerable).GetMethods(BindingFlags.Static | BindingFlags.Public)
                    .Single(m => m.Name == nameof(Enumerable.Contains) && m.GetParameters().Length == 2).MakeGenericMethod(propertyAny.PropertyType);
                dynamic toCompare = Expression.Constant(tmpField);
                FieldInfo fieldInfo = tmpField.ArrFieldInfo();
                MethodCallExpression right = Expression.Call(null, MethodContains, [Expression.Field(toCompare, fieldInfo), property]);
                left = bind(left, right);
            }
            else if (conditionOptionVal == NotIn)
            {
                dynamic? tmpField = ExpTmpFieldDynamicCall(propertyAny.PropertyType, value.GetString());
                if (tmpField == null) { continue; }
                dynamic toCompare = Expression.Constant(tmpField);
                FieldInfo fieldInfo = tmpField.ArrFieldInfo();
                MethodInfo MethodContains = typeof(Enumerable).GetMethods(BindingFlags.Static | BindingFlags.Public)
                    .Single(m => m.Name == nameof(Enumerable.Contains) && m.GetParameters().Length == 2).MakeGenericMethod(propertyAny.PropertyType);
                UnaryExpression right = Expression.Not(Expression.Call(null, MethodContains, [Expression.Field(toCompare, fieldInfo), property]));
                left = bind(left, right);
            }
            else if (conditionOptionVal == Like)
            {
                ConstantExpression toCompare = Expression.Constant(value.GetString());
                MethodInfo? contains = StringType.GetMethod(nameof(Enumerable.Contains), paramsType) ?? throw new K3ApiException("类型不可以包含");
                MethodCallExpression right = Expression.Call(property, contains, toCompare);
                left = bind(left, right);
            }
            else if (conditionOptionVal == NotLike)
            {
                ConstantExpression toCompare = Expression.Constant(value.GetString());
                MethodInfo? contains = StringType.GetMethod(nameof(Enumerable.Contains), paramsType) ?? throw new K3ApiException("类型不可以不包含");
                UnaryExpression right = Expression.Not(Expression.Call(property, contains, toCompare));
                left = bind(left, right);
            }
            else if (conditionOptionVal == Start)
            {
                ConstantExpression toCompare = Expression.Constant(value.GetString());
                MethodInfo? contains = StringType.GetMethod("StartsWith", paramsType) ?? throw new K3ApiException("类型不可以左包含");
                MethodCallExpression right = Expression.Call(property, contains, toCompare);
                left = bind(left, right);
            }
            else if (conditionOptionVal == End)
            {
                ConstantExpression toCompare = Expression.Constant(value.GetString());
                MethodInfo? contains = StringType.GetMethod("EndsWith", paramsType) ?? throw new K3ApiException("类型不可以右包含");
                MethodCallExpression right = Expression.Call(property, contains, toCompare);
                left = bind(left, right);
            }
            else if (conditionOptionVal == Unequal)
            {
                MemberExpression? rightProperty = ToPropertyExpression(parm, value.GetString());
                if (rightProperty != null)
                {
                    BinaryExpression right = Expression.NotEqual(property, rightProperty);
                    left = bind(left, right);
                }
                else
                {
                    object? propertyValue;
                    if (propertyAny.PropertyType.IsEnum)
                    {
#if NETSTANDARD2_1_OR_GREATER
                        _ = Enum.TryParse(propertyAny.PropertyType.ParseNullableToType(), value.GetString(), true, out object result);
                        propertyValue = result;
#else
                        propertyValue = Enum.Parse(propertyAny.PropertyType.ParseNullableToType(), value.GetString()!);
#endif
                    }
                    else
                    {
                        propertyValue = Convert.ChangeType(value.GetString(), propertyAny.PropertyType.ParseNullableToType());
                    }
                    ConstantExpression toCompare = Expression.Constant(propertyValue, propertyAny.PropertyType);
                    BinaryExpression right = Expression.NotEqual(property, toCompare);
                    left = bind(left, right);
                }
            }
            else if (conditionOptionVal == Empty)
            {
                //var right = Expression.Equal(property, Expression.Constant(null));
                MethodInfo? IsNullOrEmpty = StringType.GetMethod("IsNullOrEmpty", paramsType) ?? throw new K3ApiException("类型不可以空检查");
                MethodCallExpression right = Expression.Call(null, IsNullOrEmpty, property);
                left = bind(left, right);
            }
            else if (conditionOptionVal == NotEmpty)
            {
                //var right = Expression.NotEqual(property, Expression.Constant(null));
                MethodInfo? IsNullOrEmpty = StringType.GetMethod("IsNullOrEmpty", paramsType) ?? throw new K3ApiException("类型不可以非空检查");
                UnaryExpression right = Expression.Not(Expression.Call(null, IsNullOrEmpty, property));
                left = bind(left, right);
            }
            else if (conditionOptionVal == Between)
            {
                if (!string.IsNullOrEmpty(leftValue.ValueKind != JsonValueKind.Undefined ? leftValue.GetString() : null))
                {
                    object? propertyValue = Convert.ChangeType(leftValue.GetString(), propertyAny.PropertyType.ParseNullableToType());
                    if (propertyValue is DateTime dateSame)
                    {
                        propertyValue = dateSame.Date;
                    }
                    ConstantExpression toCompare = Expression.Constant(propertyValue, propertyAny.PropertyType);
                    BinaryExpression right = Expression.GreaterThanOrEqual(property, toCompare);
                    left = bind(left, right);
                }
                if (!string.IsNullOrEmpty(rightValue.ValueKind != JsonValueKind.Undefined ? rightValue.GetString() : null))
                {
                    object? propertyValue = Convert.ChangeType(rightValue.GetString(), propertyAny.PropertyType.ParseNullableToType());
                    if (propertyValue is DateTime dateSame)
                    {
                        propertyValue = dateSame.Date.AddDays(1).AddSeconds(-1);
                    }
                    ConstantExpression toCompare = Expression.Constant(propertyValue, propertyAny.PropertyType);
                    BinaryExpression right = Expression.LessThanOrEqual(property, toCompare);
                    left = bind(left, right);
                }
            }
            else if (conditionOptionVal == Greater_than)
            {
                MemberExpression? rightProperty = ToPropertyExpression(parm, value.GetString());
                if (rightProperty != null)
                {
                    BinaryExpression right = Expression.GreaterThan(property, rightProperty);
                    left = bind(left, right);
                }
                else
                {
                    object? propertyValue = Convert.ChangeType(value.GetString(), propertyAny.PropertyType.ParseNullableToType());
                    if (propertyValue is DateTime dateSame)
                    {
                        propertyValue = dateSame.Date.AddDays(1).AddSeconds(-1);
                    }
                    ConstantExpression toCompare = Expression.Constant(propertyValue, propertyAny.PropertyType);
                    BinaryExpression right = Expression.GreaterThan(property, toCompare);
                    left = bind(left, right);
                }
            }
            else if (conditionOptionVal == Less_than)
            {
                MemberExpression? rightProperty = ToPropertyExpression(parm, value.GetString());
                if (rightProperty != null)
                {
                    BinaryExpression right = Expression.LessThan(property, rightProperty);
                    left = bind(left, right);
                }
                else
                {
                    object? propertyValue = Convert.ChangeType(value.GetString(), propertyAny.PropertyType.ParseNullableToType());
                    if (propertyValue is DateTime dateSame)
                    {
                        propertyValue = dateSame.Date;
                    }
                    ConstantExpression toCompare = Expression.Constant(propertyValue, propertyAny.PropertyType);
                    BinaryExpression right = Expression.LessThan(property, toCompare);
                    left = bind(left, right);
                }
            }
            else if (conditionOptionVal == Equal)
            {
                MemberExpression? rightProperty = ToPropertyExpression(parm, value.GetString());
                if (rightProperty != null)
                {
                    BinaryExpression right = Expression.Equal(property, rightProperty);
                    left = bind(left, right);
                }
                else
                {
                    object? propertyValue;
                    if (propertyAny.PropertyType.IsEnum)
                    {
#if NETSTANDARD2_1_OR_GREATER
                        _ = Enum.TryParse(propertyAny.PropertyType.ParseNullableToType(), value.GetString(), true, out object result);
                        propertyValue = result;
#else
                        propertyValue = Enum.Parse(propertyAny.PropertyType.ParseNullableToType(), value.GetString()!);
#endif
                    }
                    else
                    {
                        propertyValue = Convert.ChangeType(value.GetString(), propertyAny.PropertyType.ParseNullableToType());
                    }
                    if (propertyValue is DateTime dataSame)
                    {
                        propertyValue = dataSame.Date;
                        ConstantExpression toCompare = Expression.Constant(propertyValue, propertyAny.PropertyType);
                        BinaryExpression right = Expression.GreaterThanOrEqual(property, toCompare);
                        left = bind(left, right);
                        propertyValue = dataSame.Date.AddDays(1).AddSeconds(-1);
                        toCompare = Expression.Constant(propertyValue, propertyAny.PropertyType);
                        right = Expression.LessThanOrEqual(property, toCompare);
                        left = bind(left, right);
                    }
                    else
                    {
                        ConstantExpression toCompare = Expression.Constant(propertyValue, propertyAny.PropertyType);
                        BinaryExpression right = Expression.Equal(property, toCompare);
                        left = bind(left, right);
                    }
                }
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        return left;
    }

    /// <summary>
    /// 根据Json生成表达式
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="doc"></param>
    /// <returns></returns>
    public static Expression<Func<T, bool>> ParseExpressionOf<T>(JsonDocument doc)
    {
        ParameterExpression itemExpression = Expression.Parameter(typeof(T));
        Expression? conditions = ParseTree<T>(doc.RootElement, itemExpression);
        if (conditions == null) { return t => true; }
        if (conditions.CanReduce)
        {
            conditions = conditions.ReduceAndCheck();
        }
        Expression<Func<T, bool>> query = Expression.Lambda<Func<T, bool>>(conditions, itemExpression);
        return query;
    }

    public static Func<T, bool> ParsePredicateOf<T>(JsonDocument doc)
    {
        Expression<Func<T, bool>> query = ParseExpressionOf<T>(doc);
        return query.Compile();
    }
    public static string GetFilterString<T>(Expression<Func<T, bool>>? expression) where T : class
    {
        if (expression == null) { return ""; }
        var fields = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance).Select(t => new
        {
            t.Name,
            t.PropertyType,
            IsAlias = t.GetCustomAttributes(typeof(AliasAttribute), true).Length > 0,
            AliasName = t.GetCustomAttribute<AliasAttribute>()?.Name ?? t.Name
        });
        var aliasFields = fields.Where(t => t.IsAlias);
        bool hasWhere = false;
        string sql = new SqlBuilderCore<T>(DatabaseType.SqlServer, (sql, parameter) =>
        {
            Debug.WriteLine("处理前sql：" + sql);
            foreach (KeyValuePair<string, object> item in (Dictionary<string, object>)parameter)
            {
                Type type = item.Value.GetType();
                var match = Regex.Match(sql, @"(\w+)\s*=\s*" + Regex.Escape(item.Key));
                if (match.Success)
                {
                    var column = match.Groups[1].Value;
                    var field = fields.FirstOrDefault(f => f.Name == column);
                    if (field != null && field.PropertyType.IsEnum) // 处理枚举类型
                    {
                        type = field.PropertyType;
                    }
                    else if (column == "1" && type == typeof(bool)) // 处理布尔类型
                    {
                        var replaceValue = match.Groups[0].Value;
                        sql = (bool)item.Value ? sql.Replace(replaceValue, "1") : sql.Replace(replaceValue, "0");
                    }
                }
                bool isString = type == typeof(string);
                bool isDateTime = type == typeof(DateTime);
                bool isGuid = type == typeof(Guid);
                bool isEnum = type.IsEnum;

                Debug.WriteLine("type : " + type.Name);

                if (isString || isDateTime || isGuid)
                {
                    Debug.WriteLine(item.Key + " = " + item.Value);
                    sql = Regex.Replace(sql, $@"(?<!\w){Regex.Escape(item.Key)}(?!\w)", "'" + item.Value.ToString() + "'");
                }
                else if (isEnum)
                {
                    Debug.WriteLine(item.Key + " = " + Enum.GetName(type, item.Value));
                    sql = Regex.Replace(sql, $@"(?<!\w){Regex.Escape(item.Key)}(?!\w)", "'" + Enum.GetName(type, item.Value) + "'");
                }
                else
                {
                    Debug.WriteLine(item.Key + " = " + item.Value);
                    sql = Regex.Replace(sql, $@"(?<!\w){Regex.Escape(item.Key)}(?!\w)", item.Value.ToString()!);
                }
            }

            // 修改原sql
            sql = sql.Replace("t.", "");
            sql = sql.Replace("AS t", "");
            sql = sql.Replace("' + '%'", "%'");
            sql = sql.Replace("'%' + '", "'%");
            sql = sql.Replace("__", ".");
            foreach (var item in aliasFields)
            {
                sql = Regex.Replace(sql, $@"(?<!\w){Regex.Escape(item.Name)}(?!\w)", item.AliasName);
            }
            Debug.WriteLine("处理后sql：" + sql);
            return sql;
        }, false).Where(expression, ref hasWhere).Sql;
#if NETSTANDARD2_1_OR_GREATER
        return hasWhere ? sql.Split("WHERE")[1].TrimStart().TrimEnd() : "";
#else
        return hasWhere ? sql.Substring(sql.IndexOf("WHERE") + 5).TrimStart().TrimEnd() : "";
#endif
    }

    [DebuggerStepThrough]
    public static string GetOrderString<T>(Expression<Func<T, object>>? expression, params OrderType[]? orders) where T : class
    {
        if (expression == null) { return ""; }
        var fields = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(t => t.GetCustomAttributes(typeof(AliasAttribute), true).Length > 0).Select(t => new
        {
            t.Name,
            AliasName = t.GetCustomAttribute<AliasAttribute>()?.Name ?? t.Name
        });
        return new SqlBuilderCore<T>(DatabaseType.SqlServer, (sql, parameter) =>
        {
            foreach (var item in fields)
            {
                sql = Regex.Replace(sql, $@"(?<!\w){Regex.Escape(item.Name)}(?!\w)", item.AliasName);
            }
            sql = sql.Replace("__", ".");
            sql = sql.Replace("ORDER BY", "");
            return sql.TrimStart().TrimEnd();
        }, false).OrderBy(expression, orders).Sql;
    }
    /// <summary>
    /// 特定规则字符转换成属性,用于属性与属性之间比较
    /// </summary>
    /// <param name="name">双花括号‘{{}}’包裹的字符</param>
    /// <returns></returns>
    private static MemberExpression? ToPropertyExpression(ParameterExpression param, string? name)
    {
        if (string.IsNullOrEmpty(name))
        {
            throw new ArgumentNullException(nameof(name));
        }
        Regex re = new(@"{{([\w\s-_.]+)}}");
        if (re.IsMatch(name))
        {
            // 取出双花括号‘{{}}’包裹的字符
            name = re.Match(name).Groups[1].Value;
            PropertyInfo? pf = param.Type.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(x => x.Name.Equals(name, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            return pf == null ? null : Expression.Property(param, pf);
        }
        return null;
    }
    private static dynamic? ExpTmpFieldDynamicCall(Type type, string? input)
    {
        if (string.IsNullOrEmpty(input))
        {
            throw new ArgumentNullException(nameof(input));
        }
        try
        {
            MethodInfo? method = typeof(ExpTmpField<>).MakeGenericType(type).GetMethod("Parse");
            return method?.Invoke(null, [input]);
        }
        catch (Exception)
        {
            throw;
        }
    }
}
/// <summary>
/// 临时赋值使用
/// </summary>
[DebuggerStepThrough]
public class ExpTmpField<T> where T : IEquatable<T>
{
    public T[] Arr;
    /// <summary>
    /// 数组长度，用于判断是否为空值
    /// </summary>
    public int Length { get; set; }
    private ExpTmpField(string arr)
    {
        Arr = ParseToArray<T>(arr);
        Length = Arr.Length;
    }
    public static ExpTmpField<T> Parse(string arr)
    {
        return new(arr);
    }
    public FieldInfo ArrFieldInfo()
    {
        return FieldInfo.GetFieldFromHandle(GetType().GetField(nameof(Arr))!.FieldHandle, GetType().TypeHandle);
    }
    private static T1[] ParseToArray<T1>(string obj)
    {
        if (!string.IsNullOrEmpty(obj))
        {
#if NETSTANDARD2_1_OR_GREATER
            string[] arrStr = obj.Split(',', StringSplitOptions.RemoveEmptyEntries);
#else
            string[] arrStr = obj.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
#endif
            T1[] arr_T1 = new T1[arrStr.Length];
            for (int i = 0; i < arrStr.Length; i++)
            {
                try { arr_T1[i] = (T1)Convert.ChangeType(arrStr[i], typeof(T1)); } catch { }
            }
            return arr_T1.Distinct().ToArray();
        }
        return [];
    }
}