﻿using System.Text;
using System.Text.Json;

namespace K3Cloud.WebApi.Core.IoC.Extensions
{
    public static partial class ExpressionBuilder
    {
        public static string ParseExpressionOf(JsonDocument doc)
        {
            string conditions = ParseTree(doc.RootElement);
            return conditions ?? "";
        }

        private static string ParseTree(JsonElement condition)
        {
            StringBuilder sb = new();
            _ = sb.Append("1=1 ");
            foreach (JsonElement rule in condition.EnumerateArray())
            {
                string? logicalOperator = rule.GetProperty(nameof(logicalOperator)).GetString();
                string? conditionFieldVal = rule.GetProperty(nameof(conditionFieldVal)).GetString();
                string? conditionOptionVal = rule.GetProperty(nameof(conditionOptionVal)).GetString();
                JsonElement conditionValueVal = rule.GetProperty(nameof(conditionValueVal));
                JsonElement conditionValueLeftVal = rule.GetProperty(nameof(conditionValueLeftVal));
                JsonElement conditionValueRightVal = rule.GetProperty(nameof(conditionValueRightVal));
                _ = conditionValueVal.TryGetProperty("value", out JsonElement value);
                _ = conditionValueLeftVal.TryGetProperty("value", out JsonElement leftValue);
                _ = conditionValueRightVal.TryGetProperty("value", out JsonElement rightValue);
                _ = sb.Append(logicalOperator + " ");
                if (conditionOptionVal == In)
                {
                    _ = sb.Append(conditionFieldVal + " IN (" + value + ") ");
                }
                else if (conditionOptionVal == NotIn)
                {
                    _ = sb.Append(conditionFieldVal + " Not IN (" + value + ") ");
                }
                else if (conditionOptionVal == Like)
                {
                    _ = sb.Append(conditionFieldVal + " Like '%" + value + "%' ");
                }
                else if (conditionOptionVal == Start)
                {
                    _ = sb.Append(conditionFieldVal + " Like '" + value + "%' ");
                }
                else if (conditionOptionVal == End)
                {
                    _ = sb.Append(conditionFieldVal + " Like '%" + value + "' ");
                }
                else if (conditionOptionVal == NotLike)
                {
                    _ = sb.Append(conditionFieldVal + " Not Like '%" + value + "'% ");
                }
                else if (conditionOptionVal == Equal)
                {
                    _ = sb.Append(conditionFieldVal + " = '" + value + "' ");
                }
                else if (conditionOptionVal == Unequal)
                {
                    _ = sb.Append(conditionFieldVal + " <> '" + value + "' ");
                }
                else if (conditionOptionVal == Empty)
                {
                    _ = sb.Append(conditionFieldVal + " IS NULL ");
                }
                else if (conditionOptionVal == NotEmpty)
                {
                    _ = sb.Append(conditionFieldVal + " IS NOT NULL ");
                }
                else if (conditionOptionVal == Greater_than)
                {
                    _ = sb.Append(conditionFieldVal + " > '" + value + "' ");
                }
                else if (conditionOptionVal == Less_than)
                {
                    _ = sb.Append(conditionFieldVal + " < '" + value + "' ");
                }
                else if (conditionOptionVal == Between)
                {
                    _ = sb.Append(conditionFieldVal + " >= '" + leftValue + "' AND " + conditionFieldVal + " <= '" + rightValue + "' ");
                }
            }
            return sb.ToString();
        }
    }
}
