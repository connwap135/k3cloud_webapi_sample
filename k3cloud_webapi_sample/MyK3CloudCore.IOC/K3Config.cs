﻿using Microsoft.Extensions.Options;

namespace K3Cloud.WebApi.Core.IoC
{
    /// <summary>
    /// 第三方系统登录授权信息配置
    /// </summary>
    public sealed class K3Config : IOptions<K3Config>
    {
        /// <summary>
        /// 服务器地址
        /// </summary>
        public string ServerHost { get; set; }
        /// <summary>
        /// 数据中心Id
        /// </summary>
        public string AcctId { get; set; }
        /// <summary>
        /// 集成用户
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 用户密码(无应用密钥必填)
        /// </summary>
        public string? Password { get; set; }
        /// <summary>
        /// 应用Id
        /// </summary>
        public string AppID { get; set; }
        /// <summary>
        /// 应用密钥
        /// </summary>
        public string AppSecret { get; set; }
        /// <summary>
        /// 区域设置标识号
        /// </summary>
        public int LCID { get; set; } = 2052;
        /// <summary>
        /// 连接有效时间(单位:分钟)
        /// 0或不设置则每次连接都进行登录验证
        /// </summary>
        public int ConnectionValidTime { get; set; }
        public K3Config Value => this;
    }

    public class IocConst
    {
        public const string CLIENT_KEY = "{EA97F979-5B45-4EFA-95D1-6208F8323308}";
    }
}