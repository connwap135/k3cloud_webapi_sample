﻿using System;

namespace K3Cloud.WebApi.Core.IoC
{
    public sealed class K3ApiException : Exception
    {
        public K3ApiException() { }

        public K3ApiException(string message) : base(message) { }

        public K3ApiException(string message, Exception innerException) : base(message, innerException) { }
    }

}
