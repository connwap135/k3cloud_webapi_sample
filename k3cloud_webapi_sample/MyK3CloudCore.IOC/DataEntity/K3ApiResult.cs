﻿using System;
using System.Collections.Generic;

namespace K3Cloud.WebApi.Core.IoC.DataEntity
{
    /// <summary>
    /// K3统一返回参数
    /// </summary>
    public class K3ApiResult<T> where T : class
    {
        public T Result { get; set; }
    }
    public sealed class K3Success
    {
        public string Id { get; set; }
        public string Number { get; set; }
        public string BillNo { get; set; }
        public int DIndex { get; set; }

    }
    public sealed class K3Errors
    {
        public string FieldName { get; set; }
        public string Message { get; set; }
        public int DIndex { get; set; }

    }
    public sealed class K3ResponseStatus
    {
        public bool IsSuccess { get; set; }
        public List<K3Errors> Errors { get; set; }
        public List<K3Success> SuccessEntitys { get; set; }
        public List<K3Errors> SuccessMessages { get; set; }
        /// <summary>
        /// 错误代码
        /// </summary>
        public K3MsgCodeType MsgCode { get; set; }
        public int ErrorCode { get; set; }
    }

    [Flags]
    public enum K3MsgCodeType : int
    {
        默认,
        上下文丢失,
        没有权限,
        操作标识为空,
        异常,
        单据标识为空,
        数据库操作失败,
        许可错误,
        参数错误,
        指定字段值不存在,
        未找到对应数据,
        验证失败,
        不可操作,
        网控冲突
    }
    [Flags]
    public enum K3LanguageType : int
    {
        English = 1033,
        简体中文 = 2052,
        繁体中文 = 3076
    }
}
