﻿using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Security.Cryptography.X509Certificates;

namespace K3Cloud.WebApi.Core.IoC.DataEntity
{
    /// <summary>
    /// 登录结果
    /// </summary>
    public sealed class LoginResult
    {
        public string Message { get; set; }
        public string MessageCode { get; set; }
        public LoginResultType LoginResultType { get; set; }
        public Context Context { get; set; }
        public string KDSVCSessionId { get; set; }
        public string FormId { get; set; }
        public RedirectFormParam RedirectFormParam { get; set; }
        public object FormInputObject { get; set; }
        public string ErrorStackTrace { get; set; }
        public int Lcid { get; set; }
        public string AccessToken { get; set; }
        public object CustomParam { get; set; }
        public AccessTokenResult KdAccessResult { get; set; }
        public bool IsSuccessByAPI { get; set; }

    }

    /// <summary>
    /// 登录上下文环境
    /// </summary>
    public sealed class Context
    {
        /// <summary>
        /// //数据库类型  登录时设置
        /// </summary>
        public DataBaseCategory DatabaseCategory { get; set; }

        /// <summary>
        /// 上下文ID
        /// </summary>
        public string ContextId { get; set; }

        /// <summary>
        /// 微博AuthInfo
        /// </summary>
        public object WeiboAuthInfo { get; set; }

        /// <summary>
        /// 是否启用时区转换
        /// </summary>
        public bool IsStartTimeZoneTransfer { get; set; }

        /// <summary>
        /// 登录时输入的名称
        /// </summary>
        public string LoginName { get; set; }

        /// <summary>
        /// 登陆时的角色：App/微信/云之家/...
        /// </summary>
        public string EntryRole { get; set; }

        /// <summary>
        /// 当前Context对应的用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 客户端类型
        /// </summary>
        public ClientType ClientType { get; set; }

        /// <summary>
        /// 密码调料
        /// </summary>
        public string Salt { get; set; }

        /// <summary>
        /// 当前Context对应的用户ID
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 用户认证令牌
        /// </summary>
        public string UserToken { get; set; }

        /// <summary>
        /// 用户手机
        /// </summary>
        public string UserPhone { get; set; }

        /// <summary>
        /// 用户资料邮箱
        /// </summary>
        public string UserEmail { get; set; }

        /// <summary>
        /// 用户认证方式
        /// </summary>
        public AuthenticationType UserAuthenticationMethod { get; set; }

        /// <summary>
        /// 用户登录类型
        /// </summary>
        public int UserLoginType { get; set; }

        /// <summary>
        /// 用户的CA证书
        /// </summary>
        public X509Certificate UserX509Certificate { get; set; }

        /// <summary>
        /// 数据库类型属性,登录时设置
        /// </summary>
        public DatabaseType DatabaseType { get; set; }

        /// <summary>
        /// 客户名称
        /// </summary>
        public string CustomName { get; set; }
        /// <summary>
        /// 事务隔离级别
        /// </summary>
        public IsolationLevel TransIsolationLevel { get; set; }

        /// <summary>
        /// 应用服务器地址
        /// </summary>
        public string ServerUrl { get; set; }

        /// <summary>
        /// 主控表单唯一标识
        /// </summary>
        public string ConsoleFormId { get; set; }

        /// <summary>
        /// 主控页面标识
        /// </summary>
        public string ConsolePageId { get; set; }

        /// <summary>
        /// 用户事务ID
        /// </summary>
        public string UserTransactionId { get; set; }

        public string DataCenterName { get; set; }

        /// <summary>
        /// 数据中心编码
        /// </summary>
        public string DataCenterNumber { get; set; }

        /// <summary>
        ///  上下文类型 判断是否可用
        /// </summary>
        public ContextResultType ContextResultType { get; set; }

        public string CurrentServerName { get; set; }

        public string ModuleName { get; set; }

        public string CallerName { get; set; }

        public string CallStack { get; set; }

        /// <summary>
        /// 租户标识；
        /// 公有云使用
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// 金蝶通行证id
        /// </summary>
        public string KDPassportId { get; set; }

        /// <summary>
        /// 登陆ip
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// 网关IP
        /// </summary>
        public string GateWayIp { get; set; }

        /// <summary>
        /// 登陆电脑名称
        /// </summary>
        public string ComputerName { get; set; }

        /// <summary>
        /// 该属性提供context创建时候的一个唯一标识
        /// </summary>
        public string CreateContextGuid { get; set; }

        /// <summary>
        /// “仅查询用户”分组，打开单据、报表时，只能是查看状态，无法进行其他业务操作
        /// </summary>
        public bool IsViewOnly { get; set; }

        /// <summary>
        /// 会话标识
        /// </summary>
        public string SessionId { get; set; }

        /// <summary>
        /// 用来显示的版本号；之后再定义一个结构性的ProductVersion来显示完整的版本；
        /// 显示版本号可能的格式如下：
        /// 1:  5.0.0.1
        /// 2:  5.0.0.1(sp1)
        /// 3:  5.0.0.1(sp1+FIN 5.0.1.2) 
        /// </summary>
        public string DisplayVersion { get; set; }

        /// <summary>
        /// 关联的查询库数据中心
        /// </summary>
        public string QueryDBId { get; set; }

        /// <summary>
        /// 日志中心DBid
        /// </summary>
        public string LogDbId { get; set; }

        /// <summary>
        /// 日志中心上下文
        /// 日志库不存在为null
        /// </summary>
        public Context LogDBContext { get; set; }

        /// <summary>
        /// 全局唯一数据中心标识(获取业务帐套上下文时赋值)
        /// </summary>
        public string GDCID { get; set; }

        /// <summary>
        /// 当前组织信息
        /// </summary>
        public OrganizationInfo CurrentOrganizationInfo { get; set; }

        public CultureInfo UserLocale { get; set; }

        /// <summary>
        /// //数据库ID（帐套管理唯一标识）====由于目前为空值,此处设置为K3Makalu，主要为缓存测试使用 by eckel
        /// </summary>
        public string DBId { get; set; }

        /// <summary>
        /// 注意，获取用户国际化信息使用UserLocale，
        /// 本属性可能为NULL，只用于记录日志用；
        /// </summary>
        public CultureInfo LogLocale { get; set; }

        /// <summary>
        /// 是否启用多组织
        /// </summary>
        public bool IsMultiOrg { get; set; }


        /// <summary>
        /// 系统中用户数据语言
        /// </summary>
        public List<LanguageInfo> UseLanguages { get; set; }
        /// <summary>
        /// 是否简繁自动转换
        /// </summary>
        public bool IsCH_ZH_AutoTrans { get; set; }

        /// <summary>
        /// 用户访问标识
        /// </summary>
        public string Gsid { get; set; }

        /// <summary>
        /// 跟踪等级
        /// </summary>
        public int TRLevel { get; set; }

        /// <summary>
        /// 产品版本：1:标准版，0:企业版
        /// </summary>
        public int ProductEdition { get; set; }


        public enum DataBaseCategory
        {
            /// <summary>
            /// 正常业务数据库
            /// </summary>
            Normal = 1,
            /// <summary>
            /// 归档数据库
            /// </summary>
            Archive,
            /// <summary>
            /// 报表数据库(只读)
            /// </summary>
            Report,
            /// <summary>
            /// 管理中心数据库
            /// </summary>
            ManagementCenter,
            /// <summary>
            /// 多语言中心数据库
            /// </summary>
            MultiLanguageCenter,
            /// <summary>
            /// 日志中心
            /// </summary>
            LogCenter
        }


    }
    public enum DatabaseType
    {
        MS_SQL_Server = 3,
        Oracle = 2,
        PostgreSQL = 5,
        MySQL,
        Oracle9,
        Oracle10
    }
    /// <summary>
	/// 客户端类型
	/// </summary>
	public enum ClientType
    {
        WPF = 1,
        Silverlight,
        JavaScript = 4,
        /// <summary>
        /// 移动终端
        /// </summary>
        Mobile = 8,
        /// <summary>
        /// HTML桌面浏览器客户端
        /// </summary>
        Html = 16,
        /// <summary>
        /// WebApi接口
        /// </summary>
        WebApi = 32,
        BOSIDE = 64,
        NotePrint = 128,
        DevReport = 256,
        /// <summary>
        /// 音箱类型
        /// </summary>
        Speaker = 512
    }
    /// <summary>
	/// 登录方式
	/// </summary>
	public enum AuthenticationType
    {
        DomainAuthentication,
        PwdAuthentication,
        DynamicPwdAuthentication,
        CAAuthentication,
        /// <summary>
        /// 讯通轻应用认证方式
        /// </summary>
        XuntongAuthentication,
        /// <summary>
        /// 微信订阅号/服务号
        /// </summary>
        WeixinPublicAuthentication,
        /// <summary>
        /// 微信企业号
        /// </summary>
        WeixinEntAuthentication,
        /// <summary>
        /// cloud云通行证认证
        /// </summary>
        CloudEntryAuthentication,
        /// <summary>
        /// boside云通行证验证
        /// </summary>
        IDECloudEntryAuthentication,
        /// <summary>
        /// 第三方系统简单通行证登录（非密码公钥加密方式）
        /// </summary>
        SimplePassportAuthentication,
        /// <summary>
        /// 云之家身份认证
        /// </summary>
        YunZhiJiaAuthentication,
        /// <summary>
        /// 云之家动态密码验证
        /// </summary>
        YunZhiJiaDynamicPwdAuthentication,
        /// <summary>
        /// 云之家二维码
        /// </summary>
        YunZhiJiaQRCodeAuthentication,
        /// <summary>
        /// 刷脸认证
        /// </summary>
        FaceAuthentication,
        /// <summary>
        /// 云之家公共号身份认证
        /// </summary>
        YunZhiJiaPubAuthentication,
        /// <summary>
        /// userToken登陆
        /// </summary>
        UserTokenAuthentication,
        /// <summary>
        /// SAML登陆
        /// </summary>
        SAMLAuthentication,
        /// <summary>
        /// OAuth2.0身份验证
        /// </summary>
        OAuth2Authentication,
        /// <summary>
        /// 加密签名身份验证
        /// </summary>
        EncryptSignAuthentication,
        /// <summary>
        /// 钉钉身份认证
        /// </summary>
        DingDingAuthentication,
        /// <summary>
        /// 金蝶云.通行证身份
        /// </summary>
        KDPassPortAuthentication,
        /// <summary>
        /// 第三方通行证RSA验权身份登录
        /// </summary>
        RSASignAuthentication,
        /// <summary>
        /// KingdeeHub 云平台通过AuthCode获取用户信息身份
        /// </summary>
        KingdeeHubAuthentication,
        /// <summary>
        /// 微信第三方登录
        /// </summary>
        WeiXin3rdAuthentication,
        /// <summary>
        /// 第三方系统登录
        /// </summary>
        ThirdAuthentication,
        /// <summary>
        /// 双因子登录
        /// </summary>
        TwoFactorAuthentication,
        /// <summary>
        /// IMC华为工业云CAS登录
        /// </summary>
        CasAuthentication
    }
    public enum ContextResultType
    {
        /// <summary>
        /// 成功
        /// </summary>
        Success,
        /// <summary>
        /// 未激活
        /// </summary>
        UnActivation,
        /// <summary>
        /// 修改用户密码
        /// </summary>
        UpdatePWD
    }
    public sealed class OrganizationInfo
    {
        public long ID { get; set; }

        public string AcctOrgType { get; set; }

        public string Name { get; set; }

        public List<long> FunctionIds { get; set; }
    }

    public sealed class LanguageInfo
    {
        public int LocaleId { get; set; }

        public string LocaleName { get; set; }

        public string Alias { get; set; }

        public LicenseType LicenseType { get; set; }
    }
    /// <summary>
	/// 许可类型
	/// </summary>
	public enum LicenseType
    {
        /// <summary>
        /// 演示许可
        /// </summary>
        DemoLicense = 1,
        /// <summary>
        /// 限时许可
        /// </summary>
        LimitedTimLicensee,
        /// <summary>
        /// 正式许可
        /// </summary>
        GenuineLicense
    }
    public sealed class RedirectFormParam
    {
        public string FormId { get; set; }

        public string PageId { get; set; }

        public string FormType { get; set; }

        /// <summary>
        /// 打开单据宽带
        /// </summary>
        public int FormWidth { get; set; }
        /// <summary>
        /// 打开单据长度
        /// </summary>
        public int FormHeight { get; set; }

        /// <summary>
        /// 需要默认打开或处理的表单的布局ID
        /// </summary>
        public string FormLayoutId { get; set; }
    }
#pragma warning disable IDE1006 // 命名样式
    public sealed class AccessTokenResult
    {
        public int errcode { get; set; }
        /// <summary>
        /// 结果或错误描述
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// 绑定结果
        /// </summary>
        public AccessTokenData data { get; set; }
    }

    public sealed class AccessTokenData
    {
        public string access_token { get; set; }

        public string avatar { get; set; }
        public string ename { get; set; }

        public string euid { get; set; }

        public int expires_in { get; set; }

        public string nickname { get; set; }

        public string uid { get; set; }
    }
#pragma warning restore IDE1006 // 命名样式
    /// <summary>
    /// 返回结果类型
    /// </summary>
    public enum LoginResultType
    {
        /// <summary>
        /// 登录成功
        /// </summary>
        Success = 1,
        /// <summary>
        /// 用户或密码错误
        /// </summary>
        PWError = 0,
        /// <summary>
        /// 登录失败
        /// </summary>
        Failure = -1,
        /// <summary>
        /// 密码验证不通过（可选的）
        /// </summary>
        PWInvalid_Optional = -2,
        /// <summary>
        /// 密码验证不通过（强制的）
        /// </summary>
        PWInvalid_Required = -3,
        /// <summary>
        /// 登录警告
        /// </summary>
        Wanning = -4,
        /// <summary>
        /// 需要表单处理
        /// </summary>
        DealWithForm = -5,
        /// <summary>
        /// 云通行证未绑定Cloud账号
        /// </summary>
        EntryCloudUnBind = -6,
        /// <summary>
        /// 激活
        /// </summary>
        Activation = -7,
        /// <summary>
        /// 正常异常,不做任何操作
        /// </summary>
        NormalException = -8
    }
}
