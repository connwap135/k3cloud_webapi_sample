﻿using System;
using System.Collections.Generic;

namespace K3Cloud.WebApi.Core.IoC.DataEntity
{
    /// <summary>
    /// K3元数据
    /// </summary>
    public sealed class K3BusinessInfoResult
    {
        public K3ResponseStatus ResponseStatus { get; set; }
        public K3BusinessNeedReturn NeedReturnData { get; set; }
    }

    public sealed class K3BusinessNeedReturn
    {
        public string Id { get; set; }
        public List<K3LanguageTranslate> Name { get; set; }
        public string MasterPKFieldName { get; set; }
        public string PkFieldName { get; set; }
        public Type PkFieldType { get; set; }
        public List<K3Table> Entrys { get; set; }
        public List<K3OperationsResult> Operations { get; set; }
    }

    public sealed class K3OperationsResult
    {
        public int OperationId { get; set; }
        public string OperationNumber { get; set; }
        public List<K3LanguageTranslate> OperationName { get; set; }
    }

    public sealed class K3Table
    {

        public string Id { get; set; }
        public string Key { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public List<K3LanguageTranslate> Name { get; set; }
        /// <summary>
        /// 返回此实体映射的表对应的名称
        /// </summary>
        public string TableName { get; set; }
        public string ParentKey { get; set; }
        /// <summary>
        /// 返回/设置 对应到ORM中实体类型的名称
        /// </summary>
        public string EntryName { get; set; }
        /// <summary>
        /// 分录主键字段名
        /// </summary>
        public string EntryPkFieldName { get; set; }
        /// <summary>
        /// 主键类型
        /// </summary>
        public Type PkFieldType { get; set; }
        /// <summary>
        /// 返回此实体定义的所有字段项
        /// </summary>
        public List<K3FieldResult> Fields { get; set; }
        /// <summary>
        /// 返回实体的类型，单分录/多分录
        /// </summary>
        public string EntityType { get; set; }
        /// <summary>
        /// 表示数据顺序的字段名称
        /// </summary>
        public string SeqFieldKey { get; set; }
    }

    public sealed class K3FieldResult
    {
        /// <summary>
        /// 元数据的key
        /// </summary>
        public string Key { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public List<K3LanguageTranslate> Name { get; set; }
        /// <summary>
        /// 返回/设置字段在数据库中的名称
        /// </summary>
        public string FieldName { get; set; }
        /// <summary>
        /// 返回/设置 此字段在ORM中的映射，即在ORM实体中通过什么属性获取其值。
        /// </summary>
        public string PropertyName { get; set; }
        /// <summary>
        /// 字段存储于数据库的数据类型
		/// </summary>
        public SqlStorageType FieldType { get; set; }
        /// <summary>
        /// 返回/设置此字段所属实体的名称(TODO:是否应该改为 Entity Entity{get;})
        /// </summary>
        public string EntityKey { get; set; }
        /// <summary>
        /// 表名
        /// </summary>
        public string TableName { get; set; }
        /// <summary>
        /// 元素类型编码
        /// 值查看 K3ElementType
        /// </summary>
        public int ElementType { get; set; }
        /// <summary>
        /// 返回此字段是否必须录入
        /// </summary>
        public int MustInput { get; set; }
        /// <summary>
        /// 关联基础资料表单
        /// </summary>
        public string LookUpObjectFormId { get; set; }
        public string EnumObjectId { get; set; }
        public List<K3ExtendsField> Extends { get; set; }
        /// <summary>
        /// 该字段的控制字段，在BusinessInfo的反序列化填充
        /// </summary>
        public string ControlFieldKey { get; set; }
        public string GroupFieldTableName { get; set; }
    }

    public sealed class K3ExtendsField
    {
        public string Value { get; set; }
        public string Caption { get; set; }
        public int Seq { get; set; }
        public bool Invalid { get; set; }
    }

    public sealed class K3LanguageTranslate
    {
        private K3LanguageType key;

        public K3LanguageType Key
        {
            get => key;
            set
            {
                key = value;
                Lcid = (int)value;
            }
        }
        public string Value { get; set; }
        public int Lcid { get; internal set; }

    }
    /// <summary>
    /// 字段数据库存储类型
    /// </summary>
    /// <remarks>
    /// SELECT * FROM systypes
    /// </remarks>
    [Flags]
    public enum SqlStorageType
    {
        SqlImage = 34,
        SqlText,
        SqlUniqueidentifier,
        SqlTinyint = 48,
        SqlSmallint = 52,
        SqlInt = 56,
        SqlSmalldatetime = 58,
        SqlReal,
        SqlMoney,
        SqlDatetime,
        SqlFloat,
        SqlnText = 99,
        SqlBit = 104,
        SqlDecimal = 106,
        SqlNumeric = 108,
        SqlSmallmoney = 122,
        SqlBigInt = 127,
        Sqlvarbinary = 165,
        Sqlvarchar = 167,
        SqlBinary = 173,
        Sqlchar = 175,
        SqlTimestamp = 189,
        Sqlnvarchar = 231,
        Sqlnchar = 239,
        SqlXML = 241
    }

    public sealed class K3BusinessToCSharpType
    {
        public static string GetType(SqlStorageType sqlStorageType)
        {
            return sqlStorageType switch
            {
                SqlStorageType.SqlImage or SqlStorageType.SqlText or SqlStorageType.Sqlvarbinary or SqlStorageType.SqlBinary or SqlStorageType.SqlTimestamp or SqlStorageType.SqlXML => "byte[]",
                SqlStorageType.SqlTinyint => "byte",
                SqlStorageType.SqlSmallint => "short",
                SqlStorageType.SqlInt => "int",
                SqlStorageType.SqlSmalldatetime or SqlStorageType.SqlDatetime => "DateTime",
                SqlStorageType.SqlReal => "float",
                SqlStorageType.SqlMoney or SqlStorageType.SqlSmallmoney or SqlStorageType.SqlDecimal or SqlStorageType.SqlNumeric => "decimal",
                SqlStorageType.SqlFloat => "double",
                SqlStorageType.SqlnText => "string",
                SqlStorageType.SqlBit => "bool",
                SqlStorageType.SqlBigInt => "long",
                SqlStorageType.Sqlvarchar or SqlStorageType.Sqlnvarchar or SqlStorageType.Sqlchar or SqlStorageType.Sqlnchar or SqlStorageType.SqlUniqueidentifier => "string",
                _ => "string",
            };
        }
    }

    public sealed class K3BusinessToCSharpTypeForJava
    {
        public static string GetJavaType(SqlStorageType sqlStorageType)
        {
            return sqlStorageType switch
            {
                SqlStorageType.SqlImage or SqlStorageType.SqlText or SqlStorageType.Sqlvarbinary or SqlStorageType.SqlBinary or SqlStorageType.SqlTimestamp or SqlStorageType.SqlXML => "Byte[]",
                SqlStorageType.SqlTinyint => "Byte",
                SqlStorageType.SqlSmallint => "Short",
                SqlStorageType.SqlInt => "Integer",
                SqlStorageType.SqlSmalldatetime or SqlStorageType.SqlDatetime => "java.util.Date",
                SqlStorageType.SqlReal => "Float",
                SqlStorageType.SqlMoney or SqlStorageType.SqlSmallmoney or SqlStorageType.SqlDecimal or SqlStorageType.SqlNumeric => "java.math.BigDecimal",
                SqlStorageType.SqlFloat => "Double",
                SqlStorageType.SqlnText => "String",
                SqlStorageType.SqlBit or SqlStorageType.Sqlchar => "Boolean",
                SqlStorageType.SqlBigInt => "Integer",
                SqlStorageType.Sqlvarchar or SqlStorageType.Sqlnvarchar or SqlStorageType.Sqlnchar or SqlStorageType.SqlUniqueidentifier => "String",
                _ => "String",
            };
        }

        public static string GetJavaType(Type fieldType)
        {
            if (fieldType == typeof(DateTime))
            {
                return "java.util.Date";
            }
            else if (fieldType == typeof(int))
            {
                return "Integer";
            }
            else if (fieldType == typeof(long))
            {
                return "Long";
            }
            else if (fieldType == typeof(double))
            {
                return "Double";
            }
            else if (fieldType == typeof(decimal))
            {
                return "java.math.BigDecimal";
            }
            else if (fieldType == typeof(string))
            {
                return "String";
            }
            else if (fieldType == typeof(bool))
            {
                return "Boolean";
            }
            else if (fieldType == typeof(byte))
            {
                return "Byte";
            }
            else if (fieldType == typeof(short))
            {
                return "Short";
            }
            else if (fieldType == typeof(float))
            {
                return "Float";
            }
            else if (fieldType == typeof(char))
            {
                return "Character";
            }
            else if (fieldType.IsArray)
            {
                Type? elementType = fieldType.GetElementType();
                if(elementType == null)
                {
                    return "String[]";
                }
                return GetJavaType(elementType) + "[]";
            }
            else
            {
                return "String";
            }
        }

    }
    // 写一个类，用来存储Java属性的信息
    public sealed class JavaPropertyInfo
    {  // 用来存储字段的名称
        public string Name { get; set; }
        // 用来存储字段的类型
        public string Type { get; set; }
        // 用来存储字段的注释
        public string Comment { get; set; }
        // 原始查询的字段名称
        public string SrcName { get; set; }
    }
}