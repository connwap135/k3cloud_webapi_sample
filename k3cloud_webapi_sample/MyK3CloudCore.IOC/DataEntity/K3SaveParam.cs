﻿using K3Cloud.WebApi.Core.IoC.Extensions;
using Newtonsoft.Json;
using System;

namespace K3Cloud.WebApi.Core.IoC.DataEntity
{
    /// <summary>
    /// K3表单<typeparamref name="T"/>保存更新类
    /// </summary>
    public sealed class K3SaveParam<T> : BaseEntify where T : class
    {
        /// <summary>
        /// K3表单<typeparamref name="T"/>保存更新类
        /// </summary>
        public K3SaveParam() { }
        public K3SaveParam(T model) : this()
        {
            Model = model;
        }
        /// <summary>
        /// 需要更新的字段
        /// 数组类型，格式：[key1,key2,...] （非必录）注（更新单据体字段得加上单据体key）
        /// </summary>
        public string[] NeedUpDateFields { get; set; } = [];
        /// <summary>
        /// 需返回结果的字段集合
        /// 数组类型，格式：[key,entitykey.key,...]（非必录） 注（返回单据体字段格式：entitykey.key）
        /// </summary>
        public string[] NeedReturnFields { get; set; } = [];
        /// <summary>
        /// 是否删除已存在的分录
        /// 布尔类型，默认true（非必录）
        /// </summary>
        public string IsDeleteEntry { get; set; } = "true";
        /// <summary>
        /// 表单所在的子系统内码
        /// 字符串类型（非必录）
        /// </summary>
        public string SubSystemId { get; set; } = "";
        /// <summary>
        /// 是否验证所有的基础资料有效性
        /// 布尔类，默认false（非必录）
        /// </summary>
        public string IsVerifyBaseDataField { get; set; } = "false";
        /// <summary>
        /// 是否批量填充分录
        /// 默认true（非必录）
        /// </summary>
        public string IsEntryBatchFill { get; set; } = "true";
        /// <summary>
        /// 是否验证标志
        /// 布尔类型，默认true（非必录）
        /// </summary>
        public string ValidateFlag { get; set; } = "true";
        /// <summary>
        /// 是否用编码搜索基础资料
        /// 布尔类型，默认true（非必录）
        /// </summary>
        public string NumberSearch { get; set; } = "true";
        /// <summary>
        /// 是否自动调整JSON字段顺序
        /// 布尔类型，默认false（非必录）
        /// </summary>
        public string IsAutoAdjustField { get; set; } = "false";
        /// <summary>
        /// 交互标志集合
        /// 字符串类型，分号分隔，格式："flag1;flag2;..."（非必录）
        /// 例如（允许负库存标识：STK_InvCheckResult）
        /// </summary>
        public string InterationFlags { get; set; } = "";
        /// <summary>
        /// 是否允许忽略交互
        /// 布尔类型，默认true（非必录）
        /// </summary>
        public string IgnoreInterationFlag { get; set; } = "true";
        /// <summary>
        /// 是否控制精度，为true时对金额、单价和数量字段进行精度验证，默认false（非必录）
        /// </summary>
        public string IsControlPrecision { get; set; } = "false";
        /// <summary>
        /// 校验Json数据包是否重复传入，一旦重复传入，接口调用失败，默认false（非必录）
        /// </summary>
        public string ValidateRepeatJson { get; set; } = "false";
        /// <summary>
        /// 表单模型
        /// </summary>
        [JsonProperty(Order = int.MaxValue)]
        public T Model { get; set; }

        /// <summary>
        /// 表单FormId
        /// </summary>
        public string GetFormId()
        {
            return typeof(T).GetAlias();
        }
    }
}
