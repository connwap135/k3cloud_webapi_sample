﻿namespace K3Cloud.WebApi.Core.IoC.DataEntity
{
    public class WorkflowParam
    {
        /// <summary>
        /// 业务对象表单Id（必录）
        /// </summary>
        public string FormId { get; set; }
        /// <summary>
        /// 单据内码集合，字符串类型，格式："Id1,Id2,..."（使用内码时必录）
        /// </summary>
        public string Ids { get; set; }
        /// <summary>
        /// 单据编码集合，数组类型，格式：[No1, No2,...]（使用编码时必录）
        /// </summary>
        public string[] Numbers { get; set; }
        /// <summary>
        /// 审批人用户Id，整型
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 用户名称，字符串类型
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 审批类型，整型（1：审批通过；2：驳回；3：终止）
        /// </summary>
        public int ApprovalType { get; set; }
        /// <summary>
        /// 审批项Id，字符串类型
        /// </summary>
        public string ActionResultId { get; set; }
        /// <summary>
        /// 岗位Id，整型
        /// </summary>
        public int PostId { get; set; }
        /// <summary>
        /// 岗位编码，字符串类型
        /// </summary>
        public string PostNumber { get; set; }
        /// <summary>
        /// 审批意见，字符串类型
        /// </summary>
        public string Disposition { get; set; }
    }
}
