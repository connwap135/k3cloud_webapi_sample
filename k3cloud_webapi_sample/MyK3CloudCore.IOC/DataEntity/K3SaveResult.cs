﻿using System.Collections.Generic;

namespace K3Cloud.WebApi.Core.IoC.DataEntity
{
    public sealed class K3SaveResult
    {
        public K3ResponseStatus ResponseStatus { get; set; }
        public string Id { get; set; }
        public string Number { get; set; }
        public List<object> NeedReturnData { get; set; }

    }
}
