﻿namespace K3Cloud.WebApi.Core.IoC.DataEntity.PresetEntity
{
    /// <summary>
    /// 金蝶用户
    /// </summary>
    public class SEC_User
    {
        /// <summary>
        /// Id
        /// </summary>
        public int FUserId { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string FName { get; set; }
        /// <summary>
        /// 分组
        /// </summary>
        public string FPRIMARYGROUP__FNumber { get; set; }
        /// <summary>
        /// 禁用状态
        /// </summary>
        public string FForbidStatus { get; set; }
    }
}
