﻿namespace K3Cloud.WebApi.Core.IoC.DataEntity.PresetEntity;

/// <summary>
/// 简单账表查询模型
/// </summary>
public class SysReportModel : BaseEntify
{
    /// <summary>
    /// 需查询的字段key集合
    /// 字符串类型，格式："key1,key2,..."（必录）注（简单账表可能存在动态字段，指定待查字段时，须确保当前查询条件的结果集中包含待查字段）
    /// </summary>
    public string FieldKeys { get; set; }
    /// <summary>
    /// 过滤方案内码，字符串类型
    /// </summary>
    public string SchemeId { get; set; }
    /// <summary>
    /// 开始行索引，整型（非必录）
    /// </summary>
    public int? StartRow { get; set; }
    /// <summary>
    /// 最大行数，整型，不能超过10000（非必录）
    /// </summary>
    public int? Limit { get; set; }
    /// <summary>
    /// 是否验证所有的基础资料有效性，布尔类，默认true（非必录）
    /// </summary>
    public bool IsVerifyBaseDataField { get; set; } = true;
    /// <summary>
    /// 表单数据包（必录）
    /// 具体字段BOS平台查看对应账表过滤条件框
    /// </summary>
    public object Model { get; set; }
}
