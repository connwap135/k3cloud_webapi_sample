﻿using System.Collections.Generic;

namespace K3Cloud.WebApi.Core.IoC.DataEntity.PresetEntity;

/// <summary>
/// 即时库存结果返回
/// </summary>
#pragma warning disable IDE1006 // 命名样式
public class ResponseModel
{
    public int rowcount { get; set; }

    /// <summary>
    /// 查询成功与否
    /// </summary>
    public bool success { get; set; }

    /// <summary>
    /// 消息
    /// </summary>
    public string message { get; set; }

    /// <summary>
    /// 返回数据
    /// </summary>
    public List<InventoryModel> data { get; set; }
}
#pragma warning restore IDE1006 // 命名样式

