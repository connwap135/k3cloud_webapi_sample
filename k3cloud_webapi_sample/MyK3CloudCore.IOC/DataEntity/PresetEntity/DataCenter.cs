﻿using System;

namespace K3Cloud.WebApi.Core.IoC.DataEntity.PresetEntity
{
    /// <summary>
	/// 数据中心基础类
	/// </summary>
    [Serializable]
    public class DataCenter
    {
        /// <summary>
        /// 唯一标识
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 转化为字符串
        /// </summary>
        public override string ToString()
        {
            return Name;
        }
    }
}

