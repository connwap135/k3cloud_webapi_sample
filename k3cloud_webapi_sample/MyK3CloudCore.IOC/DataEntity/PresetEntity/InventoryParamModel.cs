﻿namespace K3Cloud.WebApi.Core.IoC.DataEntity.PresetEntity;

/// <summary>
/// 即时库存查询参数Model
/// </summary>
#pragma warning disable IDE1006 // 命名样式
public class InventoryParamModel
{
    /// <summary>
    /// 组织编码，多个用,分隔
    /// </summary>
    public string fstockorgnumbers { get; set; }

    /// <summary>
    /// 物料编码，多个用,分隔
    /// </summary>
    public string fmaterialnumbers { get; set; }

    /// <summary>
    /// 仓库编码，多个用,分隔
    /// </summary>
    public string fstocknumbers { get; set; }

    /// <summary>
    /// 批号编码，多个用,分隔
    /// </summary>
    public string flotnumbers { get; set; }

    /// <summary>
    /// 最小库存，不包含最小值
    /// </summary>
    public decimal? fminbaseqty { get; set; }

    /// <summary>
    /// 最大库存，包含最大值
    /// </summary>
    public decimal? fmaxbaseqty { get; set; }

    /// <summary>
    /// 是否显示零库存
    /// </summary>
    public bool? isshowzeroinv { get; set; }

    /// <summary>
    /// 最近更新日期
    /// </summary>
    public string lastupdatetime { get; set; }

    /// <summary>
    /// 是否查询仓位信息
    /// </summary>
    public bool isshowstockloc { get; set; }

    /// <summary>
    /// 是否查询辅助属性信息
    /// </summary>
    public bool isshowauxprop { get; set; }

    /// <summary>
    /// 当前页
    /// </summary>
    public int pageindex { get; set; }

    /// <summary>
    /// 每页显示行数
    /// </summary>
    public int pagerows { get; set; }
}
#pragma warning restore IDE1006 // 命名样式