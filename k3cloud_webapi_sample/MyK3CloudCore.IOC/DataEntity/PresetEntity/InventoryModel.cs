﻿using System;

namespace K3Cloud.WebApi.Core.IoC.DataEntity.PresetEntity
{
    /// <summary>
    /// 即时库存MODEL
    /// </summary>
    public class InventoryModel
    {
        /// <summary>
        /// 即时库存内码
        /// </summary>
        public string FID { get; set; }

        /// <summary>
        /// 库存组织ID
        /// </summary>
        public long FSTOCKORGID { get; set; }

        /// <summary>
        /// 库存组织编码
        /// </summary>
        public string FSTOCKORGNUMBER { get; set; }

        /// <summary>
        /// 库存组织名称
        /// </summary>
        public string FSTOCKORGNAME { get; set; }

        /// <summary>
        /// 保管者类型
        /// </summary>
        public string FKEEPERTYPEID { get; set; }

        /// <summary>
        /// 保管者类型名称
        /// </summary>
        public string FKEEPERTYPENAME { get; set; }

        /// <summary>
        /// 保管者ID
        /// </summary>
        public long FKEEPERID { get; set; }

        /// <summary>
        /// 保管者编码
        /// </summary>
        public string FKEEPERNUMBER { get; set; }

        /// <summary>
        /// 保管者名称
        /// </summary>
        public string FKEEPERNAME { get; set; }

        /// <summary>
        /// 货主类型ID
        /// </summary>
        public string FOWNERTYPEID { get; set; }

        /// <summary>
        /// 货主类型
        /// </summary>
        public string FOWNERTYPENAME { get; set; }

        /// <summary>
        /// 货主ID
        /// </summary>
        public long FOWNERID { get; set; }

        /// <summary>
        /// 货主编码
        /// </summary>
        public string FOWNERNUMBER { get; set; }

        /// <summary>
        /// 货主
        /// </summary>
        public string FOWNERNAME { get; set; }

        /// <summary>
        /// 仓库ID
        /// </summary>
        public long FSTOCKID { get; set; }

        /// <summary>
        /// 仓库编码
        /// </summary>
        public string FSTOCKNUMBER { get; set; }

        /// <summary>
        /// 仓库名称
        /// </summary>
        public string FSTOCKNAME { get; set; }

        /// <summary>
        /// 仓位ID
        /// </summary>
        public long FSTOCKLOCID { get; set; }

        /// <summary>
        /// 仓位
        /// </summary>
        public string FSTOCKLOC { get; set; }

        /// <summary>
        /// 辅助属性ID
        /// </summary>
        public long FAUXPROPID { get; set; }

        /// <summary>
        /// 负责属性
        /// </summary>
        public string FAUXPROP { get; set; }

        /// <summary>
        /// 库存状态ID
        /// </summary>
        public long FSTOCKSTATUSID { get; set; }

        /// <summary>
        /// 库存状态编码
        /// </summary>
        public string FSTOCKSTATUSNUMBER { get; set; }

        /// <summary>
        /// 库存状态名称
        /// </summary>
        public string FSTOCKSTATUSNAME { get; set; }

        /// <summary>
        /// 批号Id
        /// </summary>
        public long FLOT { get; set; }

        /// <summary>
        /// 批号
        /// </summary>
        public string FLOTNUMBER { get; set; }

        /// <summary>
        /// BOM版本Id
        /// </summary>
        public long FBOMID { get; set; }

        /// <summary>
        /// BOM版本
        /// </summary>
        public string FBOMNUMBER { get; set; }

        /// <summary>
        /// 计划跟踪号
        /// </summary>
        public string FMTONO { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string FPROJECTNO { get; set; }

        /// <summary>
        /// 生产日期
        /// </summary>
        public DateTime? FPRODUCEDATE { get; set; }

        /// <summary>
        /// 有效期
        /// </summary>
        public DateTime? FEXPIRYDATE { get; set; }

        /// <summary>
        /// 基本单位Id
        /// </summary>
        public long FBASEUNITID { get; set; }

        /// <summary>
        /// 基本单位编码
        /// </summary>
        public string FBASEUNITNUMBER { get; set; }

        /// <summary>
        /// 基本单位
        /// </summary>
        public string FBASEUNITNAME { get; set; }

        /// <summary>
        /// 库存数量(基本单位)
        /// </summary>
        public decimal FBASEQTY { get; set; }

        /// <summary>
        /// 锁库数量(基本单位)
        /// </summary>
        public decimal FBASELOCKQTY { get; set; }

        /// <summary>
        /// 辅助单位数量
        /// </summary>
        public decimal FSECQTY { get; set; }

        /// <summary>
        /// 辅助单位锁库数量
        /// </summary>
        public decimal FSECLOCKQTY { get; set; }

        /// <summary>
        /// 库存单位ID
        /// </summary>
        public long FSTOCKUNITID { get; set; }

        /// <summary>
        /// 库存单位编码
        /// </summary>
        public string FSTOCKUNITNUMBER { get; set; }

        /// <summary>
        /// 库存单位
        /// </summary>
        public string FSTOCKUNITNAME { get; set; }

        /// <summary>
        /// 物料内码
        /// </summary>
        public long FMATERIALID { get; set; }

        /// <summary>
        /// 物料内码
        /// </summary>
        public long FMASTERID { get; set; }

        /// <summary>
        /// 物料编码
        /// </summary>
        public string FMATERIALNUMBER { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string FMATERIALNAME { get; set; }

        /// <summary>
        /// 库存数量
        /// </summary>
        public decimal FQTY { get; set; }

        /// <summary>
        /// 锁库数量(库存单位)
        /// </summary>
        public decimal FLOCKQTY { get; set; }

        /// <summary>
        /// 辅助单位Id
        /// </summary>
        public long FSECUNITID { get; set; }

        /// <summary>
        /// 辅助单位编码
        /// </summary>
        public string FSECUNITNUMBER { get; set; }

        /// <summary>
        /// 辅助单位
        /// </summary>
        public string FSECUNITNAME { get; set; }

        public string FOBJECTTYPEID { get; set; }

        /// <summary>
        /// 可用量(基本单位)
        /// </summary>
        public decimal FBASEAVBQTY { get; set; }

        /// <summary>
        /// 可用量(库存单位)
        /// </summary>
        public decimal FAVBQTY { get; set; }

        /// <summary>
        /// 可用量(辅助单位)
        /// </summary>
        public decimal FSECAVBQTY { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime FUPDATETIME { get; set; }
    }
}