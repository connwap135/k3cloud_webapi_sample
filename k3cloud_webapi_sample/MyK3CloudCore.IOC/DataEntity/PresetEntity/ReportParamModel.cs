﻿using System.Collections.Generic;

namespace K3Cloud.WebApi.Core.IoC.DataEntity.PresetEntity;

public class ReportParamModel : BaseEntify
{
    public string FORMID { get; set; }
    public string FSCHEMEID { get; set; }
    public List<ReportQuicklyConditionModel> QuicklyCondition { get; set; }
    public List<ReportMoreConditionModel> MoreCondition { get; set; }
    public int StartRow { get; set; }
    public int Limit { get; set; } = 10000;
    public string CurQueryId { get; set; }
    public string FieldKeys { get; set; }
}
public class ReportQuicklyConditionModel
{
    /// <summary>
    /// 绑定实体属性
    /// </summary>
    public string FieldName { get; set; }
    public string FieldValue { get; set; }
}
public class ReportMoreConditionModel
{
    public string Left { get; set; }

    public string FieldName { get; set; }

    public string Compare { get; set; }

    public string FieldValue { get; set; }

    public string Right { get; set; }

    public string Logic { get; set; }
}