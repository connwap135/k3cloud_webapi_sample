﻿using System.Collections.Generic;
using System.Data;

namespace K3Cloud.WebApi.Core.IoC.DataEntity.PresetEntity;

public class SysReportResult
{
    public Result Result { get; set; }
}

public class Result
{
    public bool IsSuccess { get; set; }
    public int RowCount { get; set; }
    public K3ResponseStatus ResponseStatus { get; set; }
    public List<List<object>> Rows { get; set; }
}

public class SCMReportResult
{
#pragma warning disable IDE1006
    public bool success { get; set; }
    public string message { get; set; }
    public string CurQueryId { get; set; }
    public DataTable data { get; set; }
#pragma warning restore IDE1006
}