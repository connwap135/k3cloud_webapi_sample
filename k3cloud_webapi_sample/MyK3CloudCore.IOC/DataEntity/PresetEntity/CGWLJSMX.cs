﻿using K3Cloud.WebApi.Core.IoC.Attributes;
using Newtonsoft.Json;
using System;

namespace K3Cloud.WebApi.Core.IoC.DataEntity.PresetEntity;

/// <summary>
/// 采购物料计算明细
/// </summary>
[Alias("ora_CGWLJSBB")]
public class CGWLJSMX
{
    /// <summary>
    /// 订单编号
    /// </summary>
    public string FSALEORDERNO { get; set; }
    /// <summary>
    /// 产品名称
    /// </summary>
    public string FPRODUCTNAME { get; set; }
    /// <summary>
    /// 规格型号
    /// </summary>
    public string FMODEL { get; set; }
    /// <summary>
    /// 订单数量
    /// </summary>
    public double FQUANTITYOFORDER { get; set; }
    /// <summary>
    /// 分子
    /// </summary>
    public int FNUMBERATOR { get; set; }
    /// <summary>
    /// 分母
    /// </summary>
    public int FDENOMINATOR { get; set; }
    /// <summary>
    /// 需求数量
    /// </summary>
    public double FREQUIREDQUANTITY { get; set; }
    /// <summary>
    /// 需求日期
    /// </summary>
    [JsonConverter(typeof(ShortDateConvert))]
    public DateTime FDATE { get; set; }
    /// <summary>
    /// 物料名称
    /// </summary>
    public string FMATERIALNAME { get; set; }
    /// <summary>
    /// 物料规格
    /// </summary>
    public string FSPECIFICATION { get; set; }
    /// <summary>
    /// 即时库存数量
    /// </summary>
    public double FINVENTORYQUANTITY { get; set; }
    /// <summary>
    /// 生产用料清单未领数量
    /// </summary>
    public double FUNCLAIMEDNUMBER { get; set; }
    /// <summary>
    /// 仓库
    /// </summary>
    public string FSTOCKNAME { get; set; }
    /// <summary>
    /// 库存状态
    /// </summary>
    public string FSTOCKSTATUS { get; set; }
    /// <summary>
    /// 采购剩余未入库数量
    /// </summary>
    public double FREMAINSTOCKINQTY { get; set; }
    /// <summary>
    /// 结存数量
    /// </summary>
    public double FENDQTY { get; set; }

}
