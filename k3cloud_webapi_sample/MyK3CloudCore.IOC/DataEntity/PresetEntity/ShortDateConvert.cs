﻿using Newtonsoft.Json.Converters;

namespace K3Cloud.WebApi.Core.IoC.DataEntity.PresetEntity;

/// <summary>
/// 短日期转换
/// </summary>
public class ShortDateConvert : IsoDateTimeConverter
{
    public ShortDateConvert() : base()
    {
        DateTimeFormat = "yyyy-MM-dd";
    }
}