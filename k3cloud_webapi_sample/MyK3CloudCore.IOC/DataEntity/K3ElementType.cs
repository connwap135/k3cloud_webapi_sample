﻿namespace K3Cloud.WebApi.Core.IoC.DataEntity
{
    /// <summary>
    /// Kingdee.BOS.Core.dll
    /// Kingdee.BOS.Core.Metadata.ElementMetadata.ElementType
    /// </summary>
    public enum K3ElementType
    {
        /// <summary>
        /// 元素分类_业务对象
        /// </summary>
        TYPE_FORM = 1,

        /// <summary>
        /// 元素分类_实体
        /// </summary>
        TYPE_ENTITY = 2,

        /// <summary>
        /// 元素分类_控件
        /// </summary>
        TYPE_CONTROL = 3,

        /// <summary>
        /// 元素分类_字段
        /// </summary>
        TYPE_FIELD = 4,

        /// <summary>
        /// 元素分类_菜单
        /// </summary>
        TYPE_BARITEM = 5,

        /// <summary>
        /// 元素分类_操作 ??
        /// </summary>
        TYPE_OPERATION = 6,

        /// <summary>
        /// 文本
        /// </summary>
        ELEMENTTYPE_TEXTFIELD = 1,

        /// <summary>
        /// 小数
        /// </summary>
        ELEMENTTYPE_DECIMALFIELD = 2,

        /// <summary>
        /// 整数
        /// </summary>
        ELEMENTTYPE_INTEGERFIELD = 3,
        /// <summary>
        /// 日期
        /// </summary>
        ELEMENTTYPE_DATEFIELD = 4,

        /// <summary>
        /// 长日期
        /// </summary>
        ELEMENTTYPE_DATETIMEFIELD = 5,

        /// <summary>
        /// 多行文本
        /// </summary>
        ELEMENTTYPE_TEXTAREAFIELD = 6,

        /// <summary>
        /// 组织
        /// </summary>
        ELEMENTTYPE_ORGFIELD = 7,

        /// <summary>
        /// 复选框
        /// </summary>
        ELEMENTTYPE_CHECKBOXFIELD = 8,

        /// <summary>
        /// 单据编号
        /// </summary>
        ELEMENTTYPE_BILLNOFIELD = 12,

        /// <summary>
        /// 基础资料
        /// </summary>
        ELEMENTTYPE_BASEDATAFIELD = 13,

        /// <summary>
        /// 基础资料属性字段
        /// </summary>
        ELEMENTTYPE_BASEPROPERTYFIELD = 14,

        /// <summary>
        /// 创建人
        /// </summary>
        ELEMENTTYPE_CREATERFIELD = 17,

        /// <summary>
        /// 用户
        /// </summary>
        ELEMENTTYPE_USERFIELD = 18,

        /// <summary>
        /// 批次
        /// </summary>
        ELEMENTTYPE_LOTFIELD = 19,

        /// <summary>
        /// 单价
        /// </summary>
        ELEMENTTYPE_PRICEFIELD = 20,

        /// <summary>
        /// 金额字段
        /// </summary>
        ELEMENTTYPE_AMOUNTFIELD = 21,

        /// <summary>
        /// 数量
        /// </summary>
        ELEMENTTYPE_QTYFIELD = 22,

        /// <summary>
        /// 创建日期
        /// </summary>
        ELEMENTTYPE_CREATEDATEFIELD = 26,

        /// <summary>
        /// 最近修改人
        /// </summary>
        ELEMENTTYPE_MODIFYERFIELD = 27,

        /// <summary>
        /// 最近修改日期
        /// </summary>
        ELEMENTTYPE_MODIFYDATEFIELD = 28,

        /// <summary>
        /// 代理字段
        /// </summary>
        ELEMENTTYPE_PROXYFIELD = 39,

        /// <summary>
        /// 单选辅助资料
        /// </summary>
        ELEMENTTYPE_ASSISTANTFIELD = 30,

        /// <summary>
        /// 多选辅助资料列表
        /// </summary>
        ELEMENTTYPE_MULASSISTANTFIELD = 31,

        /// <summary>
        /// 分组
        /// </summary>
        ELEMENTTYPE_GROUPFIELD = 33,

        /// <summary>
        /// 单据状态字段
        /// </summary>
        ELEMENTTYPE_BILLSTATUSFIELD = 40,

        /// <summary>
        /// 单据类型
        /// </summary>
        ELEMENTTYPE_BILLTYPEFIELD = 44,

        /// <summary>
        /// 大文本字段
        /// </summary>
        ELEMENTTYPE_LARGETEXTFIELD = 45,

        /// <summary>
        /// 基本单位数量
        /// </summary>
        ELEMENTTYPE_BASEQTYFIELD = 47,

        /// <summary>
        /// 业务流程
        /// </summary>
        ELEMENTTYPE_BUSINESSFLOWFIELD = 56,

        /// <summary>
        /// 图片字段
        /// </summary>
        ELEMENTTYPE_IMAGEFIELD = 1011,

        /// <summary>
        /// 附件字段
        /// </summary>
        ELEMENTTYPE_ATTACHMENT = 60014,

        /// <summary>
        /// 图片（文件服务）字段
        /// </summary>
        ELEMENTTYPE_IMAGEFILESERVER = 60015,

        /// <summary>
        /// 税务明细单据体
        /// </summary>
        ELEMENTTYPE_TAXDETAILSUBENTRYENTITY = 60528,

        /// <summary>
        /// 拆分数量字段
        /// </summary>
        ELEMENTTYPE_DISAFIELD = 41,

        /// <summary>
        /// 序列号子单据体
        /// </summary>
        ELEMENTTYPE_SNSUBENTRYENTITY = 60531,

        /// <summary>
        /// 多选基础资料
        /// </summary>
        ELEMENTTYPE_MULBASEDATAFIELD = 24,

        /// <summary>
        /// 多类别基础资料列表
        /// </summary>
        ELEMENTTYPE_ITEMTYPECLASSFIELD = 15,

        /// <summary>
        /// 多类别基础资料
        /// </summary>
        ELEMENTTYPE_ITEMCLASSFIELD = 16,

        /// <summary>
        /// 下拉列表字段
        /// </summary>
        ELEMENTTYPE_COMBOFIELD = 9,

        /// <summary>
        /// 多选下拉列表
        /// </summary>
        ELEMENTTYPE_MULCOMBOFIELD = 29,

        /// <summary>
        /// 基础资料文本字段
        /// </summary>
        ELEMENTTYPE_BASEDATATEXTFIELD = 42,

        /// <summary>
        /// 单位类型字段
        /// </summary>
        ELEMENTTYPE_UNITFIELD = 46,

        /// <summary>
        /// 图章(水印)
        /// </summary>
        ELEMENTTYPE_WATERMARK = 80236,

        /// <summary>
        /// 电子签章
        /// </summary>
        ELEMENTTYPE_ESIGNATURE = 82479,

        /// <summary>
        /// 单选按钮组
        /// </summary>
        ELEMENTTYPE_RADIOGROUPFIELD = 32,

        /// <summary>
        /// 单选按钮
        /// </summary>
        ELEMENTTYPE_RADIOFIELD = 25,

        /// <summary>
        /// 单分录单据体
        /// </summary>
        ELEMENTTYPE_SINGLEROWENTITY = 80,

        /// <summary>
        /// 打印次数
        /// </summary>
        ELEMENTTYPE_PRINTTIMES = 82469,

        /// <summary>
        /// 打印时间
        /// </summary>
        ELEMENTTYPE_PRINTDATETIME = 82472,

        /// <summary>
        /// 表格图片
        /// </summary>
        ELEMENTTYPE_PICTURE = 82473,

        /// <summary>
        /// 私有下拉列表
        /// </summary>
        ELEMENTTYPE_PRIVATECOMBO = 2011,

        /// <summary>
        /// 富文本内容编辑
        /// </summary>
        ELEMENTTYPE_LARGERICHTEXTFIELD = 85,

        /// <summary>
        /// 组合字段
        /// </summary>
        ELEMENTTYPE_COMBINEFIELD = 1800,

        /// <summary>
        /// 维度关联字段。
        /// </summary>
        ELEMENTTYPE_RELATEDFLEXGROUPFIELD = 60523,

        /// <summary>
        /// 树形子单据体
        /// </summary>
        ELEMENTTYPE_TREESUBENTRY = 88,

        /// <summary>
        /// 移动代理分录
        /// </summary>
        ELEMENTTYPE_MOBILEPROXYENTRY = 1064,

        /// <summary>
        /// 元素类型常量_单据模版
        /// </summary>
        ELEMENTTYPE_BILL_MODEL = -100,

        /// <summary>
        /// 元素类型常量_动态表单
        /// </summary>
        ELEMENTTYPE_DYNAMICFORM = 500,

        ELEMENTTYPE_DYNAMICFILTER = 502,

        /// <summary>
        /// 套打
        /// </summary>
        ELEMENTTYPE_Note = 600,

        /// <summary>
        /// 元素类型常量_对话框
        /// </summary>
        ELEMENTTYPE_DIALOG = 220,

        /// <summary>
        /// 元素类型常量_混合界面
        /// </summary>
        ELEMENTTYPE_COMBINUI = 210,

        /// <summary>
        /// 元素类型常量_业务单据
        /// </summary>
        ELEMENTTYPE_BILL = 100,

        /// <summary>
        ///
        /// </summary>
        ELEMENTTYPE_FLEX = 800,

        /// <summary>
        /// 元素类型常量_日志单据
        /// </summary>
        ELEMENTTYPE_LOGBILL = 2200,

        /// <summary>
        /// 元素类型常量_基础资料
        /// </summary>
        ELEMENTTYPE_BASE = 400,

        /// <summary>
        /// 辅助资料
        /// </summary>
        ELEMENTTYPE_ASSISTANTDATA = 401,

        /// <summary>
        /// 核算项目
        /// </summary>
        ELEMENTTYPE_ITEMCLASS = 402,

        /// <summary>
        /// 元素类型常量_过滤条件
        /// </summary>
        ELEMENTTYPE_FILTER = 200,

        /// <summary>
        /// 元素类型常量_工作流
        /// </summary>
        ELEMENTTYPE_WORKFLOW = 300,

        /// <summary>
        /// 元素类型常量_帐表
        /// </summary>
        ELEMENTTYPE_SYSREPORT = 900,

        /// <summary>
        /// 元素类型常量_简单汇总账表
        /// </summary>
        ELEMENTTYPE_EASYSUMMARYREPORT = 901,

        /// <summary>
        /// 元素类型常量_简单明细账表
        /// </summary>
        ELEMENTTYPE_EASYDETAILREPORT = 902,

        /// <summary>
        /// 元素类型常量_简单交叉账表
        /// </summary>
        ELEMENTTYPE_EASYCROSSREPORT = 903,

        /// <summary>
        /// 元素类型常量_万能报表
        /// </summary>
        ELEMENTTYPE_WNREPORT = 1600,

        /// <summary>
        /// 元素类型常量_BOS表达式
        /// </summary>
        ELEMENTTYPE_BOSEXPRESSION = 1200,

        /// <summary>
        /// 元素类型常量_电子表格
        /// </summary>
        ELEMENTTYPE_GRID = 1000,

        ELEMENTTYPE_SIMPLESYSREPORT = 900,

        ELEMENTTYPE_TREESYSREPORT = 901,

        ELEMENTTYPE_PAEGEDSYSREPORT = 902,

        ELEMENTTYPE_CROSSREPORT = 904,

        /// <summary>
        ///  SQL帐表
        /// </summary>
        ELEMENTTYPE_SQLREPORT = 903,

        /// <summary>
        /// 元素类型常量_单据转换
        /// </summary>
        ELEMENTTYPE_CONVERT = 700,

        /// <summary>
        /// 元素类型常量_状态迁移图
        /// </summary>
        ELEMENTTYPE_TRACKER = 710,

        /// <summary>
        /// 元素类型常量_单据头
        /// </summary>
        ELEMENTTYPE_BILLHEAD = 34,

        /// <summary>
        /// 元素类型常量_子单据头
        /// </summary>
        ELEMENTTYPE_SUBBILLHEAD = 38,

        /// <summary>
        /// 元素类型常量_单据体
        /// </summary>
        ELEMENTTYPE_BILLBODY = 35,

        /// <summary>
        /// 元素类型常量_树形单据体
        /// </summary>
        ELEMENTTYPE_TREEBILLBODY = 48,

        /// <summary>
        /// 元素类型常量_子单据体
        /// </summary>
        ELEMENTTYPE_SUBBILLBODY = 60502,

        /// <summary>
        /// 元素类型常量_甘特图单据体
        /// </summary>
        ELEMENTTYPE_GANTTCHART = 60530,

        /// <summary>
        /// 元素类型常量_过滤器定制页签
        /// </summary>
        ELEMENTTYPE_CUSTOMFILTERPANEL = 201,

        /// <summary>
        /// 元素类型常量_过滤器过滤条件面板
        /// </summary>
        ELEMENTTYPE_FILTERPANEL = 202,

        /// <summary>
        /// 元素类型常量_过滤器排序面板
        /// </summary>
        ELEMENTTYPE_SORTPANEL = 203,

        /// <summary>
        /// 元素类型常量_过滤器汇总面板
        /// </summary>
        ELEMENTTYPE_GROUPPANEL = 204,

        /// <summary>
        /// 元素类型常量_参数面板
        /// </summary>
        ELEMENTTYPE_COMBINUITOPPANEL = 211,

        /// <summary>
        /// 元素类型常量_插件页签
        /// </summary>
        ELEMENTTYPE_COMBINUIPLUGIN = 212,

        /// <summary>
        /// 元素类型常量_页签控件
        /// </summary>
        ELEMENTTYPE_TABCONTROL = 1005,

        /// <summary>
        /// 元素类型常量_子页签
        /// </summary>
        ELEMENTTYPE_TABPAGE = 1004,

        /// <summary>
        /// 元素类型常量_单据编号
        /// </summary>
        ELEMENTTYPE_BILLNO = 12,

        /// <summary>
        /// 链接控件
        /// </summary>
        ELEMENTTYPE_LINKBTN = 1014,

        /// <summary>
        /// 按钮控件
        /// </summary>
        ELEMENTTYPE_BUTTON = 1002,

        /// <summary>
        /// 源单类型字段
        /// </summary>
        ELEMENTTYPE_SOURCEBILLTYPEFIELD = 50,

        /// <summary>
        /// 元素类型常量_是否多组织
        /// </summary>
        ELEMENTTYPE_ISMULTIORG = 12088,

        /// <summary>
        /// 元素类型常量_当前组织
        /// </summary>
        ELEMENTTYPE_CURRENTORGID = 12078,

        /// <summary>
        /// 元素类型常量_主菜单
        /// </summary>
        BarItemElementType_MenuBar = 2000,

        /// <summary>
        /// 元素类型常量_工具栏
        /// </summary>
        BarItemElementType_ToolBar = 2001,

        /// <summary>
        /// 元素类型常量_右键菜单
        /// </summary>
        BarItemElementType_PopupMenu = 2002,

        /// <summary>
        /// 元素类型常量_状态栏
        /// </summary>
        BarItemElementType_StatusBar = 2003,

        /// <summary>
        /// 元素类型常量_分割菜单
        /// </summary>
        BarItemElementType_Seperator = 2004,

        /// <summary>
        /// 元素类型常量_按钮菜单
        /// </summary>
        BarItemElementType_Button = 2005,

        BarItemElementType_SplitButton = 2018,

        /// <summary>
        /// 元素类型常量_复选菜单
        /// </summary>
        BarItemElementType_Check = 2006,

        /// <summary>
        /// 元素类型常量_下拉框菜单
        /// </summary>
        BarItemElementType_Combox = 2007,

        /// <summary>
        /// 元素类型常量_下拉按钮菜单
        /// </summary>
        BarItemElementType_DropDownButton = 2008,

        /// <summary>
        /// 元素类型常量_子下拉按钮菜单
        /// </summary>
        BarItemElementType_SubDropDownButton = 2009,

        /// <summary>
        /// 元素类型常量_文本框菜单
        /// </summary>
        BarItemElementType_TextField = 2010,

        /// <summary>
        /// 元素类型常量_子菜单
        /// </summary>
        BarItemElementType_SubItem = 2015,

        /// <summary>
        /// 元素类型常量_子分类菜单
        /// </summary>
        BarItemElementType_SubCategory = 2016,

        /// <summary>
        /// 表单上的向导步骤元素，便于构建向导式表单应用
        /// </summary>
        FormElement_WizardStep = 2017,
    }
}