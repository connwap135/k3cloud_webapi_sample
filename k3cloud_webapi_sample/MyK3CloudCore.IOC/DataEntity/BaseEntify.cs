﻿using Newtonsoft.Json;

namespace K3Cloud.WebApi.Core.IoC.DataEntity
{
    /// <summary>
    /// 实体基类
    /// </summary>
    public class BaseEntify
    {
        /// <summary>
        /// JSON格式数据
        /// </summary>
        public virtual string ToJson()
        {
            JsonSerializerSettings settings = new()
            {
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Ignore,
                DateFormatString = "yyyy-MM-dd HH:mm:ss"
            };
            return JsonConvert.SerializeObject(this, settings);
        }
        /// <summary>
        /// 序列化
        /// </summary>
        public static T? Parse<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}
