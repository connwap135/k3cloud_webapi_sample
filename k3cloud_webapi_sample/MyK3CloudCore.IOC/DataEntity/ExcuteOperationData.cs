﻿using System.Collections.Generic;

namespace K3Cloud.WebApi.Core.IoC.DataEntity
{
    /// <summary>
    /// 操作接口JSON格式数据
    /// </summary>
    public class ExcuteOperationData
    {
        /// <summary>
        /// 创建者组织内码，字符串类型（非必录）
        /// </summary>
        public int CreateOrgId { get; set; }

        /// <summary>
        /// 单据编码集合，数组类型，格式：[No1,No2,...]（使用编码时必录）
        /// </summary>
        public List<string> Numbers { get; set; } = [];

        /// <summary>
        /// 单据内码集合，字符串类型，格式："Id1,Id2,..."（使用内码时必录）
        /// </summary>
        public string Ids { get; set; } = "";

        /// <summary>
        /// 单据内码与分录内码对应关系的集合，字符串类型，格式：[{"Id":"Id1","EntryIds":"EntryId1,EntryId2,..."}] (使用分录状态转换时必录)
        /// </summary>
        public List<PkEntryIdsItem> PkEntryIds { get; set; }

        /// <summary>
        /// 是否启用网控，布尔类型，默认false（非必录）
        /// </summary>
        public string NetworkCtrl { get; set; } = "false";

        /// <summary>
        /// 是否允许忽略交互，布尔类型，默认true（非必录）
        /// </summary>
        public string IgnoreInterationFlag { get; set; } = "true";
    }

    public class PkEntryIdsItem
    {
        public string Id { get; set; }
        public string EntryIds { get; set; }
    }
}
