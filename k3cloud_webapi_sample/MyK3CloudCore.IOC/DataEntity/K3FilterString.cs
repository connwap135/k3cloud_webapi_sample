﻿using Newtonsoft.Json;

namespace K3Cloud.WebApi.Core.IoC.DataEntity
{
    /// <summary>
    /// 过滤条件，数组类型，
    /// 如：[{"Left":"(","FieldName":"Field1","Compare":"=","Value":"111","Right":")","Logic":"AND"},
    /// {"Left":"(","FieldName":"Field2","Compare":"=","Value":"222","Right":")","Logic":""}]
    /// </summary>
    public sealed class K3FilterString
    {
        /// <summary>
        /// 左括号
        /// </summary>
        [JsonProperty(Order = 0)]
        public string Left { get; set; } = "";
        /// <summary>
        /// 字段名
        /// </summary>
        [JsonProperty(Order = 1)]
        public string FieldName { get; set; }
        /// <summary>
        /// <![CDATA[比较运算符，如　大于">"、小于"<"、等于"="、包含"like"、左包含"llike"、右包含"rlike"]]> 
        /// </summary>
        [JsonProperty(Order = 2)]
        public string Compare { get; set; }
        /// <summary>
        /// 比较值
        /// </summary>
        [JsonProperty(Order = 3)]
        public string Value { get; set; }
        /// <summary>
        /// 右括号
        /// </summary>
        [JsonProperty(Order = 4)]
        public string Right { get; set; } = "";
        /// <summary>
        /// 逻辑运算符，如 "and"、"or"
        /// </summary>
        [JsonProperty(Order = 5)]
        public string Logic { get; set; } = "";
    }
}
