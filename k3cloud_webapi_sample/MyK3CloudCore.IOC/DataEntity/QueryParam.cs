﻿using System.Text;

namespace K3Cloud.WebApi.Core.IoC.DataEntity
{
    public sealed class QueryParam : BaseEntify
    {
        public string FormId { get; set; }

        public string FieldKeys { get; set; }

        public string FilterString { get; set; }

        public string OrderString { get; set; }

        public int StartRow { get; set; }

        public int Limit { get; set; }

        public int TopRowCount { get; set; }

        /// <summary>
        /// 生成缓存键的方法
        /// </summary>
        public string GenerateCacheKey()
        {
            StringBuilder cacheKeyBuilder = new();
            _ = cacheKeyBuilder.Append("FormId=").Append(FormId);
            _ = cacheKeyBuilder.Append("|FieldKeys=").Append(FieldKeys);
            _ = cacheKeyBuilder.Append("|FilterString=").Append(FilterString);
            _ = cacheKeyBuilder.Append("|OrderString=").Append(OrderString);
            _ = cacheKeyBuilder.Append("|StartRow=").Append(StartRow);
            _ = cacheKeyBuilder.Append("|Limit=").Append(Limit);
            _ = cacheKeyBuilder.Append("|TopRowCount=").Append(TopRowCount);
            return cacheKeyBuilder.ToString();
        }
    }
}
