﻿using System.Collections.Generic;

namespace K3Cloud.WebApi.Core.IoC.DataEntity
{
    public sealed class K3CloudMessage
    {
        public List<K3MessageModel> Model { get; set; }
        public K3CloudMessage() { }
        public K3CloudMessage(List<K3MessageModel> model)
        {
            Model = model;
        }
    }

    public sealed class K3MessageModel
    {
        /// <summary>
        /// 标题
        /// </summary>
        public string? FTitle { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public string? FContent { get; set; }
        /// <summary>
        /// 收件人
        /// (多个联系人用逗号隔开)
        /// </summary>
        public string? FReceivers { get; set; }
        /// <summary>
        /// 消息类型
        /// </summary>
        public MsgType FType { get; set; } = MsgType.CommonMessage;
        /// <summary>
        /// 单据FormId
        /// </summary>
        public string? FObjectTypeId { get; set; }
        /// <summary>
        /// 单据内码(FId)
        /// </summary>
        public string? FKeyValue { get; set; }
        /// <summary>
        /// 附件(数据库)
        /// </summary>
        public string? FFileUpdate { get; set; }
    }
    /// <summary>
	/// 消息类型
	/// </summary>
	public enum MsgType
    {
        /// <summary>
        /// 工作流消息
        /// </summary>
        WorkflowMessage,
        /// <summary>
        /// 普通消息
        /// </summary>
        CommonMessage,
        /// <summary>
        /// 业务流程普通消息
        /// </summary>
        BusinessFlowMessage
    }
}
