using Kingdee.BOS;
using Kingdee.BOS.Authentication;
using Kingdee.BOS.CDP.APITestPlugIn;
using Kingdee.BOS.Core.Interaction;
using Kingdee.BOS.Core.List;
using Kingdee.BOS.Core.Log;
using Kingdee.BOS.Core.Metadata;
using Kingdee.BOS.Core.Metadata.ConvertElement.ServiceArgs;
using Kingdee.BOS.Core.Metadata.EntityElement;
using Kingdee.BOS.Core.Metadata.FieldElement;
using Kingdee.BOS.Core.Metadata.Util;
using Kingdee.BOS.Core.SqlBuilder;
using Kingdee.BOS.JSON;
using Kingdee.BOS.Log;
using Kingdee.BOS.Orm;
using Kingdee.BOS.Orm.DataEntity;
using Kingdee.BOS.ServiceFacade.KDServiceClient;
using Kingdee.BOS.ServiceFacade.KDServiceClient.Account;
using Kingdee.BOS.ServiceFacade.KDServiceClient.BusinessData;
using Kingdee.BOS.ServiceFacade.KDServiceClient.DB;
using Kingdee.BOS.ServiceFacade.KDServiceClient.DynamicForm;
using Kingdee.BOS.ServiceFacade.KDServiceClient.Metadata;
using Kingdee.BOS.ServiceFacade.KDServiceClient.User;
using Kingdee.BOS.ServiceFacade.KDServiceEntity;
using Kingdee.BOS.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace K3DesktopClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private DataTable CreateTable(object rows)
        {
            DataTable dt = new();
            DataColumn dc = new("结果");
            dt.Columns.Add(dc);
            DataRow dr = dt.NewRow();
            dr[0] = rows.ToString();
            dt.Rows.Add(dr);
            return dt;
        }
        //private static readonly string serverUrl = "https://gzweibo.ik3cloud.com/k3cloud/";
        private static readonly string serverUrl = "http://192.168.1.3/k3cloud/";
        private string DbId { get; set; }
        private Context Ctx { get; set; }
        private void OkBtn_Click(object sender, EventArgs e)
        {
            string userName = userTxt.Text.Trim();
            string userPwd = passTxt.Text.Trim();
            int lcid = 2052;
            string acctID = DbId;

            //var loginInfo = new LoginInfo()
            //{
            //    Username = userName,
            //    Password = userPwd,
            //    AcctID = acctID,
            //    Lcid = lcid,
            //    KickoutFlag = 2,
            //    ValidationCode = veryTxt.Text,
            //    AuthenticateType = AuthenticationType.PwdAuthentication
            //};
            #region 使用第三方授权登录
            long timestamp = DateTimeFormatUtils.CurrentTimeSeconds();
            string[] arr = [DbId, userName, "217311_Q6dO67sGTmmUQ5TKT27q7yXvzq2U5AOP", "b53e78db1bae4f859e2acb0de4d52552", timestamp.ToString()];
            string sign = KingdeeSignature.GetSignature(arr);
            LoginInfo loginInfo = new()
            {
                Username = userName,
                Password = userPwd,
                AppId = "217311_Q6dO67sGTmmUQ5TKT27q7yXvzq2U5AOP",
                AppSecret = "b53e78db1bae4f859e2acb0de4d52552",
                Timestamp = timestamp,
                SignedData = sign,
                AcctID = acctID,
                Lcid = lcid,
                KickoutFlag = 2,
                AuthenticateType = AuthenticationType.SimplePassportAuthentication
            };
            #endregion
            UserServiceProxy userServiceProxy = new()
            {
                HostURL = serverUrl
            };
            LoginResult result = userServiceProxy.ValidateUser(serverUrl, loginInfo);
            if (result.LoginResultType == LoginResultType.Success)
            {
                Text = result.Context.UserName;
                Ctx = result.Context;
                okBtn.Enabled = false;
            }
            else
            {
                _ = MessageBox.Show(result.Message);
            }
            foreach (Control control in Controls)
            {
                if (control is Button btn)
                {
                    if (btn.Name != "okBtn")
                    {
                        btn.Enabled = true;
                    }
                }
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                BusinessDataServiceProxy logService = new()
                {
                    HostURL = serverUrl
                };
                LogObject log = new()
                {
                    OperateName = "保存",
                    ObjectTypeId = "SAL_SaleOrder",
                    Environment = OperatingEnvironment.BizOperate,
                    Description = "上机操作日志写入测试！！！",
                    pkValue = Guid.NewGuid().ToString()
                };
                logService.WriteLog(log);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void DataGridView_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            if (sender is not DataGridView dgv)
            {
                return;
            }

            if (dgv.RowHeadersVisible)
            {
                Rectangle rect = new(e.RowBounds.Left, e.RowBounds.Top, dgv.RowHeadersWidth, e.RowBounds.Height);
                StringFormat strFormat = new()
                {
                    LineAlignment = StringAlignment.Center,
                    Alignment = StringAlignment.Far
                };
                if (e.State == (DataGridViewElementStates.Selected | DataGridViewElementStates.Visible | DataGridViewElementStates.Displayed))
                {
                    e.Graphics.DrawString((e.RowIndex + 1).ToString(), e.InheritedRowStyle.Font, new SolidBrush(dgv.RowHeadersDefaultCellStyle.SelectionForeColor), rect, strFormat);
                }
                else
                {
                    e.Graphics.DrawString((e.RowIndex + 1).ToString(), e.InheritedRowStyle.Font, new SolidBrush(dgv.RowHeadersDefaultCellStyle.ForeColor), rect, strFormat);
                }
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            // 解决 持久化系统中找不到指定的数据格式Binary
            ClienterServiceProxy proxy = new()
            {
                HostURL = serverUrl
            };
            _ = proxy.GetServerCommonConfig(ClickOnceType.CloudClient);
            //var OnceUrl = proxy.GetClickOnceUrl(ClickOnceType.CloudClient);
            //var Plugins = proxy.GetClientControlPlugins(ClickOnceType.CloudClient);
            // 加载验证码
            VeryPic_Click(veryPic, e);
            foreach (Control control in Controls)
            {
                if (control is Button btn)
                {
                    if (btn.Name != "okBtn")
                    {
                        btn.Enabled = false;
                    }
                }
            }
        }

        private void VeryPic_Click(object sender, EventArgs e)
        {
            try
            {
                AccountClientProxy accountClientProxy = new()
                {
                    HostURL = serverUrl
                };
                List<Kingdee.BOS.DataCenterInfo.DataCenter> dataCenterList = accountClientProxy.GetDataCenterList();
                DbId = dataCenterList.First().Id;
                byte[] validationCodeImageByte = accountClientProxy.GetValidationCodeImageByte();
                if (validationCodeImageByte != null)
                {
                    using MemoryStream memoryStream = new(validationCodeImageByte);
                    memoryStream.Position = 0;
                    Bitmap bmp = (Bitmap)Image.FromStream(memoryStream);
                    veryPic.Image = bmp;
                    memoryStream.Close();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        #region 数据库操作
        private void Button2_Click(object sender, EventArgs e)
        {
            // 查询用户信息
            try
            {
                SafeDoServiceProxy dBService = new()
                {
                    HostURL = serverUrl
                };
                string strSQL = "SELECT FUSERID,FNAME,FUSERTYPE,FFORBIDSTATUS FROM T_SEC_USER";
                DataSet result = dBService.SafeDoDataSetDynamic(strSQL);
                DataTable data = result.Tables[0];
                string json = JsonConvert.SerializeObject(data);
                Debug.WriteLine(json);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            // 修改用户信息
            try
            {
                SafeDoServiceProxy dBService = new()
                {
                    HostURL = serverUrl
                };
                string strSQL = "UPDATE  T_SEC_USER SET FTYPE =1 WHERE (FNAME = N'test')";
                int result = dBService.SafeDoDynamic(strSQL);
                Debug.WriteLine(result);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            // 重置第三方系统登录授权二次密钥
            try
            {
                SafeDoServiceProxy dBService = new()
                {
                    HostURL = serverUrl
                };
                string strSQL = "delete T_BAS_USERPARAMETER where FPARAMETEROBJID = 'SEC_CHECKIDENTITY'";
                int result = dBService.SafeDoDynamic(strSQL);
                dataGridView1.DataSource = CreateTable(result);
                Debug.WriteLine(result);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
        #endregion

        private void Button5_Click(object sender, EventArgs e)
        {
            try
            {
                BusinessDataServiceProxy dBService = new()
                {
                    HostURL = serverUrl
                };
                //var st = SelectorItemInfo.CreateItems("FBillNo,FMaterialId.FName,FQty");
                //OQLFilter o = new();
                //OQLFilterHeadEntityItem item = new()
                //{
                //    FilterString = "FQty=1"
                //};
                //o.Add(item);
                //var result = dBService.Load("SAL_SaleOrder", st, o);
                //var json = JsonConvert.SerializeObject(result);
                //Debug.WriteLine(json);



                //DynamicObjectType dt = new("SaleOrder");
                //dt.RegisterSimpleProperty("Id", typeof(int), 0, false, new SimplePropertyAttribute(true));
                //var FBrNoProperty = dt.RegisterSimpleProperty("BillNo", typeof(string));
                //var result2 = dBService.Load("100002", result[0].DynamicObjectType);
                //var json2 = JsonConvert.SerializeObject(result2);
                //Debug.WriteLine(json2);

                // 反审核
                //dBService.SetBillStatus("SAL_SaleOrder", "UnAudit", new object[] { 100002 });

                // 获取采购入库单元数据信息
                MetadataServiceProxy meta = new()
                {
                    HostURL = serverUrl
                };
                FormMetadata fmeta = meta.GetFormMetadata("STK_InStock");

                OperateOption saveOption = OperateOption.Create();
                saveOption.SetIgnoreWarning(true);
                saveOption.SetIgnoreInteractionFlag(true);
                ConvertServiceProxy convertServiceProxy = new()
                {
                    HostURL = serverUrl
                };
                // 单据转换
                Kingdee.BOS.Core.Metadata.ConvertElement.ConvertRuleMetaData rule = convertServiceProxy.GetConvertRule("PUR_PurchaseOrder-STK_InStock");
                Kingdee.BOS.Core.Metadata.ConvertElement.ConvertRuleElement ele = rule.Rule;
                ListSelectedRow[] select1 = new ListSelectedRow[] { new("100003", "100003", 0, "PUR_PurchaseOrder") };
                PushArgs serviceArgs = new(ele, select1);
                Kingdee.BOS.Core.DynamicForm.Operation.ConvertOperationResult result36 = convertServiceProxy.Push(serviceArgs, saveOption);
                //var json36 = JsonConvert.SerializeObject(result36.TargetDataEntities);
                //Debug.WriteLine(json36);
                List<DynamicObject> ooooo = [];
                foreach (Kingdee.BOS.Core.ExtendedDataEntity item in result36.TargetDataEntities)
                {
                    ooooo.Add(item.DataEntity);
                }
                // 有问题下推数据保存不成功
                Kingdee.BOS.Core.DynamicForm.IOperationResult result37 = dBService.SaveData(fmeta.BusinessInfo, ooooo.FirstOrDefault());
                string json37 = JsonConvert.SerializeObject(result37);
                Debug.WriteLine(json37);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            try
            {
                SafeDoServiceProxy dBService = new()
                {
                    HostURL = serverUrl
                };
                /*
                 * 修复物料 库存属性控制为空，132834为一个正常物料用作覆盖为空的属性
                               if (textBox2.Text.Length == 0) return;
                               var materialNumber = textBox2.Text;
                               var MATERIALResult = dBService.SafeDoDataSetDynamic($"SELECT FMATERIALID FROM T_BD_MATERIAL WHERE   (FNUMBER = N'{materialNumber}')");
                               var MATERIALID= (int)MATERIALResult.Tables[0].Rows[0][0];
                               var maxResult = dBService.SafeDoDataSetDynamic("SELECT MAX(FENTRYID) AS MaxFENTRYID FROM T_BD_MATERIALINVPTY;");
                               int maxValue = (int)maxResult.Tables[0].Rows[0][0];
                               string strSQL = $@"INSERT INTO T_BD_MATERIALINVPTY (FENTRYID, FMATERIALID, FINVPTYID, FISENABLE, FISAFFECTPRICE, FISAFFECTPLAN, FISAFFECTCOST, FUSEORGID)
               SELECT ROW_NUMBER() OVER (ORDER BY FENTRYID) + {maxValue}, {MATERIALID}, FINVPTYID, FISENABLE, FISAFFECTPRICE, FISAFFECTPLAN, FISAFFECTCOST, FUSEORGID
               FROM T_BD_MATERIALINVPTY
               WHERE FMATERIALID = 132834;";
                */

                string strSQL = textBox1.Text;
                //var safe = dBService.SafeCheckXTDoDynamic(strSQL, "1.sql", "", true, new IllegalSqlCheckType[] { IllegalSqlCheckType.CloudErp });
                //if (safe)
                //{
                int result = dBService.SafeDoDynamic(strSQL);
                dataGridView1.DataSource = CreateTable(result);
                dataGridView1.AutoGenerateColumns = true;
                Debug.WriteLine(result);
                //}
            }
            catch (Exception ex)
            {
                dataGridView1.DataSource = CreateTable(ex.Message);
                dataGridView1.AutoGenerateColumns = true;
                Debug.WriteLine(ex.Message);
            }
        }

        private void Button7_Click(object sender, EventArgs e)
        {

            try
            {
                SafeDoServiceProxy dBService = new()
                {
                    HostURL = serverUrl
                };
                string strSQL = textBox1.Text;
                strSQL = string.IsNullOrEmpty(strSQL) ? @"/*dialect*/
SELECT a.FID AS 操作内码,b.FNAME AS 操作名称,a.FOPERATION AS 操作编码
,a.FSERVICECLASS AS 运行时类全名,a.FDESIGNERCLASS AS 设计时类全名
,a.* FROM T_MDL_FORMOPERATIONTYPE a 
JOIN T_MDL_FORMOPERATIONTYPE_L b ON a.FID=b.FID AND b.FLOCALEID=2052;" : strSQL;
                //var safe = dBService.SafeCheckXTDoDynamic(strSQL, "1.sql", "", true, new IllegalSqlCheckType[] { IllegalSqlCheckType.CloudErp });
                //if (safe)
                //{
                DataSet result = dBService.SafeDoDataSetDynamic(strSQL);
                dataGridView1.DataSource = result.Tables[0];
                dataGridView1.AutoGenerateColumns = true;
                Debug.WriteLine(result.Tables[0].Rows.Count);
                //}
            }
            catch (Exception ex)
            {
                dataGridView1.DataSource = CreateTable(ex.Message);
                dataGridView1.AutoGenerateColumns = true;
                Debug.WriteLine(ex.Message);
            }
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            //List<long> list = new();
            //list.Add(100001);
            //list.Add(100002);
            QueryBuilderParemeter queryBuilderParemeter = new()
            {
                FormId = "PUR_PurchaseOrder",
                SelectItems = SelectorItemInfo.CreateItems("FID,FBillNo,FPOOrderEntry_FEntryID,FBUSINESSTYPE,FMaterialId.FName,FQty")
            };
            //queryBuilderParemeter.FilterClauseWihtKey = string.Format(" FID IN ({0}) ", string.Join(",", list));
            QueryBuilderParemeter para = queryBuilderParemeter;

            QueryServiceProxy query = new()
            {
                HostURL = serverUrl
            };
            DataTable dt = query.GetDataTable(para);
            dataGridView1.DataSource = dt;
            dataGridView1.AutoGenerateColumns = true;
        }

        private static List<string> GetErrorMessages(Dictionary<string, object> dict)
        {
            List<string> errorMessages = [];
            if (dict.TryGetValue("Errors", out object errorsObj))
            {
                if (errorsObj is List<object> errorsList)
                {
                    foreach (object errorItem in errorsList)
                    {
                        if (errorItem is Dictionary<string, object> errorDict && errorDict.TryGetValue("Message", out object messageObj))
                        {
                            if (messageObj is string message)
                            {
                                errorMessages.Add(message);
                            }
                        }
                    }
                }
            }
            return errorMessages;
        }

        /// <summary>
        /// 附件上传
        /// </summary>
        private void Button9_Click(object sender, EventArgs e)
        {
            string formId = "PUR_PurchaseOrder";
            int fId = 100005;
            string fBillNO = "CGDD000003";
            QueryBuilderParemeter queryBuilderParemeter = new()
            {
                FormId = "BOS_Attachment",
                SelectItems = SelectorItemInfo.CreateItems("FID,FAttachmentName,FExtName,FCreateMen,FCreateTime,FaliasFileName,FAttachmentDes,FFileStorage,FFileId,FInterID,FBillNo,FEntryKey,FEntryInterID,FBillType"),
                FilterClauseWihtKey = string.Format("FBillType='{0}' AND FInterID = {1} AND FBillNo='{2}' ", formId, fId, fBillNO)
            };
            QueryServiceProxy query = new()
            {
                HostURL = serverUrl
            };
            DataTable dt = query.GetDataTable(queryBuilderParemeter);
            if (dt.Rows.Count > 0)
            {
                dataGridView1.DataSource = dt;
                dataGridView1.AutoGenerateColumns = true;
            }
            else
            {
                CustomServiceProxy cs = new()
                {
                    HostURL = serverUrl
                };
                string pdfUrl = "https://wx.g127.com:9008/UploadFiles/background.jpg";
                string pdfName = "1.jpg";

                string base64EncodedData = ExecuteWebRequest(pdfUrl);
                JSONObject AttObject = [];
                AttObject.Put("FileName", pdfName);
                AttObject.Put("FormId", formId);
                AttObject.Put("IsLast", true);
                AttObject.Put("InterId", fId);
                AttObject.Put("BillNO", fBillNO);
                AttObject.Put("AliasFileName", pdfName);
                AttObject.Put("SendByte", base64EncodedData);
                Dictionary<string, object> result = cs.AttachmentUpload(AttObject.ToJSONString());
                Dictionary<string, object> resultObj = result["Result"] as Dictionary<string, object>;
                Dictionary<string, object> responseStatusObj = resultObj["ResponseStatus"] as Dictionary<string, object>;
                bool isSuccess = (bool)responseStatusObj["IsSuccess"];
                if (!isSuccess)
                {
                    List<string> errorsObj = GetErrorMessages(responseStatusObj);
                    object errCode = responseStatusObj["ErrorCode"];
                    object msgCode = responseStatusObj["MsgCode"];
                    StringBuilder message = new();
                    foreach (string msg in errorsObj)
                    {
                        _ = message.AppendLine(msg);
                    }
                    throw new Exception($"errCode:{errCode},msgCode:{msgCode} - {message}");
                }
                string FileIdObj = (string)resultObj["FileId"];
                Debug.WriteLine("附件上传成功，FileId：" + FileIdObj);
                dataGridView1.DataSource = CreateTable("附件上传成功，FileId：" + FileIdObj);
                dataGridView1.AutoGenerateColumns = true;
            }
        }

        public static string ExecuteWebRequest(string url)
        {
            Uri uri = new(url);
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            using WebResponse response = httpWebRequest.GetResponse();
            using Stream responseStream = response.GetResponseStream();
            using MemoryStream memoryStream = new();
            responseStream.CopyTo(memoryStream);
            byte[] fileData = memoryStream.ToArray();
            return Convert.ToBase64String(fileData);
        }


        private void Button10_Click(object sender, EventArgs e)
        {
            DynamicFormServiceProxy dsp = new()
            {
                HostURL = serverUrl
            };

            string url = "Service.aspx?servicename=FormMetaDataService&methodname=GetListMetadata&pageid=PRD_MO&paramtype=url&paramdata=%5b%5d&dbid=61271b176f110c&lang=2052&ver=638195080426656412638144151613288445";
            dsp.CallByUrl(url, s =>
            {
                Debug.WriteLine(s.ReturnValue);
            }, (queryResult, progress) =>
            {
                string json = JsonConvert.SerializeObject(queryResult.Result);
                Debug.WriteLine(json);
            });

            string json = @"[""FLIST"",0,200,null]";
            JSONArray param = JSONArray.Parse(json);
            dsp.Call(s =>
            {
                Debug.WriteLine(s.ReturnValue);
            }, "ListService", Guid.NewGuid().ToString(), "GetEntryData", param, (queryResult, progress) =>
            {
                string json = JsonConvert.SerializeObject(queryResult.Result);
                Debug.WriteLine(json);
            });


        }

        private void Button11_Click(object sender, EventArgs e)
        {
            DataCenterServiceProxy dcs = new()
            {
                HostURL = serverUrl
            };
            string mrgUrl = dcs.GetMrgUrl();
            Debug.WriteLine(mrgUrl);
            Context ctx = dcs.GetDataCenterContextByID(DbId);
            _ = dcs.GetLangList(ctx);
            _ = dcs.GetMCInfoByClient();
        }

        private void Button12_Click(object sender, EventArgs e)
        {
            MetadataServiceProxy meta = new()
            {
                HostURL = serverUrl
            };
            List<ApiMetaEntity> list = [];
            FormMetadata formMetadata = meta.GetFormMetadata(textBox3.Text);
            List<MetaField> list2 = [];
            foreach (Entity entry in formMetadata.BusinessInfo.Entrys)
            {
                if (formMetadata.ModelTypeId == 900 && entry.Seq == 0)
                {
                    continue;
                }
                ApiMetaEntity apiMetaEntity = new()
                {
                    EntryName = entry.Name.ToString(),
                    EntityKey = entry.Key,
                    EntityType = entry.EntityType,
                    ElementType = entry.ElementType,
                    EntryPkFieldName = entry.EntryPkFieldName,
                    EntryProperty = entry.EntryName,
                    PkFieldType = entry.PkFieldType?.Name,
                    TableName = entry.TableName,
                    Seq = entry.Seq,
                    Name = entry.Name
                };
                if (entry is SubEntryEntity)
                {
                    apiMetaEntity.ParentEntityKey = ((SubEntryEntity)entry).ParentEntityKey;
                }
                List<Field> list3 = entry.Fields.Where((Field x) => x is not ProxyField and not BaseDataPropertyField).ToList();
                apiMetaEntity.MetaField = [];
                foreach (Field item in list3)
                {
                    MetaField metaField = GetApiMetaField(apiMetaEntity.EntityKey, item);
                    if (list2.Any((MetaField m) => m.Key.Equals(metaField.Key, StringComparison.OrdinalIgnoreCase)))
                    {
                        metaField.IsFieldKey = true;
                    }
                    apiMetaEntity.MetaField.Add(metaField);
                }
                list.Add(apiMetaEntity);
            }

            // 将list中MetaField提取出来
            List<MetaField> listMetaField = [];
            foreach (ApiMetaEntity item in list)
            {
                listMetaField.AddRange(item.MetaField);
            }
            // 将listMetaField中Name和Key加载到listBox1控件
            // 将listMetaField转换成DataSource
            BindingSource bindingSource = [];
            bindingSource.DataSource = listMetaField;
            listBox1.DataSource = bindingSource;
            listBox1.DisplayMember = "Name";
            listBox1.ValueMember = "Key";


        }

        private MetaField GetApiMetaField(string entity, Field field)
        {
            MetaField metaField = new()
            {
                Entity = entity,
                ID = Guid.NewGuid().ToString(),
                MaxDataScopeValue = field.MaxDataScope,
                MinDataScopeValue = field.MinDataScope,
                Key = field.Key
            };
            Kingdee.BOS.Core.Metadata.Appearance appearance = new(metaField.Key);
            metaField.VisibleExt = appearance.VisibleExt;
            metaField.Visible = appearance.Visible;
            PropertyInfo[] properties = field.GetType().GetProperties();
            PropertyInfo[] array = properties;
            foreach (PropertyInfo propertyInfo in array)
            {
                if (propertyInfo.Name == "Editlen")
                {
                    metaField.Length = (int)propertyInfo.GetValue(field, null);
                    break;
                }
            }
            metaField.Name = field.Name.ToString();
            metaField.FieldName = field.FieldName;
            metaField.FieldType = field.ElementType;
            metaField.DatabaseType = field.DatabaseType;
            metaField.ConditionType = field.ConditionType;
            metaField.ControlFieldKey = field.ControlFieldKey;
            metaField.DataScope = (field.DataScope == null) ? "" : field.DataScope.ToString();
            metaField.DefValue = (field.DefValue == null) ? "" : field.DefValue.ToString();
            metaField.MustInput = field.IsMustInput() ? 1 : 0;
            metaField.Tabindex = field.Tabindex;
            metaField.PropertyName = field.PropertyName;
            if (field is BaseDataField)
            {
                metaField.DefaultGroupCatalog = ((BaseDataField)field).DefaultGroupCatalog;
            }
            else if (field is BasePropertyField)
            {
                metaField.SrcDisplayFieldName = ((BasePropertyField)field).SrcDisplayFieldName;
            }
            switch (field.FieldType)
            {
                case 167:
                case 231:
                    try
                    {
                        object propertyValue = field.GetPropertyValue("Editlen");
                        metaField.Length = propertyValue != null ? (int)propertyValue : -1;
                    }
                    catch (Exception)
                    {
                        metaField.Length = -1;
                    }
                    break;
                case 106:
                    metaField.FieldScale = (field as DecimalField).FieldScale;
                    metaField.DataScope = (field as DecimalField).DataScope;
                    metaField.FieldPrecision = (field as DecimalField).FieldPrecision;
                    break;
                default:
                    metaField.Length = -1;
                    break;
            }
            if (field is ILookUpField && ((ILookUpField)field).LookUpObject != null)
            {
                metaField.LookUpObjectID = ((ILookUpField)field).LookUpObject.FormId;
                metaField.NameProperty = (((ILookUpField)field).NameProperty != null) ? ((ILookUpField)field).NameProperty.Key : "";
                try
                {
                    metaField.NumberProperty = (((ILookUpField)field).NumberProperty != null) ? ((ILookUpField)field).NumberProperty.Key : "";
                }
                catch (Exception ex2)
                {
                    Logger.Error("", "", ex2);
                }
            }
            return metaField;
        }

        private void ListBox1_DoubleClick(object sender, EventArgs e)
        {
            Box2box(listBox1, listBox2);
        }

        private void Box2box(ListBox listBox1, ListBox listBox2, bool all = false)
        {
            if (all)
            {
                listBox2.DataSource = listBox1.DataSource;
                listBox2.DisplayMember = "Name";
                listBox2.ValueMember = "Key";
                listBox1.DataSource = null;
                return;
            }
            MetaField metaField = listBox1.SelectedItem as MetaField;
            if (listBox2.DataSource is not BindingSource bindingList2)
            {
                bindingList2 = new BindingSource
                {
                    DataSource = new List<MetaField>()
                };
            }
            listBox2.DataSource = bindingList2;
            listBox2.DisplayMember = "Name";
            listBox2.ValueMember = "Key";
            _ = bindingList2.Add(metaField);
            BindingSource bindingSource = listBox1.DataSource as BindingSource;
            bindingSource.RemoveCurrent();
        }


        private void ListBox2_DoubleClick(object sender, EventArgs e)
        {
            Box2box(listBox2, listBox1);

        }

        private void Button13_Click(object sender, EventArgs e)
        {
            // 将listBox2中的数据转换成List<MetaField>
            List<MetaField> listMetaField = [];
            foreach (object item in listBox2.Items)
            {
                MetaField obj = item as MetaField;
                //if (obj != null)
                //{
                //    if (obj.NameProperty != null && !obj.Key.Contains('.'))
                //    {
                //        obj.Key = obj.Key + "." + obj.NameProperty;
                //    }
                //    else
                //    {
                //        if(obj.NumberProperty != null && !obj.Key.Contains('.'))
                //        {
                //            obj.Key = obj.Key + "." + obj.NumberProperty;
                //        }
                //    }
                //}
                listMetaField.Add(obj);
            }
            List<string> keys = listMetaField.Select(x => x.Key).ToList();
            string str = string.Join(",", keys);
            textBox1.Text = str;
        }

        private void Label1_Click(object sender, EventArgs e)
        {
            Box2box(listBox1, listBox2, true);
        }

        private void label2_Click(object sender, EventArgs e)
        {
            Box2box(listBox2, listBox1, true);
        }
    }
}