﻿using Kingdee.BOS.ServiceFacade.KDServiceClient;
using System.Collections.Generic;

namespace K3DesktopClient
{
    /// <summary>
    /// 自定义代理接口
    /// </summary>
    public class CustomServiceProxy : BaseServiceProxy
    {
        protected override string ServiceName => "Kingdee.BOS.WebApi.ServicesStub.DynamicFormService";

        /// <summary>
        /// 文件上传
        /// </summary>
        /// <param name="method"></param>
        /// <param name="data"></param>
        public Dictionary<string, object> AttachmentUpload(string data)
        {
            return ExecuteService<Dictionary<string, object>>("AttachmentUpload", [data]);
        }


    }
}
