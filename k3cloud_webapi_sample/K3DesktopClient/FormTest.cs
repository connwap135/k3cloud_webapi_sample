﻿using Kingdee.BOS.Authentication;
using Kingdee.BOS.ServiceFacade.KDServiceClient;
using Kingdee.BOS.ServiceFacade.KDServiceClient.DB;
using Kingdee.BOS.ServiceFacade.KDServiceClient.User;
using Kingdee.BOS.ServiceFacade.KDServiceEntity;
using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace K3DesktopClient
{
    public partial class FormTest : Form
    {
        public FormTest()
        {
            InitializeComponent();
            listBox1.DoubleClick += ListBox1_DoubleClick;
        }



        private void button1_Click(object sender, EventArgs e)
        {
            // 解决 持久化系统中找不到指定的数据格式Binary
            ClienterServiceProxy proxy = new()
            {
                HostURL = textBox1.Text
            };
            _ = proxy.GetServerCommonConfig(ClickOnceType.CloudClient);
            long timestamp = DateTimeFormatUtils.CurrentTimeSeconds();
            string[] arr = new string[] { textBox2.Text, textBox3.Text, textBox4.Text, textBox5.Text, timestamp.ToString() };
            string sign = KingdeeSignature.GetSignature(arr);
            LoginInfo loginInfo = new()
            {
                Username = textBox3.Text,
                Password = textBox3.Text,
                AppId = textBox4.Text,
                AppSecret = textBox5.Text,
                Timestamp = timestamp,
                SignedData = sign,
                AcctID = textBox2.Text,
                Lcid = 2052,
                KickoutFlag = 2,
                AuthenticateType = AuthenticationType.SimplePassportAuthentication
            };
            UserServiceProxy userServiceProxy = new()
            {
                HostURL = textBox1.Text,
            };
            LoginResult result = userServiceProxy.ValidateUser(textBox1.Text, loginInfo);
            if (result.LoginResultType == LoginResultType.Success)
            {
                Text = result.Context.UserName;
                button1.Enabled = false;
                button2.Enabled = true;
                Properties.Settings.Default.serverUlr = textBox1.Text;
                Properties.Settings.Default.DbId = textBox2.Text;
                Properties.Settings.Default.UserName = textBox3.Text;
                Properties.Settings.Default.AppId = textBox4.Text;
                Properties.Settings.Default.AppSecret = textBox5.Text;
                Properties.Settings.Default.Save();
            }
            else
            {
                _ = MessageBox.Show(result.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                SafeDoServiceProxy dBService = new()
                {
                    HostURL = textBox1.Text
                };
                string strSQL = textBox6.Text;
                strSQL = string.IsNullOrEmpty(strSQL) ? @"/*dialect*/
SELECT a.FID AS 操作内码,b.FNAME AS 操作名称,a.FOPERATION AS 操作编码
,a.FSERVICECLASS AS 运行时类全名,a.FDESIGNERCLASS AS 设计时类全名
,a.* FROM T_MDL_FORMOPERATIONTYPE a 
JOIN T_MDL_FORMOPERATIONTYPE_L b ON a.FID=b.FID AND b.FLOCALEID=2052;" : strSQL;
                //var safe = dBService.SafeCheckXTDoDynamic(strSQL, "1.sql", "", true, new IllegalSqlCheckType[] { IllegalSqlCheckType.CloudErp });
                //if (safe)
                //{
                DataSet result = dBService.SafeDoDataSetDynamic(strSQL);
                dataGridView1.DataSource = result.Tables[0];
                dataGridView1.AutoGenerateColumns = true;
                Debug.WriteLine(result.Tables[0].Rows.Count);
                //}
            }
            catch (Exception ex)
            {
                dataGridView1.DataSource = CreateTable(ex.Message);
                dataGridView1.AutoGenerateColumns = true;
                Debug.WriteLine(ex.Message);
            }
        }
        private DataTable CreateTable(object rows)
        {
            DataTable dt = new();
            DataColumn dc = new("结果");
            dt.Columns.Add(dc);
            DataRow dr = dt.NewRow();
            dr[0] = rows.ToString();
            dt.Rows.Add(dr);
            return dt;
        }

        private void FormTest_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Properties.Settings.Default.serverUlr))
            {
                textBox1.Text = Properties.Settings.Default.serverUlr;
                textBox2.Text = Properties.Settings.Default.DbId;
                textBox3.Text = Properties.Settings.Default.UserName;
                textBox4.Text = Properties.Settings.Default.AppId;
                textBox5.Text = Properties.Settings.Default.AppSecret;
            }
            loadXmlNode();
        }
        private void DataGridView_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            if (sender is not DataGridView dgv)
            {
                return;
            }

            if (dgv.RowHeadersVisible)
            {
                Rectangle rect = new(e.RowBounds.Left, e.RowBounds.Top, dgv.RowHeadersWidth, e.RowBounds.Height);
                StringFormat strFormat = new()
                {
                    LineAlignment = StringAlignment.Center,
                    Alignment = StringAlignment.Far
                };
                if (e.State == (DataGridViewElementStates.Selected | DataGridViewElementStates.Visible | DataGridViewElementStates.Displayed))
                {
                    e.Graphics.DrawString((e.RowIndex + 1).ToString(), e.InheritedRowStyle.Font, new SolidBrush(dgv.RowHeadersDefaultCellStyle.SelectionForeColor), rect, strFormat);
                }
                else
                {
                    e.Graphics.DrawString((e.RowIndex + 1).ToString(), e.InheritedRowStyle.Font, new SolidBrush(dgv.RowHeadersDefaultCellStyle.ForeColor), rect, strFormat);
                }
            }
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            string defaultName = listBox1.SelectedItem.ToString();
            string strBookmarkName = Interaction.InputBox("请输入书签名称", "保存书签", defaultName, 100, 100);
            if (string.IsNullOrEmpty(strBookmarkName))
            {
                MessageBox.Show("书签名称不能为空", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            string strSQL = textBox6.Text;
            if (string.IsNullOrEmpty(strSQL))
            {
                MessageBox.Show("SQL语句不能为空", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            System.Xml.XmlDocument doc = new();
            string filePath = "sql.xml";
            if (System.IO.File.Exists(filePath))
            {
                doc.Load(filePath);
            }
            else
            {
                System.Xml.XmlElement root = doc.CreateElement("root");
                doc.AppendChild(root);
            }

            System.Xml.XmlElement rootElement = doc.DocumentElement;
            System.Xml.XmlElement existingElement = null;
            foreach (System.Xml.XmlElement element in rootElement.GetElementsByTagName("sql"))
            {
                if (element.GetAttribute("name") == strBookmarkName)
                {
                    existingElement = element;
                    break;
                }
            }
            if (existingElement != null)
            {
                existingElement.InnerText = strSQL;
            }
            else
            {
                System.Xml.XmlElement sqlElement = doc.CreateElement("sql");
                sqlElement.SetAttribute("name", strBookmarkName);
                sqlElement.InnerText = strSQL;
                rootElement.AppendChild(sqlElement);
            }
            doc.Save(filePath);
            MessageBox.Show("书签已成功保存", "信息", MessageBoxButtons.OK, MessageBoxIcon.Information);
            // 刷新listBox
            listBox1.Items.Clear();
            foreach (System.Xml.XmlElement element in rootElement.GetElementsByTagName("sql"))
            {
                listBox1.Items.Add(element.GetAttribute("name"));
            }
        }
        // 加载sql.xml文件，获取书签列表
        private void loadXmlNode()
        {
            string filePath = "sql.xml";
            if (System.IO.File.Exists(filePath))
            {
                System.Xml.XmlDocument doc = new();
                doc.Load(filePath);
                System.Xml.XmlElement rootElement = doc.DocumentElement;
                foreach (System.Xml.XmlElement element in rootElement.GetElementsByTagName("sql"))
                {
                    listBox1.Items.Add(element.GetAttribute("name"));
                }
            }
        }
        // 按name获取sql.xml文件中的sql语句
        private string getSqlByName(string name)
        {
            string filePath = "sql.xml";
            if (System.IO.File.Exists(filePath))
            {
                System.Xml.XmlDocument doc = new();
                doc.Load(filePath);
                System.Xml.XmlElement rootElement = doc.DocumentElement;
                foreach (System.Xml.XmlElement element in rootElement.GetElementsByTagName("sql"))
                {
                    if (element.GetAttribute("name") == name)
                    {
                        return element.InnerText;
                    }
                }
            }
            return "";
        }
        private void ListBox1_DoubleClick(object sender, EventArgs e)
        {
            // 获取listBox1中选中的书签名称
            string name = listBox1.SelectedItem.ToString();
            // 获取书签名称对应的sql语句
            string sql = getSqlByName(name);
            // 将sql语句显示到textBox6中
            textBox6.Text = sql;
        }
    }
}
