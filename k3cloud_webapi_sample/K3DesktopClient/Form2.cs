﻿using Kingdee.BOS.Log;
using Kingdee.BOS.Util;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace K3DesktopClient
{
    public partial class Form2 : Form
    {
        private readonly string serviceUrl = "https://www.ik3cloud.com/test.aspx";

        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Uri uri = new(serviceUrl);
            HttpWebRequest httpWebRequest = WebRequestHelper.Create(uri);
            try
            {
                using HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                //UpdateCookie(httpWebResponse.Cookies, httpWebRequest);
                string result = GetResponseContent(httpWebResponse);
                _ = ((int)httpWebResponse.StatusCode).ToString();
                Debug.WriteLine(result);
            }
            catch (WebException) { }
        }

        private string GetResponseContent(HttpWebResponse rsp)
        {
            if (rsp == null)
            {
                Logger.Info("DoPost", "ServiceException: HttpWebResponse is NULL.");
                return "";
            }
            bool flag = rsp.StatusCode == HttpStatusCode.OK;
            string statusDescription = rsp.StatusDescription;
            if (flag)
            {
                //ProcessResponseState(kdrequest, rsp);
                if (!IsChunked(rsp) && rsp.ContentLength <= 0)
                {
                    return "";
                }
                string text = "";
                using (Stream stream = rsp.GetResponseStream())
                {
                    using (StreamReader streamReader = new(stream, Encoding.UTF8))
                    {
                        try
                        {
                            text = streamReader.ReadToEnd();
                        }
                        catch (OutOfMemoryException)
                        {
                            //string message = $"url:{kdrequest.GetServiceUrl()};method:{kdrequest.GetMethodName()};";
                            //WriteClientLog(new Exception(message, innerException));
                        }
                        streamReader.Close();
                    }
                    stream.Close();
                    rsp.Close();
                }
                if (text.StartsWith("response_error:"))
                {
                    //ParseException(text.TrimStart("response_error:".ToCharArray()), Encoding.UTF8, kdrequest.Format);
                }
                return text;
            }
            rsp.Close();
            Logger.Info("DoPost", "ServiceException:" + statusDescription);
            return "";
            //throw new ServiceException((int)statusCode, statusDescription);
        }

        private static bool IsChunked(HttpWebResponse rsp)
        {
            bool result = false;
            try
            {
                if (rsp != null && rsp.Headers != null)
                {
                    string value = rsp.Headers.Get("Transfer-Encoding");
                    result = !string.IsNullOrWhiteSpace(value) && "chunked".Equals(value, StringComparison.OrdinalIgnoreCase);
                }
            }
            catch (Exception ex)
            {
                Logger.Info("DoPost.IsChunked()", ex.Message, bByLogModel: true);
            }
            return result;
        }
    }
}
