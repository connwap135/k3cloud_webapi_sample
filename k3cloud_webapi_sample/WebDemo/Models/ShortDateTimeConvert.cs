﻿using Newtonsoft.Json.Converters;

namespace WebDemo.Models
{
    public class ShortDateTimeConvert : IsoDateTimeConverter
    {
        public ShortDateTimeConvert() : base()
        {
            base.DateTimeFormat = "yyyy-MM-dd";
        }
    }
}