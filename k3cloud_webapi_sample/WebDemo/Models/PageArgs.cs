﻿namespace WebDemo.Models
{
    public class PageArgs
    {
#pragma warning disable IDE1006
        /// <summary>
        /// 页面索引
        /// </summary>
        public int page { get; set; }
        /// <summary>
        /// 页面大小
        /// </summary>
        public int limit { get; set; }
#pragma warning restore IDE1006
    }
}