﻿namespace WebDemo.Models
{
    public class SFC_OperationPlanning
    {
        /// <summary>
        /// 工序计划头Id
        /// </summary>
        public int FID { get; set; }
        /// <summary>
        /// 单据体分录行内码
        /// 工序计划序列Id
        /// </summary>
        public int FEntity_FENTRYID { get; set; }
        public int FSubEntity_FSeq { get; set; }
        /// <summary>
        /// 生产订单编号
        /// </summary>
        public string FMONumber { get; set; }

        /// <summary>
        /// 工序计划工序Id
        /// </summary>
        public int FSubEntity_FDetailID { get; set; }
        /// <summary>
        /// 单据编号
        /// </summary>
        public string FBILLNO { get; set; }
        /// <summary>
        /// 产品编码
        /// </summary>
        public string FProductId__FNumber { get; set; }
        /// <summary>
        /// 产品名称
        /// </summary>
        public string FProductName { get; set; }
        /// <summary>
        /// 型号规格
        /// </summary>
        public string FProSpecification { get; set; }
        /// <summary>
        /// 生产车间
        /// </summary>
        public string FProDepartmentId__FName { get; set; }
        /// <summary>
        /// 工艺路线
        /// </summary>
        public string FRouteId__FName { get; set; }
        /// <summary>
        /// 作业
        /// </summary>
        public string FProcessId__FName { get; set; }
        /// <summary>
        /// 工序号
        /// </summary>
        public string FOperNumber { get; set; }
        /// <summary>
        /// 需求单据
        /// </summary>
        public string FSaleOrderNumber { get; set; }
        /// <summary>
        /// 工序数量
        /// </summary>
        public float FOperQty { get; set; }
        /// <summary>
        /// 合格数量
        /// </summary>
        public int FQualifiedQty { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int FOperStatus { get; set; }

        /// <summary>
        /// 单据状态
        /// </summary>
        public string FDocumentStatus { get; set; }
    }
}
