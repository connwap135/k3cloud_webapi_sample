﻿using K3Cloud.WebApi.Core.IoC.Attributes;
using K3Cloud.WebApi.Core.IoC.DataEntity;
using System;
using System.Collections.Generic;

namespace WebDemo.Models.EntityTable
{
    [Alias("PUR_PurchaseOrder")]
    public class PUR_PurchaseOrderModel
    {
        public int FID { get; set; }
        public K3Number FBillTypeID { get; set; }
        public DateTime FDate { get; set; }
        public K3Number FSupplierId { get; set; }
        public K3Number FPurchaseOrgId { get; set; }
        public K3Number FProviderId { get; set; }
        public K3Number FSettleId { get; set; }
        public K3Number FStockOrgId { get; set; }
        public K3Number FChargeId { get; set; }
        public bool FIsModificationOperator { get; set; }
        public string FChangeStatus { get; set; }
        public string FACCTYPE { get; set; }
        public bool FIsMobBill { get; set; }
        public FPOOrderFinance_Entity FPOOrderFinance { get; set; }
        public FPOOrderPay_Entity FPOOrderPay { get; set; }
        public List<FPOOrderEntry_Entity> FPOOrderEntry { get; set; }
    }

    public class FPOOrderEntry_Entity
    {
        public K3Number FMaterialId { get; set; }
        public K3Number FUnitId { get; set; }
        public decimal FQty { get; set; }
        public K3Number FPriceUnitId { get; set; }
        public decimal FPriceUnitQty { get; set; }
        public decimal FPriceBaseQty { get; set; }
        public DateTime FDeliveryDate { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        public decimal FPrice { get; set; }
        /// <summary>
        /// 含税单价
        /// </summary>
        public decimal FTaxPrice { get; set; }
        /// <summary>
        /// 折扣率
        /// </summary>
        public decimal FEntryDiscountRate { get; set; }
        /// <summary>
        /// 税率
        /// </summary>
        public decimal FEntryTaxRate { get; set; }
        public K3Number FRequireOrgId { get; set; }
        public K3Number FReceiveOrgId { get; set; }
        public K3Number FEntrySettleOrgId { get; set; }
        public bool FGiveAway { get; set; }
        public K3Number FStockUnitID { get; set; }
        public decimal FStockQty { get; set; }
        public decimal FStockBaseQty { get; set; }
        public bool FDeliveryControl { get; set; }
        public bool FTimeControl { get; set; }
        public decimal FDeliveryMaxQty { get; set; }
        public decimal FDeliveryMinQty { get; set; }
        public int FDeliveryBeforeDays { get; set; }
        public int FDeliveryDelayDays { get; set; }
        public DateTime FDeliveryEarlyDate { get; set; }
        public DateTime FDeliveryLastDate { get; set; }
        public decimal FPriceCoefficient { get; set; }
        public decimal FConsumeSumQty { get; set; }
        public string FSrcBillTypeId { get; set; }
        public string FSrcBillNo { get; set; }
        public int FDEMANDBILLENTRYSEQ { get; set; }
        public int FDEMANDBILLENTRYID { get; set; }
        public bool FPlanConfirm { get; set; }
        public K3Number FSalUnitID { get; set; }
        public decimal FSalQty { get; set; }
        public decimal FSalJoinQty { get; set; }
        public decimal FBaseSalJoinQty { get; set; }
        public decimal FInventoryQty { get; set; }
        public K3Number FCentSettleOrgId { get; set; }
        public K3Number FDispSettleOrgId { get; set; }
        public int FGroup { get; set; }
        public K3Number FDeliveryStockStatus { get; set; }
        public decimal FMaxPrice { get; set; }
        public decimal FMinPrice { get; set; }
        public bool FIsStock { get; set; }
        public decimal FBaseConsumeSumQty { get; set; }
        public decimal FSalBaseQty { get; set; }
        public K3Number FSubOrgId { get; set; }
        public K3Number FEntryPayOrgId { get; set; }
        /// <summary>
        /// 单价折扣
        /// </summary>
        public decimal FPriceDiscount { get; set; }
        /// <summary>
        ///  价税合计（折前）
        /// </summary>
        public decimal FAllAmountExceptDisCount { get; set; }
        public List<FEntryDeliveryPlan_Entity> FEntryDeliveryPlan { get; set; }
        public List<FPOOrderEntry_Link> FPOOrderEntry_Link { get; set; }
    }

    public class FEntryDeliveryPlan_Entity
    {
        public DateTime FDeliveryDate_Plan { get; set; }
        public decimal FPlanQty { get; set; }
        public DateTime FSUPPLIERDELIVERYDATE { get; set; }
        public DateTime FPREARRIVALDATE { get; set; }
        public int FTRLT { get; set; }
        public decimal FConfirmDeliQty { get; set; }
    }

    public class FPOOrderEntry_Link
    {
        public string FPOOrderEntry_Link_FRuleId { get; set; }
        public string FPOOrderEntry_Link_FSTableName { get; set; }
        public int FPOOrderEntry_Link_FSBillId { get; set; }
        public long FPOOrderEntry_Link_FSId { get; set; }
        public decimal FPOOrderEntry_Link_FBaseUnitQtyOld { get; set; }
        public decimal FPOOrderEntry_Link_FBaseUnitQty { get; set; }
        public decimal FPOOrderEntry_Link_FSalBaseQtyOld { get; set; }
        public decimal FPOOrderEntry_Link_FSalBaseQty { get; set; }
        public decimal FPOOrderEntry_Link_FStockBaseQtyOld { get; set; }
        public decimal FPOOrderEntry_Link_FStockBaseQty { get; set; }
    }

    public class FPOOrderPay_Entity
    {
    }

    public class FPOOrderFinance_Entity
    {
        public K3Number FSettleCurrId { get; set; }
        public K3Number FExchangeTypeId { get; set; }
        public decimal FExchangeRate { get; set; }
        public string FPriceTimePoint { get; set; }
        public K3Number FFOCUSSETTLEORGID { get; set; }
        public bool FIsIncludedTax { get; set; }
        public bool FISPRICEEXCLUDETAX { get; set; }
        public K3Number FLocalCurrId { get; set; }
        public decimal FPAYADVANCEAMOUNT { get; set; }
        public decimal FSupToOderExchangeBusRate { get; set; }
        public bool FSEPSETTLE { get; set; }
        public decimal FDepositRatio { get; set; }
        public decimal FAllDisCount { get; set; }
        public decimal FUPPERBELIEL { get; set; }
    }
}
