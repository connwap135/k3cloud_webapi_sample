﻿using System.ComponentModel;

namespace WebDemo.Models.EntityTable
{
    public enum KForbidStatus
    {
        /// <summary>
        /// 正常,否
        /// </summary>
        [Description("正常")]
        A,
        /// <summary>
        /// 禁用,是
        /// </summary>
        [Description("禁用")]
        B
    }
}