﻿using System;

namespace WebDemo.Models.EntityTable
{
    /// <summary>
    /// 出库申请单
    /// </summary>
    public class STK_OutStockApply
    {
        /// <summary>
        /// 申请日期
        /// </summary>
        public DateTime FDate { get; set; }
        /// <summary>
        /// 单据状态
        /// </summary>
        public string FDocumentStatus { get; set; }
        /// <summary>
        /// 领用部门
        /// </summary>
        public string FDeptId__FNumber { get; set; }
        /// <summary>
        /// 物料编码
        /// </summary>
        public string FMaterialId__FNumber { get; set; }
        /// <summary>
        /// 申请数量
        /// </summary>
        public decimal FQty { get; set; }
    }
}
