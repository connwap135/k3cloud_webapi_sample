﻿using K3Cloud.WebApi.Core.IoC.Attributes;
using System;
using System.ComponentModel;
using System.Text.Json.Serialization;

namespace WebDemo.Models.EntityTable
{
    /// <summary>
    /// 销售订单
    /// <code>注;带下划线属性建议使用Alias特性</code>
    /// </summary>
    [Description("销售订单")]
    public class SAL_SaleOrder
    {
        [ignore]
        public int FId { get; set; }
        /// <summary>
        /// 订单编号
        /// </summary>
        [Description("销售订单编号")]
        public string FBillNo { get; set; }
        [Description("单据状态")]
        public KFDocumentStatus? FDocumentStatus { get; set; }
        /// <summary>
        /// 客户编码
        /// </summary>
        [Description("客户编码")]
        [Alias("FCUSTID.FNumber")]
        public string FCUSTID { get; set; }
        /// <summary>
        /// 客户简称
        /// </summary>
        [Description("客户简称")]
        [Alias("FCUSTID.FShortName")]
        public string FCUSTIDFShortName { get; set; }
        /// <summary>
        /// 客户名
        /// </summary>
        [Description("客户名")]
        //[Alias("FCUSTID.FName")]
        public string FCUSTID__FName { get; set; }
        /// <summary>
        /// 物料编码
        /// </summary>
        [Description("物料编码")]
        [Alias("FMaterialId.FNumber")]
        public string FMaterialId { get; set; }
        /// <summary>
        /// 物料名称
        /// </summary>
        [Description("产品名称")]
        public string FMaterialName { get; set; }
        [Description("规格型号")]
        public string FMaterialModel { get; set; }
        /// <summary>
        /// 销售数量
        /// </summary>
        [Description("数量")]
        public double FQty { get; set; }
        [Description("单据日期")]
        [Newtonsoft.Json.JsonConverter(typeof(ShortDateTimeConvert))]
        public DateTime FDate { get; set; }
        [Description("要货日期")]
        [Newtonsoft.Json.JsonConverter(typeof(ShortDateTimeConvert))]
        public DateTime FPlanDate { get; set; }
        [Description("已出库数量")]
        public double FStockOutQty { get; set; }
        [Description("剩余未出数量")]
        public double FRemainOutQty { get; set; }
    }
}
