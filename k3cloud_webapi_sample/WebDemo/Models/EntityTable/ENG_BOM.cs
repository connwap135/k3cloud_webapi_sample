﻿using K3Cloud.WebApi.Core.IoC.Attributes;
using System;
using System.ComponentModel;

namespace WebDemo.Models.EntityTable
{
    public class ENG_BOM
    {
        /// <summary>
        /// BOM版本Id
        /// </summary>
        public int FId { get; set; }
        /// <summary>
        /// 单据状态
        /// A、创建
        /// B、提交
        /// C、审核
        /// D、重新审核
        /// Z、暂存
        /// </summary>
        [Description("数据状态")]
        public KFDocumentStatus FDocumentStatus { get; set; }
        /// <summary>
        /// 关闭状态
        /// A、正常
        /// B、作废|取消
        /// </summary>
        [Description("禁用状态")]
        public KForbidStatus FForbidStatus { get; set; }
        /// <summary>
        /// 分录内码（主键）
        /// </summary>
        public int FTreeEntity_FENTRYID { get; set; }
        /// <summary>
        /// 子项物料内码
        /// </summary>
        public int FMATERIALIDCHILD { get; set; }
        /// <summary>
        /// 子项物料属性
        /// 1、外购
        /// 2、自制
        /// </summary>
        [ignore]
        public int FCHILDITEMPROPERTY { get; set; }
        /// <summary>
        /// 修改日期
        /// </summary>
        [ignore]
        public DateTime FModifyDate { get; set; }
        /// <summary>
        /// BOM版本
        /// </summary>
        [ignore]
        public string FNumber { get; set; }
        /// <summary>
        /// BOM用途
        /// 99、通用
        /// 2、自制
        /// 3、委外
        /// 4、组装
        /// 5、报价
        /// </summary>
        [ignore]
        public int FBOMUSE { get; set; }
    }
}
