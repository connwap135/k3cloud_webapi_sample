﻿using K3Cloud.WebApi.Core.IoC.Attributes;
using K3Cloud.WebApi.Core.IoC.DataEntity;
using System;
using System.Collections.Generic;

namespace WebDemo.Models.EntityTable
{
    [Alias("PRD_PickMtrl")]
    public class PRD_PickMtrlModel
    {
        /// <summary>
        /// FID
        /// </summary>
        public int FID { get; set; }
        /// <summary>
        /// 日期
        /// </summary>
        public DateTime FDate { get; set; }
        /// <summary>
        /// 发料组织
        /// </summary>
        public K3Number FStockOrgId { get; set; }
        /// <summary>
        /// 单据类型
        /// </summary>
        public K3Number FBillType { get; set; }
        /// <summary>
        /// 生产组织
        /// </summary>
        public K3Number FPrdOrgId { get; set; }
        /// <summary>
        /// 明细
        /// </summary>
        [ignore]
        public List<PRD_PickMtrlModel_Entity> FEntity { get; set; }
    }

    /// <summary>
    /// 生产领料单分录
    /// </summary>
    [Alias("PRD_PickMtrl")]
    public class PRD_PickMtrlModel_Entity
    {
        /// <summary>
        /// 系统源单类型
        /// </summary>
        public string FSrcBillType { get; set; }
        /// <summary>
        /// 生产订单编号
        /// </summary>
        public string FMoBillNo { get; set; }
        /// <summary>
        /// 系统源单编号
        /// </summary>
        public string FSrcBillNo { get; set; }
        /// <summary>
        /// 实发数量
        /// </summary>
        public decimal FActualQty { get; set; }
        /// <summary>
        /// 生产订单内码
        /// </summary>
        public int? FMoId { get; set; }
        /// <summary>
        /// 用料清单分录内码
        /// </summary>
        public long FPPBomEntryId { get; set; }
        /// <summary>
        /// 主库存单位
        /// </summary>
        public K3Number FStockUnitId { get; set; }
        /// <summary>
        /// 货主类型
        /// </summary>
        public string FOwnerTypeId { get; set; } = "BD_OwnerOrg";
        /// <summary>
        /// 产品编码
        /// </summary>
        public K3Number FParentMaterialId { get; set; }
        /// <summary>
        /// 产品货主类型
        /// </summary>
        public string FParentOwnerTypeId { get; set; }
        /// <summary>
        /// 产品货主
        /// </summary>
        public K3Number FParentOwnerId { get; set; }
        /// <summary>
        /// 物料编码
        /// </summary>
        public K3Number FMaterialId { get; set; }
        /// <summary>
        /// 基本单位实发数量
        /// </summary>
        public decimal FBaseActualQty { get; set; }
        /// <summary>
        /// 主库存单位申请数量
        /// </summary>
        public decimal FStockAppQty { get; set; }
        /// <summary>
        /// 基本单位申请数量
        /// </summary>
        public decimal FBaseAppQty { get; set; }
        /// <summary>
        /// 单位
        /// </summary>
        public K3Number FUnitID { get; set; }
        /// <summary>
        /// 批号
        /// </summary>
        public K3Number FLot { get; set; }
        /// <summary>
        /// 库存状态
        /// </summary>
        public K3Number FStockStatusId { get; set; }
        /// <summary>
        /// 保管者类型
        /// </summary>
        public string FKeeperTypeId { get; set; } = "BD_KeeperOrg";
        /// <summary>
        /// 辅库存单位
        /// </summary>
        public K3Number FSecUnitId { get; set; }
        /// <summary>
        /// 货主
        /// </summary>
        public K3Number FOwnerId { get; set; }
        /// <summary>
        /// 系统源单分录内码
        /// </summary>
        public long FEntrySrcEnteryId { get; set; }
        /// <summary>
        /// 系统源单内码
        /// </summary>
        public int FEntrySrcInterId { get; set; }
        /// <summary>
        /// 申请数量
        /// </summary>
        public decimal FAppQty { get; set; }
        /// <summary>
        /// 辅库存单位实发数量
        /// </summary>
        public decimal FSecActualQty { get; set; }
        /// <summary>
        /// 更新库存标志
        /// </summary>
        public string FStockFlag { get; set; }
        /// <summary>
        /// 主库存基本单位实发数量
        /// </summary>
        public decimal FBaseStockActualQty { get; set; }
        /// <summary>
        /// 主库存单位实发数量
        /// </summary>
        public decimal FStockActualQty { get; set; }
        /// <summary>
        /// 系统源单行号
        /// </summary>
        public int FEntrySrcEntrySeq { get; set; }
        /// <summary>
        /// 保管者
        /// </summary>
        public K3Number FKeeperId { get; set; }
        /// <summary>
        /// 仓库
        /// </summary>
        public K3Number FStockId { get; set; }
        /// <summary>
        /// 用料清单编号
        /// </summary>
        public string FPPBomBillNo { get; set; }
        /// <summary>
        /// 车间
        /// </summary>
        public K3Number FEntryWorkShopId { get; set; }
        /// <summary>
        /// 生产订单分录内码
        /// </summary>
        public int? FMoEntryId { get; set; }
        /// <summary>
        /// 生产订单行号
        /// </summary>
        public int? FMoEntrySeq { get; set; }
        /// <summary>
        /// 基本单位
        /// </summary>
        public K3Number FBaseUnitId { get; set; }
        /// <summary>
        /// 关联关系表
        /// </summary>
        [ignore]
        public List<FEntity_Link> FEntity_Link { get; set; }
    }

    /// <summary>
    /// 关联关系表
    /// </summary>
    public class FEntity_Link
    {
        /// <summary>
        /// 推进路线
        /// </summary>
        public int FEntity_Link_FFlowLineId { get; set; } = 5;
        /// <summary>
        /// 修改携带量(实发)
        /// </summary>
        public decimal FEntity_Link_FBaseActualQty { get; set; }
        /// <summary>
        /// 生产用料清单分录内码
        /// </summary>
        public long FEntity_Link_FSId { get; set; }
        /// <summary>
        /// 原始携带量(应发)
        /// </summary>
        public decimal FEntity_Link_FBaseActualQtyOld { get; set; }
        /// <summary>
        /// 单据转换标识
        /// </summary>
        public string FEntity_Link_FRuleId { get; set; }
        /// <summary>
        /// 源单表内码
        /// </summary>
        public string FEntity_Link_FSTableId { get; set; }
        /// <summary>
        /// 业务流程
        /// </summary>
        public string FEntity_Link_FFlowId { get; set; }
        /// <summary>
        /// 生产用料清单内码
        /// </summary>
        public int FEntity_Link_FSBillId { get; set; }
        /// <summary>
        /// 生产用料清单表体表名
        /// </summary>
        public string FEntity_Link_FSTableName { get; set; }
    }
}
