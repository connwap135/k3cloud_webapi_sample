﻿using System.ComponentModel;

namespace WebDemo.Models.EntityTable
{
    /// <summary>
    /// 即时库存明细
    /// </summary>
    [Description("即时库存明细")]
    public class STK_Inventory
    {
        [Description("货主编码")]
        public string FOwnerId__FNumber { get; set; }
        [Description("货主")]
        public string FOwnerId__FName { get; set; }
        [Description("保管者编码")]
        public string FKeeperId__FNumber { get; set; }
        [Description("保管者")]
        public string FKeeperId__FName { get; set; }
        [Description("物料编码")]
        public string FMaterialId__FNumber { get; set; }
        [Description("物料名称")]
        public string FMaterialName { get; set; }
        [Description("规格型号")]
        public string FModel { get; set; }
        [Description("批号")]
        public string FLot__FNumber { get; set; }
        [Description("仓库编码")]
        public string FStockId__FNumber { get; set; }
        [Description("仓库名称")]
        public string FStockName { get; set; }
        [Description("库存状态编码")]
        public string FStockStatusId__FNumber { get; set; }
        [Description("库存状态")]
        public string FStockStatusId__FName { get; set; }
        [Description("单位编码")]
        public string FStockUnitId__FNumber { get; set; }
        [Description("单位")]
        public string FStockUnitId__FName { get; set; }
        [Description("数量")]
        public decimal FBaseQty { get; set; }
    }
}
