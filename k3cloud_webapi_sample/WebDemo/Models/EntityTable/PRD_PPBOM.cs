using System;

namespace WebDemo.Models.EntityTable
{
    /// <summary>
    /// 生产用料清单
    /// </summary>
    public class PRD_PPBOM
    {
        /// <summary>
        /// FID
        /// </summary>
        public int FID { get; set; }
        #region 单据头
        /// <summary>
        /// 单据编号
        /// </summary>
        public string? FBillNo { get; set; }
        /// <summary>
        /// 单据状态
        /// </summary>
        public string? FDocumentStatus { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        public long? FApproverId { get; set; }
        /// <summary>
        /// 审核人用户名称
        /// </summary>
        public string FApproverId__FName { get; set; }
        /// <summary>
        /// 审核日期
        /// </summary>
        public DateTime? FApproveDate { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        public long? FModifierId { get; set; }
        /// <summary>
        /// 修改人用户名称
        /// </summary>
        public string FModifierId__FName { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime? FCreateDate { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public long? FCreatorId { get; set; }
        /// <summary>
        /// 创建人用户名称
        /// </summary>
        public string FCreatorId__FName { get; set; }
        /// <summary>
        /// 修改日期
        /// </summary>
        public DateTime? FModifyDate { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string? FDescription { get; set; }
        /// <summary>
        /// 产品编码
        /// </summary>
        public long FMaterialID { get; set; }
        /// <summary>
        /// 产品编码名称
        /// </summary>
        public string FMaterialID__FName { get; set; }
        /// <summary>
        /// 产品编码编码
        /// </summary>
        public string FMaterialID__FNumber { get; set; }
        /// <summary>
        /// 生产车间
        /// </summary>
        public long? FWorkshopID { get; set; }
        /// <summary>
        /// 生产车间名称
        /// </summary>
        public string FWorkshopID__FName { get; set; }
        /// <summary>
        /// 生产车间编码
        /// </summary>
        public string FWorkshopID__FNumber { get; set; }
        /// <summary>
        /// BOM版本
        /// </summary>
        public long? FBOMID { get; set; }
        /// <summary>
        /// BOM版本BOM简称
        /// </summary>
        public string FBOMID__FName { get; set; }
        /// <summary>
        /// BOM版本BOM版本
        /// </summary>
        public string FBOMID__FNumber { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public decimal? FQty { get; set; }
        /// <summary>
        /// 生产订单编号
        /// </summary>
        public string FMOBillNO { get; set; }
        /// <summary>
        /// 生产订单行号
        /// </summary>
        public int? FMOEntrySeq { get; set; }
        /// <summary>
        /// 生产订单分录内码
        /// </summary>
        public int? FMOEntryID { get; set; }
        /// <summary>
        /// 基本单位数量
        /// </summary>
        public decimal? FBaseQty { get; set; }
        /// <summary>
        /// 生产订单内码
        /// </summary>
        public int? FMoId { get; set; }
        /// <summary>
        /// 基本单位
        /// </summary>
        public long? FBaseUnitID { get; set; }
        /// <summary>
        /// 基本单位名称
        /// </summary>
        public string FBaseUnitID__FName { get; set; }
        /// <summary>
        /// 基本单位编码
        /// </summary>
        public string FBaseUnitID__FNumber { get; set; }
        /// <summary>
        /// 生产订单类型
        /// </summary>
        public long? FMOType { get; set; }
        /// <summary>
        /// 生产订单类型名称
        /// </summary>
        public string FMOType__FName { get; set; }
        /// <summary>
        /// 生产订单类型编码
        /// </summary>
        public string FMOType__FNumber { get; set; }
        /// <summary>
        /// 单位
        /// </summary>
        public long FUnitID { get; set; }
        /// <summary>
        /// 单位名称
        /// </summary>
        public string FUnitID__FName { get; set; }
        /// <summary>
        /// 单位编码
        /// </summary>
        public string FUnitID__FNumber { get; set; }
        /// <summary>
        /// 生产组织
        /// </summary>
        public long FPrdOrgId { get; set; }
        /// <summary>
        /// 生产组织名称
        /// </summary>
        public string FPrdOrgId__FName { get; set; }
        /// <summary>
        /// 生产组织编码
        /// </summary>
        public string FPrdOrgId__FNumber { get; set; }
        /// <summary>
        /// 辅助属性
        /// </summary>
        public long? FAuxPropIDHead { get; set; }
        /// <summary>
        /// 产品货主类型
        /// </summary>
        public string? FParentOwnerTypeId { get; set; }
        /// <summary>
        /// 产品货主
        /// </summary>
        public int? FParentOwnerId { get; set; }
        /// <summary>
        /// 产品货主名称
        /// </summary>
        public string FParentOwnerId__FName { get; set; }
        /// <summary>
        /// 产品货主编码
        /// </summary>
        public string FParentOwnerId__FNumber { get; set; }
        /// <summary>
        /// 委托组织
        /// </summary>
        public long? FEntrustOrgId { get; set; }
        /// <summary>
        /// 委托组织名称
        /// </summary>
        public string FEntrustOrgId__FName { get; set; }
        /// <summary>
        /// 委托组织编码
        /// </summary>
        public string FEntrustOrgId__FNumber { get; set; }
        /// <summary>
        /// 生产订单行镜像
        /// </summary>
        public long? FMoEntryMirror { get; set; }
        /// <summary>
        /// 生产订单行镜像名称
        /// </summary>
        public string FMoEntryMirror__FName { get; set; }
        /// <summary>
        /// 生产订单行镜像编码
        /// </summary>
        public string FMoEntryMirror__FNumber { get; set; }
        /// <summary>
        /// 销售订单ID
        /// </summary>
        public int? FSaleOrderId { get; set; }
        /// <summary>
        /// 销售订单分录内码
        /// </summary>
        public int? FSaleOrderEntryId { get; set; }
        /// <summary>
        /// 销售订单
        /// </summary>
        public string? FSALEORDERNO { get; set; }
        /// <summary>
        /// 销售订单行号
        /// </summary>
        public int? FSaleOrderEntrySeq { get; set; }
        /// <summary>
        /// 需求来源
        /// </summary>
        public string? FReqSrc { get; set; }
        /// <summary>
        /// 库存日期
        /// </summary>
        public DateTime? FInventoryDate { get; set; }
        /// <summary>
        /// BOM日期
        /// </summary>
        public DateTime? FGeneRateDate { get; set; }
        /// <summary>
        /// 期初生产订单
        /// </summary>
        public string? FIsQCMo { get; set; }
        #endregion
        #region 子项明细
        /// <summary>
        /// 子项明细主键FEntryID
        /// </summary>
        public long FEntity_FEntryID { get; set; }
        /// <summary>
        /// 子项明细序列
        /// </summary>
        public int FEntity_FSeq { get; set; }
        /// <summary>
        /// 父级行主键
        /// </summary>
        public string? FParentRowId { get; set; }
        /// <summary>
        /// 行展开类型
        /// </summary>
        public int? FRowExpandType { get; set; }
        /// <summary>
        /// 行标识
        /// </summary>
        public string? FRowId { get; set; }
        /// <summary>
        /// BOM分录内码
        /// </summary>
        public int? FBOMEntryID { get; set; }
        /// <summary>
        /// 项次
        /// </summary>
        public int? FReplaceGroup { get; set; }
        /// <summary>
        /// 子项物料编码
        /// </summary>
        public long FMaterialID2 { get; set; }
        /// <summary>
        /// 子项物料编码名称
        /// </summary>
        public string FMaterialID2__FName { get; set; }
        /// <summary>
        /// 子项物料编码编码
        /// </summary>
        public string FMaterialID2__FNumber { get; set; }
        /// <summary>
        /// 子项类型
        /// </summary>
        public string FMaterialType { get; set; }
        /// <summary>
        /// 偏置提前期单位
        /// </summary>
        public string FTimeUnit2 { get; set; }
        /// <summary>
        /// 用量类型
        /// </summary>
        public string FDosageType { get; set; }
        /// <summary>
        /// 使用比例(%)
        /// </summary>
        public decimal? FUseRate { get; set; }
        /// <summary>
        /// 变动损耗率%
        /// </summary>
        public decimal? FScrapRate { get; set; }
        /// <summary>
        /// 工序
        /// </summary>
        public int? FOperID { get; set; }
        /// <summary>
        /// 作业
        /// </summary>
        public long? FProcessID { get; set; }
        /// <summary>
        /// 作业名称
        /// </summary>
        public string FProcessID__FName { get; set; }
        /// <summary>
        /// 作业编码
        /// </summary>
        public string FProcessID__FNumber { get; set; }
        /// <summary>
        /// 需求日期
        /// </summary>
        public DateTime? FNeedDate2 { get; set; }
        /// <summary>
        /// 标准用量
        /// </summary>
        public decimal? FStdQty { get; set; }
        /// <summary>
        /// 需求数量
        /// </summary>
        public decimal? FNeedQty2 { get; set; }
        /// <summary>
        /// 应发数量
        /// </summary>
        public decimal? FMustQty { get; set; }
        /// <summary>
        /// 已领数量
        /// </summary>
        public decimal? FPickedQty { get; set; }
        /// <summary>
        /// 补领数量
        /// </summary>
        public decimal? FRePickedQty { get; set; }
        /// <summary>
        /// 报废数量
        /// </summary>
        public decimal? FScrapQty { get; set; }
        /// <summary>
        /// 良品退料数量
        /// </summary>
        public decimal? FGoodReturnQty { get; set; }
        /// <summary>
        /// 来料不良退料数量
        /// </summary>
        public decimal? FINCDefectReturnQty { get; set; }
        /// <summary>
        /// 作业不良退料数量
        /// </summary>
        public decimal? FProcessDefectReturnQty { get; set; }
        /// <summary>
        /// 已消耗数量
        /// </summary>
        public decimal? FConsumeQty { get; set; }
        /// <summary>
        /// 在制材料数量
        /// </summary>
        public decimal? FWipQty { get; set; }
        /// <summary>
        /// 发料方式
        /// </summary>
        public string FIssueType { get; set; }
        /// <summary>
        /// 倒冲时机
        /// </summary>
        public string? FBackFlushType { get; set; }
        /// <summary>
        /// 超发比例
        /// </summary>
        public decimal? FOverRate { get; set; }
        /// <summary>
        /// 仓位
        /// </summary>
        public long? FStockLOCID { get; set; }
        /// <summary>
        /// 仓库
        /// </summary>
        public long? FStockID { get; set; }
        /// <summary>
        /// 仓库名称
        /// </summary>
        public string FStockID__FName { get; set; }
        /// <summary>
        /// 仓库编码
        /// </summary>
        public string FStockID__FNumber { get; set; }
        /// <summary>
        /// 计划跟踪号
        /// </summary>
        public string? FMTONO { get; set; }
        /// <summary>
        /// 项目编号
        /// </summary>
        public string? FProjectNO { get; set; }
        /// <summary>
        /// 位置号
        /// </summary>
        public string? FPositionNO { get; set; }
        /// <summary>
        /// 领料选单数量
        /// </summary>
        public decimal? FSelPickedQty { get; set; }
        /// <summary>
        /// 补料选单数量
        /// </summary>
        public decimal? FSelRePickedQty { get; set; }
        /// <summary>
        /// 退料选单数量
        /// </summary>
        public decimal? FSelPrcdReturnQty { get; set; }
        /// <summary>
        /// 拨出组织
        /// </summary>
        public long? FSrcTransOrgId { get; set; }
        /// <summary>
        /// 拨出组织名称
        /// </summary>
        public string FSrcTransOrgId__FName { get; set; }
        /// <summary>
        /// 拨出组织编码
        /// </summary>
        public string FSrcTransOrgId__FNumber { get; set; }
        /// <summary>
        /// 拨出仓库
        /// </summary>
        public long? FSrcTransStockId { get; set; }
        /// <summary>
        /// 拨出仓库名称
        /// </summary>
        public string FSrcTransStockId__FName { get; set; }
        /// <summary>
        /// 拨出仓库编码
        /// </summary>
        public string FSrcTransStockId__FNumber { get; set; }
        /// <summary>
        /// 拨出仓位
        /// </summary>
        public long? FSrcTransStockLocId { get; set; }
        /// <summary>
        /// 调拨选单数量
        /// </summary>
        public decimal? FSelTranslateQty { get; set; }
        /// <summary>
        /// 调拨数量
        /// </summary>
        public decimal? FTranslateQty { get; set; }
        /// <summary>
        /// 是否发损耗
        /// </summary>
        public string? FIsGetScrap { get; set; }
        /// <summary>
        /// 是否超发
        /// </summary>
        public string? FAllowOver { get; set; }
        /// <summary>
        /// BOM版本
        /// </summary>
        public long? FBomId2 { get; set; }
        /// <summary>
        /// BOM版本BOM简称
        /// </summary>
        public string FBomId2__FName { get; set; }
        /// <summary>
        /// BOM版本BOM版本
        /// </summary>
        public string FBomId2__FNumber { get; set; }
        /// <summary>
        /// 发料组织
        /// </summary>
        public long FSupplyOrg { get; set; }
        /// <summary>
        /// 发料组织名称
        /// </summary>
        public string FSupplyOrg__FName { get; set; }
        /// <summary>
        /// 发料组织编码
        /// </summary>
        public string FSupplyOrg__FNumber { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string? FMEMO1 { get; set; }
        /// <summary>
        /// 基本单位标准用量
        /// </summary>
        public decimal? FBaseStdQty { get; set; }
        /// <summary>
        /// 基本单位需求数量
        /// </summary>
        public decimal? FBaseNeedQty { get; set; }
        /// <summary>
        /// 子项基本单位
        /// </summary>
        public long? FBaseUnitID1 { get; set; }
        /// <summary>
        /// 子项基本单位名称
        /// </summary>
        public string FBaseUnitID1__FName { get; set; }
        /// <summary>
        /// 子项基本单位编码
        /// </summary>
        public string FBaseUnitID1__FNumber { get; set; }
        /// <summary>
        /// 原始携带量
        /// </summary>
        public decimal? FBaseMustQty { get; set; }
        /// <summary>
        /// 基本单位已领数量
        /// </summary>
        public decimal? FBasePickedQty { get; set; }
        /// <summary>
        /// 基本单位补领数量
        /// </summary>
        public decimal? FBaseRepickedQty { get; set; }
        /// <summary>
        /// 基本单位报废数量
        /// </summary>
        public decimal? FBaseScrapQty { get; set; }
        /// <summary>
        /// 基本单位良品退料数量
        /// </summary>
        public decimal? FBaseGoodReturnQty { get; set; }
        /// <summary>
        /// 基本单位来料不良退料数量
        /// </summary>
        public decimal? FBaseIncDefectReturnQty { get; set; }
        /// <summary>
        /// 基本单位作业不良退料数量
        /// </summary>
        public decimal? FBasePrcDefectReturnQty { get; set; }
        /// <summary>
        /// 基本单位已消耗数量
        /// </summary>
        public decimal? FBaseConsumeQty { get; set; }
        /// <summary>
        /// 基本单位在制材料数量
        /// </summary>
        public decimal? FBaseWipQty { get; set; }
        /// <summary>
        /// 基本单位良品退料选单数量
        /// </summary>
        public decimal? FBaseSelGoodReturnQty { get; set; }
        /// <summary>
        /// 基本单位来料不良退料选单数量
        /// </summary>
        public decimal? FBaseSelIncDefectReturnQty { get; set; }
        /// <summary>
        /// 基本单位作业不良退料选单数量
        /// </summary>
        public decimal? FBaseSelPrcDefectReturnQty { get; set; }
        /// <summary>
        /// 基本单位领料选单数量
        /// </summary>
        public decimal? FBaseSelPickedQty { get; set; }
        /// <summary>
        /// 基本单位补料选单数量
        /// </summary>
        public decimal? FBaseSelRePickedQty { get; set; }
        /// <summary>
        /// 基本单位退料选单数量
        /// </summary>
        public decimal? FBaseSelPrcdReturnQty { get; set; }
        /// <summary>
        /// 基本单位调拨选单数量
        /// </summary>
        public decimal? FBaseSelTranslateQty { get; set; }
        /// <summary>
        /// 基本单位调拨数量
        /// </summary>
        public decimal? FBaseTranslateQty { get; set; }
        /// <summary>
        /// 基本单位开单未退数量
        /// </summary>
        public decimal? FBaseReturnNoOkQty { get; set; }
        /// <summary>
        /// 开单未退数量
        /// </summary>
        public decimal? FReturnNoOkQty { get; set; }
        /// <summary>
        /// 生产订单类型
        /// </summary>
        public long? FMoType1 { get; set; }
        /// <summary>
        /// 生产订单类型名称
        /// </summary>
        public string FMoType1__FName { get; set; }
        /// <summary>
        /// 生产订单类型编码
        /// </summary>
        public string FMoType1__FNumber { get; set; }
        /// <summary>
        /// 生产订单编号
        /// </summary>
        public string? FMoBillNo1 { get; set; }
        /// <summary>
        /// 生产订单内码
        /// </summary>
        public int? FMoId1 { get; set; }
        /// <summary>
        /// 生产订单分录内码
        /// </summary>
        public int? FMoEntryId1 { get; set; }
        /// <summary>
        /// 生产订单行号
        /// </summary>
        public int? FMoEntrySeq1 { get; set; }
        /// <summary>
        /// 子项单位
        /// </summary>
        public long FUnitID2 { get; set; }
        /// <summary>
        /// 子项单位名称
        /// </summary>
        public string FUnitID2__FName { get; set; }
        /// <summary>
        /// 子项单位编码
        /// </summary>
        public string FUnitID2__FNumber { get; set; }
        /// <summary>
        /// 原分子
        /// </summary>
        public decimal? FBOMNumerator { get; set; }
        /// <summary>
        /// 原分母
        /// </summary>
        public decimal? FBOMDenominator { get; set; }
        /// <summary>
        /// 分子
        /// </summary>
        public decimal? FNumerator { get; set; }
        /// <summary>
        /// 分母
        /// </summary>
        public decimal? FDenominator { get; set; }
        /// <summary>
        /// 基本单位原分子
        /// </summary>
        public decimal? FBaseBOMNumerator { get; set; }
        /// <summary>
        /// 基本单位分子
        /// </summary>
        public decimal? FBaseNumerator { get; set; }
        /// <summary>
        /// 辅助属性
        /// </summary>
        public long? FAuxPropID { get; set; }
        /// <summary>
        /// 货主
        /// </summary>
        public int? FOwnerID { get; set; }
        /// <summary>
        /// 货主名称
        /// </summary>
        public string FOwnerID__FName { get; set; }
        /// <summary>
        /// 货主编码
        /// </summary>
        public string FOwnerID__FNumber { get; set; }
        /// <summary>
        /// 货主类型
        /// </summary>
        public string? FOwnerTypeId { get; set; }
        /// <summary>
        /// 替代主料
        /// </summary>
        public string? FIsKeyItem { get; set; }
        /// <summary>
        /// 固定损耗
        /// </summary>
        public decimal? FFixScrapQty { get; set; }
        /// <summary>
        /// 批号
        /// </summary>
        public long? FLot { get; set; }
        /// <summary>
        /// 批号名称
        /// </summary>
        public string FLot__FName { get; set; }
        /// <summary>
        /// 批号批号
        /// </summary>
        public string FLot__FNumber { get; set; }
        /// <summary>
        /// 偏置提前期
        /// </summary>
        public int? FOffsetTime { get; set; }
        /// <summary>
        /// 基本单位固定损耗数量
        /// </summary>
        public decimal? FBaseFixScrapQTY { get; set; }
        /// <summary>
        /// 基本单位分母
        /// </summary>
        public decimal? FBASEDENOMINATOR { get; set; }
        /// <summary>
        /// 基本单位原分母
        /// </summary>
        public decimal? FBASEBOMDENOMINATOR { get; set; }
        /// <summary>
        /// 是否关键件
        /// </summary>
        public string? FIsKeyComponent { get; set; }
        /// <summary>
        /// 业务流程
        /// </summary>
        public string? FBFLowId { get; set; }
        /// <summary>
        /// 业务流程名称
        /// </summary>
        public string FBFLowId__FName { get; set; }
        /// <summary>
        /// 子项工作日历
        /// </summary>
        public long? FWorkCalId2 { get; set; }
        /// <summary>
        /// 子项工作日历名称
        /// </summary>
        public string FWorkCalId2__FName { get; set; }
        /// <summary>
        /// 子项工作日历编码
        /// </summary>
        public string FWorkCalId2__FNumber { get; set; }
        /// <summary>
        /// 优先级
        /// </summary>
        public int? FPriority { get; set; }
        /// <summary>
        /// 预留类型
        /// </summary>
        public string FReserveType { get; set; }
        /// <summary>
        /// 替代策略
        /// </summary>
        public string? FReplacePolicy { get; set; }
        /// <summary>
        /// 替代方式
        /// </summary>
        public string? FReplaceType { get; set; }
        /// <summary>
        /// 替代优先级
        /// </summary>
        public int? FReplacePriority { get; set; }
        /// <summary>
        /// 超发控制方式
        /// </summary>
        public string FOverControlMode { get; set; }
        /// <summary>
        /// 货源
        /// </summary>
        public long? FSMId { get; set; }
        /// <summary>
        /// 货源名称
        /// </summary>
        public string FSMId__FName { get; set; }
        /// <summary>
        /// 货源编码
        /// </summary>
        public string FSMId__FNumber { get; set; }
        /// <summary>
        /// 货源分录
        /// </summary>
        public int? FSMEntryId { get; set; }
        /// <summary>
        /// 基本单位备料数量
        /// </summary>
        public decimal? FBaseStockReadyQty { get; set; }
        /// <summary>
        /// 备料数量
        /// </summary>
        public decimal? FStockReadyQty { get; set; }
        /// <summary>
        /// 供应组织
        /// </summary>
        public long? FChildSupplyOrgId { get; set; }
        /// <summary>
        /// 供应组织名称
        /// </summary>
        public string FChildSupplyOrgId__FName { get; set; }
        /// <summary>
        /// 供应组织编码
        /// </summary>
        public string FChildSupplyOrgId__FNumber { get; set; }
        /// <summary>
        /// 工序序列
        /// </summary>
        public string? FOptQueue { get; set; }
        /// <summary>
        /// 库存状态
        /// </summary>
        public long? FStockStatusId { get; set; }
        /// <summary>
        /// 库存状态名称
        /// </summary>
        public string FStockStatusId__FName { get; set; }
        /// <summary>
        /// 库存状态编码
        /// </summary>
        public string FStockStatusId__FNumber { get; set; }
        /// <summary>
        /// 产品货主
        /// </summary>
        public long? FEntrustPickOrgId { get; set; }
        /// <summary>
        /// 产品货主名称
        /// </summary>
        public string FEntrustPickOrgId__FName { get; set; }
        /// <summary>
        /// 产品货主编码
        /// </summary>
        public string FEntrustPickOrgId__FNumber { get; set; }
        /// <summary>
        /// 跳层
        /// </summary>
        public string? FIsSkip { get; set; }
        /// <summary>
        /// 领料考虑最小发料批量
        /// </summary>
        public string? FISMinIssueQty { get; set; }
        /// <summary>
        /// 用料清单类型
        /// </summary>
        public string? FPPBomEntryType { get; set; }
        /// <summary>
        /// 最后更新人
        /// </summary>
        public long? FUPDATERID { get; set; }
        /// <summary>
        /// 最后更新人用户名称
        /// </summary>
        public string FUPDATERID__FName { get; set; }
        /// <summary>
        /// 最后更新时间
        /// </summary>
        public DateTime? FUPDateDate { get; set; }
        /// <summary>
        /// 分组货主
        /// </summary>
        public int? FGroupByOwnerId { get; set; }
        /// <summary>
        /// 分组货主名称
        /// </summary>
        public string FGroupByOwnerId__FName { get; set; }
        /// <summary>
        /// 分组货主编码
        /// </summary>
        public string FGroupByOwnerId__FNumber { get; set; }
        /// <summary>
        /// 供料方式
        /// </summary>
        public string? FSupplyMode { get; set; }
        /// <summary>
        /// 基本单位未领数量
        /// </summary>
        public decimal? FBaseNoPickedQty { get; set; }
        /// <summary>
        /// 未领数量
        /// </summary>
        public decimal? FNoPickedQty { get; set; }
        /// <summary>
        /// 发料选单数量
        /// </summary>
        public decimal? FSelIssueQty { get; set; }
        /// <summary>
        /// 基本单位发料选单数量
        /// </summary>
        public decimal? FBaseSelIssueQty { get; set; }
        /// <summary>
        /// 发料数量
        /// </summary>
        public decimal? FIssueQty { get; set; }
        /// <summary>
        /// 基本单位发料数量
        /// </summary>
        public decimal? FBaseIssueQty { get; set; }
        /// <summary>
        /// MRP运算
        /// </summary>
        public string? FIsMrpRun { get; set; }
        /// <summary>
        /// 原用料清单内码
        /// </summary>
        public int? FSrcPPBOMID { get; set; }
        /// <summary>
        /// 原用料清单分录内码
        /// </summary>
        public int? FSrcPPBOMEntryId { get; set; }
        /// <summary>
        /// BOM展开路径
        /// </summary>
        public string? FPathEntryID { get; set; }
        /// <summary>
        /// 基本单位可用库存数量
        /// </summary>
        public decimal? FBaseInventoryQty { get; set; }
        /// <summary>
        /// 可用库存
        /// </summary>
        public decimal? FInventoryQty { get; set; }
        /// <summary>
        /// 供应类型
        /// </summary>
        public string? FSupplyType { get; set; }
        /// <summary>
        /// 原BOM展开路径
        /// </summary>
        public string? FSrcPathEntryID { get; set; }
        /// <summary>
        /// 多领退回
        /// </summary>
        public decimal? FReturnQty { get; set; }
        /// <summary>
        /// 基本单位多领退回数量
        /// </summary>
        public decimal? FBaseReturnQty { get; set; }
        /// <summary>
        /// 是否展开
        /// </summary>
        public string? FIsExpand { get; set; }
        /// <summary>
        /// 生产退料检验
        /// </summary>
        public string? FCheckReturnMtrl { get; set; }
        /// <summary>
        /// 基本单位生产退料请检选单数量
        /// </summary>
        public decimal? FBaseReturnAppSelQty { get; set; }
        /// <summary>
        /// 生产退料请检选单数量
        /// </summary>
        public decimal? FReturnAppSelQty { get; set; }
        /// <summary>
        /// 累计成品率
        /// </summary>
        public decimal? FBillAccuYieldRate { get; set; }
        /// <summary>
        /// 应发数量(最小发料批量)
        /// </summary>
        public decimal? FMinIssueQty { get; set; }
        /// <summary>
        /// 基本单位应发数量(最小发料批量)
        /// </summary>
        public decimal? FBaseMinIssueQty { get; set; }
        /// <summary>
        /// 初始化数量
        /// </summary>
        public decimal? FALLOCATEQTY { get; set; }
        /// <summary>
        /// 基本单位初始化数量
        /// </summary>
        public decimal? FBASEALLOCATEQTY { get; set; }
        /// <summary>
        /// 是否应发修改
        /// </summary>
        public string? FISMODIFYMQ { get; set; }
        /// <summary>
        /// 顶层物料编码
        /// </summary>
        public long? FTOPMATERIALID { get; set; }
        /// <summary>
        /// 顶层物料编码名称
        /// </summary>
        public string FTOPMATERIALID__FName { get; set; }
        /// <summary>
        /// 顶层物料编码编码
        /// </summary>
        public string FTOPMATERIALID__FNumber { get; set; }
        /// <summary>
        /// 父项物料编码
        /// </summary>
        public long? FPARENTMATERIALID { get; set; }
        /// <summary>
        /// 父项物料编码名称
        /// </summary>
        public string FPARENTMATERIALID__FName { get; set; }
        /// <summary>
        /// 父项物料编码编码
        /// </summary>
        public string FPARENTMATERIALID__FNumber { get; set; }
        /// <summary>
        /// 实领数量
        /// </summary>
        public decimal? FACTUALPICKQTY { get; set; }
        /// <summary>
        /// 基本单位实领数量
        /// </summary>
        public decimal? FBASEACTUALPICKQTY { get; set; }
        /// <summary>
        /// 变更标志
        /// </summary>
        public string? FPPBOMChangeFlag { get; set; }
        #endregion
    }
}