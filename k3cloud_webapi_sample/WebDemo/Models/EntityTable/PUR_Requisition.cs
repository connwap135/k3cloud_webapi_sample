using System;

namespace WebDemo.Models.EntityTable
{
    /// <summary>
    /// 采购申请单
    /// </summary>
    public class PUR_Requisition
    {
        /// <summary>
        /// FID
        /// </summary>
        public int FID { get; set; }
        #region 基本信息
        /// <summary>
        /// 单据编号
        /// </summary>
        public string? FBillNo { get; set; }
        /// <summary>
        /// 数据状态
        /// </summary>
        public string? FDocumentStatus { get; set; }
        /// <summary>
        /// 单据类型
        /// </summary>
        public string FBillTypeID { get; set; }
        /// <summary>
        /// 单据类型名称
        /// </summary>
        public string FBillTypeID__FName { get; set; }
        /// <summary>
        /// 单据类型编码
        /// </summary>
        public string FBillTypeID__FNumber { get; set; }
        /// <summary>
        /// 申请组织
        /// </summary>
        public long FApplicationOrgId { get; set; }
        /// <summary>
        /// 申请组织名称
        /// </summary>
        public string FApplicationOrgId__FName { get; set; }
        /// <summary>
        /// 申请组织编码
        /// </summary>
        public string FApplicationOrgId__FNumber { get; set; }
        /// <summary>
        /// 申请部门
        /// </summary>
        public long? FApplicationDeptId { get; set; }
        /// <summary>
        /// 申请部门名称
        /// </summary>
        public string FApplicationDeptId__FName { get; set; }
        /// <summary>
        /// 申请部门编码
        /// </summary>
        public string FApplicationDeptId__FNumber { get; set; }
        /// <summary>
        /// 申请人
        /// </summary>
        public long? FApplicantId { get; set; }
        /// <summary>
        /// 申请人员工编码
        /// </summary>
        public string FApplicantId__FNumber { get; set; }
        /// <summary>
        /// 申请人员工名称
        /// </summary>
        public string FApplicantId__FName { get; set; }
        /// <summary>
        /// 币别
        /// </summary>
        public long? FCurrencyId { get; set; }
        /// <summary>
        /// 币别名称
        /// </summary>
        public string FCurrencyId__FName { get; set; }
        /// <summary>
        /// 币别编码
        /// </summary>
        public string FCurrencyId__FNumber { get; set; }
        /// <summary>
        /// 含税金额合计
        /// </summary>
        public decimal? FTotalAmount { get; set; }
        /// <summary>
        /// 作废人
        /// </summary>
        public long? FCancellerId { get; set; }
        /// <summary>
        /// 作废人用户名称
        /// </summary>
        public string FCancellerId__FName { get; set; }
        /// <summary>
        /// 关闭状态
        /// </summary>
        public string? FCloseStatus { get; set; }
        /// <summary>
        /// 关闭人
        /// </summary>
        public long? FCloserId { get; set; }
        /// <summary>
        /// 关闭人用户名称
        /// </summary>
        public string FCloserId__FName { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public long? FCreatorId { get; set; }
        /// <summary>
        /// 创建人用户名称
        /// </summary>
        public string FCreatorId__FName { get; set; }
        /// <summary>
        /// 关闭日期
        /// </summary>
        public DateTime? FCloseDate { get; set; }
        /// <summary>
        /// 作废日期
        /// </summary>
        public DateTime? FCancelDate { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        public long? FApproverId { get; set; }
        /// <summary>
        /// 审核人用户名称
        /// </summary>
        public string FApproverId__FName { get; set; }
        /// <summary>
        /// 最后修改人
        /// </summary>
        public long? FModifierId { get; set; }
        /// <summary>
        /// 最后修改人用户名称
        /// </summary>
        public string FModifierId__FName { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime? FCreateDate { get; set; }
        /// <summary>
        /// 作废状态
        /// </summary>
        public string? FCancelStatus { get; set; }
        /// <summary>
        /// 最后修改日期
        /// </summary>
        public DateTime? FModifyDate { get; set; }
        /// <summary>
        /// 审核日期
        /// </summary>
        public DateTime? FApproveDate { get; set; }
        /// <summary>
        /// 汇率类型
        /// </summary>
        public long? FExchangeTypeId { get; set; }
        /// <summary>
        /// 汇率类型名称
        /// </summary>
        public string FExchangeTypeId__FName { get; set; }
        /// <summary>
        /// 汇率类型编码
        /// </summary>
        public string FExchangeTypeId__FNumber { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string? FNote { get; set; }
        /// <summary>
        /// 是否是单据转换
        /// </summary>
        public string? FIsConvert { get; set; }
        /// <summary>
        /// 申请类型
        /// </summary>
        public string FRequestType { get; set; }
        /// <summary>
        /// 申请日期
        /// </summary>
        public DateTime FApplicationDate { get; set; }
        /// <summary>
        /// 合并作废
        /// </summary>
        public string? FIsMergeCancel { get; set; }
        /// <summary>
        /// 来源类型
        /// </summary>
        public string? FSrcType { get; set; }
        /// <summary>
        /// 供应商协同发布状态(6.1废弃)
        /// </summary>
        public string? FPublishStatus { get; set; }
        /// <summary>
        /// 价外税
        /// </summary>
        public string? FISPRICEEXCLUDETAX { get; set; }
        /// <summary>
        /// 验收方式
        /// </summary>
        public string? FACCTYPE { get; set; }
        /// <summary>
        /// 手工关闭
        /// </summary>
        public string? FMANUALCLOSE { get; set; }
        /// <summary>
        /// 关闭原因
        /// </summary>
        public string? FCloseReason { get; set; }
        #endregion
        #region 明细信息
        /// <summary>
        /// 明细信息主键FEntryID
        /// </summary>
        public long FEntity_FEntryID { get; set; }
        /// <summary>
        /// 明细信息序列
        /// </summary>
        public int FEntity_FSeq { get; set; }
        /// <summary>
        /// 申请数量
        /// </summary>
        public decimal? FReqQty { get; set; }
        /// <summary>
        /// 批准数量
        /// </summary>
        public decimal? FApproveQty { get; set; }
        /// <summary>
        /// 建议供应商
        /// </summary>
        public long? FSuggestSupplierId { get; set; }
        /// <summary>
        /// 建议供应商名称
        /// </summary>
        public string FSuggestSupplierId__FName { get; set; }
        /// <summary>
        /// 建议供应商编码
        /// </summary>
        public string FSuggestSupplierId__FNumber { get; set; }
        /// <summary>
        /// 建议供应商简称
        /// </summary>
        public string FSuggestSupplierId__FShortName { get; set; }
        /// <summary>
        /// 提前期
        /// </summary>
        public int? FLeadTime { get; set; }
        /// <summary>
        /// 含税金额
        /// </summary>
        public decimal? FReqAmount { get; set; }
        /// <summary>
        /// 采购组织
        /// </summary>
        public long FPurchaseOrgId { get; set; }
        /// <summary>
        /// 采购组织名称
        /// </summary>
        public string FPurchaseOrgId__FName { get; set; }
        /// <summary>
        /// 采购组织编码
        /// </summary>
        public string FPurchaseOrgId__FNumber { get; set; }
        /// <summary>
        /// 采购组
        /// </summary>
        public long? FPurchaseGroupId { get; set; }
        /// <summary>
        /// 采购组业务组名称
        /// </summary>
        public string FPurchaseGroupId__FName { get; set; }
        /// <summary>
        /// 采购组业务组编码
        /// </summary>
        public string FPurchaseGroupId__FNumber { get; set; }
        /// <summary>
        /// 收料组织
        /// </summary>
        public long? FReceiveOrgId { get; set; }
        /// <summary>
        /// 收料组织名称
        /// </summary>
        public string FReceiveOrgId__FName { get; set; }
        /// <summary>
        /// 收料组织编码
        /// </summary>
        public string FReceiveOrgId__FNumber { get; set; }
        /// <summary>
        /// 仓库
        /// </summary>
        public long? FStockId { get; set; }
        /// <summary>
        /// 仓库名称
        /// </summary>
        public string FStockId__FName { get; set; }
        /// <summary>
        /// 仓库编码
        /// </summary>
        public string FStockId__FNumber { get; set; }
        /// <summary>
        /// 订单数量
        /// </summary>
        public decimal? FOrderQty { get; set; }
        /// <summary>
        /// 剩余数量
        /// </summary>
        public decimal? FRemainQty { get; set; }
        /// <summary>
        /// 合同号
        /// </summary>
        public string? FContractNo { get; set; }
        /// <summary>
        /// 需求跟踪号
        /// </summary>
        public string? FReqTraceNo { get; set; }
        /// <summary>
        /// 业务终止
        /// </summary>
        public string? FMRPTerminateStatus { get; set; }
        /// <summary>
        /// 终止人
        /// </summary>
        public long? FTerminaterId { get; set; }
        /// <summary>
        /// 终止人用户名称
        /// </summary>
        public string FTerminaterId__FName { get; set; }
        /// <summary>
        /// 终止日期
        /// </summary>
        public DateTime? FTerminateDate { get; set; }
        /// <summary>
        /// 业务关闭
        /// </summary>
        public string? FMRPCloseStatus { get; set; }
        /// <summary>
        /// 计划确认
        /// </summary>
        public string? FPlanConfirm { get; set; }
        /// <summary>
        /// 指定供应商(6.0作废)
        /// </summary>
        public long? FSupplierId { get; set; }
        /// <summary>
        /// 指定供应商(6.0作废)名称
        /// </summary>
        public string FSupplierId__FName { get; set; }
        /// <summary>
        /// 指定供应商(6.0作废)编码
        /// </summary>
        public string FSupplierId__FNumber { get; set; }
        /// <summary>
        /// 指定供应商(6.0作废)简称
        /// </summary>
        public string FSupplierId__FShortName { get; set; }
        /// <summary>
        /// 物料编码
        /// </summary>
        public long FMaterialId { get; set; }
        /// <summary>
        /// 物料编码名称
        /// </summary>
        public string FMaterialId__FName { get; set; }
        /// <summary>
        /// 物料编码编码
        /// </summary>
        public string FMaterialId__FNumber { get; set; }
        /// <summary>
        /// 计价数量
        /// </summary>
        public decimal? FPriceUnitQty { get; set; }
        /// <summary>
        /// 采购部门
        /// </summary>
        public long? FPurchaseDeptId { get; set; }
        /// <summary>
        /// 采购部门名称
        /// </summary>
        public string FPurchaseDeptId__FName { get; set; }
        /// <summary>
        /// 采购部门编码
        /// </summary>
        public string FPurchaseDeptId__FNumber { get; set; }
        /// <summary>
        /// 交货地址
        /// </summary>
        public string? FReceiveAddress { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string? FEntryNote { get; set; }
        /// <summary>
        /// 订单关联数量（基本单位）
        /// </summary>
        public decimal? FOrderJNBaseQty { get; set; }
        /// <summary>
        /// 订单数量（基本单位）
        /// </summary>
        public decimal? FOrderBaseQty { get; set; }
        /// <summary>
        /// 基本单位
        /// </summary>
        public long? FBaseUnitId { get; set; }
        /// <summary>
        /// 基本单位名称
        /// </summary>
        public string FBaseUnitId__FName { get; set; }
        /// <summary>
        /// 基本单位编码
        /// </summary>
        public string FBaseUnitId__FNumber { get; set; }
        /// <summary>
        /// 计价单位
        /// </summary>
        public long FPriceUnitId { get; set; }
        /// <summary>
        /// 计价单位名称
        /// </summary>
        public string FPriceUnitId__FName { get; set; }
        /// <summary>
        /// 计价单位编码
        /// </summary>
        public string FPriceUnitId__FNumber { get; set; }
        /// <summary>
        /// 申请单位
        /// </summary>
        public long FUnitId { get; set; }
        /// <summary>
        /// 申请单位名称
        /// </summary>
        public string FUnitId__FName { get; set; }
        /// <summary>
        /// 申请单位编码
        /// </summary>
        public string FUnitId__FNumber { get; set; }
        /// <summary>
        /// 订单关联数量
        /// </summary>
        public decimal? FOrderJoinQty { get; set; }
        /// <summary>
        /// 批准数量(基本单位)
        /// </summary>
        public decimal? FBaseUnitQty { get; set; }
        /// <summary>
        /// 辅助属性
        /// </summary>
        public string? FAuxpropId { get; set; }
        /// <summary>
        /// BOM版本
        /// </summary>
        public long? FBOMNoId { get; set; }
        /// <summary>
        /// BOM版本BOM简称
        /// </summary>
        public string FBOMNoId__FName { get; set; }
        /// <summary>
        /// BOM版本BOM版本
        /// </summary>
        public string FBOMNoId__FNumber { get; set; }
        /// <summary>
        /// 源单类型
        /// </summary>
        public string? FSrcBillTypeId { get; set; }
        /// <summary>
        /// 源单编号
        /// </summary>
        public string? FSrcBillNo { get; set; }
        /// <summary>
        /// 物料说明
        /// </summary>
        public string? FMaterialDesc { get; set; }
        /// <summary>
        /// 建议采购日期
        /// </summary>
        public DateTime? FSuggestPurDate { get; set; }
        /// <summary>
        /// 到货日期
        /// </summary>
        public DateTime? FArrivalDate { get; set; }
        /// <summary>
        /// 入库数量(基本单位)
        /// </summary>
        public decimal? FBaseStockQty { get; set; }
        /// <summary>
        /// 收料数量(基本单位)
        /// </summary>
        public decimal? FBaseReceiveQty { get; set; }
        /// <summary>
        /// 入库数量
        /// </summary>
        public decimal? FStockQty { get; set; }
        /// <summary>
        /// 收料数量
        /// </summary>
        public decimal? FReceiveQty { get; set; }
        /// <summary>
        /// 业务流程
        /// </summary>
        public string? FBFLowId { get; set; }
        /// <summary>
        /// 业务流程名称
        /// </summary>
        public string FBFLowId__FName { get; set; }
        /// <summary>
        /// 供货地点
        /// </summary>
        public long? FProviderId { get; set; }
        /// <summary>
        /// 供货地点名称
        /// </summary>
        public string FProviderId__FName { get; set; }
        /// <summary>
        /// 供货地点编码
        /// </summary>
        public string FProviderId__FNumber { get; set; }
        /// <summary>
        /// 拆分作废
        /// </summary>
        public string? FIsSplitCancel { get; set; }
        /// <summary>
        /// 计划跟踪号
        /// </summary>
        public string? FMtoNo { get; set; }
        /// <summary>
        /// 需求组织
        /// </summary>
        public long FRequireOrgId { get; set; }
        /// <summary>
        /// 需求组织名称
        /// </summary>
        public string FRequireOrgId__FName { get; set; }
        /// <summary>
        /// 需求组织编码
        /// </summary>
        public string FRequireOrgId__FNumber { get; set; }
        /// <summary>
        /// 采购员
        /// </summary>
        public long? FPurchaserId { get; set; }
        /// <summary>
        /// 采购员名称
        /// </summary>
        public string FPurchaserId__FName { get; set; }
        /// <summary>
        /// 采购员职员编码
        /// </summary>
        public string FPurchaserId__FNumber { get; set; }
        /// <summary>
        /// 申请数量(基本单位)
        /// </summary>
        public decimal? FBaseReqQty { get; set; }
        /// <summary>
        /// 收料部门
        /// </summary>
        public long? FReceiveDeptId { get; set; }
        /// <summary>
        /// 收料部门名称
        /// </summary>
        public string FReceiveDeptId__FName { get; set; }
        /// <summary>
        /// 收料部门编码
        /// </summary>
        public string FReceiveDeptId__FNumber { get; set; }
        /// <summary>
        /// 需求部门
        /// </summary>
        public long? FRequireDeptId { get; set; }
        /// <summary>
        /// 需求部门名称
        /// </summary>
        public string FRequireDeptId__FName { get; set; }
        /// <summary>
        /// 需求部门编码
        /// </summary>
        public string FRequireDeptId__FNumber { get; set; }
        /// <summary>
        /// 费用项目
        /// </summary>
        public long? FChargeProjectID { get; set; }
        /// <summary>
        /// 费用项目名称
        /// </summary>
        public string FChargeProjectID__FName { get; set; }
        /// <summary>
        /// 费用项目编码
        /// </summary>
        public string FChargeProjectID__FNumber { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        public decimal? FEvaluatePrice { get; set; }
        /// <summary>
        /// 1688询价单ID
        /// </summary>
        public int? FQueryPriceId { get; set; }
        /// <summary>
        /// 供应商协同行发布状态(6.1废弃)
        /// </summary>
        public string? FRowPublishStatus { get; set; }
        /// <summary>
        /// 销售单位
        /// </summary>
        public long? FSalUnitID { get; set; }
        /// <summary>
        /// 销售单位名称
        /// </summary>
        public string FSalUnitID__FName { get; set; }
        /// <summary>
        /// 销售单位编码
        /// </summary>
        public string FSalUnitID__FNumber { get; set; }
        /// <summary>
        /// 销售数量
        /// </summary>
        public decimal? FSalQty { get; set; }
        /// <summary>
        /// 销售基本数量
        /// </summary>
        public decimal? FSalBaseQty { get; set; }
        /// <summary>
        /// 含税单价
        /// </summary>
        public decimal? FTAXPRICE { get; set; }
        /// <summary>
        /// 1688发布状态
        /// </summary>
        public string? FHASPUBLISH { get; set; }
        /// <summary>
        /// 库存单位
        /// </summary>
        public long FREQSTOCKUNITID { get; set; }
        /// <summary>
        /// 库存单位名称
        /// </summary>
        public string FREQSTOCKUNITID__FName { get; set; }
        /// <summary>
        /// 库存单位编码
        /// </summary>
        public string FREQSTOCKUNITID__FNumber { get; set; }
        /// <summary>
        /// 库存单位数量
        /// </summary>
        public decimal? FREQSTOCKQTY { get; set; }
        /// <summary>
        /// 库存基本数量
        /// </summary>
        public decimal? FREQSTOCKBASEQTY { get; set; }
        /// <summary>
        /// 订单库存关联数量(基本单位)
        /// </summary>
        public decimal? FORDERSTOCKJNBASEQTY { get; set; }
        /// <summary>
        /// 计价基本数量
        /// </summary>
        public decimal? FPRICEBASEQTY { get; set; }
        /// <summary>
        /// 携带的主业务单位
        /// </summary>
        public long? FSRCBIZUNITID { get; set; }
        /// <summary>
        /// 携带的主业务单位名称
        /// </summary>
        public string FSRCBIZUNITID__FName { get; set; }
        /// <summary>
        /// 携带的主业务单位编码
        /// </summary>
        public string FSRCBIZUNITID__FNumber { get; set; }
        /// <summary>
        /// 采购基本分子
        /// </summary>
        public decimal? FPURBASENUM { get; set; }
        /// <summary>
        /// 库存基本分母
        /// </summary>
        public decimal? FSTOCKBASEDEN { get; set; }
        /// <summary>
        /// VMI业务
        /// </summary>
        public string? FIsVmiBusiness { get; set; }
        /// <summary>
        /// 税率%
        /// </summary>
        public decimal? FTAXRATE { get; set; }
        /// <summary>
        /// 金额
        /// </summary>
        public decimal? FAmount { get; set; }
        /// <summary>
        /// 价目表
        /// </summary>
        public long? FPriceListEntry { get; set; }
        /// <summary>
        /// 价目表名称
        /// </summary>
        public string FPriceListEntry__FName { get; set; }
        /// <summary>
        /// 价目表编码
        /// </summary>
        public string FPriceListEntry__FNumber { get; set; }
        /// <summary>
        /// 关联询价数量
        /// </summary>
        public decimal? FInquiryJoinQty { get; set; }
        /// <summary>
        /// 关联询价数量（基本单位）
        /// </summary>
        public decimal? FInquiryJoinBaseQty { get; set; }
        /// <summary>
        /// 价格下限
        /// </summary>
        public decimal? FDownPrice { get; set; }
        /// <summary>
        /// 价格上限
        /// </summary>
        public decimal? FUpPrice { get; set; }
        /// <summary>
        /// 需求来源
        /// </summary>
        public string? FDEMANDTYPE { get; set; }
        /// <summary>
        /// 需求单据编号
        /// </summary>
        public string? FDEMANDBILLNO { get; set; }
        /// <summary>
        /// 需求单据行号
        /// </summary>
        public int? FDEMANDBILLENTRYSEQ { get; set; }
        /// <summary>
        /// 需求单据分录内码
        /// </summary>
        public int? FDEMANDBILLENTRYID { get; set; }
        /// <summary>
        /// 产品类型
        /// </summary>
        public string? FRowType { get; set; }
        /// <summary>
        /// 父项产品
        /// </summary>
        public long? FParentMaterialId { get; set; }
        /// <summary>
        /// 父项产品名称
        /// </summary>
        public string FParentMaterialId__FName { get; set; }
        /// <summary>
        /// 父项产品编码
        /// </summary>
        public string FParentMaterialId__FNumber { get; set; }
        /// <summary>
        /// 父项BOM版本
        /// </summary>
        public long? FParentBomId { get; set; }
        /// <summary>
        /// 父项BOM版本BOM简称
        /// </summary>
        public string FParentBomId__FName { get; set; }
        /// <summary>
        /// 父项BOM版本BOM版本
        /// </summary>
        public string FParentBomId__FNumber { get; set; }
        /// <summary>
        /// 父产品行主键
        /// </summary>
        public string? FParentRowId { get; set; }
        /// <summary>
        /// 申请单拆单前分录内码
        /// </summary>
        public int? FSrcReqSplitEntryId { get; set; }
        /// <summary>
        /// 申请单合并前分录内码
        /// </summary>
        public string? FSrcReqMergeEntryIds { get; set; }
        /// <summary>
        /// 价目表分录id
        /// </summary>
        public int? FPRILSTENTRYID { get; set; }
        /// <summary>
        /// 配套单据编号
        /// </summary>
        public string? FAssortBillNo { get; set; }
        /// <summary>
        /// 配套单据行号
        /// </summary>
        public string? FASSORTBILLSEQ { get; set; }
        /// <summary>
        /// 供应商物料编码
        /// </summary>
        public string? FSupMatId { get; set; }
        /// <summary>
        /// 供应商物料名称
        /// </summary>
        public string? FSupMatName { get; set; }
        #endregion
        #region 移动单据
        /// <summary>
        /// 扩展字段
        /// </summary>
        public string? FExtendField { get; set; }
        /// <summary>
        /// 订单名称
        /// </summary>
        public string? FOrderName { get; set; }
        /// <summary>
        /// 移动单据
        /// </summary>
        public string? FIsMobBill { get; set; }
        /// <summary>
        /// 是否挂单
        /// </summary>
        public string? FMobIsPending { get; set; }
        #endregion
    }
}