﻿using System.ComponentModel;
using System.Text.Json.Serialization;
namespace WebDemo.Models;

public enum KFDocumentStatus
{
    /// <summary>
    /// 创建(A)
    /// </summary>
    [Description("创建")]
    A = 65,
    /// <summary>
    /// 提交(B)
    /// </summary>
    [Description("提交")]
    B,
    /// <summary>
    /// 审核(C)
    /// </summary>
    [Description("审核")]
    C,
    /// <summary>
    /// 重新审核(D)
    /// </summary>
    [Description("重新审核")]
    D,
    /// <summary>
    /// 暂存(Z)
    /// </summary>
    [Description("暂存")]
    Z = 90,
}