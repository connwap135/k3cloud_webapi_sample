﻿using K3Cloud.WebApi.Core.IoC.Attributes;
using System.ComponentModel;

namespace WebDemo.Models.EntityTable
{
    /// <summary>
    /// 仓库信息
    /// </summary>
    [Description("仓库信息")]
    public class BD_STOCK
    {
        [Description("仓库内码ID")]
        [ignore]
        public int FStockId { get; set; }
        [Description("仓库编码")]
        public string FNumber { get; set; }
        [Description("仓库名称")]
        public string FName { get; set; }
        [Description("仓库属性")]
        [ignore]
        public int FStockProperty { get; set; }
        [Description("数据状态")]
        [ignore]
        public string FDocumentStatus { get; set; }
        [Description("禁用状态")]
        [ignore]
        public string FForbidStatus { get; set; }
    }

}
