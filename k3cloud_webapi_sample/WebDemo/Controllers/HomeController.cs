﻿using K3Cloud.WebApi.Core.IoC;
using K3Cloud.WebApi.Core.IoC.DataEntity;
using K3Cloud.WebApi.Core.IoC.DataEntity.PresetEntity;
using K3Cloud.WebApi.Core.IoC.Extensions;
using K3Cloud.WebApi.Core.IoC.K3Client;
using K3Cloud.WebApi.Core.IoC.Types;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SQLBuilder.Enums;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using WebDemo.Models;
using WebDemo.Models.EntityTable;

namespace WebDemo.Controllers
{
    public class HomeController(ILogger<HomeController> logger) : Controller
    {
        private readonly ILogger<HomeController> _logger = logger;

        public async Task<IActionResult> IndexAsync(PageArgs p)
        {
            if (Request.Method == "POST")
            {
                string jsonStr = Request.Form["jsonStr"];
                JsonDocument jsonDocument = JsonDocument.Parse(jsonStr);
                Expression<Func<STK_Inventory, bool>> u = ExpressionBuilder.ParseExpressionOf<STK_Inventory>(jsonDocument);
                Refasync<int> total = 0;
                List<STK_Inventory> res = await K3Scoped.Client.Queryable(u).WithCache(60).ToPageListAsync(p.page, p.limit, total);
                Dictionary<string, decimal> sumData = await K3Scoped.Client.Queryable(u).WithCache(60).SumAsync(t => t.FBaseQty);
                return Json(new
                {
                    code = 0,
                    count = total.Value,
                    totalRow = sumData,
                    data = res
                });
            }
            ViewBag.StockList = await K3Scoped.Client.Queryable<BD_STOCK>().Where(x => x.FForbidStatus.Equals("A")).OrderBy(x => x.FName).WithCache().ToListAsync();
            return View();
        }

        public IActionResult SOAsync()
        {
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> SOAsync([FromForm] int page, [FromForm] int limit, [FromForm] string jsonStr = "[]")
        {
            JsonDocument jsonDocument = JsonDocument.Parse(jsonStr);
            Expression<Func<SAL_SaleOrder, bool>> u = ExpressionBuilder.ParseExpressionOf<SAL_SaleOrder>(jsonDocument);
            u = u.AndFilter(t => t.FDocumentStatus == KFDocumentStatus.C);
            Refasync<int> total = 0;
            var res = await K3Scoped.Client.Queryable(u)
                .OrderBy(u => new { u.FId, u.FQty }, [OrderType.Descending, OrderType.Ascending])
                .WithCache()
                .Select(t => new { t.FBillNo, t.FCUSTID__FName, t.FMaterialId, t.FQty, t.FDocumentStatus,t.FPlanDate })
                .ToPageListAsync(page, limit, t => t.FQty, total);
            Dictionary<string, decimal> sumData = await K3Scoped.Client.Queryable(u).WithCache().SumAsync(t => new { t.FQty, t.FStockOutQty, t.FRemainOutQty });
            return Json(new
            {
                code = 0,
                totalRow = sumData,
                count = total.Value,
                data = res
            });
        }


        [HttpGet]
        public async Task<IActionResult> BomAsync() {
            DateTime startDate = true ? DateTime.Today.AddDays(-7) : DateTime.Parse("2019-01-01");
            DateTime endDate = DateTime.Today.AddDays(1);
            Expression<Func<ENG_BOM, bool>> expression = t => t.FCHILDITEMPROPERTY == 2 && t.FDocumentStatus == KFDocumentStatus.C && t.FForbidStatus == KForbidStatus.A && t.FModifyDate >= startDate && t.FModifyDate < endDate;
            int bomCount = await K3Scoped.Client.Queryable(expression).CountAsync(t => t.FTreeEntity_FENTRYID);
            return Ok(bomCount);
        }


        /// <summary>
        /// 切换账套配置查询
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> SO2Async([FromForm] int page, [FromForm] int limit, [FromForm] string jsonStr = "[]")
        {
            JsonDocument jsonDocument = JsonDocument.Parse(jsonStr);
            Expression<Func<SAL_SaleOrder, bool>> u = ExpressionBuilder.ParseExpressionOf<SAL_SaleOrder>(jsonDocument);
            Refasync<int> total = 0;
            List<SAL_SaleOrder> res = await K3Scoped.GetClient("config2").Queryable(u)
                .OrderBy(u => new { u.FId, u.FQty }, [OrderType.Descending, OrderType.Ascending])
                //.WithCache() //同样的参数去除缓存才能看到效果
                .ToPageListAsync(page, limit, t => t.FMaterialId, total);
            Dictionary<string, decimal> sumData = await K3Scoped.GetClient("config2").Queryable(u).SumAsync(t => new { t.FQty, t.FStockOutQty, t.FRemainOutQty });
            return Json(new
            {
                code = 0,
                totalRow = sumData,
                count = total.Value,
                data = res
            });
        }

        /// <summary>
        /// 附件查询
        /// https://localhost:5001/Home/Atta
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Atta()
        {
            List<BOS_Attachment> ret = await K3Scoped.Client.ExecuteBillQueryAsync<BOS_Attachment>();
            return Json(ret);
        }

        /// <summary>
        /// 上传或更新附件
        /// https://localhost:5001/Home/CreateUpdateAtta
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> CreateUpdateAtta()
        {
            /** 方法1 */
            _ = new K3SaveParam<BOS_AttachmentModel>()
            {
                // atta.NeedUpDateFields = new string[] { "FFileStorage", "FAttachment" };
                Model = new BOS_AttachmentModel
                {
                    // FId = 171863,
                    FEntryKey = " ",
                    FEntryInterID = -1,
                    FInterID = 100023,
                    FBillNo = "CGDD000014",
                    FBillType = "PUR_PurchaseOrder",
                    FAttachment = "/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCALpAREDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD3+iiigAooooAKKKKACiiigAooooAKKKKACiiigAooprusUbSOwVFBZmJ4AFADqK8Ouv2gxF4geKDR0l0pZNgl80iRhnG4dvwr2y1uI7u0huYjmOVA6n2IzQBLRRRQAUVl6x4j0bQLdptV1K3tVHaRxuP0Xqa8w1/4/aZbK8WhafLeS9BNP+7jH4dT+lAHsdZkniHR4tRh059TtftszbY4BIC7H6CvlzxB8TvFniJWiudTe3t26wWv7tT9SOT+ddN8C9COo+L5tXmBZLGI7Secu3H8s0AfSFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABXivxo+IbW0UvhbSnHmyLi9mVuUB/gHue9dL8XvGWp+EtAtv7LCpNeSGI3BGfLAGePevmdVutQuHdEnup3YsxVS7MT1JxTQFMivrX4W+IoPEPgWwZGH2i0jFvOmeQyjAP4jBr5qTwd4nli81PDeqsmM5Fq3+FaPhPWfEfgrXRJapPabx+/gu4WVHUeoOOfTFAH1rLLHDE8srqkaAszMcAAdSTXzp4/+MOqanqU1l4dvGtNNjJQTxDEk3qc9h9Kg8cfGC88U6EmlWtm9gr/APH2d+TJ6KPQeua84azl/ssXkfIMpjPouADz9c8fSgCKeaa5lMtxLJNIeryMWJ/E1HiphbXKWi3E0JSN22q3Zj7VHTEJivqX4QeHToXgiCWVcXF6fPf6Hp+lfO3hLRJPEPiiw01FJEsoL+yjk19iW8K29vHCgwkahVHsKl7ldCSiiigQUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABWdr2s2ugaHd6ndyLHFBGW+Y/ePYD3Jq/JIkMbSSOqRoMszHAA9TXzH8VvHh8W6z9isZidItDiPHSV+7n29KAON1zxFqXiDUZr7UbqWdnk3hHYlF9AB0AA4r6y8H29iPC+mXVpY2lsbi1jkcW8QQFioJ6e+a+OjGe1fRHwX8c2l/ocPhy8mEeoWgIhDnHmx9se49KbEet15j8YvGNnoehnSVggudRvUIVZFDCJOhc+/pXXeMfFdn4P8PzaldfM/wByCIdZH7D6etfJmr6pe69qtzqd87S3E7FnPYegHoBQhmbKxVMjk+9SadrOp6TLJPYOgSTAlilUMpI9QeKQjIwaqvbhmz1+ozQ1cR0Oqare6n4fs7i+dDI9w4QIAqqoAHAHArIFdnpF5YaHZ6E15Bb3CurGSGWINtVnPzg9j0qHx5oUOl+JlTT4wILxFlhjXoC3YfjQtg6nffATQC9ze67KnyqPJhJH5mvd65vwHoi6B4PsLIABxGHkOOrHrXSVKKYUUUUxBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUVwPxT8c2/hTw9Lawy51W8jKQIvVAeC59AKAOL+M/jmyvtOj0XRtWWR1uCl7FFnkAdN3QjPUCvOdD+HXivXwHs9ImjhPSa5HlIfoT1/CuT3kOHJJYNuJ9TnNfaHhu+XUvDOm3atuEtshJznnAzT2A8Jh+AXiaRAZdV0yFsfdw7Y/HFUtS+EXiDw1DJql1PbSWtohleW1lIYY6cHB/I19L188/GL4g/wBsXreHtLnBsLd/9IkQ/wCtkHbPoP50Aecaz4j1fXzB/amoTXYt12RBz90f4+9SXGnXVxoFhJZJNJGS5nMC7ir5wAwHPTH51iTAtGQP0qCG+u7J2ktL64t5G+/5RIB/KhgdDf6HPpWi2s94rrPPK2AwwQgAxkdjWfaWzXV3DborM0rhQEGScntV+6kmm8Haa8szyySXczM75ycBa1/AekjUNeWYyyqbMCYLCAWbBHr29fahbAzp9b8I6JcPBZSahJpl5DAkcEl2P3Mygdj6+tWNJs7fxR8S9Ks4JRd2ek26LJOgyrle/wBM1vXGpadfWOo+dJFq9jbsxubN0CzQD1T1A/yat/A/Q4odLvtZEe0XMxWEHqEFEthLc9bACgADAHQUtFFIYUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFZuv61a+HdDu9VvCfJt0LEDqx7Ae5oAo+L/F2neDtFfUL5izH5YYV+9K/oP8a+UvEOv3vibXLjVb9szTHhR0Reyj2Fd1q3juy+J2qafpGsac+nq1xtt7q2k3tGW4+ZTww+ld7o3wG8PWU3mand3WpYPEZ/dJ+O3k/nTEfOhVT3Ar0/4X/E6fwwyaLqMU11psj/ujGMvAT1wO6+3avaR8N/Bgh8r/AIRrTtuO8IJ/PrXD+PfCHgnwZok+rW9rLZ30iNDbJBKTvYj+62QB60DD4j/FrT18P/YfDd6s93dgrJKmR5Cd/wDgR6V4HMjQwxzykKkmdpY9cdTUbN1JwMmti3v9DvtMj07UpfsV1bPmK6MJkVkPOMDpzRsBleWxt0mGDG/Qg9Pr6VEtobiZY44y8jnCqByT6V10o0ebwXfHTGedra5i8y5dNm8kEcDsB/Wubt55LWeO4gcpLGwZGHYihO4M1tTtZbLQNItJo2ikUzOyMMEEtjp+Fdh4F8LwvJb6tba7su4l3yWscfzBTxg56g1ak1bQtfsdPtvFKGG8uYBJHfRjGw5IwfQcZ9K6jVPDkjto2rWNxE01lGUkmRgiyx7Ttz2xnH501awrmT8Q0vbOx3ad9iVbt1tZjHCBP83IXd3Br1/wtoyaB4asNOT/AJYxKG9zjmvJ9y+LfiJ4ftIrmOf7JH594kDbokcdwehOcflXuA4FJ7gtgooopDCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAr50+NHjeXV9Wbw9YzA6faN++KH/WSjtn0H867b4vePrnw4sOiWIeKa8hLSXKH5o1zj5fevn/AOxTzPm2V7sMesSlmyfUdc00hFW1ne0u4blM74ZFcY9jmvsrw3rlp4j0C01OzlWSOaMFsHlWxyp9wa+TB4U8RMm8aBqRU9/szf4Vv+C9d8T+CdTma3t5kt9u+4tLmMqsn0zzu9xQwPp/UtRtdJ06e/vZlitoELu7dhXyf448YXXjLxBJfSlktUyltCTwif4nvWz4/wDijeeNLa3sordrGxT5pYd+4yP2yfQelcZfxQafNFbskkszBWcg4HIzx+B60DKTKGUqe9VZIm6BQyjsecVpz23lpHMgYQyltm7rx6/nUO2mI3NFQjwHrinj/SIOw/2qxSPlxXceCJNNtdC1CfVovNsftUKyLjPZsHHoDW7qPw103WLZr7wtqMTqefIZ9y/QHqPxpRBnPpp9vfeJNDsr0SfZVsoTMUB+VcZOfQc9a9N03T59K1cafZ6c8ekyRlt3n+bAx7YB5Unv2rG8PadND4t1CSNhFfRWccVpHPkRzLtAb6gEDpV3VNX/ALK8O315qGnXdjcBWgNvHITEzMOHQ9h9Ka2B7l34R2MU+t+IdaS0itleb7PHHH91AOTj6/0r1muR+Gek/wBkeAtNib/WzJ58meu5znFddUjCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAyNb8MaL4kiWPV9Ohugn3S+Qw+hHNP0fw3o3h+Ly9K063tRjBKJ8x+p6mtSigAryb42eKrOw0ZdCSGGe/uhuJdQTAn94ehPavUb+4e10+4uIo/MkjjZlTONxA4FfIviU6vc67dXutQyx3c7lm8wcewB9AKFa4WdjAfC4J6Vrr4ls5rGG11bSXuWt+Ip7dwjMo6AnB/Ss1kDDBqsYGViUbbnrgU2hI668uxr3g9r1bVLZrG6CCJP4ImGB9Tnv71zddH4Nh+0aR4gsCfv2wmAx1Knj9SK50KWYKoJYnAAoj2GzpbBWX4e6o2Dh7yIA+uAazNHu7+yv4JLGeaBnkVN0bYzk9D61t5A+H95EOVS9ROD1IU5/U1keHyraxbW7DCSSpgj+Eg8H+n40dxHtv2y+hikfVNPgudNibKzW0mZoF/vMvUfga5fxnHdXVtYWqas1/p+o3cQticEqpOCCR1696ztQ8c6HevPbarokkjxsYxLbTlC4BwM1s+DTB4h8Q+GY7axW2sbWSe4SAMWwqgAEk9TuND2Etz3K1hFvaQwgACNAuB04FS0UUigooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigBsiLLGyOMqwwRXMah4PjukaMNHNC3/LK4QMBXU0VMoKW5Sk1seF+JvhLEwaWxjaymH8P3oW/qK8n1TR73R7o299btE46E9GHqD3r7LIBGCMg1zfiPwTpXiKwe3mhVSclSB90+o9KFzR8wbTPmvwNJs8UxwE4S5ikhb3ypx+uKy44mtpbiVhgwMUB/284H9T+Fbt1oV34O8eWlrdg4SdTHJjh1J61T8WIlpr13bKcKJmkOeOX5/lirW5L2LEXHw8uP9q/H/oNZ/htd3iOw9pgfy5q+px8PW99QP/oAql4a416Bv7qyN+SE0dGHUpalh7hrlQAJWOQOm4Hn/H8a9m+DVqrajHIc5t9OGMdMvIxP9K8ZicTxXFvkHOXX2Yf/AFs/pXvnwajAh1NscolvH+Uef602JHqdFFFSMKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigDy742aKlx4dttZjUefYzpk46qxxj88V4x44i87xG8shRfMiR8H6V9C/FUA/DnVARn/V/+hrXhvj3RZ1nttU27rd4liJA+6wHf60uo7aGYUkHgJFyCp1Fvm/4AKqaFHKl9JImG2W8pOO3yEVdcbfh5APXUH/8AQBVbw3kXN7/15S/yovoFjGhVoJVkiK7w24DHevob4NSyMviBJI9hW7Xp/u4x+lfO44mU+4r6L+EZxd+JV/6e1b9Kdwsen0UUUCCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA4v4rEj4d6hg4y8I/wDIi1hNpdvqmkSWN0m6KQYPqPce9bPxaI/4QOdSeXuIAP8AvsVVsOYz7NSXxA9jyPxVoUvh/wALwafI4fF67qw/iUrxWF4cX/SLz/ryl/lXuGqaPZ62k9nexB4yAQe6n1B9a83l8HXnh29v5G/e2TWcqxzD19D6GhrQqLPO9oLD619BfCR/+J14kj9ZEb+dfP8AtwQfevePhM+PFevx56qjY/Gl1H0Z67RRRVEBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAcF8WyT4RhjClvMvYRx9c/0rlJvGtlpF7PZz2127owO6OPKnKjvXW/FO+nsdF0027hHl1COMkgHghs4zXjXiLxXrVjrt3b2l88cMbAKoUcfKPapvqVa6O+bxvYQ2cepPbXnlXDFFURfMCPUVEnjjTNU8yFLW7xGhlcSQ4BUdR7muNu/E+sxeFdMvEvnW4nkkEjgD5gDx2qHQvFmt3l1dJPfNII7V5Eyo4YdD0o5nYXKReIB4Wv/ADJrCK9s5zztEBKE/Tt+Fd98KN3/AAneuAggeQuM/UV5o/jnxCpIN/n6xr/hXq3wv1K41HxfqyXBRhb26CMhADg8kE9+lPdj20PW6KKKZIUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAHnHxe5sdATHXU0P/AI6f8a8L8VE/8JHqH/XUivcPi02ZfDUeeuoZx9BXhPiRt/iDUj/08P8AzqHuWtjT1AY8GaID3eY/rVTw0Ntxfn0spf6Vb1T/AJFHQB2JmP6iqfhw/vtQ/wCvOX+VIfQxpeWzXtvwjwPGGsH+/bRGvEX5P4V7Z8J/+Rxv/e0j/lT6oH1PaKKKKszCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA81+KcLTan4ZCAsVu2YgDoABk14xrHh7WrjV7+WLS7t0kuJGVhESCCxwRXs/wASp5F8R+G4UkZQ8j71B6jjrXmniCw1OPRhrVpeXPltNKJlEh+T5yAR7VO7KWhV1LRNUl8N6LBHp9y8sQl8xFjJKZIxkdqpaFoOrW81552m3Ue+0kRd0ZG5j0A96n1m/vo/DmiOl3MrushZg5BbkdaraBqd/LNfb7yd9lnIy5kJweOanoPUzW8N62BzpN70/wCeLV658MYJ7bxpOJ4ZIg9ku0upG4gDOK8g/tzVcD/iY3X/AH9Neu/DK+ubjxnLDNPJJGtmjKrNnBIGTT6oNbM9moooqyAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAPLviQ27xx4Zjx0EjUnhqCO58MxxyorxyNKGVhkEF2qP4hc/Efw6CeBbzNj8Kn8HEN4Xsz6lz/4+aI7hLY4Px3oTWNrZJaQubK2LoW67MkEA1y3h1Ns2pD/pxk/pXvIijnFzHKivGxwysMgjFcPqXgdNPmvrzTCSs9s8QtupDHpg+lJx6opSVrM8fb+H6V7F8K3H/CdOPWwT/wBBFeR3VvLbSmGaN45V4ZHGCK9Y+Fg/4r7/ALh6f+gil1K6HutFFFUZhRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAeXeOo4ZPiHpckkwR4bKXandsg5P4CuQ07xTZaVZR2UHiAeXFkANZEnk565rofHzH/AIWXanPEelyH8814YG+br1apvqVa57Vc+LbezhtpH1RI/tCbwxtS2/nrjPFMt/F1veSSKuqRyeVGZTi1K7QO/Xn6V554kJWz0IelmP8A0I0zQf8AWaj/ANeMv9KnmY+RWudVrOtaBrcBW81G0ZiMLL9jcOv0Oa6T4dWNlF4zjmtdQWeT7CA0RQqQoAw3vmvDzzGo717F8K/+R8H/AGCl/wDZKq+oWsj3GiiiqICiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA8d8eD/i5EzZ+5o5P5sRXhyclfrXuPjdJZfiDqpSNmCaOiAgdSWPFeNromrcf8S274/6ZGofU0RreK22waIv/Tip/wDHmpnh1tzagf8Apxk/pVzxNpOoTx6R5NlcSbLFFbbGTg7m4PvUfh3StRgkv/OsbmPdZSBd0ZGTxwPelbRDvucwi4Y59K9e+FT/APFexjudKH/steY/2Pqm4n+zrvGP+eTV6f8ADO1uLfx/aSS28satpmzcykDIC8fWn1E9j3OiiirMwooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAPH/Fd5c/8Jzr0KzMI49PRlA7Nnr9a5LxVZeItCZbq31O9k0+RQd3mnMZ9D/jXSeJGB8d+KW/u2can/vkGu0+zQ3Nl5E0ayROm1kYZBGKSV7jbtY8W8S67qtoNMFvf3Me+xR32yEZYk5J96i8Pa9rF0195+o3Mgjs5HTdIThhjBHvXQ+OPB17Psv8ATYfMt7ePyTCnLKq55HqOa5XwxHtGpAggixkBB6jpUNWRasymPFGvYwdWu+n/AD1NekfDrWNQufHOn2093LJDJp/mFGbILY6/WvIW/wBZ+Fen/Db/AJKBox9dNP8AKmtwklY9/oooqzMKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigDxDxA+fF/jOQ8bI41/8hiu4t7238lP38X3R/GK43xFq3k6n4tdbW23W0ijcY8+YSo+/nr6V5+fGd3uI+x6fwOMWq1KlYpxue529xCsbbpEBLkjLCsTVNA0u8a4mgEUF1cRtC8sZHQ9yO9cB4l8T3Wn3NskdtZuGto3PmQAnJGePaq+geKrq+a+L2lkvlWjyDZABkjHB9qObQXK0yhrngfVdJdpEVLu3z/rIDkj6jqK6X4dts+IHh8Y+9p7D+f8AhXLN4zu84OnaYf8At2Fdx4E177R4y0K2bTrFTcWrSeakW10xu4U+ntSVr6FO9tT3OiiirICiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA+fvEZHmeO3HP+koP1Ary8cy59q9K8QsPsvjdgfvXyjP/AxXmsePMHPbFZs1R0/jUD7bbj0tIf8A0AVT8Jf6zUx/04yf0qx42+XVEGelvEMf8AFVvB339UJI4sJf6UlsDMh8lx9a9D+H5x4/8Ke9nIP/AEOvO3kBfg969B8Ctjx94POfvW0o/wDRlOO4SPo6iiitDIKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiijtQB4jcXn2yTWo47OzVxqIgw0W5XJf7zDua5bWNTvtD1E2l7oelqxPyOLVdrj1BroNPObvUS3Pma4oz6/NXfapolhrVoba+gEkfUHup9Qe1So3KbszyfxV4jbT9V8kaZp82IkO6aAMfujvUOgeJGvHv86Vp0XlWbyfu4AN2McH1FWvHvhLVHum1S1gM9qVAPl8sgAxyPwrA8MIVbVFI5GnyZ/Spd0i0kxp8WNjB0XS+Tj/AI9xXceDdfV/Ffhq3Ol2Ia6ifEqx4aLBf7vp0/U15TIDuU+9d54OyPGvgo/7Mo/8eeiL1CSSR9IUUUVoZBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUjfcb6UtNk/1bfQ0AeCWbm3haeVWSM65uJKnhQc5+legp4i0k9L6L9R/SvMtc17U4vDz3C3sgmGovCHGPuKOB9K5O38Wa3JOiNqExBPqPX6VCdkU43PeRq+n26CKa7jRx1VjWPe2HhzUGuJYXt47iaJo5ZYSAdh6kjp6c1554t8SapY+IrmCC9kjiTGFXGBwPameGvEmq3a6iZ72VzHZu6ZA4IIwelPm0Fyu43V/A5t2L2GqWd3GDkBnCN/hWl4Vt54/Fng9jC/7t5FcgZC/M3eubPi7Xt2BqUxX6D/Cu28LeI9VGseG4zeOUu5WScED5xuPt9KlWuW+a2p7xRRRWhmFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABTJjiCQ+in+VPqG6/485ucfu25/CgD5o8RN/xSS9t+pTGuRsl/fxn/bH869A1vQZZfD1paC+sdy3MshkMw2Nknoe9c/a+FZopos6jprYcE4uBzzWfQ1vqR+OufFN57MP5Cm+Eeuq/9eEn8xW54q8PveeIrub+0NOiVm+5LOFYcdxTPDvh1rQ6gP7Q0+TzLN0Hl3AO3OOT6D3o6Cucnt4zjvXaeHGK6r4RYDP+lsP/AB7/AOvWT/wjMoAH9p6Uf+3oV1fh3w/M994eK3tgPst20jf6QPnGRwvqaSWpUpKx73RRRWpiFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABVe/wAf2dc56eU38jViqWsP5ei3z5xtgc5/4CaAPGfCdtHdWehRzxrJG8c7MjDIOSaj1r4bvFfR3ukMWhEgZ7djyoz/AAnuParXhBkji8PBmABtZjyfc133nwmNgJEzj+8KUUmhttPQ+fvG4P8AwlV+eeX4/Kk8HqwfVWPewkx+Yr2vVNB0bWrNI9QtonbbgSA7XX8etctH4Bi0v7W+nXolE9u8Kxy4ypOO47cUnF2KUlc8sKDNdZonE3hc+mpEfqtZd/4W1qwP7yyZlX+KIhx+laukgpF4fYgqyanyCOhylZJNPU1bTWh9K0UUVuc4UUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFZ2vz/ZfD2oT7Fk8u3dtjdGwDwa0aw/GT+X4L1ps4xZy8/8BNAHiWua+LbR9IuBp1mfOjZhGUO2PnovPArP07xO1xqVtCNOsl3SKNyq2Rz9aq+K/l0bQ09Lf/CsnQ8rrlkB/FOn86z6GiR1Gt+KXt9durcafZyBHxucNk8d+am0XxI1yL4mwtE8q0d8IrfNjHB56VyniIk+Kb8/9NDVvwySRq2f+fGT+YoYJKxaHjAnJOl2P5N/8VXSaR4hiEOmzNpNixlvjGBtPyEhfmHPXn9K82KDGM811OmHbp2lE9tTH8kpXK5UfTg6ClpF+6PpS1qYhRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAVieMEhl8IapFcXAt4ZLdkeYjIQHjOK265T4kuU+HusEHGYMfqKAPHvE2m2E0GmxTavHCkcICMYmO8YHPHSs7SNG0xNasnTXopXWdCsfksCxz06VF4xkKtpK5/5c0/kKytAYt4isCTz9pj/AJ1n0NDd1nR9Il1u8ll16GGQyEtGYWJU+nSrGhaXpcD6h5OuRTb7Rw+IWGwcfNXMa8x/4SPUf+uzfzq74YXM2qD1sJP5ihgloTf2RoobP/CRwZH/AEwf/Ct/TdL0ttPtVbXYRHHe71k8psM21fl+vA/OvOpTsmPHbmut0b/kFWGR/wAxRD+i0mUkz6dT/Vr9BTqbH/q1+gp1amIUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFcx8QrOfUPBN/aWyq00oVVDMFH3h3PFdPXEfFhynw/u8HG6WIf8AjwoA8k8V6BqV/c6eLaFGEVoitmVV5GR3NUdE8L6tba9YzzW8YiSZWYidDgA+gNd1aaNY6xdvBewLKq2UQGeq8t0PaqR+Hsmmava31jdB7aKUO0cvDAZ7HvUWdrlX6HIaz4X1e41q+mit0KPMxU+eg4z9at6B4e1S0lvjNAq+ZaPGuJkOScehrH8RWd5b67evPBLEsk7MhZSAwJPQ1N4bY+ZqQz0sJP5ikylexDJ4Q1p23fZo8/8AXdP8a6LSfDeqvYW1stuvnJfJJjzkxtAGTnPtXBSyMAfmPJ9a6PSJWOhIQTkX0fehjV7n1HASbePPXaM/lUlQWZ3WUDesa/yqetDIKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAoooJAGTwBQAVxnjrxfqfhFrSe30+1urSYlSZJijhhzgcY6d6TXfiBDZTPaaTam/uV4LA4jU/Xv+FeW+LJfGXieNlvZALUHckEceFQ+vqadhXO10f446BfMsd9aXVkx4LAeao/Ln9K9LtbqG9tY7m3cPDKoZGA6ivnT4ZPa+FfE5k1nTkkWRdiXbHJgPf5fQ+tfR6Mjxq0ZBQjKkdCKQx1FFFABRRRQAVxHxUtLm+8HG2tIJJpWnjbagycA5J/Ku3rhfixLJD4PV4pnik+1RgFGwSCcEflQCOWtL59L1aVpLO7lRreNQ0MJYZBbIP51rDxDHcjyPsN+hfjdJbkAfU15Z4xvbiLxAyJPIqrGgwrkDpmqfh2/upPElmjXMrIX5UuSDU3diuXqeu3Wt2kqvbz6ZeTKCVObUsp+lYg03QZGuWttMvbSSSFkciBlyp6gA8ZrzLVNUvY9avlW6nA858ASHA5Nanhy+upW1AyXMzYs3I3SE4PFDYKJYvvBcLEm0l1EjsJbFv5in6doGpxac1qllcSOLyNlAiYEjucGuWbVb8zHbe3GPTzWre07Ur5dKmkW7nEi3MWGEhyOvekyle59N2H/IPt89fLX+VWKqaWSdKtCeSYVz+VW6szCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAK5bxBd3GpSvpdo5igXi5mB5P+wP61v6jO8FmxjOJW+VPqa5t0WBBAhJC8sx6s3cmmhMpWunWllGEgiUAd8cmpyikfdFLS1RBheIPDtvfadLcRRhbiIZ4/iFdD8OtRe+8LrDK2ZLVzEfp1H6URDcXTsyEVnfDgNFda1B/AJlI+uKTKW539FFFSUFFFFABXB/FWxu9Q8O20VpA0zJcrI4Xso6mu8rzf4wSOmk6WqOV3Xgzg4zxSew1ueWeKdF1O88QzzW1jNLFhAGQZB+UVW8P6Dqttr1rNPp88caMSzsvAqt4tuZB4qvFDuBuUABj/AHRTfDM0p8S2qGVypJyCx9DUldB2oeGtam1O6lj0y5ZGlYghevJrQ8P6Fq1q18LjT5499q6ruX7xOOBXNX93cf2pcjz5Mea3G8+tbfhuaUrqDNI5xZyHlj7USBXM/wD4RbXd2f7KuRz/AHa29P8AD2sNp1xbrp0/nPPEypt5IGc1yhurjP8ArpP++zW3p13L/Yd2wlcOJI+dxz/FQwR9S2Ufk2FvFgjZGq4PsKnqvYNv0+2b1iU/pVirICiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAydSf/SCx+7CmQP8AaNYecnPer97N5kLSA582QkfQcCs6qRD3FooopiJIDidPc4rP8Fs0Pi7WbfHysA59uauqcOp9DWVpkgsviaUY7VuYmA9z1FIaPR6KKKksKKKKACvPvinp8moWmliOWFPJuN7CVwu4eg9T7V6DXmXxbO6fw7H63mf0pPYa3PMde8O3N74gu7iO5slV34WScKw4HaneH/Dd3aa9bzyT2bKpORHcKx6eldxYaVp2qWt8l7axTZu3wWXkcDoeoqvF4E0/TtSi1KynlTy85ib5gcjHXrSs7BzdDz648J3kt7LKLqwAZycG6XPWtXRNBurX7YJJ7NvMtXQbLhWwTjr6D3qnqfgTXLe4eZbdbhGJOYmBP5VHo9ldWY1Bbi2liP2OQfOhHpSZSehW/wCETvM/8fmnf+BS1qaZ4ZuvsF1b/a7DdJJHhvtC7RjPU/jXGdOOK1tP/wCQRqH+/H/WmxI+rdNBXTLZSQSIlGR06VarO0E50CwP/TBP5Vo1RIUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFI5IRiOuOKWg8jFAHLXY229uo4wp4qpVu6DfZbfd94AqfrmqdUiGLS0lJTELXOeKpG0/WNK1ZOPLdST9Dg10VZ3iCx/tLQpYwMyRfOv070gPQ4pFmhSVTlXUMPoafXAfDjxQLy0/sS9lAvLYYi3dZEH9RXf1JoFFFFABXnPxMOntqegR3TTi4M5NuIwNpORnd7V6NXlfxSJPi3wmoHHmuf1Wk9hrc4efxDa2F/dQrqGoxHzmLLHGhUN3xmtHRvE0V7qcNquo30u7PySxoFPHcivPtYbOt3h9Zm/nWj4R/5Gm1+h/kakdjoH8Zw+YwOragNpxjyEq3Z+KIrszIdRu5AkLSEPCgwB3Hqa82bmV/qa1tDbNzfKO1lIP5U22FlY2LjWtFuc+fc3D+v+hxZ/lRZjw7JbXhM96sBZPM2xKCOT0FcaQa1LDP9k6hjP8AB/OhoSPqnQjGdBsTCWMfkrsLdSMcZrQrJ8LnPhfTD626fyrWqhBRRRQAUUUUAFFVNT1CDStNnvrltsMKFm9/avDtZ8d+JNZu2MF3LZ25PyQ25xx7nqTTSE2e+UV4Pp3iDxnaFZEvLuVB/DKN4P516X4S8Y/22TZ38Qt79RkDoJB7e/tRYLo62iiikMKKKKACiiigAooooAwdTi2rKB/DJu/Pmsiug1CMmd1P3ZY+PqK5/vVIh7hRRRTEJSq21s9exHqKSkoA4PxNpFxpGopqunMyANvR16oa77wf8QbPX0SzvmS21IDG0nCy+6+/tUUsaTRPFIoaNhgqa8/1/wAHywyNcWGWTOcDqtJoaZ71RXz7p3j3xToA+zm4M8S8CO6Tdj6HrWv/AMLi1wpgadZbv73zf40rFXPa68r8c+Io5vGWk6ZZT28yDP2nChihB4Abt+Fcxe+MvFniNDbmfyIG+8lsmzPsW61Q07RDbeIYZZbuFJI1z9nYne2QTkflUy0Q09TSGmalewefZy2auzN8ktqpHDEdetM0608Q2mrxfbrK1FtzmWGJR245HNR2+vSWBMC32l7UZgA8jhupPPFaVl4ie+ukga409gecRSsW49iKWlh6nAyeI9TidlktLRCOzWig/wAquaN4gu7uW58yG0Gy3dxsgUcj19q6WfxHFOMTHRpB/tyk/wDstVYH0y4efy7fSkYwtua3lbOO+eOBQ7Ajlj4svwOIbH/wFT/Cr1j4ovxaXdx5dpviClf9HXHXuMc02XSdKkJ2XNhH9Lpz/wCy0610W2NveRJqtiqSIoLb2IT5hyeKNAPonwxcPd+GdOuJNu+WBXbaMDJ9BWtWR4XiEHhfTYVlSZUgVRIn3XGOo9q16oQUUUUAFFFFAHn3xYvGj0Wzs1JAnny3uF5/nWP4Y0K3XShfzxhpHbCAjp71V+JGoDUfF0NhE25bVArY/vtyf0rrIovs2m2dsONseT9TTJYIioMKAKkFvbXEsbSII5kbdFMowVNR07NUSdZZ3DTxFZBtlThx2+o9jVmsTTrg/u5M9D5cme47GtuoLQUUUUDCiiigAooooAq34/0cSY5jYNXN3kYjuXA6E5Fda6h0ZGGVYYIrmb+Hag5yYmMZP8qaJkUKSlpDVEhRSUZoAKQgHrS0lAFO40yzuf8AWwI2fUVSPhrSw277MoP0rZphosBSjs7e2XbFEq+4FcRen/ivkwPuWz/+gmu8kPWuMn1SceKptOHleR5Ty5MY37tn97rj2qJ7FwPNb5s39wc9ZG/nWn4RP/FQKR/zzb+VdfJpmpXEKz2stiWYZ2S2i/zxTNNttdtr4/bLG0WHY2JIYl6445HNSyrnmznqPetrw9jzbwZ/5dZKsT67qts5Wewto2z0ezUf0q3o+vXNxNcB4LRdkDuNluo5Hr7UMZzHQ9a0LIf8S7Uef+WQ/wDQhVn/AISa9OcQWPH/AE6p/hVyx8Q3r215KUtQ0cQZcW6AfeHUY5pDPoTwb/yJejf9ecf/AKCK3KxvCd3JfeE9LupQoklt1dtq4GSOw7Vs1ZAUUUUAFUNZ1SDRdJuL+5YLHChP1PYfnV2SRIo2kkdURRlmY4AFeIeO/GB8TagtjYtnToG4Yf8ALVvX6elAMq+GoJtd8TNdz8tJIZXJr0ieQSTsw+6OB9BWB4S03+y9KM7riacYX2FbIqkQOopM0UxGhpj/AL10PIIzj6V1AORmuR044vF9wa6uE5hT/dFSy0PooopDCiiigAooooAKytStx5pI6TjB/wB4dK1ar3sBntWVeHX5k+ooA5FgQSO4ptWbxf3gkAwJBn8e9VaszCiikoAXNJmikoAU0xqWmMaAIZO9cTKumt4puJBNP/aARg8eweXsxwQfXrXaOa4g6ZdL4ludQITyJVKIQ4zkHHI6jrUTLgZcXiS1txsXV7wBeAPsinH61p6X4hhvLhol1K4lxGzbXtlUDA65BrjW8N6o5JWGM5/6bJ/jWp4e0HUbO9mkmhUKbeRQRIp5I46Gp6FF6TxVbSHD6tcN7GyUj+dPsNV0+6lmCXG8iFi2bNV479Dz9K5hvDWrb/8Aj3X/AL+r/jWnouhahbS3BlhA327ouJFPJ/Gkx2Qst5oDcNIfwsVH9aLQaJLHe7Li5WMwjzNtuFwNw5Az1rObwxqpOTDGP+2yf41d07QNQjivY2jjBkg2r++XruHvQGh614R8eaPY2FtpN2721tEgS0uZVwJUHTd6H9K9Egure6iWS3njljYZDRsGB/KvI7bw7Be+GbfTb4JHeW6kLIjBhn0yOorkbrwxqumyMIJJUXPWNyufyrRaohuzPouSeKFS0sqIB1LMBXNav8Q/DekI27UEuZV6RW3zkn6jj9a8Il0nU5TiYzSf77lv51ZtPC17MR8m0fSiwXNfxP4+1TxSTbIDaafn/UIcl/8AePf6VY8K+HDLIt1cqVhXkDHWruj+D47dlkuDvI7V1yRrGgRQAo6AdqdibkhOccYAGAPQUU2lpiFopKWgC1p//H0p9AT+ldZCMQRjGPlFc1pcYMrMenCj3zXUVLKQUUUUigooooAKKKKACiiigDntStijSpjgHzE+h6j86xzXUasm2OO4xkIdr/7p4rmp4zFMyHseKpESI6SiiqEFJQaSgANRtTjTG6UAQua89iilHi3UZmicRlflYqcN8w6GvQJDiuDj1G8Ot6ratdO1vAheOPPCHIORWcy4jmXw1c2sSz26B9gywiYHOPUCjT9K0KOeaSxuZldoHVlYEgKRyeRXGnxJrCnH9pT/APfda/h/XdTuZrsTXssipayOoZuhA4NSyrFefw5aeYTBqyMPR4mFWNI0eS3muCLiCQNbuo2k9/qKyP8AhJtZ34/tKf8A76rW0bW9TuZ7hZb6Vwtu7LluhHekxoyH0e77CM/Rqs6ZZzxC/jaJiWtmxtUnPIpreI9Ywf8AiZT/APfVXdJ1zVJ3ujJfzsUt3Zcv0IxzTDU9D8MJJD4dsY5UZJFTBVhgjmt5TuXB5Hoa57wzeS32g2txPK0szgl3Y5JOe9b8fStI7Gctx/kxH+AflT1jReiigUtMkWjNJRQAtLSUUwHUtNqWCPzJVXt1P0pAbujW/wAis3b5sY79q2aq2MLRQbn4Z+ceg7CrVSy0FFFFIYUUUUAFFFFABRRRQBHcRedbyRH+NSK5O5/eW8Up+8Pkf2IrsK5y/hCXV5Eo4YCYD3PWmhMx6SikqyApKWkNACGmMKfTTQBWkUkHArhG1GZtS1iB4oAttCdpEYDHv8x716Fisu80+0MoYW0W+ZisjbRlxjofWoktBxep5GfEEqtg2WnnHrbCtXQ9Xe4e9zaWSbLSRx5cIGeOh9q9EPhvRzydNtf+/Ypg0TTbc/ubKBN/yNtTGVPUVLjoVzHkv9uylv8Ajx0//wABxWvomrSTXE4NrZrtgdspCBnHb6V6KvhrRuv9m23/AH7FB0XTbcZhsYEJIUlUAyD1FDiCmeUtr046WWn/APgOK0NJ1ieaa4Bt7Ndtu7DbAozgd/avS10LSx/zDrX/AL9CnppGnxMDHY26k8HEY5B7Uco+dFDwxcS3+iQXcqRq0mSRGgVRz2ArfQYpsMMcEYjiRUQdFUYAqUVolZGbYop1NpaYC0UUUAFLTaWgBa2dJsjKctwDy309KzLWEzSgYJA7DvXX2kAgt1XGGPLfWpbGkT0UUVJYUUUUAFFFFABRRRQAUUUUAFZGppjULd+zxuhH61r1jalJu1e2i7JE7nn8KBM5xxhyPem06Q5kY+9MrQgXNITRTSaAFpDSZppNADqqXPM0A/2z/I1Y3VWnb9/B/vH+VTLYFuWe1QS9U/3hUu7ioXb5k/3qb2BFgdKhl+6v+8P508HiopDwP94fzoewIsikPb60gakY8fiKHsCJhS0zNLmmA+im0UAOzRSUtABRSVNap5l1Gp7tQB0GjWZRVkIxjk+5raqG1XbAvGM84qaoZaCiiikMKKKKACiiigAooooAKKKKACuXa7W5vdRvV/1cY+zxn1I6n860PEWqnTrERQjdd3J8uFfc9T9BWLPGtjYQWKnLKNzn1NNEtlImkoNJVkhmmk0E1E7YFABJMqKSSAB1zWHceK9JhYq1/Bkej1i+MPFNnZWV1Yxs0t26FNqDhSfU/wBK81FxKEUb8YA4wKASPWj4z0fvqEA/4FTk8R6fcwyXUd3E0Vv99w3C59a8iNxL/f8A0Fb+lyv/AMIjrTkjO+IDgepqWykjuR4z0bH/ACEYP++6ltvEunXgmaG7icQIZHIb7o9a8e89+ny/98it7w5KfsuskheLJv4R/eFDegJHfjxlo/8A0Ebf/vup7bxJpt9J5cN7C7D5iFccAd68b845+7H/AN8Ctjw7KWvbgFUx9lk6KB2ob0BI9oinWRQykEEZBB608tlRj1FcF4M8U2NzZWunSO0N1GgQLIMB8ehruFbgfUU+grWZbBpRUYNOBpiH5paZmloAdRSUUALVrT/+P2L61UqSGUwzpIP4WBoYHbWUwntEcY7gj0IqxXK2mqjStd+x3LgWV8fMt5D0Vz1X8a6qoZaYUUUUhhRRRQAUUUUAFFFFABUVzcw2dtJcXDhIo1LMx7Cpa4vWrl/EWr/2TAcWFs265kH8bD+H6CgTZFYSyanfTa/egiMDZaRt/Cv+JqOaVpZWdjyTmrN7Op2wQjEMYwoFUiatIhgaSikpgIelQyVMajYZoA8Z8YaBqFrc+aYna3Ds3npyOTnn0rA7V7pqlh9v025tc4MsZUH0NeSP4T11GK/2XcHacZAHP60thpiaHBocyT/2vcSwsP8AV7ehHf8AGtbQItOfw/qiXczpYtcoN54OBnGaxG8M62Oul3P/AHzWvBpGoxeELy0eymFxLOjLHt5IHeokUmjCEOlHXvK+0SjTd+PNx82P/wBddDZwaTDLq6WFzJJafYwHfqR8wzj1rBg0DUBcJ9p0+8WHd85SPnHtXVWkeiQ2F9Cv2uFYbfbceZEAxUtnP17U2tBJnMa3b6NAIf7KupJ2YfPu5A9PxpfDf/H7cH/p2k/lUs58JyXLyJdXscZ+7GiL8v4n8KTRjaLfzC0meUfZZdxZQO3am1ZAmJ4T0W+v7oHyZBCHV/PPAXBzx6mvYlICgZ7j+deX6V42Frp9vB9guT5aBcqBg4rZ07xqL6/hthZXCbz95gMDAz/SjoGtz0AOKcHFec/8LCQMR9guuDjoP8akT4hR97C7/wC+R/jVE2Z6IGp2a5nQvFVprUskMSSxzRgMySLjj1FdCj5oAmzS0wGloEOoptLmgZbktYtb0t9OmOJV+aB+4NXfCPiCW4L6NqfyahbcAn/lqo7/AFrJR2RwynDA5Bp+r2DatDHqVg3lana/Nx/Fik0NM7+isbw1rqa7pglK+XcxHZPH3Vv8DWzUFhRRRQAUUUUAFFFR3E6W1vJPIcJGpZj7CgDC8T6vJaRR6dZZN/d/KuP+Wa92NZkcEWjactjB/rCMyN3zTrFZHkuNcvR/pE/EKH+BOwqpI5dyzHJJqkiGxhNMNKTTSaoQZopuaWgQU2ikoAQimEU+mmgYwjiqUv8Ax+x/7pq8apS83sY/2G/mKUgRi+I9eTR7YfIZJnB2rnAwOpJ7CuG03UJ9Us/EV5cbdzwoAF6YzXc+JdCGsWZCP5c6qQjEcfQ1w2m6fNpemeIbWcKJEjTO08c5pSKiczj2FbHhv/kITf8AXtJ/KshSA4JGQDyPWuyt9X07UJo4rPTktXjtpN7gAZ+Xpx1/Gk9ho40k54P61p+H2xr1nz/Gf5GrF1rFhNoUdjFpscdwoAabAzx39cmtLwzq9sscOnrpaSXJfJnC5IHqe/FDvYDl5CfNfn+I/wA6Aea29L1q30qK8SXTY7kyMQsjAcex4rC3bmJAxk5wO1MRqaJrU+iaqk8SRSLIuxkdtueexr1zRdVg1exW5g3LyVdG6ow6g15Do+hXGu3nlW7JG0QDNI3YV65ommRaTZC3jJdiS8jnqzHqaaEzYBp2ajBo3UySXNGajDCnZoAfU1tcPbzLIh5H61XzS5oGWLxW0m/j8R6ap8lvlvIV7r3OK7i1uob21jubdw8Ui7lYdxXF2F2IJDHKN0EnDqau6JI2hasdLds2F0S9ox/hbun49qloaZ1tFFFSWFFFFABWNrQN3NDYZxEf3s+O6joPxP8AKtnoK5uSUsJZj9+ds/RR0FCEyC8l8xsAYReFFZkgwauSVWkGRVogrmmGnGmGmIKKTNJQA6kpM0maAA0lITSE0ABqm3N+vtGf5irJaqmf9P8Aon9aTGidkBWuc1rQfPtr02aj7ReKqtubAOM4rpCRioXPzx/739DRLYEeXf8ACAa3/dg/7+Vd0vwdqthdNLMIdrRsgw+eSMCvSRjFRTY2j/eH86TWg02eYDwBrPrb/wDfyt/RdB1XSURDY6e4ziSbed5UnP8An6V2oAoONpp2Fc5BtF1Mo8S6Po4hZi20s2M+v1qreeE9TvBxY6TAdu0GPcMe+PWu8GMUUxHNeE/DMmhQztdNG9xKwwUJwFHb866YYFFMZqAHNJgVWlvFTqaZMxxXK+JJp47bCMV3nBIpgdINYtw+0zxhvQsKvxXIcAg14W99GszDZIxB5Nd14N1Npz5O5jGRwGPKmlcdj0IPmnbqqoTiplNAEoNaUITVLFrCVsSL88EndWHSssVLHI0bq6nDA5FDA7fSL03tgrScTxny5l7hh1/x/Gr9YWmTobhLheFuBtcDpvHSt2oZaCiiikMqajKYrGQr95vlH48Vgzn5sDoBgVs6sf3MQI/jzn8KwnPJpolkLVA9TNUTc1RJVkHNRGrEgqseDTEBpKQmkoAXNNJoJqJmoAJJQoyTVCXVbdDgyr+dYPjm4ePQX2u6hpFVthwSM+teZtcZb/VrjtzRcaVz2N9atgOZU/OsnWPEqabbreRxGfzMIgVgBnnv+FeXmYEcotbWrKr+EdLH3RuyMf8AAqmTKSOjsPiCssgS9tvIUnAkV9yj6+ldJFq0E8LTrKpSNhkg+oNeI+QpcEsSAeg711OklB4T1nEagB4uB/wKhvQLI9HGtW2P9cn/AH0Kb/a1vIcCVTj5jz2FeNb4/wDnglavh4xnUZB5SrmCTp/umm3oCR12pfEO3tpWisrc3TLwz7tqj8e9XNH8b22pSrbzxG2uG+6rMCG+hryg2qbm2MyA/wAI6Va0uBY9Ws3Z2fEyYB7cihsLI9s/tW3BI81eDjrTk1SBjgSp/wB9CvGL4w/b7j9yp/eN3PrUAMP/ADxH4Madxcp70kocZBpTzXG+Bp3bRipd2VJCF3tkgema7BGyKCWNaPNYPiiynk0aWS2jLyx/MFUZOO+K6LIxTGkA70AeBZ59+9dT4HjuZtej8lWMajMp7AV31xoOi3s3mz6dbvITkttxn8q07O1tbKLyrWCOGP8AuouKVmVcsqtSAU0EU8GmIUU4UylFAjZ0eb5pID/ENy/UV2ETiSFHBzuANef2kvlXUb56NXbaa37uSPOdrZHHQHn/ABqZFxLtFFFSUZesNgQj1JP6VivWxrfAtz/tEfpWM9NEvchNRtUjVEaokieq8g71YaoWpgQE03NK3BptAgqNhxT80hpgct4z06a90GUQruaMiQj1ArycivfiAarGwtCcm1gJP/TMUmNOx5BZ64LPSJrA2MEjSA4lYcjNaOq3ePB9hD5Mf71VGccrtLdPrXpbadZ/8+kH/fsVmXeh2GqO8FxDhYseWU+XYfaoa1KUjyzR9RXSr77Q1rHcDaV2yDp7j3rptK1hBpmt6mbKHaZYv3A4XoRXUaX4P0rSsusbXEp/jn+bA9h0rS+w2q5jFtCEbBZQgwfrQ0CkeJyyebcvNsVdzFtoHA56V1un+II77UbVRp0EQt4nY7R9/C9PpXoH9mWOP+PO3/79io306zjwyWsKtnGQgFNrQSkeWahcv4j1KMWVhHCwTaEjIGfc102ladf6WI7IaTDPvYM11JgbM8n64rc1Lwfp2oHzIg1ncAYEkHH6Va03w7ZaZaiJVaZurSSksSf6U7Bc5HVbXUJNMmsxocKMG/4+Ny7mHXIHXJrjZoJbaUxTLskABKk8ivbf7Nsn+Z7WFmPJJXvSf2Ppv/Phbf8AfsUW00FdmD4Js3h0CORhjzWLj6V1A4FOVFRQqqAoGAAOBTSKaERyS7Qa57WdbGnpnBZjwqjvW9IhIrkfFVmimK4lYrEqtuIpgihF4uvfN5hj2+mTmuv0fVl1GDeoKsvDKe1eLncXY+Y2CeMnmu+8E3yNciDkSFdp9/Q1Nymj0NXqUNVdRUq0ySUGnUwUoNAEinBBrudIk3wn3UH9K4QGuz0FsoR6Rr/WlIqO5tUUUVBZma4v+hK+PuSDJ9M8Vgsa6u7gFzaSwn+NSAfQ9q5Dccc9ehpomQjGomp5NRsaokjaomqRqjagCBxUWamaq7cGmIXNJmmk0maYDs00mkJqrd3kdpbyTynCIMk0AWWNVIT/AKTOf9rH6CuVf4iacCdsM5H+6KsXniy102KG4lilK3eXUAdBgdal7opJ2OqLcVET++H0rjz8Q9Px/qJ/yFWovF9rNpk+oiGURwsEI7nJFDBJnVhuKhmPyj/eH865D/hYen4/1E35CpbTxrZ6hceRHDKG2l+f9kZ/pTewJO52AagtXEj4h6f/AM+835CnL8QbB2CiCbLHFMVmdqp4p2a4248eWVpdSW7wTFo22kitzSNcttYtvPt92AcEMOQaSBpmvmg0wNS7qYgIFZus6TDrGnSWkrMm7lXXqpq80gAqrLeKnegDzef4f61HPth8iaPPDiTH5g11vhXwidFkN1dTLLclcBU+6n+JrXi1CJ22rIpPoDV6OYNS5R3ZYApaYGp2aBDxTs0zNLmgB46122gKNkpz0Cr+lcXAN0yj3rv9Jtzb2CB873+dvbPalIqO5eoooqCwrl9YtPst2XT/AFcvzD2PcV1FV7y1S8tmiYDJ5U+hoEzjCaYafMjwTNFIpV1OCDURqiBjGoyaeajamBG1QPUxqJqYiI03NK1MJpgBNYniQ/8AEivf+uRrXY1ieJD/AMSK9P8A0yNAHkGcg10Xit1Wz0eMnnyCR+S1yElvcCRsN1Oa6fxhGXtdNjC/vBaLtb06Vk9zXoYBkUNgsBXQ20yp4F1Ek8eevT6iuMNjO55bkV1OnW5j8CXySDcDcLxn6U2wMN7qONQS2fpWv4WkZ9Z83B8loZQpPrsNc6mms0p3HCnkCuj8MxeXqUcOflWKXH/fBob0AxnvoFYjdnmnWdxJczq0SHbGwL89qiGmRli3OAeatpBBA6eXIPnIyFPvQIn1++jTXL1SDkSmu++Hkm7SJX/vS5/SuF1mC3l1273AAmQ5JrvfAcappcyKQVWXAI6dKaEzsw9BemAUpFWQQzOcHFcJ4nvbj7SbdXZEAy2D1rvjHmuO8aIbCNLyOPLOpi3Y+6fX8qTGtzjIbxElBSUhs8EE16X4bvZrmzKzkmWM4JPUivJFODmu/wDAVzcXFxNG2WiROWPb0FSmUzvkapQaiC1IKogkBpwqMU4UAa2h2jXd+qgZA5OewrvgAqgDoBgVjeHNONpZCaT/AFkozjHQVtVDeppFWQUUUUhhRRRQBmatpQv4t8fy3CD5T2b2NcjIrxSNHIhSRT8ynqK9BrP1PSYdSjyfkmUfLIByPr6immJq5xRNRtVi9tJ7CbyrlNuT8rj7r/Q1VJqiBpqJqeTTDTERNUJNTGon60ARNWJ4lONAvf8Arma2mrB8Tt/xIb3/AK5mgDyVpHkxwoGQOnNbvjBzHdaenGRZx1zzyIsi7WLcjrWx4rnE+qR5P+qt44/0z/Ws3ua9DG3vyMrz7VvQsU8DXJzy1yFHH0rnWfa2Oo9c1tPcAeEIYB1kuWf8hQwRjmRyoBb5QfTpWr4ZyNWRic5hlz/3y1YRm2yFT0z+VbOhuEvEbOB5Un/oJpvYEZxGCQCeT60sQzOMeo/nUH2jL8A4zU9mwe4JHOCOaGI0Na51m8P/AE0Nd38PuNHk/wCuprz/AFaZDrN0xwR5pGAcV3/gIr/Y77TlTKcU0J7HaCngVEpqQVRA7FRXNnBeQPBcRLLE4wysODUwpaAOWb4f6I028JcKM/cEnH8s10Fhp1rptuILSFYoxzgd/r61apaLIBQKWkzQMs6ooLOxwqqMk0wFzgV0vh3QWuGW8u0xCpyiN1f3+lTaH4XIK3Worz1WD09z/hXW9BgVLfYpR7h0GBRRRUFhRRRQAUUUUAFFFFAEVxbQ3cLQzxrJG3VWFctf+FZ4SX0+TzU/54ynkfQ/4111FO4WPLpd8MhjnjeGTONsgwf/AK9NJr0y6tLe9hMVzCksZ7MM1gXng23cZsriSBv7rfOv+Ip3RDiccxqJua27rwxq1uCVhSdR3ibn8jWTLb3EJxNbTRn0aM1QrMpvmsbxBb+bol4pJ5iPStlpI84LqD6ZppCMMZBBoEeB3FnJDcFA6ttPUd6ku5p7t2klIMjdT9BivY5fDejzSF30+AsepxiqyeD9FR9xslY+hJx+VTYrmPGDbz5/1i4qyJJ/IigdlaOMkgema9i/4RfR/wDoHQ/kaibwlozPuNin+6MgUWYcx4o9lI0jMGUAnOM1ehkmgRfLKq4Urn2IIr2H/hGNH/6B0H/fNRHwhopYt9iUZ7AnFFmHMeKmymzkSKKsWyTW5Y/KxPfNe0Dwvo+P+QdB/wB802TwlosowbCNf93Iosw5jxeeCe5uXmklXc5ya9V8B2gh0QgPvBfOce1bMfhjRo1AGn25x3Zc1pwxQW0SxxLHHGvRVwAKaTE3ceikVKKjEsecB1P0NSqrtjbFK2emEPNUIUUtTR6fqMzbYtNu2I/6ZkD8zWjD4U1uYAtDDAD/AM9JMn8hQOzMnNMeaOIfO4Fdba+BMnN/qDsP7kC7R+Z5rdsfDek6c4kgs0Mo6SP8zfmaXMh8rOH03RNS1b5oYTBB/wA9ZhjP0HU13Gk+H7PSVDRqZLjGGmfqfp6Vq0VDk2UkkFFFFIYUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUYB6iiigCCSztZRiS2hcf7SA1Ufw/o8n3tNtefSMCtKigDI/4RbQ85/s2H9ajfwlob/8ALio/3WYf1rbop3YHPnwXoZP/AB7OPpK3+NZ2t+AbS60i4i0qV7S+K/uZmcsFb3HpXY0UXYHzvdeCvibb3McKWy3CqeZY7hAHH49K7rwT8P8AV4Jri78VXCSiRdsNnG+RHz1LDqa9Ooo5mFjEHhHRB/y5A/V2/wAacPCuiD/mHxn6k/41s0UXYGP/AMIpoWc/2ZAT7g1PHoOkQnMemWin18oVo0UXYEK2lsgwlvEoHogFSgADAAGKWikAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB/9k=",
                    FAttachmentName = "图片53.jpg",
                    FAttachmentSize = "25.33",
                    FCreateTime = DateTime.Now,
                    FBillStatus = "A",
                    FExtName = ".jpg",
                    FCreateMen = 113111,
                    FFileStorage = "0",
                    FIsAllowDownLoad = false
                }
            };
            //var ret = await K3Scoped.Client.SaveAsync(atta);

            /** 方法2 */
            string base64EncodedData = "/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCALpAREDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD3+iiigAooooAKKKKACiiigAooooAKKKKACiiigAooprusUbSOwVFBZmJ4AFADqK8Ouv2gxF4geKDR0l0pZNgl80iRhnG4dvwr2y1uI7u0huYjmOVA6n2IzQBLRRRQAUVl6x4j0bQLdptV1K3tVHaRxuP0Xqa8w1/4/aZbK8WhafLeS9BNP+7jH4dT+lAHsdZkniHR4tRh059TtftszbY4BIC7H6CvlzxB8TvFniJWiudTe3t26wWv7tT9SOT+ddN8C9COo+L5tXmBZLGI7Secu3H8s0AfSFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABXivxo+IbW0UvhbSnHmyLi9mVuUB/gHue9dL8XvGWp+EtAtv7LCpNeSGI3BGfLAGePevmdVutQuHdEnup3YsxVS7MT1JxTQFMivrX4W+IoPEPgWwZGH2i0jFvOmeQyjAP4jBr5qTwd4nli81PDeqsmM5Fq3+FaPhPWfEfgrXRJapPabx+/gu4WVHUeoOOfTFAH1rLLHDE8srqkaAszMcAAdSTXzp4/+MOqanqU1l4dvGtNNjJQTxDEk3qc9h9Kg8cfGC88U6EmlWtm9gr/APH2d+TJ6KPQeua84azl/ssXkfIMpjPouADz9c8fSgCKeaa5lMtxLJNIeryMWJ/E1HiphbXKWi3E0JSN22q3Zj7VHTEJivqX4QeHToXgiCWVcXF6fPf6Hp+lfO3hLRJPEPiiw01FJEsoL+yjk19iW8K29vHCgwkahVHsKl7ldCSiiigQUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABWdr2s2ugaHd6ndyLHFBGW+Y/ePYD3Jq/JIkMbSSOqRoMszHAA9TXzH8VvHh8W6z9isZidItDiPHSV+7n29KAON1zxFqXiDUZr7UbqWdnk3hHYlF9AB0AA4r6y8H29iPC+mXVpY2lsbi1jkcW8QQFioJ6e+a+OjGe1fRHwX8c2l/ocPhy8mEeoWgIhDnHmx9se49KbEet15j8YvGNnoehnSVggudRvUIVZFDCJOhc+/pXXeMfFdn4P8PzaldfM/wByCIdZH7D6etfJmr6pe69qtzqd87S3E7FnPYegHoBQhmbKxVMjk+9SadrOp6TLJPYOgSTAlilUMpI9QeKQjIwaqvbhmz1+ozQ1cR0Oqare6n4fs7i+dDI9w4QIAqqoAHAHArIFdnpF5YaHZ6E15Bb3CurGSGWINtVnPzg9j0qHx5oUOl+JlTT4wILxFlhjXoC3YfjQtg6nffATQC9ze67KnyqPJhJH5mvd65vwHoi6B4PsLIABxGHkOOrHrXSVKKYUUUUxBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUVwPxT8c2/hTw9Lawy51W8jKQIvVAeC59AKAOL+M/jmyvtOj0XRtWWR1uCl7FFnkAdN3QjPUCvOdD+HXivXwHs9ImjhPSa5HlIfoT1/CuT3kOHJJYNuJ9TnNfaHhu+XUvDOm3atuEtshJznnAzT2A8Jh+AXiaRAZdV0yFsfdw7Y/HFUtS+EXiDw1DJql1PbSWtohleW1lIYY6cHB/I19L188/GL4g/wBsXreHtLnBsLd/9IkQ/wCtkHbPoP50Aecaz4j1fXzB/amoTXYt12RBz90f4+9SXGnXVxoFhJZJNJGS5nMC7ir5wAwHPTH51iTAtGQP0qCG+u7J2ktL64t5G+/5RIB/KhgdDf6HPpWi2s94rrPPK2AwwQgAxkdjWfaWzXV3DborM0rhQEGScntV+6kmm8Haa8szyySXczM75ycBa1/AekjUNeWYyyqbMCYLCAWbBHr29fahbAzp9b8I6JcPBZSahJpl5DAkcEl2P3Mygdj6+tWNJs7fxR8S9Ks4JRd2ek26LJOgyrle/wBM1vXGpadfWOo+dJFq9jbsxubN0CzQD1T1A/yat/A/Q4odLvtZEe0XMxWEHqEFEthLc9bACgADAHQUtFFIYUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFZuv61a+HdDu9VvCfJt0LEDqx7Ae5oAo+L/F2neDtFfUL5izH5YYV+9K/oP8a+UvEOv3vibXLjVb9szTHhR0Reyj2Fd1q3juy+J2qafpGsac+nq1xtt7q2k3tGW4+ZTww+ld7o3wG8PWU3mand3WpYPEZ/dJ+O3k/nTEfOhVT3Ar0/4X/E6fwwyaLqMU11psj/ujGMvAT1wO6+3avaR8N/Bgh8r/AIRrTtuO8IJ/PrXD+PfCHgnwZok+rW9rLZ30iNDbJBKTvYj+62QB60DD4j/FrT18P/YfDd6s93dgrJKmR5Cd/wDgR6V4HMjQwxzykKkmdpY9cdTUbN1JwMmti3v9DvtMj07UpfsV1bPmK6MJkVkPOMDpzRsBleWxt0mGDG/Qg9Pr6VEtobiZY44y8jnCqByT6V10o0ebwXfHTGedra5i8y5dNm8kEcDsB/Wubt55LWeO4gcpLGwZGHYihO4M1tTtZbLQNItJo2ikUzOyMMEEtjp+Fdh4F8LwvJb6tba7su4l3yWscfzBTxg56g1ak1bQtfsdPtvFKGG8uYBJHfRjGw5IwfQcZ9K6jVPDkjto2rWNxE01lGUkmRgiyx7Ttz2xnH501awrmT8Q0vbOx3ad9iVbt1tZjHCBP83IXd3Br1/wtoyaB4asNOT/AJYxKG9zjmvJ9y+LfiJ4ftIrmOf7JH594kDbokcdwehOcflXuA4FJ7gtgooopDCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAr50+NHjeXV9Wbw9YzA6faN++KH/WSjtn0H867b4vePrnw4sOiWIeKa8hLSXKH5o1zj5fevn/AOxTzPm2V7sMesSlmyfUdc00hFW1ne0u4blM74ZFcY9jmvsrw3rlp4j0C01OzlWSOaMFsHlWxyp9wa+TB4U8RMm8aBqRU9/szf4Vv+C9d8T+CdTma3t5kt9u+4tLmMqsn0zzu9xQwPp/UtRtdJ06e/vZlitoELu7dhXyf448YXXjLxBJfSlktUyltCTwif4nvWz4/wDijeeNLa3sordrGxT5pYd+4yP2yfQelcZfxQafNFbskkszBWcg4HIzx+B60DKTKGUqe9VZIm6BQyjsecVpz23lpHMgYQyltm7rx6/nUO2mI3NFQjwHrinj/SIOw/2qxSPlxXceCJNNtdC1CfVovNsftUKyLjPZsHHoDW7qPw103WLZr7wtqMTqefIZ9y/QHqPxpRBnPpp9vfeJNDsr0SfZVsoTMUB+VcZOfQc9a9N03T59K1cafZ6c8ekyRlt3n+bAx7YB5Unv2rG8PadND4t1CSNhFfRWccVpHPkRzLtAb6gEDpV3VNX/ALK8O315qGnXdjcBWgNvHITEzMOHQ9h9Ka2B7l34R2MU+t+IdaS0itleb7PHHH91AOTj6/0r1muR+Gek/wBkeAtNib/WzJ58meu5znFddUjCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAyNb8MaL4kiWPV9Ohugn3S+Qw+hHNP0fw3o3h+Ly9K063tRjBKJ8x+p6mtSigAryb42eKrOw0ZdCSGGe/uhuJdQTAn94ehPavUb+4e10+4uIo/MkjjZlTONxA4FfIviU6vc67dXutQyx3c7lm8wcewB9AKFa4WdjAfC4J6Vrr4ls5rGG11bSXuWt+Ip7dwjMo6AnB/Ss1kDDBqsYGViUbbnrgU2hI668uxr3g9r1bVLZrG6CCJP4ImGB9Tnv71zddH4Nh+0aR4gsCfv2wmAx1Knj9SK50KWYKoJYnAAoj2GzpbBWX4e6o2Dh7yIA+uAazNHu7+yv4JLGeaBnkVN0bYzk9D61t5A+H95EOVS9ROD1IU5/U1keHyraxbW7DCSSpgj+Eg8H+n40dxHtv2y+hikfVNPgudNibKzW0mZoF/vMvUfga5fxnHdXVtYWqas1/p+o3cQticEqpOCCR1696ztQ8c6HevPbarokkjxsYxLbTlC4BwM1s+DTB4h8Q+GY7axW2sbWSe4SAMWwqgAEk9TuND2Etz3K1hFvaQwgACNAuB04FS0UUigooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigBsiLLGyOMqwwRXMah4PjukaMNHNC3/LK4QMBXU0VMoKW5Sk1seF+JvhLEwaWxjaymH8P3oW/qK8n1TR73R7o299btE46E9GHqD3r7LIBGCMg1zfiPwTpXiKwe3mhVSclSB90+o9KFzR8wbTPmvwNJs8UxwE4S5ikhb3ypx+uKy44mtpbiVhgwMUB/284H9T+Fbt1oV34O8eWlrdg4SdTHJjh1J61T8WIlpr13bKcKJmkOeOX5/lirW5L2LEXHw8uP9q/H/oNZ/htd3iOw9pgfy5q+px8PW99QP/oAql4a416Bv7qyN+SE0dGHUpalh7hrlQAJWOQOm4Hn/H8a9m+DVqrajHIc5t9OGMdMvIxP9K8ZicTxXFvkHOXX2Yf/AFs/pXvnwajAh1NscolvH+Uef602JHqdFFFSMKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigDy742aKlx4dttZjUefYzpk46qxxj88V4x44i87xG8shRfMiR8H6V9C/FUA/DnVARn/V/+hrXhvj3RZ1nttU27rd4liJA+6wHf60uo7aGYUkHgJFyCp1Fvm/4AKqaFHKl9JImG2W8pOO3yEVdcbfh5APXUH/8AQBVbw3kXN7/15S/yovoFjGhVoJVkiK7w24DHevob4NSyMviBJI9hW7Xp/u4x+lfO44mU+4r6L+EZxd+JV/6e1b9Kdwsen0UUUCCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA4v4rEj4d6hg4y8I/wDIi1hNpdvqmkSWN0m6KQYPqPce9bPxaI/4QOdSeXuIAP8AvsVVsOYz7NSXxA9jyPxVoUvh/wALwafI4fF67qw/iUrxWF4cX/SLz/ryl/lXuGqaPZ62k9nexB4yAQe6n1B9a83l8HXnh29v5G/e2TWcqxzD19D6GhrQqLPO9oLD619BfCR/+J14kj9ZEb+dfP8AtwQfevePhM+PFevx56qjY/Gl1H0Z67RRRVEBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAcF8WyT4RhjClvMvYRx9c/0rlJvGtlpF7PZz2127owO6OPKnKjvXW/FO+nsdF0027hHl1COMkgHghs4zXjXiLxXrVjrt3b2l88cMbAKoUcfKPapvqVa6O+bxvYQ2cepPbXnlXDFFURfMCPUVEnjjTNU8yFLW7xGhlcSQ4BUdR7muNu/E+sxeFdMvEvnW4nkkEjgD5gDx2qHQvFmt3l1dJPfNII7V5Eyo4YdD0o5nYXKReIB4Wv/ADJrCK9s5zztEBKE/Tt+Fd98KN3/AAneuAggeQuM/UV5o/jnxCpIN/n6xr/hXq3wv1K41HxfqyXBRhb26CMhADg8kE9+lPdj20PW6KKKZIUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAHnHxe5sdATHXU0P/AI6f8a8L8VE/8JHqH/XUivcPi02ZfDUeeuoZx9BXhPiRt/iDUj/08P8AzqHuWtjT1AY8GaID3eY/rVTw0Ntxfn0spf6Vb1T/AJFHQB2JmP6iqfhw/vtQ/wCvOX+VIfQxpeWzXtvwjwPGGsH+/bRGvEX5P4V7Z8J/+Rxv/e0j/lT6oH1PaKKKKszCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA81+KcLTan4ZCAsVu2YgDoABk14xrHh7WrjV7+WLS7t0kuJGVhESCCxwRXs/wASp5F8R+G4UkZQ8j71B6jjrXmniCw1OPRhrVpeXPltNKJlEh+T5yAR7VO7KWhV1LRNUl8N6LBHp9y8sQl8xFjJKZIxkdqpaFoOrW81552m3Ue+0kRd0ZG5j0A96n1m/vo/DmiOl3MrushZg5BbkdaraBqd/LNfb7yd9lnIy5kJweOanoPUzW8N62BzpN70/wCeLV658MYJ7bxpOJ4ZIg9ku0upG4gDOK8g/tzVcD/iY3X/AH9Neu/DK+ubjxnLDNPJJGtmjKrNnBIGTT6oNbM9moooqyAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAPLviQ27xx4Zjx0EjUnhqCO58MxxyorxyNKGVhkEF2qP4hc/Efw6CeBbzNj8Kn8HEN4Xsz6lz/4+aI7hLY4Px3oTWNrZJaQubK2LoW67MkEA1y3h1Ns2pD/pxk/pXvIijnFzHKivGxwysMgjFcPqXgdNPmvrzTCSs9s8QtupDHpg+lJx6opSVrM8fb+H6V7F8K3H/CdOPWwT/wBBFeR3VvLbSmGaN45V4ZHGCK9Y+Fg/4r7/ALh6f+gil1K6HutFFFUZhRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAeXeOo4ZPiHpckkwR4bKXandsg5P4CuQ07xTZaVZR2UHiAeXFkANZEnk565rofHzH/AIWXanPEelyH8814YG+br1apvqVa57Vc+LbezhtpH1RI/tCbwxtS2/nrjPFMt/F1veSSKuqRyeVGZTi1K7QO/Xn6V554kJWz0IelmP8A0I0zQf8AWaj/ANeMv9KnmY+RWudVrOtaBrcBW81G0ZiMLL9jcOv0Oa6T4dWNlF4zjmtdQWeT7CA0RQqQoAw3vmvDzzGo717F8K/+R8H/AGCl/wDZKq+oWsj3GiiiqICiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA8d8eD/i5EzZ+5o5P5sRXhyclfrXuPjdJZfiDqpSNmCaOiAgdSWPFeNromrcf8S274/6ZGofU0RreK22waIv/Tip/wDHmpnh1tzagf8Apxk/pVzxNpOoTx6R5NlcSbLFFbbGTg7m4PvUfh3StRgkv/OsbmPdZSBd0ZGTxwPelbRDvucwi4Y59K9e+FT/APFexjudKH/steY/2Pqm4n+zrvGP+eTV6f8ADO1uLfx/aSS28satpmzcykDIC8fWn1E9j3OiiirMwooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAPH/Fd5c/8Jzr0KzMI49PRlA7Nnr9a5LxVZeItCZbq31O9k0+RQd3mnMZ9D/jXSeJGB8d+KW/u2can/vkGu0+zQ3Nl5E0ayROm1kYZBGKSV7jbtY8W8S67qtoNMFvf3Me+xR32yEZYk5J96i8Pa9rF0195+o3Mgjs5HTdIThhjBHvXQ+OPB17Psv8ATYfMt7ePyTCnLKq55HqOa5XwxHtGpAggixkBB6jpUNWRasymPFGvYwdWu+n/AD1NekfDrWNQufHOn2093LJDJp/mFGbILY6/WvIW/wBZ+Fen/Db/AJKBox9dNP8AKmtwklY9/oooqzMKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigDxDxA+fF/jOQ8bI41/8hiu4t7238lP38X3R/GK43xFq3k6n4tdbW23W0ijcY8+YSo+/nr6V5+fGd3uI+x6fwOMWq1KlYpxue529xCsbbpEBLkjLCsTVNA0u8a4mgEUF1cRtC8sZHQ9yO9cB4l8T3Wn3NskdtZuGto3PmQAnJGePaq+geKrq+a+L2lkvlWjyDZABkjHB9qObQXK0yhrngfVdJdpEVLu3z/rIDkj6jqK6X4dts+IHh8Y+9p7D+f8AhXLN4zu84OnaYf8At2Fdx4E177R4y0K2bTrFTcWrSeakW10xu4U+ntSVr6FO9tT3OiiirICiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA+fvEZHmeO3HP+koP1Ary8cy59q9K8QsPsvjdgfvXyjP/AxXmsePMHPbFZs1R0/jUD7bbj0tIf8A0AVT8Jf6zUx/04yf0qx42+XVEGelvEMf8AFVvB339UJI4sJf6UlsDMh8lx9a9D+H5x4/8Ke9nIP/AEOvO3kBfg969B8Ctjx94POfvW0o/wDRlOO4SPo6iiitDIKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiijtQB4jcXn2yTWo47OzVxqIgw0W5XJf7zDua5bWNTvtD1E2l7oelqxPyOLVdrj1BroNPObvUS3Pma4oz6/NXfapolhrVoba+gEkfUHup9Qe1So3KbszyfxV4jbT9V8kaZp82IkO6aAMfujvUOgeJGvHv86Vp0XlWbyfu4AN2McH1FWvHvhLVHum1S1gM9qVAPl8sgAxyPwrA8MIVbVFI5GnyZ/Spd0i0kxp8WNjB0XS+Tj/AI9xXceDdfV/Ffhq3Ol2Ia6ifEqx4aLBf7vp0/U15TIDuU+9d54OyPGvgo/7Mo/8eeiL1CSSR9IUUUVoZBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUjfcb6UtNk/1bfQ0AeCWbm3haeVWSM65uJKnhQc5+legp4i0k9L6L9R/SvMtc17U4vDz3C3sgmGovCHGPuKOB9K5O38Wa3JOiNqExBPqPX6VCdkU43PeRq+n26CKa7jRx1VjWPe2HhzUGuJYXt47iaJo5ZYSAdh6kjp6c1554t8SapY+IrmCC9kjiTGFXGBwPameGvEmq3a6iZ72VzHZu6ZA4IIwelPm0Fyu43V/A5t2L2GqWd3GDkBnCN/hWl4Vt54/Fng9jC/7t5FcgZC/M3eubPi7Xt2BqUxX6D/Cu28LeI9VGseG4zeOUu5WScED5xuPt9KlWuW+a2p7xRRRWhmFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABTJjiCQ+in+VPqG6/485ucfu25/CgD5o8RN/xSS9t+pTGuRsl/fxn/bH869A1vQZZfD1paC+sdy3MshkMw2Nknoe9c/a+FZopos6jprYcE4uBzzWfQ1vqR+OufFN57MP5Cm+Eeuq/9eEn8xW54q8PveeIrub+0NOiVm+5LOFYcdxTPDvh1rQ6gP7Q0+TzLN0Hl3AO3OOT6D3o6Cucnt4zjvXaeHGK6r4RYDP+lsP/AB7/AOvWT/wjMoAH9p6Uf+3oV1fh3w/M994eK3tgPst20jf6QPnGRwvqaSWpUpKx73RRRWpiFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABVe/wAf2dc56eU38jViqWsP5ei3z5xtgc5/4CaAPGfCdtHdWehRzxrJG8c7MjDIOSaj1r4bvFfR3ukMWhEgZ7djyoz/AAnuParXhBkji8PBmABtZjyfc133nwmNgJEzj+8KUUmhttPQ+fvG4P8AwlV+eeX4/Kk8HqwfVWPewkx+Yr2vVNB0bWrNI9QtonbbgSA7XX8etctH4Bi0v7W+nXolE9u8Kxy4ypOO47cUnF2KUlc8sKDNdZonE3hc+mpEfqtZd/4W1qwP7yyZlX+KIhx+laukgpF4fYgqyanyCOhylZJNPU1bTWh9K0UUVuc4UUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFZ2vz/ZfD2oT7Fk8u3dtjdGwDwa0aw/GT+X4L1ps4xZy8/8BNAHiWua+LbR9IuBp1mfOjZhGUO2PnovPArP07xO1xqVtCNOsl3SKNyq2Rz9aq+K/l0bQ09Lf/CsnQ8rrlkB/FOn86z6GiR1Gt+KXt9durcafZyBHxucNk8d+am0XxI1yL4mwtE8q0d8IrfNjHB56VyniIk+Kb8/9NDVvwySRq2f+fGT+YoYJKxaHjAnJOl2P5N/8VXSaR4hiEOmzNpNixlvjGBtPyEhfmHPXn9K82KDGM811OmHbp2lE9tTH8kpXK5UfTg6ClpF+6PpS1qYhRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAVieMEhl8IapFcXAt4ZLdkeYjIQHjOK265T4kuU+HusEHGYMfqKAPHvE2m2E0GmxTavHCkcICMYmO8YHPHSs7SNG0xNasnTXopXWdCsfksCxz06VF4xkKtpK5/5c0/kKytAYt4isCTz9pj/AJ1n0NDd1nR9Il1u8ll16GGQyEtGYWJU+nSrGhaXpcD6h5OuRTb7Rw+IWGwcfNXMa8x/4SPUf+uzfzq74YXM2qD1sJP5ihgloTf2RoobP/CRwZH/AEwf/Ct/TdL0ttPtVbXYRHHe71k8psM21fl+vA/OvOpTsmPHbmut0b/kFWGR/wAxRD+i0mUkz6dT/Vr9BTqbH/q1+gp1amIUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFcx8QrOfUPBN/aWyq00oVVDMFH3h3PFdPXEfFhynw/u8HG6WIf8AjwoA8k8V6BqV/c6eLaFGEVoitmVV5GR3NUdE8L6tba9YzzW8YiSZWYidDgA+gNd1aaNY6xdvBewLKq2UQGeq8t0PaqR+Hsmmava31jdB7aKUO0cvDAZ7HvUWdrlX6HIaz4X1e41q+mit0KPMxU+eg4z9at6B4e1S0lvjNAq+ZaPGuJkOScehrH8RWd5b67evPBLEsk7MhZSAwJPQ1N4bY+ZqQz0sJP5ikylexDJ4Q1p23fZo8/8AXdP8a6LSfDeqvYW1stuvnJfJJjzkxtAGTnPtXBSyMAfmPJ9a6PSJWOhIQTkX0fehjV7n1HASbePPXaM/lUlQWZ3WUDesa/yqetDIKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAoooJAGTwBQAVxnjrxfqfhFrSe30+1urSYlSZJijhhzgcY6d6TXfiBDZTPaaTam/uV4LA4jU/Xv+FeW+LJfGXieNlvZALUHckEceFQ+vqadhXO10f446BfMsd9aXVkx4LAeao/Ln9K9LtbqG9tY7m3cPDKoZGA6ivnT4ZPa+FfE5k1nTkkWRdiXbHJgPf5fQ+tfR6Mjxq0ZBQjKkdCKQx1FFFABRRRQAVxHxUtLm+8HG2tIJJpWnjbagycA5J/Ku3rhfixLJD4PV4pnik+1RgFGwSCcEflQCOWtL59L1aVpLO7lRreNQ0MJYZBbIP51rDxDHcjyPsN+hfjdJbkAfU15Z4xvbiLxAyJPIqrGgwrkDpmqfh2/upPElmjXMrIX5UuSDU3diuXqeu3Wt2kqvbz6ZeTKCVObUsp+lYg03QZGuWttMvbSSSFkciBlyp6gA8ZrzLVNUvY9avlW6nA858ASHA5Nanhy+upW1AyXMzYs3I3SE4PFDYKJYvvBcLEm0l1EjsJbFv5in6doGpxac1qllcSOLyNlAiYEjucGuWbVb8zHbe3GPTzWre07Ur5dKmkW7nEi3MWGEhyOvekyle59N2H/IPt89fLX+VWKqaWSdKtCeSYVz+VW6szCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAK5bxBd3GpSvpdo5igXi5mB5P+wP61v6jO8FmxjOJW+VPqa5t0WBBAhJC8sx6s3cmmhMpWunWllGEgiUAd8cmpyikfdFLS1RBheIPDtvfadLcRRhbiIZ4/iFdD8OtRe+8LrDK2ZLVzEfp1H6URDcXTsyEVnfDgNFda1B/AJlI+uKTKW539FFFSUFFFFABXB/FWxu9Q8O20VpA0zJcrI4Xso6mu8rzf4wSOmk6WqOV3Xgzg4zxSew1ueWeKdF1O88QzzW1jNLFhAGQZB+UVW8P6Dqttr1rNPp88caMSzsvAqt4tuZB4qvFDuBuUABj/AHRTfDM0p8S2qGVypJyCx9DUldB2oeGtam1O6lj0y5ZGlYghevJrQ8P6Fq1q18LjT5499q6ruX7xOOBXNX93cf2pcjz5Mea3G8+tbfhuaUrqDNI5xZyHlj7USBXM/wD4RbXd2f7KuRz/AHa29P8AD2sNp1xbrp0/nPPEypt5IGc1yhurjP8ArpP++zW3p13L/Yd2wlcOJI+dxz/FQwR9S2Ufk2FvFgjZGq4PsKnqvYNv0+2b1iU/pVirICiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAydSf/SCx+7CmQP8AaNYecnPer97N5kLSA582QkfQcCs6qRD3FooopiJIDidPc4rP8Fs0Pi7WbfHysA59uauqcOp9DWVpkgsviaUY7VuYmA9z1FIaPR6KKKksKKKKACvPvinp8moWmliOWFPJuN7CVwu4eg9T7V6DXmXxbO6fw7H63mf0pPYa3PMde8O3N74gu7iO5slV34WScKw4HaneH/Dd3aa9bzyT2bKpORHcKx6eldxYaVp2qWt8l7axTZu3wWXkcDoeoqvF4E0/TtSi1KynlTy85ib5gcjHXrSs7BzdDz648J3kt7LKLqwAZycG6XPWtXRNBurX7YJJ7NvMtXQbLhWwTjr6D3qnqfgTXLe4eZbdbhGJOYmBP5VHo9ldWY1Bbi2liP2OQfOhHpSZSehW/wCETvM/8fmnf+BS1qaZ4ZuvsF1b/a7DdJJHhvtC7RjPU/jXGdOOK1tP/wCQRqH+/H/WmxI+rdNBXTLZSQSIlGR06VarO0E50CwP/TBP5Vo1RIUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFI5IRiOuOKWg8jFAHLXY229uo4wp4qpVu6DfZbfd94AqfrmqdUiGLS0lJTELXOeKpG0/WNK1ZOPLdST9Dg10VZ3iCx/tLQpYwMyRfOv070gPQ4pFmhSVTlXUMPoafXAfDjxQLy0/sS9lAvLYYi3dZEH9RXf1JoFFFFABXnPxMOntqegR3TTi4M5NuIwNpORnd7V6NXlfxSJPi3wmoHHmuf1Wk9hrc4efxDa2F/dQrqGoxHzmLLHGhUN3xmtHRvE0V7qcNquo30u7PySxoFPHcivPtYbOt3h9Zm/nWj4R/5Gm1+h/kakdjoH8Zw+YwOragNpxjyEq3Z+KIrszIdRu5AkLSEPCgwB3Hqa82bmV/qa1tDbNzfKO1lIP5U22FlY2LjWtFuc+fc3D+v+hxZ/lRZjw7JbXhM96sBZPM2xKCOT0FcaQa1LDP9k6hjP8AB/OhoSPqnQjGdBsTCWMfkrsLdSMcZrQrJ8LnPhfTD626fyrWqhBRRRQAUUUUAFFVNT1CDStNnvrltsMKFm9/avDtZ8d+JNZu2MF3LZ25PyQ25xx7nqTTSE2e+UV4Pp3iDxnaFZEvLuVB/DKN4P516X4S8Y/22TZ38Qt79RkDoJB7e/tRYLo62iiikMKKKKACiiigAooooAwdTi2rKB/DJu/Pmsiug1CMmd1P3ZY+PqK5/vVIh7hRRRTEJSq21s9exHqKSkoA4PxNpFxpGopqunMyANvR16oa77wf8QbPX0SzvmS21IDG0nCy+6+/tUUsaTRPFIoaNhgqa8/1/wAHywyNcWGWTOcDqtJoaZ71RXz7p3j3xToA+zm4M8S8CO6Tdj6HrWv/AMLi1wpgadZbv73zf40rFXPa68r8c+Io5vGWk6ZZT28yDP2nChihB4Abt+Fcxe+MvFniNDbmfyIG+8lsmzPsW61Q07RDbeIYZZbuFJI1z9nYne2QTkflUy0Q09TSGmalewefZy2auzN8ktqpHDEdetM0608Q2mrxfbrK1FtzmWGJR245HNR2+vSWBMC32l7UZgA8jhupPPFaVl4ie+ukga409gecRSsW49iKWlh6nAyeI9TidlktLRCOzWig/wAquaN4gu7uW58yG0Gy3dxsgUcj19q6WfxHFOMTHRpB/tyk/wDstVYH0y4efy7fSkYwtua3lbOO+eOBQ7Ajlj4svwOIbH/wFT/Cr1j4ovxaXdx5dpviClf9HXHXuMc02XSdKkJ2XNhH9Lpz/wCy0610W2NveRJqtiqSIoLb2IT5hyeKNAPonwxcPd+GdOuJNu+WBXbaMDJ9BWtWR4XiEHhfTYVlSZUgVRIn3XGOo9q16oQUUUUAFFFFAHn3xYvGj0Wzs1JAnny3uF5/nWP4Y0K3XShfzxhpHbCAjp71V+JGoDUfF0NhE25bVArY/vtyf0rrIovs2m2dsONseT9TTJYIioMKAKkFvbXEsbSII5kbdFMowVNR07NUSdZZ3DTxFZBtlThx2+o9jVmsTTrg/u5M9D5cme47GtuoLQUUUUDCiiigAooooAq34/0cSY5jYNXN3kYjuXA6E5Fda6h0ZGGVYYIrmb+Hag5yYmMZP8qaJkUKSlpDVEhRSUZoAKQgHrS0lAFO40yzuf8AWwI2fUVSPhrSw277MoP0rZphosBSjs7e2XbFEq+4FcRen/ivkwPuWz/+gmu8kPWuMn1SceKptOHleR5Ty5MY37tn97rj2qJ7FwPNb5s39wc9ZG/nWn4RP/FQKR/zzb+VdfJpmpXEKz2stiWYZ2S2i/zxTNNttdtr4/bLG0WHY2JIYl6445HNSyrnmznqPetrw9jzbwZ/5dZKsT67qts5Wewto2z0ezUf0q3o+vXNxNcB4LRdkDuNluo5Hr7UMZzHQ9a0LIf8S7Uef+WQ/wDQhVn/AISa9OcQWPH/AE6p/hVyx8Q3r215KUtQ0cQZcW6AfeHUY5pDPoTwb/yJejf9ecf/AKCK3KxvCd3JfeE9LupQoklt1dtq4GSOw7Vs1ZAUUUUAFUNZ1SDRdJuL+5YLHChP1PYfnV2SRIo2kkdURRlmY4AFeIeO/GB8TagtjYtnToG4Yf8ALVvX6elAMq+GoJtd8TNdz8tJIZXJr0ieQSTsw+6OB9BWB4S03+y9KM7riacYX2FbIqkQOopM0UxGhpj/AL10PIIzj6V1AORmuR044vF9wa6uE5hT/dFSy0PooopDCiiigAooooAKytStx5pI6TjB/wB4dK1ar3sBntWVeHX5k+ooA5FgQSO4ptWbxf3gkAwJBn8e9VaszCiikoAXNJmikoAU0xqWmMaAIZO9cTKumt4puJBNP/aARg8eweXsxwQfXrXaOa4g6ZdL4ludQITyJVKIQ4zkHHI6jrUTLgZcXiS1txsXV7wBeAPsinH61p6X4hhvLhol1K4lxGzbXtlUDA65BrjW8N6o5JWGM5/6bJ/jWp4e0HUbO9mkmhUKbeRQRIp5I46Gp6FF6TxVbSHD6tcN7GyUj+dPsNV0+6lmCXG8iFi2bNV479Dz9K5hvDWrb/8Aj3X/AL+r/jWnouhahbS3BlhA327ouJFPJ/Gkx2Qst5oDcNIfwsVH9aLQaJLHe7Li5WMwjzNtuFwNw5Az1rObwxqpOTDGP+2yf41d07QNQjivY2jjBkg2r++XruHvQGh614R8eaPY2FtpN2721tEgS0uZVwJUHTd6H9K9Egure6iWS3njljYZDRsGB/KvI7bw7Be+GbfTb4JHeW6kLIjBhn0yOorkbrwxqumyMIJJUXPWNyufyrRaohuzPouSeKFS0sqIB1LMBXNav8Q/DekI27UEuZV6RW3zkn6jj9a8Il0nU5TiYzSf77lv51ZtPC17MR8m0fSiwXNfxP4+1TxSTbIDaafn/UIcl/8AePf6VY8K+HDLIt1cqVhXkDHWruj+D47dlkuDvI7V1yRrGgRQAo6AdqdibkhOccYAGAPQUU2lpiFopKWgC1p//H0p9AT+ldZCMQRjGPlFc1pcYMrMenCj3zXUVLKQUUUUigooooAKKKKACiiigDntStijSpjgHzE+h6j86xzXUasm2OO4xkIdr/7p4rmp4zFMyHseKpESI6SiiqEFJQaSgANRtTjTG6UAQua89iilHi3UZmicRlflYqcN8w6GvQJDiuDj1G8Ot6ratdO1vAheOPPCHIORWcy4jmXw1c2sSz26B9gywiYHOPUCjT9K0KOeaSxuZldoHVlYEgKRyeRXGnxJrCnH9pT/APfda/h/XdTuZrsTXssipayOoZuhA4NSyrFefw5aeYTBqyMPR4mFWNI0eS3muCLiCQNbuo2k9/qKyP8AhJtZ34/tKf8A76rW0bW9TuZ7hZb6Vwtu7LluhHekxoyH0e77CM/Rqs6ZZzxC/jaJiWtmxtUnPIpreI9Ywf8AiZT/APfVXdJ1zVJ3ujJfzsUt3Zcv0IxzTDU9D8MJJD4dsY5UZJFTBVhgjmt5TuXB5Hoa57wzeS32g2txPK0szgl3Y5JOe9b8fStI7Gctx/kxH+AflT1jReiigUtMkWjNJRQAtLSUUwHUtNqWCPzJVXt1P0pAbujW/wAis3b5sY79q2aq2MLRQbn4Z+ceg7CrVSy0FFFFIYUUUUAFFFFABRRRQBHcRedbyRH+NSK5O5/eW8Up+8Pkf2IrsK5y/hCXV5Eo4YCYD3PWmhMx6SikqyApKWkNACGmMKfTTQBWkUkHArhG1GZtS1iB4oAttCdpEYDHv8x716Fisu80+0MoYW0W+ZisjbRlxjofWoktBxep5GfEEqtg2WnnHrbCtXQ9Xe4e9zaWSbLSRx5cIGeOh9q9EPhvRzydNtf+/Ypg0TTbc/ubKBN/yNtTGVPUVLjoVzHkv9uylv8Ajx0//wABxWvomrSTXE4NrZrtgdspCBnHb6V6KvhrRuv9m23/AH7FB0XTbcZhsYEJIUlUAyD1FDiCmeUtr046WWn/APgOK0NJ1ieaa4Bt7Ndtu7DbAozgd/avS10LSx/zDrX/AL9CnppGnxMDHY26k8HEY5B7Uco+dFDwxcS3+iQXcqRq0mSRGgVRz2ArfQYpsMMcEYjiRUQdFUYAqUVolZGbYop1NpaYC0UUUAFLTaWgBa2dJsjKctwDy309KzLWEzSgYJA7DvXX2kAgt1XGGPLfWpbGkT0UUVJYUUUUAFFFFABRRRQAUUUUAFZGppjULd+zxuhH61r1jalJu1e2i7JE7nn8KBM5xxhyPem06Q5kY+9MrQgXNITRTSaAFpDSZppNADqqXPM0A/2z/I1Y3VWnb9/B/vH+VTLYFuWe1QS9U/3hUu7ioXb5k/3qb2BFgdKhl+6v+8P508HiopDwP94fzoewIsikPb60gakY8fiKHsCJhS0zNLmmA+im0UAOzRSUtABRSVNap5l1Gp7tQB0GjWZRVkIxjk+5raqG1XbAvGM84qaoZaCiiikMKKKKACiiigAooooAKKKKACuXa7W5vdRvV/1cY+zxn1I6n860PEWqnTrERQjdd3J8uFfc9T9BWLPGtjYQWKnLKNzn1NNEtlImkoNJVkhmmk0E1E7YFABJMqKSSAB1zWHceK9JhYq1/Bkej1i+MPFNnZWV1Yxs0t26FNqDhSfU/wBK81FxKEUb8YA4wKASPWj4z0fvqEA/4FTk8R6fcwyXUd3E0Vv99w3C59a8iNxL/f8A0Fb+lyv/AMIjrTkjO+IDgepqWykjuR4z0bH/ACEYP++6ltvEunXgmaG7icQIZHIb7o9a8e89+ny/98it7w5KfsuskheLJv4R/eFDegJHfjxlo/8A0Ebf/vup7bxJpt9J5cN7C7D5iFccAd68b845+7H/AN8Ctjw7KWvbgFUx9lk6KB2ob0BI9oinWRQykEEZBB608tlRj1FcF4M8U2NzZWunSO0N1GgQLIMB8ehruFbgfUU+grWZbBpRUYNOBpiH5paZmloAdRSUUALVrT/+P2L61UqSGUwzpIP4WBoYHbWUwntEcY7gj0IqxXK2mqjStd+x3LgWV8fMt5D0Vz1X8a6qoZaYUUUUhhRRRQAUUUUAFFFFABUVzcw2dtJcXDhIo1LMx7Cpa4vWrl/EWr/2TAcWFs265kH8bD+H6CgTZFYSyanfTa/egiMDZaRt/Cv+JqOaVpZWdjyTmrN7Op2wQjEMYwoFUiatIhgaSikpgIelQyVMajYZoA8Z8YaBqFrc+aYna3Ds3npyOTnn0rA7V7pqlh9v025tc4MsZUH0NeSP4T11GK/2XcHacZAHP60thpiaHBocyT/2vcSwsP8AV7ehHf8AGtbQItOfw/qiXczpYtcoN54OBnGaxG8M62Oul3P/AHzWvBpGoxeELy0eymFxLOjLHt5IHeokUmjCEOlHXvK+0SjTd+PNx82P/wBddDZwaTDLq6WFzJJafYwHfqR8wzj1rBg0DUBcJ9p0+8WHd85SPnHtXVWkeiQ2F9Cv2uFYbfbceZEAxUtnP17U2tBJnMa3b6NAIf7KupJ2YfPu5A9PxpfDf/H7cH/p2k/lUs58JyXLyJdXscZ+7GiL8v4n8KTRjaLfzC0meUfZZdxZQO3am1ZAmJ4T0W+v7oHyZBCHV/PPAXBzx6mvYlICgZ7j+deX6V42Frp9vB9guT5aBcqBg4rZ07xqL6/hthZXCbz95gMDAz/SjoGtz0AOKcHFec/8LCQMR9guuDjoP8akT4hR97C7/wC+R/jVE2Z6IGp2a5nQvFVprUskMSSxzRgMySLjj1FdCj5oAmzS0wGloEOoptLmgZbktYtb0t9OmOJV+aB+4NXfCPiCW4L6NqfyahbcAn/lqo7/AFrJR2RwynDA5Bp+r2DatDHqVg3lana/Nx/Fik0NM7+isbw1rqa7pglK+XcxHZPH3Vv8DWzUFhRRRQAUUUUAFFFR3E6W1vJPIcJGpZj7CgDC8T6vJaRR6dZZN/d/KuP+Wa92NZkcEWjactjB/rCMyN3zTrFZHkuNcvR/pE/EKH+BOwqpI5dyzHJJqkiGxhNMNKTTSaoQZopuaWgQU2ikoAQimEU+mmgYwjiqUv8Ax+x/7pq8apS83sY/2G/mKUgRi+I9eTR7YfIZJnB2rnAwOpJ7CuG03UJ9Us/EV5cbdzwoAF6YzXc+JdCGsWZCP5c6qQjEcfQ1w2m6fNpemeIbWcKJEjTO08c5pSKiczj2FbHhv/kITf8AXtJ/KshSA4JGQDyPWuyt9X07UJo4rPTktXjtpN7gAZ+Xpx1/Gk9ho40k54P61p+H2xr1nz/Gf5GrF1rFhNoUdjFpscdwoAabAzx39cmtLwzq9sscOnrpaSXJfJnC5IHqe/FDvYDl5CfNfn+I/wA6Aea29L1q30qK8SXTY7kyMQsjAcex4rC3bmJAxk5wO1MRqaJrU+iaqk8SRSLIuxkdtueexr1zRdVg1exW5g3LyVdG6ow6g15Do+hXGu3nlW7JG0QDNI3YV65ommRaTZC3jJdiS8jnqzHqaaEzYBp2ajBo3UySXNGajDCnZoAfU1tcPbzLIh5H61XzS5oGWLxW0m/j8R6ap8lvlvIV7r3OK7i1uob21jubdw8Ui7lYdxXF2F2IJDHKN0EnDqau6JI2hasdLds2F0S9ox/hbun49qloaZ1tFFFSWFFFFABWNrQN3NDYZxEf3s+O6joPxP8AKtnoK5uSUsJZj9+ds/RR0FCEyC8l8xsAYReFFZkgwauSVWkGRVogrmmGnGmGmIKKTNJQA6kpM0maAA0lITSE0ABqm3N+vtGf5irJaqmf9P8Aon9aTGidkBWuc1rQfPtr02aj7ReKqtubAOM4rpCRioXPzx/739DRLYEeXf8ACAa3/dg/7+Vd0vwdqthdNLMIdrRsgw+eSMCvSRjFRTY2j/eH86TWg02eYDwBrPrb/wDfyt/RdB1XSURDY6e4ziSbed5UnP8An6V2oAoONpp2Fc5BtF1Mo8S6Po4hZi20s2M+v1qreeE9TvBxY6TAdu0GPcMe+PWu8GMUUxHNeE/DMmhQztdNG9xKwwUJwFHb866YYFFMZqAHNJgVWlvFTqaZMxxXK+JJp47bCMV3nBIpgdINYtw+0zxhvQsKvxXIcAg14W99GszDZIxB5Nd14N1Npz5O5jGRwGPKmlcdj0IPmnbqqoTiplNAEoNaUITVLFrCVsSL88EndWHSssVLHI0bq6nDA5FDA7fSL03tgrScTxny5l7hh1/x/Gr9YWmTobhLheFuBtcDpvHSt2oZaCiiikMqajKYrGQr95vlH48Vgzn5sDoBgVs6sf3MQI/jzn8KwnPJpolkLVA9TNUTc1RJVkHNRGrEgqseDTEBpKQmkoAXNNJoJqJmoAJJQoyTVCXVbdDgyr+dYPjm4ePQX2u6hpFVthwSM+teZtcZb/VrjtzRcaVz2N9atgOZU/OsnWPEqabbreRxGfzMIgVgBnnv+FeXmYEcotbWrKr+EdLH3RuyMf8AAqmTKSOjsPiCssgS9tvIUnAkV9yj6+ldJFq0E8LTrKpSNhkg+oNeI+QpcEsSAeg711OklB4T1nEagB4uB/wKhvQLI9HGtW2P9cn/AH0Kb/a1vIcCVTj5jz2FeNb4/wDnglavh4xnUZB5SrmCTp/umm3oCR12pfEO3tpWisrc3TLwz7tqj8e9XNH8b22pSrbzxG2uG+6rMCG+hryg2qbm2MyA/wAI6Va0uBY9Ws3Z2fEyYB7cihsLI9s/tW3BI81eDjrTk1SBjgSp/wB9CvGL4w/b7j9yp/eN3PrUAMP/ADxH4Madxcp70kocZBpTzXG+Bp3bRipd2VJCF3tkgema7BGyKCWNaPNYPiiynk0aWS2jLyx/MFUZOO+K6LIxTGkA70AeBZ59+9dT4HjuZtej8lWMajMp7AV31xoOi3s3mz6dbvITkttxn8q07O1tbKLyrWCOGP8AuouKVmVcsqtSAU0EU8GmIUU4UylFAjZ0eb5pID/ENy/UV2ETiSFHBzuANef2kvlXUb56NXbaa37uSPOdrZHHQHn/ABqZFxLtFFFSUZesNgQj1JP6VivWxrfAtz/tEfpWM9NEvchNRtUjVEaokieq8g71YaoWpgQE03NK3BptAgqNhxT80hpgct4z06a90GUQruaMiQj1ArycivfiAarGwtCcm1gJP/TMUmNOx5BZ64LPSJrA2MEjSA4lYcjNaOq3ePB9hD5Mf71VGccrtLdPrXpbadZ/8+kH/fsVmXeh2GqO8FxDhYseWU+XYfaoa1KUjyzR9RXSr77Q1rHcDaV2yDp7j3rptK1hBpmt6mbKHaZYv3A4XoRXUaX4P0rSsusbXEp/jn+bA9h0rS+w2q5jFtCEbBZQgwfrQ0CkeJyyebcvNsVdzFtoHA56V1un+II77UbVRp0EQt4nY7R9/C9PpXoH9mWOP+PO3/79io306zjwyWsKtnGQgFNrQSkeWahcv4j1KMWVhHCwTaEjIGfc102ladf6WI7IaTDPvYM11JgbM8n64rc1Lwfp2oHzIg1ncAYEkHH6Va03w7ZaZaiJVaZurSSksSf6U7Bc5HVbXUJNMmsxocKMG/4+Ny7mHXIHXJrjZoJbaUxTLskABKk8ivbf7Nsn+Z7WFmPJJXvSf2Ppv/Phbf8AfsUW00FdmD4Js3h0CORhjzWLj6V1A4FOVFRQqqAoGAAOBTSKaERyS7Qa57WdbGnpnBZjwqjvW9IhIrkfFVmimK4lYrEqtuIpgihF4uvfN5hj2+mTmuv0fVl1GDeoKsvDKe1eLncXY+Y2CeMnmu+8E3yNciDkSFdp9/Q1Nymj0NXqUNVdRUq0ySUGnUwUoNAEinBBrudIk3wn3UH9K4QGuz0FsoR6Rr/WlIqO5tUUUVBZma4v+hK+PuSDJ9M8Vgsa6u7gFzaSwn+NSAfQ9q5Dccc9ehpomQjGomp5NRsaokjaomqRqjagCBxUWamaq7cGmIXNJmmk0maYDs00mkJqrd3kdpbyTynCIMk0AWWNVIT/AKTOf9rH6CuVf4iacCdsM5H+6KsXniy102KG4lilK3eXUAdBgdal7opJ2OqLcVET++H0rjz8Q9Px/qJ/yFWovF9rNpk+oiGURwsEI7nJFDBJnVhuKhmPyj/eH865D/hYen4/1E35CpbTxrZ6hceRHDKG2l+f9kZ/pTewJO52AagtXEj4h6f/AM+835CnL8QbB2CiCbLHFMVmdqp4p2a4248eWVpdSW7wTFo22kitzSNcttYtvPt92AcEMOQaSBpmvmg0wNS7qYgIFZus6TDrGnSWkrMm7lXXqpq80gAqrLeKnegDzef4f61HPth8iaPPDiTH5g11vhXwidFkN1dTLLclcBU+6n+JrXi1CJ22rIpPoDV6OYNS5R3ZYApaYGp2aBDxTs0zNLmgB46122gKNkpz0Cr+lcXAN0yj3rv9Jtzb2CB873+dvbPalIqO5eoooqCwrl9YtPst2XT/AFcvzD2PcV1FV7y1S8tmiYDJ5U+hoEzjCaYafMjwTNFIpV1OCDURqiBjGoyaeajamBG1QPUxqJqYiI03NK1MJpgBNYniQ/8AEivf+uRrXY1ieJD/AMSK9P8A0yNAHkGcg10Xit1Wz0eMnnyCR+S1yElvcCRsN1Oa6fxhGXtdNjC/vBaLtb06Vk9zXoYBkUNgsBXQ20yp4F1Ek8eevT6iuMNjO55bkV1OnW5j8CXySDcDcLxn6U2wMN7qONQS2fpWv4WkZ9Z83B8loZQpPrsNc6mms0p3HCnkCuj8MxeXqUcOflWKXH/fBob0AxnvoFYjdnmnWdxJczq0SHbGwL89qiGmRli3OAeatpBBA6eXIPnIyFPvQIn1++jTXL1SDkSmu++Hkm7SJX/vS5/SuF1mC3l1273AAmQ5JrvfAcappcyKQVWXAI6dKaEzsw9BemAUpFWQQzOcHFcJ4nvbj7SbdXZEAy2D1rvjHmuO8aIbCNLyOPLOpi3Y+6fX8qTGtzjIbxElBSUhs8EE16X4bvZrmzKzkmWM4JPUivJFODmu/wDAVzcXFxNG2WiROWPb0FSmUzvkapQaiC1IKogkBpwqMU4UAa2h2jXd+qgZA5OewrvgAqgDoBgVjeHNONpZCaT/AFkozjHQVtVDeppFWQUUUUhhRRRQBmatpQv4t8fy3CD5T2b2NcjIrxSNHIhSRT8ynqK9BrP1PSYdSjyfkmUfLIByPr6immJq5xRNRtVi9tJ7CbyrlNuT8rj7r/Q1VJqiBpqJqeTTDTERNUJNTGon60ARNWJ4lONAvf8Arma2mrB8Tt/xIb3/AK5mgDyVpHkxwoGQOnNbvjBzHdaenGRZx1zzyIsi7WLcjrWx4rnE+qR5P+qt44/0z/Ws3ua9DG3vyMrz7VvQsU8DXJzy1yFHH0rnWfa2Oo9c1tPcAeEIYB1kuWf8hQwRjmRyoBb5QfTpWr4ZyNWRic5hlz/3y1YRm2yFT0z+VbOhuEvEbOB5Un/oJpvYEZxGCQCeT60sQzOMeo/nUH2jL8A4zU9mwe4JHOCOaGI0Na51m8P/AE0Nd38PuNHk/wCuprz/AFaZDrN0xwR5pGAcV3/gIr/Y77TlTKcU0J7HaCngVEpqQVRA7FRXNnBeQPBcRLLE4wysODUwpaAOWb4f6I028JcKM/cEnH8s10Fhp1rptuILSFYoxzgd/r61apaLIBQKWkzQMs6ooLOxwqqMk0wFzgV0vh3QWuGW8u0xCpyiN1f3+lTaH4XIK3Worz1WD09z/hXW9BgVLfYpR7h0GBRRRUFhRRRQAUUUUAFFFFAEVxbQ3cLQzxrJG3VWFctf+FZ4SX0+TzU/54ynkfQ/4111FO4WPLpd8MhjnjeGTONsgwf/AK9NJr0y6tLe9hMVzCksZ7MM1gXng23cZsriSBv7rfOv+Ip3RDiccxqJua27rwxq1uCVhSdR3ibn8jWTLb3EJxNbTRn0aM1QrMpvmsbxBb+bol4pJ5iPStlpI84LqD6ZppCMMZBBoEeB3FnJDcFA6ttPUd6ku5p7t2klIMjdT9BivY5fDejzSF30+AsepxiqyeD9FR9xslY+hJx+VTYrmPGDbz5/1i4qyJJ/IigdlaOMkgema9i/4RfR/wDoHQ/kaibwlozPuNin+6MgUWYcx4o9lI0jMGUAnOM1ehkmgRfLKq4Urn2IIr2H/hGNH/6B0H/fNRHwhopYt9iUZ7AnFFmHMeKmymzkSKKsWyTW5Y/KxPfNe0Dwvo+P+QdB/wB802TwlosowbCNf93Iosw5jxeeCe5uXmklXc5ya9V8B2gh0QgPvBfOce1bMfhjRo1AGn25x3Zc1pwxQW0SxxLHHGvRVwAKaTE3ceikVKKjEsecB1P0NSqrtjbFK2emEPNUIUUtTR6fqMzbYtNu2I/6ZkD8zWjD4U1uYAtDDAD/AM9JMn8hQOzMnNMeaOIfO4Fdba+BMnN/qDsP7kC7R+Z5rdsfDek6c4kgs0Mo6SP8zfmaXMh8rOH03RNS1b5oYTBB/wA9ZhjP0HU13Gk+H7PSVDRqZLjGGmfqfp6Vq0VDk2UkkFFFFIYUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUYB6iiigCCSztZRiS2hcf7SA1Ufw/o8n3tNtefSMCtKigDI/4RbQ85/s2H9ajfwlob/8ALio/3WYf1rbop3YHPnwXoZP/AB7OPpK3+NZ2t+AbS60i4i0qV7S+K/uZmcsFb3HpXY0UXYHzvdeCvibb3McKWy3CqeZY7hAHH49K7rwT8P8AV4Jri78VXCSiRdsNnG+RHz1LDqa9Ooo5mFjEHhHRB/y5A/V2/wAacPCuiD/mHxn6k/41s0UXYGP/AMIpoWc/2ZAT7g1PHoOkQnMemWin18oVo0UXYEK2lsgwlvEoHogFSgADAAGKWikAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB/9k=";
            string FAttachmentName = "图片53.jpg";
            var AttObject = new
            {
                FileName = FAttachmentName,
                FormId = "PUR_PurchaseOrder",
                IsLast = true,
                InterId = 100005,
                BillNO = "CGDD000003",
                AliasFileName = FAttachmentName,
                SendByte = base64EncodedData
            };
            string ret = await K3Scoped.Client.UploadToBill(AttObject);


            return Json(ret);
        }
        /// <summary>
        /// 删除附件
        /// https://localhost:5001/Home/DeleteAtta
        /// </summary>
        public async Task<IActionResult> DeleteAtta(CancellationToken cancellation = default)
        {
            return Json(await K3Scoped.Client.DeleteAsync("BOS_Attachment", cancellation, 171867));
        }

        /// <summary>
        /// 生产领料单
        /// https://localhost:5001/Home/PRD_PickMtrlTest
        /// </summary>
        public async Task<IActionResult> PRD_PickMtrlTest()
        {
            List<PRD_PPBOM> ppbomList = await K3Scoped.Client.Queryable<PRD_PPBOM>().Where(p => p.FBillNo == "PPBOM00000006").ToListAsync();
            DateTime ts = DateTime.Today;
            decimal qty = 1m;
            List<PRD_PickMtrlModel_Entity> fEntity = [];
            for (int i = 0; i < ppbomList.Count; i++)
            {
                // 产品编码
                string ParentMaterialId = ppbomList[i].FMaterialID__FNumber;
                // 子项物料编码
                string MaterialId = ppbomList[i].FMaterialID2__FNumber;
                // 单位
                string UnitId = ppbomList[i].FUnitID2__FNumber;
                FEntity_Link entity_link1 = new()
                {
                    FEntity_Link_FFlowId = "81119477-4778-4d0b-94b9-1c43a1c1f768",
                    FEntity_Link_FRuleId = "PRD_PPBOM2PICKMTRL_NORMAL",
                    FEntity_Link_FSTableId = "PRD_PPBOM",
                    FEntity_Link_FSTableName = "T_PRD_PPBOMENTRY",
                    FEntity_Link_FSBillId = ppbomList[i].FID,
                    FEntity_Link_FSId = ppbomList[i].FEntity_FEntryID,
                    FEntity_Link_FBaseActualQtyOld = qty,
                    FEntity_Link_FBaseActualQty = qty,

                };
                List<FEntity_Link> fEntity_Link = [entity_link1];
                PRD_PickMtrlModel_Entity entity1 = new()
                {
                    FSrcBillType = "PRD_PPBOM",
                    FMoBillNo = ppbomList[i].FMOBillNO,
                    FSrcBillNo = ppbomList[i].FBillNo,
                    FActualQty = qty,
                    FMoId = ppbomList[i].FMoId,
                    FPPBomEntryId = ppbomList[i].FEntity_FEntryID,
                    FStockUnitId = UnitId,
                    FParentMaterialId = ParentMaterialId,
                    FParentOwnerId = "100",
                    FMaterialId = MaterialId,
                    FBaseActualQty = qty,
                    FStockAppQty = qty,
                    FBaseAppQty = qty,
                    FUnitID = UnitId,
                    FStockStatusId = "KCZT01_SYS",
                    FOwnerId = "100",
                    FEntrySrcEnteryId = ppbomList[i].FEntity_FEntryID,
                    FEntrySrcInterId = ppbomList[i].FID,
                    FAppQty = qty,
                    FSecActualQty = 0,
                    FStockFlag = "True",
                    FBaseStockActualQty = qty,
                    FStockActualQty = qty,
                    FEntrySrcEntrySeq = ppbomList[i].FEntity_FSeq,
                    FKeeperId = "100",
                    FStockId = "CK002",
                    FPPBomBillNo = ppbomList[i].FBillNo,
                    FEntryWorkShopId = "BM000001",
                    FMoEntryId = ppbomList[i].FMOEntryID,
                    FMoEntrySeq = ppbomList[i].FMOEntrySeq,
                    FBaseUnitId = UnitId,
                    FEntity_Link = fEntity_Link
                };
                if (entity1.FMaterialId == "30002")
                {
                    entity1.FLot = "123";
                }
                fEntity.Add(entity1);
            }

            K3SaveParam<PRD_PickMtrlModel> data = new()
            {
                NeedReturnFields = ["FId"],
                IsDeleteEntry = "true",
                SubSystemId = "",
                IsEntryBatchFill = "true",
                IsAutoAdjustField = "false",
                Model = new PRD_PickMtrlModel
                {
                    //FID = 100044,
                    FDate = ts,
                    FBillType = K3Number.Parse("SCLLD01_SYS"),
                    FPrdOrgId = K3Number.Parse("100"),
                    FStockOrgId = K3Number.Parse("100"),
                    FEntity = fEntity
                },
            };
            Console.WriteLine(data.ToJson());
            K3ApiResult<K3SaveResult> result = await K3Scoped.Client.SaveAsync(data);
            List<PRD_PickMtrlModel> result1 = await K3Scoped.Client.Queryable<PRD_PickMtrlModel>().ToListAsync();
            return Json(result1);
        }
        /// <summary>
        /// 采购订单关联采购申请单
        /// https://localhost:5001/Home/PUR_PurchaseOrderTest
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> PUR_PurchaseOrderTest()
        {
            List<PUR_Requisition> reqList = await K3Scoped.Client.Queryable<PUR_Requisition>().Where(t => t.FBillNo == "CGSQ000003").ToListAsync();
            DateTime ts = DateTime.Today;
            DateTime ts2 = ts.AddMonths(1);
            List<FPOOrderEntry_Entity> fEntity = [];
            for (int i = 0; i < reqList.Count; i++)
            {
                // 批准数量
                decimal qty = (decimal)reqList[i].FApproveQty;
                // 物料编码
                string MaterialId = reqList[i].FMaterialId__FNumber;
                // 单位
                string UnitId = reqList[i].FUnitId__FNumber;
                FPOOrderEntry_Link entity_link1 = new()
                {
                    FPOOrderEntry_Link_FRuleId = "PUR_Requisition-PUR_PurchaseOrder",
                    FPOOrderEntry_Link_FSTableName = "T_PUR_ReqEntry",
                    FPOOrderEntry_Link_FSBillId = reqList[i].FID,
                    FPOOrderEntry_Link_FSId = reqList[i].FEntity_FEntryID,
                    FPOOrderEntry_Link_FBaseUnitQtyOld = qty,
                    FPOOrderEntry_Link_FBaseUnitQty = qty,
                    FPOOrderEntry_Link_FSalBaseQtyOld = 0m,
                    FPOOrderEntry_Link_FSalBaseQty = qty,
                    FPOOrderEntry_Link_FStockBaseQtyOld = qty,
                    FPOOrderEntry_Link_FStockBaseQty = qty
                };
                List<FPOOrderEntry_Link> fEntity_Link = [entity_link1];
                FPOOrderEntry_Entity entity1 = new()
                {
                    FMaterialId = MaterialId,
                    FUnitId = UnitId,
                    FQty = qty,
                    FPriceUnitId = UnitId,
                    FPriceUnitQty = qty,
                    FPriceBaseQty = qty,
                    FDeliveryDate = ts2,
                    FPrice = 88.495575m,
                    FTaxPrice = 100,
                    FEntryDiscountRate = 0,
                    FEntryTaxRate = 13,
                    FRequireOrgId = "100",
                    FReceiveOrgId = "100",
                    FEntrySettleOrgId = "100",
                    FGiveAway = false,
                    FStockUnitID = UnitId,
                    FStockQty = qty,
                    FStockBaseQty = qty,
                    FDeliveryControl = false,
                    FTimeControl = false,
                    FDeliveryMaxQty = qty,
                    FDeliveryMinQty = qty,
                    FDeliveryBeforeDays = 0,
                    FDeliveryDelayDays = 0,
                    FDeliveryEarlyDate = ts2,
                    FDeliveryLastDate = ts2,
                    FPriceCoefficient = 1,
                    FConsumeSumQty = 0,
                    FSrcBillTypeId = "PUR_Requisition",
                    FSrcBillNo = reqList[i].FBillNo,
                    FDEMANDBILLENTRYSEQ = 0,
                    FDEMANDBILLENTRYID = 0,
                    FPlanConfirm = true,
                    FSalUnitID = UnitId,
                    FSalQty = qty,
                    FSalJoinQty = 0,
                    FBaseSalJoinQty = 0,
                    FInventoryQty = 0,
                    FCentSettleOrgId = "100",
                    FDispSettleOrgId = "100",
                    FGroup = 0,
                    FDeliveryStockStatus = "KCZT02_SYS",
                    FMaxPrice = 0,
                    FMinPrice = 0,
                    FIsStock = false,
                    FBaseConsumeSumQty = 0,
                    FSalBaseQty = qty,
                    FSubOrgId = "100",
                    FEntryPayOrgId = "100",
                    FPriceDiscount = 0,
                    FAllAmountExceptDisCount = 2000,
                    FEntryDeliveryPlan =
                    [
                          new FEntryDeliveryPlan_Entity
                          {
                              FDeliveryDate_Plan = ts2,
                              FPlanQty = qty,
                              FSUPPLIERDELIVERYDATE = ts2,
                              FPREARRIVALDATE = ts2,
                              FTRLT = 0,
                              FConfirmDeliQty = 0
                          }
                    ],
                    FPOOrderEntry_Link = fEntity_Link
                };
                fEntity.Add(entity1);
            }

            FPOOrderFinance_Entity finance = new()
            {
                FSettleCurrId = "PRE001",
                FExchangeTypeId = "HLTX01_SYS",
                FExchangeRate = 1,
                FPriceTimePoint = "1",
                FFOCUSSETTLEORGID = "100",
                FIsIncludedTax = true,
                FISPRICEEXCLUDETAX = true,
                FLocalCurrId = "PRE001",
                FPAYADVANCEAMOUNT = 0,
                FSupToOderExchangeBusRate = 1,
                FSEPSETTLE = false,
                FDepositRatio = 0,
                FAllDisCount = 0,
                FUPPERBELIEL = 0
            };

            K3SaveParam<PUR_PurchaseOrderModel> data = new()
            {
                IsDeleteEntry = "true",
                SubSystemId = "",
                IsEntryBatchFill = "true",
                IsAutoAdjustField = "false",
                Model = new PUR_PurchaseOrderModel
                {
                    FBillTypeID = "CGDD01_SYS",
                    FDate = ts,
                    FSupplierId = "VEN00002",
                    FPurchaseOrgId = "100",
                    FProviderId = "VEN00002",
                    FSettleId = "VEN00002",
                    FChargeId = "VEN00002",
                    FIsModificationOperator = false,
                    FChangeStatus = "A",
                    FACCTYPE = "Q",
                    FIsMobBill = false,
                    FPOOrderFinance = finance,
                    FPOOrderPay = new FPOOrderPay_Entity(),
                    FPOOrderEntry = fEntity,
                }
            };
            Debug.WriteLine(data.ToJson());
            K3ApiResult<K3SaveResult> result = await K3Scoped.Client.SaveAsync(data);
            return Json(result);
        }

        /// <summary>
        /// 采购申请单下推采购订单
        /// https://localhost:5001/Home/PUR_PurchaseOrderPushTest
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> PUR_PurchaseOrderPushTest()
        {
            var queryResult = await K3Scoped.Client.Queryable<PUR_Requisition>().Where(t => t.FBillNo == "CGSQ000003").Select(t => new { t.FEntity_FEntryID }).ToListAsync();
            string entryIds = string.Join(',', queryResult.Select(t => t.FEntity_FEntryID).ToArray());
            K3ApiResult<K3SaveResult> pushResult = await K3Scoped.Client.PushAsync("PUR_Requisition", entryIds, "PUR_Requisition-PUR_PurchaseOrder");
            return Json(pushResult);
        }

        /// <summary>
        /// https://localhost:5001/Home/GetUserList
        /// </summary>
        public async Task<IActionResult> GetUserList()
        {
            List<SEC_User> usrList = await K3Scoped.Client.ExecuteBillQueryAsync<SEC_User>();
            return Json(usrList);
        }
        /// <summary>
        /// 工序下推
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> GX()
        {
            SFC_OperationPlanning entrty = await K3Scoped.Client.Queryable<SFC_OperationPlanning>().OrderBy(t => new { t.FBILLNO, t.FSubEntity_FSeq }, [OrderType.Descending, OrderType.Ascending]).FirstAsync();
            List<Dictionary<string, object>> customers =
            [
                new Dictionary<string, object>
                {
                    { "EntryId", entrty.FEntity_FENTRYID },
                    { "DetailId",entrty.FSubEntity_FDetailID },
                    { "F_ZRHB_Qty1", 1 }
                }
            ];
            GXJH datas = new()
            {
                AutoAudit = true,
                Datas = [
                    new GXJH.GXDatas
                    {
                        Id = entrty.FID,
                        DetailIds = [
                            new GXJH.GXDetailIds
                            {
                                EntryId = entrty.FEntity_FENTRYID,
                                DetailId = entrty.FSubEntity_FDetailID,
                                QuaQty = 1,
                                FinishQty = 1,
                                //EpmIds=new List<GXJH.GXEpmIds>
                                //{
                                //    new GXJH.GXEpmIds
                                //    {
                                //        EpmId=100616
                                //    }
                                //}
                            }
                        ],
                        Sst_Customers = customers
                    }
                ]
            };
            string s = await K3Scoped.Client.ExecuteAsync("Kingdee.K3.MFG.WebApi.ServicesStub.OptPlanOptRtpApiService.Push", datas);
            return Content(s);
        }
        /// <summary>
        /// 工序下推
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> GX2()
        {
            SFC_OperationPlanning entrty = await K3Scoped.Client.Queryable<SFC_OperationPlanning>().OrderBy(t => new { t.FBILLNO, t.FSubEntity_FSeq }, [OrderType.Descending, OrderType.Ascending]).FirstAsync();
            List<Dictionary<string, object>> customers =
            [
                new Dictionary<string, object>
                {
                    { "EntryId", entrty.FEntity_FENTRYID },
                    { "DetailId",entrty.FSubEntity_FDetailID },
                    { "F_ZRHB_Qty1", 1 }
                }
            ];
            GXJH datas = new()
            {
                AutoAudit = true,
                Datas = [
                    new GXJH.GXDatas
                    {
                        Id = entrty.FID,
                        DetailIds = [
                            new GXJH.GXDetailIds
                            {
                                EntryId = entrty.FEntity_FENTRYID,
                                DetailId = entrty.FSubEntity_FDetailID,
                                QuaQty = 1,
                                FinishQty = 1,
                                //EpmIds = new List<GXJH.GXEpmIds>
                                //{
                                //    new GXJH.GXEpmIds
                                //    {
                                //        EpmId = 100616
                                //    }
                                //}
                            }
                        ],
                        Sst_Customers = customers
                    }
                ]
            };
            string s = await K3CloudApiSdk.Builder().Execute("Kingdee.K3.MFG.WebApi.ServicesStub.OptPlanOptRtpApiService.Push", JsonConvert.SerializeObject(new { parameters = new object[1] { datas } }));
            return Content(s);
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet]
        public async Task<IActionResult> OutStockApplyAsync()
        {
            string[] FDepts = ["04", "06", "07", "08", "09", "10", "11"];
            Expression<Func<STK_OutStockApply, bool>> t = t => t.FDate > DateTime.Parse("2023-07-10") && t.FDate < DateTime.Parse("2023-07-14");
            t = t.AndFilter(t => t.FDocumentStatus == "C");
            t = t.AndFilter(t => FDepts.Contains(t.FDeptId__FNumber));
            List<STK_OutStockApply> result = await K3Scoped.Client.Queryable<STK_OutStockApply>()
                .Where(t)
                .ToPageListAsync(1, 20);
            return Json(result);
        }

        public async Task<IActionResult> ITestAsync()
        {
            List<PRD_PickMtrlModel_Entity> result = await K3Scoped.Client.Queryable<PRD_PickMtrlModel_Entity>().ToListAsync();
            return Json(result);
        }

        /// <summary>
        /// 物料收发明细表
        /// </summary>
        public async Task<IActionResult> ReportDataAsync()
        {
            DateTime tmp = DateTime.Today.AddMonths(-1);
            DateTime ts = new(tmp.Year, tmp.Month, 1);
            string ts1 = ts.ToShortDateString();
            string ts2 = DateTime.Today.ToShortDateString();
            // 物料收发明细表 查询没有记录请更换过滤方案FSCHEMEID
            // 过滤方案查询SQL: SELECT FSCHEMEID,FSCHEMENAME FROM T_BAS_FILTERSCHEME WHERE FFORMID='STK_StockDetailRpt'
            ReportParamModel data = new()
            {
                FORMID = "STK_StockDetailRpt",
                FSCHEMEID = "62db7daabeacac",
                FieldKeys = "FBILLTYPE,FBILLNO,FBILLNAME,FBillSeqId,FMATERIALID,FMATERIALNAME,FBASEQCQTY,FBASEINQTY,FBASEOUTQTY,FBASEJCQTY",
                QuicklyCondition =
                [
                    new ReportQuicklyConditionModel
                    {
                        FieldName = "BeginDate",
                        FieldValue = ts1
                    },
                    new ReportQuicklyConditionModel
                    {
                        FieldName = "EndDate",
                        FieldValue = ts2
                    },
                    //new ReportQuicklyConditionModel
                    //{
                    //    FieldName = "BeginMaterialId",
                    //    FieldValue = "ABGAT0001-0201"
                    //},
                    // new ReportQuicklyConditionModel
                    //{
                    //    FieldName = "EndMaterialId",
                    //    FieldValue = "ABGAT0001-0201"
                    //}
                ],
                CurQueryId = Guid.NewGuid().ToString()
            };
            System.Data.DataTable result = await K3Scoped.Client.GetSCMReportDataTableAsync(data);
            return Json(result);
        }

        /// <summary>
        /// 采购订单执行明细表
        /// https://localhost:5001/Home/Report
        /// </summary>
        public async Task<IActionResult> ReportAsync()
        {
            string formid = "PUR_PurchaseOrderDetailRpt";
            SysReportModel data = new()
            {
                FieldKeys = "FDATE,FMATERIALID,FMATERIALNAME,FORDERQTY",
                SchemeId = "",
                StartRow = 0,
                Limit = 200,
                IsVerifyBaseDataField = true,
                Model = new
                {
                    FPurchaseOrgIdList = "1",
                    FOrderStartDate = DateTime.Parse("2021-07-01"),
                    FOrderEndDate = DateTime.Today,
                    FBusinessType = "CG",
                    FDocumentStatus = "C",
                    FLineStatus = "C"
                }
            };
            //var result = await K3Scoped.Client.GetSysReportDataAsync(formid, data);
            string result = await K3CloudApiSdk.Builder().GetSysReportData(formid, data.ToJson());
            return Json(result);
        }

        [HttpGet]
        public async Task<IActionResult> QueryAsync(string FormId)
        {
            K3ApiResult<K3BusinessInfoResult> k3BusinessInfoResult = await K3Scoped.Client.GetBusinessInfoAsync(FormId);
            if (!k3BusinessInfoResult.Result.ResponseStatus.IsSuccess)
            {
                return NotFound();
            }
            int LanguageId1 = 2052;
            int LanguageId2 = 1033;
            List<JavaPropertyInfo> pyList = [];
            K3BusinessNeedReturn needReturnData = k3BusinessInfoResult.Result.NeedReturnData;
            string FormName = string.Empty;
            K3LanguageTranslate kl = needReturnData.Name.Find(x => x.Lcid == LanguageId1);
            FormName = kl != null ? kl.Value : needReturnData.Name.Find(x => x.Lcid == LanguageId2).Value;
            ViewBag.Title = FormName;
            string pkFieldName = needReturnData.PkFieldName;
            Type pkFieldType = needReturnData.PkFieldType;
            pyList.Add(new JavaPropertyInfo
            {
                Name = pkFieldName,
                Type = K3BusinessToCSharpTypeForJava.GetJavaType(pkFieldType),
                Comment = pkFieldName
            });
            List<K3Table> entrys = needReturnData.Entrys;
            foreach (K3Table entry in entrys)
            {
                string entryName = string.Empty;
                K3LanguageTranslate ekl = entry.Name.Find(x => x.Lcid == LanguageId1);
                entryName = ekl != null ? ekl.Value : entry.Name.Find(x => x.Lcid == LanguageId2).Value;
                string entryKey = entry.Key;
                string entryPkFieldName = entry.EntryPkFieldName;
                Type entryPkFieldType = entry.PkFieldType;
                string seqFieldKey = entry.SeqFieldKey;
                if (!string.IsNullOrEmpty(seqFieldKey))
                {
                    pyList.Add(new JavaPropertyInfo
                    {
                        Name = entryKey + "_" + entryPkFieldName,
                        Type = K3BusinessToCSharpTypeForJava.GetJavaType(entryPkFieldType),
                        Comment = entryName + "主键" + entryPkFieldName
                    });
                    pyList.Add(new JavaPropertyInfo
                    {
                        Name = entryKey + "_" + seqFieldKey,
                        Type = "Long",
                        Comment = entryName + "序列"
                    });

                }
                List<K3FieldResult> fields = entry.Fields;
                // 遍历fields
                foreach (K3FieldResult field in fields)
                {
                    // 获取field的Name的LanguageId语言值
                    string fieldName = string.Empty;
                    K3LanguageTranslate klv = field.Name.Find(x => x.Lcid == LanguageId1);
                    fieldName = klv != null ? klv.Value : field.Name.Find(x => x.Lcid == LanguageId2).Value;
                    string key = field.Key;
                    int elementType = field.ElementType;
                    SqlStorageType fieldType = field.FieldType;
                    int mustInput = field.MustInput;
                    string mustInputStr = mustInput == 1 ? "" : "?";
                    if (fieldType == SqlStorageType.Sqlchar)
                    {
                        Debug.WriteLine(key);
                    }
                    string fieldTypeString = K3BusinessToCSharpTypeForJava.GetJavaType(fieldType);
                    pyList.Add(new JavaPropertyInfo
                    {
                        Name = key,
                        Type = fieldTypeString,
                        Comment = fieldName
                    });
                    string lookUpObjectFormId = field.LookUpObjectFormId;
                    if (!string.IsNullOrEmpty(lookUpObjectFormId))
                    {
                        K3ApiResult<K3BusinessInfoResult> subBusinessInfo = await K3Scoped.Client.GetBusinessInfoAsync(lookUpObjectFormId);
                        List<K3FieldResult> subFields = subBusinessInfo.Result.NeedReturnData.Entrys
                            .Where(t => t.Fields.Any(x => x.Key is "FNumber" or "FName" or "FShortName" or "FDataValue"))
                            .Select(t => t.Fields).FirstOrDefault();
                        if (subFields == null) { continue; }
                        int subFieldsCount = 0;
                        for (int i = 0; i < subFields.Count; i++)
                        {
                            if (subFieldsCount >= 3) { break; }
                            if (subFields[i].Key.Equals("FNumber") || subFields[i].Key.Equals("FName") || subFields[i].Key.Equals("FShortName") || subFields[i].Key.Equals("FDataValue"))
                            {
                                string summaryTitle = string.Empty;
                                K3LanguageTranslate slv = subFields[i].Name.Find(t => t.Lcid == LanguageId1);
                                summaryTitle = slv != null ? slv.Value : subFields[i].Name.Find(x => x.Lcid == LanguageId2).Value;
                                pyList.Add(new JavaPropertyInfo
                                {
                                    Name = key + "_" + subFields[i].Key,
                                    Type = "String",
                                    Comment = fieldName + summaryTitle,
                                    SrcName = key + "." + subFields[i].Key
                                });
                                subFieldsCount++;
                            }
                        }
                    }
                }
            }
            List<string> layFieldList = [];
            foreach (JavaPropertyInfo item in pyList)
            {
                layFieldList.Add("{field: '" + (string.IsNullOrEmpty(item.SrcName) ? item.Name : item.SrcName) + "', title: '" + item.Comment + "', edit: 'text'}");
            }
            ViewBag.PyList = "[[" + string.Join(',', layFieldList) + "]]";
            ViewBag.FormId = FormId;
            ViewBag.Fields = string.Join(',', pyList.Select(t => string.IsNullOrEmpty(t.SrcName) ? t.Name : t.SrcName));
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> QueryAsync([FromQuery] string FormId, [FromForm] string FieldKeys, [FromForm] int page, [FromForm] int limit, [FromForm] string jsonStr = "[]", CancellationToken cancellation = default)
        {
            JsonDocument jsonDocument = JsonDocument.Parse(jsonStr);
            string filterString = ExpressionBuilder.ParseExpressionOf(jsonDocument);
            string[] Fields = FieldKeys.Split(',');
            string k3Result = await K3Scoped.Client.NativeBillQueryAsync(FormId, FieldKeys, filterString, "", 0, ((page <= 0 ? 1 : page) - 1) * limit, limit, cancellation);
            int k3Count = await K3Scoped.Client.GetTotalCountAsync(FormId, filterString, Fields.Where(t => t.Contains("Qty")).FirstOrDefault(), cancellation);
            Dictionary<string, decimal> k3Sum = await K3Scoped.Client.GetSumAsync(FormId, filterString, cancellation, Fields.Where(t => t.Contains("Qty")).Take(3).ToArray());
            var result = new
            {
                code = 0,
                count = k3Count,
                totalRow = k3Sum,
                data = JArray.Parse(k3Result)
            };
            return Json(result);
        }
    }
}