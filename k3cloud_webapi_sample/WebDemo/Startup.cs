using K3Cloud.WebApi.Core.IoC;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Text.Encodings.Web;
using System.Text.Json.Serialization;
using System.Text.Unicode;
using WebDemo.Helpers;

namespace WebDemo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            _ = services.AddSingleton(HtmlEncoder.Create(UnicodeRanges.All));
            _ = services.AddControllersWithViews()
                 .AddJsonOptions(options =>
                 {
                     options.JsonSerializerOptions.Converters.Add(new EnumToStringConverter());
                     options.JsonSerializerOptions.Converters.Add(new DateTimeConverter());
                     options.JsonSerializerOptions.PropertyNamingPolicy = null;
                 });
                //.AddNewtonsoftJson(options =>
                //{
                //    options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                //    options.SerializerSettings.Formatting = Formatting.None;
                //    options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                //    options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
                //});
            //_ = services.AddStackExchangeRedisCache(options =>
            //{
            //    options.Configuration = "localhost,abortConnect=false,defaultDatabase=2";
            //    options.InstanceName = "test_";
            //});
            //_ = services.AddSingleton<ICacheService, MyRedisCache>();
            _ = services.AddK3Sugar();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            _ = env.IsDevelopment() ? app.UseDeveloperExceptionPage() : app.UseExceptionHandler("/Home/Error");
            _ = app.UseStaticFiles();

            _ = app.UseRouting();

            _ = app.UseAuthorization();

            _ = app.UseEndpoints(endpoints =>
            {
                _ = endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
