﻿#region License
/***
 * Copyright © 2018-2021, 张强 (943620963@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * without warranties or conditions of any kind, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#endregion

using SQLBuilder.Entry;
using SQLBuilder.Enums;
using SQLBuilder.Extensions;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace SQLBuilder.Expressions
{
    /// <summary>
    /// 表示访问字段或属性
    /// </summary>
	public class MemberExpressionResolver : BaseExpression<MemberExpression>
    {
        #region Insert
        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper Insert(MemberExpression expression, SqlWrapper sqlWrapper)
        {
            List<object> objectArray = [];
            List<string> fields = [];
            object convertRes = expression.ToObject();
            if (convertRes.IsNull())
            {
                return sqlWrapper;
            }

            if (!convertRes.GetType().IsDictionaryType() &&
                convertRes is IEnumerable collection)
            {
                foreach (object item in collection)
                {
                    objectArray.Add(item);
                }
            }
            else
            {
                objectArray.Add(convertRes);
            }

            for (int i = 0; i < objectArray.Count; i++)
            {
                if (sqlWrapper.DatabaseType != DatabaseType.Oracle)
                {
                    _ = sqlWrapper.Append("(");
                }

                if (i > 0 && sqlWrapper.DatabaseType == DatabaseType.Oracle)
                {
                    _ = sqlWrapper.Append(" UNION ALL SELECT ");
                }

                System.Type objectType = objectArray[i]?.GetType();
                bool isDictionaryType = objectType.IsDictionaryType();

                PropertyInfo[] properties;
                IDictionary<string, object> objectDic = null;

                if (!isDictionaryType)
                {
                    properties = objectType?.GetProperties();
                }
                else
                {
                    properties = sqlWrapper.DefaultType.GetProperties();
                    objectDic = objectArray[i] as IDictionary<string, object>;
                }

                foreach (PropertyInfo property in properties)
                {
                    if (isDictionaryType && !objectDic.Any(x => x.Key.EqualIgnoreCase(property.Name)))
                    {
                        continue;
                    }

                    System.Type type = property.DeclaringType.IsAnonymousType() || isDictionaryType ?
                        sqlWrapper.DefaultType :
                        property.DeclaringType;

                    (string columnName, bool isInsert, bool isUpdate) = sqlWrapper.GetColumnInfo(type, property);
                    if (isInsert)
                    {
                        object value = isDictionaryType
                            ? objectDic.FirstOrDefault(x => x.Key.EqualIgnoreCase(property.Name)).Value
                            : property.GetValue(objectArray[i], null);
                        if (value != null || (sqlWrapper.IsEnableNullValue && value == null))
                        {
                            sqlWrapper.AddDbParameter(value);

                            if (!fields.Contains(columnName))
                            {
                                fields.Add(columnName);
                            }

                            sqlWrapper += ",";
                        }
                    }
                }
#if NETSTANDARD2_1_OR_GREATER
                if (sqlWrapper[^1] == ',')
#else
                if (sqlWrapper[sqlWrapper.Length - 1] == ',')
#endif
                {
                    _ = sqlWrapper.Remove(sqlWrapper.Length - 1, 1);
                    _ = sqlWrapper.DatabaseType != DatabaseType.Oracle ? sqlWrapper.Append("),") : sqlWrapper.Append(" FROM DUAL");
                }
            }

            _ = sqlWrapper.RemoveLast(',');

            _ = sqlWrapper.Reset(string.Format(sqlWrapper.ToString(), string.Join(",", fields).TrimEnd(',')));

            return sqlWrapper;
        }
        #endregion

        #region Update
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper Update(MemberExpression expression, SqlWrapper sqlWrapper)
        {
            object convertRes = expression.ToObject();
            if (convertRes.IsNull())
            {
                return sqlWrapper;
            }

            System.Type convertType = convertRes?.GetType();
            bool isDictionaryType = convertType.IsDictionaryType();

            PropertyInfo[] properties;
            IDictionary<string, object> convertDic = null;

            if (!isDictionaryType)
            {
                properties = convertType?.GetProperties();
            }
            else
            {
                properties = sqlWrapper.DefaultType.GetProperties();
                convertDic = convertRes as IDictionary<string, object>;
            }

            if (properties.IsNotNullOrEmpty())
            {
                foreach (PropertyInfo item in properties)
                {
                    if (isDictionaryType && !convertDic.Any(x => x.Key.EqualIgnoreCase(item.Name)))
                    {
                        continue;
                    }

                    System.Type type = item.DeclaringType.IsAnonymousType() || isDictionaryType ?
                        sqlWrapper.DefaultType :
                        item.DeclaringType;

                    (string columnName, bool isInsert, bool isUpdate) = sqlWrapper.GetColumnInfo(type, item);
                    if (isUpdate)
                    {
                        object value = isDictionaryType ? convertDic.FirstOrDefault(x => x.Key.EqualIgnoreCase(item.Name)).Value : item.GetValue(convertRes, null);
                        if (value != null || (sqlWrapper.IsEnableNullValue && value == null))
                        {
                            sqlWrapper += columnName + " = ";
                            sqlWrapper.AddDbParameter(value);
                            sqlWrapper += ",";
                        }
                    }
                }
            }

            _ = sqlWrapper.RemoveLast(',');

            return sqlWrapper;
        }
        #endregion

        #region Select
        /// <summary>
        /// Select
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper Select(MemberExpression expression, SqlWrapper sqlWrapper)
        {
            if (expression.Expression.NodeType != ExpressionType.Constant)
            {
                System.Type type = expression.Expression.Type != expression.Member.DeclaringType ?
                           expression.Expression.Type :
                           expression.Member.DeclaringType;

                string tableName = sqlWrapper.GetTableName(type);
                ParameterExpression parameter = expression.Expression as ParameterExpression;
                string tableAlias = sqlWrapper.GetTableAlias(tableName, parameter?.Name);

                if (tableAlias.IsNotNullOrEmpty())
                {
                    tableAlias += ".";
                }

                _ = sqlWrapper.AddField(tableAlias + sqlWrapper.GetColumnInfo(expression.Member.DeclaringType, expression.Member).columnName);
            }
            else
            {
                string field = expression.ToObject()?.ToString();
                _ = sqlWrapper.AddField(field);
            }

            return sqlWrapper;
        }
        #endregion

        #region Join
        /// <summary>
        /// Join
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper Join(MemberExpression expression, SqlWrapper sqlWrapper)
        {
            System.Type type = expression.Expression.Type != expression.Member.DeclaringType ?
                       expression.Expression.Type :
                       expression.Member.DeclaringType;

            string tableName = sqlWrapper.GetTableName(type);
            ParameterExpression parameter = expression.Expression as ParameterExpression;
            string tableAlias = sqlWrapper.GetTableAlias(tableName, parameter?.Name);

            if (tableAlias.IsNotNullOrEmpty())
            {
                tableAlias += ".";
            }

            sqlWrapper += tableAlias + sqlWrapper.GetColumnInfo(expression.Member.DeclaringType, expression.Member).columnName;

            return sqlWrapper;
        }
        #endregion

        #region Max
        /// <summary>
        /// Max
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper Max(MemberExpression expression, SqlWrapper sqlWrapper)
        {
            if (expression?.Member != null)
            {
                string columnName = sqlWrapper.GetColumnInfo(expression.Member.DeclaringType, expression.Member).columnName;
                _ = sqlWrapper.AddField(columnName);
            }

            return sqlWrapper;
        }
        #endregion

        #region Min
        /// <summary>
        /// Min
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper Min(MemberExpression expression, SqlWrapper sqlWrapper)
        {
            if (expression?.Member != null)
            {
                string columnName = sqlWrapper.GetColumnInfo(expression.Member.DeclaringType, expression.Member).columnName;
                _ = sqlWrapper.AddField(columnName);
            }

            return sqlWrapper;
        }
        #endregion

        #region Avg
        /// <summary>
        /// Avg
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper Avg(MemberExpression expression, SqlWrapper sqlWrapper)
        {
            if (expression?.Member != null)
            {
                string columnName = sqlWrapper.GetColumnInfo(expression.Member.DeclaringType, expression.Member).columnName;
                _ = sqlWrapper.AddField(columnName);
            }

            return sqlWrapper;
        }
        #endregion

        #region Count
        /// <summary>
        /// Count
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper Count(MemberExpression expression, SqlWrapper sqlWrapper)
        {
            if (expression?.Member != null)
            {
                string columnName = sqlWrapper.GetColumnInfo(expression.Member.DeclaringType, expression.Member).columnName;
                _ = sqlWrapper.AddField(columnName);
            }

            return sqlWrapper;
        }
        #endregion

        #region Sum
        /// <summary>
        /// Sum
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper Sum(MemberExpression expression, SqlWrapper sqlWrapper)
        {
            if (expression?.Member != null)
            {
                string columnName = sqlWrapper.GetColumnInfo(expression.Member.DeclaringType, expression.Member).columnName;
                _ = sqlWrapper.AddField(columnName);
            }

            return sqlWrapper;
        }
        #endregion

        #region Where
        /// <summary>
        /// Where
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper Where(MemberExpression expression, SqlWrapper sqlWrapper)
        {
            //此处判断expression的Member是否是可空值类型
            if (expression.Expression?.NodeType == ExpressionType.MemberAccess && expression.Member.DeclaringType.IsNullable())
            {
                expression = expression.Expression as MemberExpression;
            }

            if (expression != null)
            {
                if (expression.Expression?.NodeType == ExpressionType.Parameter)
                {
                    System.Type type = expression.Expression.Type != expression.Member.DeclaringType ?
                               expression.Expression.Type :
                               expression.Member.DeclaringType;

                    string tableName = sqlWrapper.GetTableName(type);
                    string tableAlias = (expression.Expression as ParameterExpression)?.Name;
                    tableAlias = sqlWrapper.GetTableAlias(tableName, tableAlias);

                    if (tableAlias.IsNotNullOrEmpty())
                    {
                        tableAlias += ".";
                    }

                    sqlWrapper += tableAlias + sqlWrapper.GetColumnInfo(expression.Member.DeclaringType, expression.Member).columnName;

                    //字段是bool类型
                    if (expression.NodeType == ExpressionType.MemberAccess && expression.Type.GetCoreType() == typeof(bool))
                    {
                        sqlWrapper += " = 1";
                    }
                }
                else
                {
                    sqlWrapper.AddDbParameter(expression.ToObject());
                }
            }

            return sqlWrapper;
        }
        #endregion

        #region In
        /// <summary>
        /// In
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper In(MemberExpression expression, SqlWrapper sqlWrapper)
        {
            object convertRes = expression.ToObject();
            if (convertRes is IEnumerable collection)
            {
                sqlWrapper += "(";
                foreach (object item in collection)
                {
                    SqlExpressionProvider.In(Expression.Constant(item), sqlWrapper);
                    sqlWrapper += ",";
                }

                _ = sqlWrapper.RemoveLast(',');

                sqlWrapper += ")";
            }

            return sqlWrapper;
        }
        #endregion

        #region GroupBy
        /// <summary>
        /// GroupBy
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper GroupBy(MemberExpression expression, SqlWrapper sqlWrapper)
        {
            string tableAlias = string.Empty;
            string tableName = string.Empty;

            if (expression.Expression.NodeType == ExpressionType.Parameter)
            {
                System.Type type = expression.Expression.Type != expression.Member.DeclaringType ?
                           expression.Expression.Type :
                           expression.Member.DeclaringType;
                tableName = sqlWrapper.GetTableName(type);
                tableAlias = (expression.Expression as ParameterExpression)?.Name;
            }

            if (expression.Expression.NodeType == ExpressionType.Constant)
            {
                tableName = sqlWrapper.GetTableName(sqlWrapper.DefaultType);
            }

            tableAlias = sqlWrapper.GetTableAlias(tableName, tableAlias);

            if (tableAlias.IsNotNullOrEmpty())
            {
                tableAlias += ".";
            }

            if (expression.Expression.NodeType == ExpressionType.Parameter)
            {
                sqlWrapper += tableAlias + sqlWrapper.GetColumnInfo(expression.Member.DeclaringType, expression.Member).columnName;
            }

            if (expression.Expression.NodeType is ExpressionType.Constant or
                ExpressionType.MemberAccess)
            {
                object convertRes = expression.ToObject();
                if (convertRes != null)
                {
                    if (convertRes is IList collection)
                    {
                        foreach (object item in collection)
                        {
                            SqlExpressionProvider.GroupBy(Expression.Constant(item), sqlWrapper);

                            sqlWrapper += ",";
                        }

                        _ = sqlWrapper.RemoveLast(',');
                    }

                    if (typeof(string) == convertRes.GetType() && convertRes is string str)
                    {
                        SqlExpressionProvider.GroupBy(Expression.Constant(str), sqlWrapper);
                    }
                }
            }

            return sqlWrapper;
        }
        #endregion

        #region Having
        /// <summary>
        /// Having
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper Having(MemberExpression expression, SqlWrapper sqlWrapper)
        {
            //此处判断expression的Member是否是可空值类型
            if (expression.Expression?.NodeType == ExpressionType.MemberAccess && expression.Member.DeclaringType.IsNullable())
            {
                expression = expression.Expression as MemberExpression;
            }

            if (expression != null)
            {
                if (expression.Expression?.NodeType == ExpressionType.Parameter)
                {
                    System.Type type = expression.Expression.Type != expression.Member.DeclaringType ?
                               expression.Expression.Type :
                               expression.Member.DeclaringType;

                    string tableName = sqlWrapper.GetTableName(type);
                    string tableAlias = (expression.Expression as ParameterExpression)?.Name;
                    tableAlias = sqlWrapper.GetTableAlias(tableName, tableAlias);

                    if (tableAlias.IsNotNullOrEmpty())
                    {
                        tableAlias += ".";
                    }

                    sqlWrapper += tableAlias + sqlWrapper.GetColumnInfo(expression.Member.DeclaringType, expression.Member).columnName;

                    //字段是bool类型
                    if (expression.NodeType == ExpressionType.MemberAccess && expression.Type.GetCoreType() == typeof(bool))
                    {
                        sqlWrapper += " = 1";
                    }
                }
                else
                {
                    sqlWrapper.AddDbParameter(expression.ToObject());
                }
            }

            return sqlWrapper;
        }
        #endregion

        #region OrderBy
        /// <summary>
        /// OrderBy
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <param name="orders">排序方式</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper OrderBy(MemberExpression expression, SqlWrapper sqlWrapper, params OrderType[] orders)
        {
            string tableAlias = string.Empty;
            string tableName = string.Empty;

            if (expression.Expression.NodeType == ExpressionType.Parameter)
            {
                System.Type type = expression.Expression.Type != expression.Member.DeclaringType ?
                           expression.Expression.Type :
                           expression.Member.DeclaringType;
                tableName = sqlWrapper.GetTableName(type);
            }

            if (expression.Expression.NodeType == ExpressionType.Constant)
            {
                tableName = sqlWrapper.GetTableName(sqlWrapper.DefaultType);
            }

            tableAlias = sqlWrapper.GetTableAlias(tableName, tableAlias);

            if (tableAlias.IsNotNullOrEmpty())
            {
                tableAlias += ".";
            }

            if (expression.Expression.NodeType == ExpressionType.Parameter)
            {
                sqlWrapper += tableAlias + sqlWrapper.GetColumnInfo(expression.Member.DeclaringType, expression.Member).columnName;
                if (orders?.Length > 0)
                {
                    sqlWrapper += $" {(orders[0] == OrderType.Descending ? "DESC" : "ASC")}";
                }
            }

            if (expression.Expression.NodeType is ExpressionType.Constant or
                ExpressionType.MemberAccess)
            {
                object convertRes = expression.ToObject();
                if (convertRes != null)
                {
                    if (convertRes is IList collection)
                    {
                        int i = 0;

                        foreach (object item in collection)
                        {
                            SqlExpressionProvider.OrderBy(Expression.Constant(item), sqlWrapper);

                            if (i <= orders.Length - 1)
                            {
                                sqlWrapper += $" {(orders[i] == OrderType.Descending ? "DESC" : "ASC")},";
                            }
                            else if (item.ToString().ContainsIgnoreCase("ASC", "DESC") == false)
                            {
                                sqlWrapper += " ASC,";
                            }
                            else
                            {
                                sqlWrapper += ",";
                            }

                            i++;
                        }

                        _ = sqlWrapper.RemoveLast(',');
                    }

                    if (typeof(string) == convertRes.GetType() && convertRes is string str)
                    {
                        SqlExpressionProvider.OrderBy(Expression.Constant(str), sqlWrapper);

                        if (!str.ContainsIgnoreCase("ASC", "DESC"))
                        {
                            if (orders.Length >= 1)
                            {
                                sqlWrapper += $" {(orders[0] == OrderType.Descending ? "DESC" : "ASC")},";
                            }
                            else
                            {
                                sqlWrapper += " ASC,";
                            }

                            _ = sqlWrapper.RemoveLast(',');
                        }
                    }
                }
            }

            return sqlWrapper;
        }
        #endregion
    }
}