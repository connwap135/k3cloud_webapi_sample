﻿#region License
/***
 * Copyright © 2018-2021, 张强 (943620963@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * without warranties or conditions of any kind, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#endregion

using SQLBuilder.Entry;
using SQLBuilder.Enums;
using SQLBuilder.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SQLBuilder.Expressions
{
    /// <summary>
    /// 表示一个构造函数调用
    /// </summary>
	public class NewExpressionResolver : BaseExpression<NewExpression>
    {
        #region Update
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper Update(NewExpression expression, SqlWrapper sqlWrapper)
        {
            for (int i = 0; i < expression.Members.Count; i++)
            {
                System.Reflection.MemberInfo m = expression.Members[i];
                System.Type t = m.DeclaringType.IsAnonymousType() ?
                    sqlWrapper.DefaultType :
                    m.DeclaringType;

                (string columnName, bool isInsert, bool isUpdate) = sqlWrapper.GetColumnInfo(t, m);
                if (isUpdate)
                {
                    object value = expression.Arguments[i]?.ToObject();
                    if (value != null || (sqlWrapper.IsEnableNullValue && value == null))
                    {
                        sqlWrapper += columnName + " = ";
                        sqlWrapper.AddDbParameter(value);
                        sqlWrapper += ",";
                    }
                }
            }

            _ = sqlWrapper.RemoveLast(',');

            return sqlWrapper;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper Insert(NewExpression expression, SqlWrapper sqlWrapper)
        {
            if (sqlWrapper.DatabaseType != DatabaseType.Oracle)
            {
                _ = sqlWrapper.Append("(");
            }

            List<string> fields = [];
            for (int i = 0; i < expression.Members?.Count; i++)
            {
                System.Reflection.MemberInfo m = expression.Members[i];
                System.Type t = m.DeclaringType.IsAnonymousType() ?
                    sqlWrapper.DefaultType :
                    m.DeclaringType;

                (string columnName, bool isInsert, bool isUpdate) = sqlWrapper.GetColumnInfo(t, m);
                if (isInsert)
                {
                    object value = expression.Arguments[i]?.ToObject();
                    if (value != null || (sqlWrapper.IsEnableNullValue && value == null))
                    {
                        sqlWrapper.AddDbParameter(value);
                        if (!fields.Contains(columnName))
                        {
                            fields.Add(columnName);
                        }

                        sqlWrapper += ",";
                    }
                }
            }
#if NETSTANDARD2_1_OR_GREATER
            if (sqlWrapper[^1] == ',')
#else
            if (sqlWrapper[sqlWrapper.Length - 1] == ',')
#endif
            {
                _ = sqlWrapper.Remove(sqlWrapper.Length - 1, 1);
                _ = sqlWrapper.DatabaseType != DatabaseType.Oracle ? sqlWrapper.Append(")") : sqlWrapper.Append(" FROM DUAL");
            }

            _ = sqlWrapper.Reset(string.Format(sqlWrapper.ToString(), string.Join(",", fields).TrimEnd(',')));

            return sqlWrapper;
        }
        #endregion

        #region Select
        /// <summary>
        /// Select
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper Select(NewExpression expression, SqlWrapper sqlWrapper)
        {
            if (expression.Members != null)
            {
                for (int i = 0; i < expression.Members.Count; i++)
                {
                    Expression argument = expression.Arguments[i];

                    SqlExpressionProvider.Select(argument, sqlWrapper);

                    string fieldName = sqlWrapper.SelectFields[sqlWrapper.FieldCount - 1];
                    if (fieldName.IsNotNullOrEmpty() && fieldName.Contains(".") && !fieldName.Contains("(", ")"))
                    {
                        fieldName = fieldName.Split('.').LastOrDefault();
                    }

                    string memberName = sqlWrapper.GetColumnName(expression.Members[i].Name);

                    //添加字段别名
                    if (argument is MemberExpression memberExpression)
                    {
                        string agrumentName = sqlWrapper.GetColumnName(memberExpression.Member.Name);

                        if (!agrumentName.Equals(!sqlWrapper.IsEnableFormat, memberName))
                        {
                            sqlWrapper.SelectFields[sqlWrapper.FieldCount - 1] += $" AS {memberName}";
                        }
                        else if (!agrumentName.Equals(!sqlWrapper.IsEnableFormat, fieldName))
                        {
                            sqlWrapper.SelectFields[sqlWrapper.FieldCount - 1] += $" AS {agrumentName}";
                        }
                    }
                    else if (argument is ConstantExpression constantExpression)
                    {
                        string agrumentName = sqlWrapper.GetColumnName(constantExpression.Value?.ToString());

                        if (!agrumentName.Equals(!sqlWrapper.IsEnableFormat, memberName))
                        {
                            sqlWrapper.SelectFields[sqlWrapper.FieldCount - 1] += $" AS {memberName}";
                        }
                        else if (!agrumentName.Equals(!sqlWrapper.IsEnableFormat, fieldName))
                        {
                            sqlWrapper.SelectFields[sqlWrapper.FieldCount - 1] += $" AS {agrumentName}";
                        }
                    }
                    else if (argument is MethodCallExpression methodCallExpression)
                    {
                        string agrumentName = methodCallExpression.ToObject<string>(out bool res);
                        if (!res)
                        {
                            agrumentName = sqlWrapper.SelectFields.LastOrDefault();
                        }

                        agrumentName = sqlWrapper.GetColumnName(agrumentName);

                        if (!agrumentName.Equals(!sqlWrapper.IsEnableFormat, memberName))
                        {
                            sqlWrapper.SelectFields[sqlWrapper.FieldCount - 1] += $" AS {memberName}";
                        }
                        else if (!agrumentName.Equals(!sqlWrapper.IsEnableFormat, fieldName))
                        {
                            sqlWrapper.SelectFields[sqlWrapper.FieldCount - 1] += $" AS {agrumentName}";
                        }
                    }
                    else if (argument is BinaryExpression binaryExpression)
                    {
                        string agrumentName = sqlWrapper.GetColumnName(binaryExpression.ToObject()?.ToString());

                        if (!agrumentName.Equals(!sqlWrapper.IsEnableFormat, memberName))
                        {
                            sqlWrapper.SelectFields[sqlWrapper.FieldCount - 1] += $" AS {memberName}";
                        }
                        else if (!agrumentName.Equals(!sqlWrapper.IsEnableFormat, fieldName))
                        {
                            sqlWrapper.SelectFields[sqlWrapper.FieldCount - 1] += $" AS {agrumentName}";
                        }
                    }
                }
            }
            else
            {
                _ = sqlWrapper.AddField("*");
            }

            return sqlWrapper;
        }
        #endregion

        #region Count
        /// <summary>
        /// Count
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper Count(NewExpression expression, SqlWrapper sqlWrapper)
        {
            if (expression.Members != null)
            {
                for (int i = 0; i < expression.Members.Count; i++)
                {
                    Expression argument = expression.Arguments[i];
                    SqlExpressionProvider.Count(argument, sqlWrapper);
                }
            }
            else
            {
                _ = sqlWrapper.AddField("*");
            }

            return sqlWrapper;
        }
        #endregion

        #region Sum
        /// <summary>
        /// Sum
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper Sum(NewExpression expression, SqlWrapper sqlWrapper)
        {
            if (expression.Members != null)
            {
                for (int i = 0; i < expression.Members.Count; i++)
                {
                    Expression argument = expression.Arguments[i];
                    SqlExpressionProvider.Sum(argument, sqlWrapper);
                }
            }

            return sqlWrapper;
        }
        #endregion

        #region Max
        /// <summary>
        /// Max
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper Max(NewExpression expression, SqlWrapper sqlWrapper)
        {
            if (expression.Members != null)
            {
                for (int i = 0; i < expression.Members.Count; i++)
                {
                    Expression argument = expression.Arguments[i];
                    SqlExpressionProvider.Max(argument, sqlWrapper);
                }
            }

            return sqlWrapper;
        }
        #endregion

        #region Min
        /// <summary>
        /// Min
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper Min(NewExpression expression, SqlWrapper sqlWrapper)
        {
            if (expression.Members != null)
            {
                for (int i = 0; i < expression.Members.Count; i++)
                {
                    Expression argument = expression.Arguments[i];
                    SqlExpressionProvider.Min(argument, sqlWrapper);
                }
            }

            return sqlWrapper;
        }
        #endregion

        #region Avg
        /// <summary>
        /// Avg
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper Avg(NewExpression expression, SqlWrapper sqlWrapper)
        {
            if (expression.Members != null)
            {
                for (int i = 0; i < expression.Members.Count; i++)
                {
                    Expression argument = expression.Arguments[i];
                    SqlExpressionProvider.Avg(argument, sqlWrapper);
                }
            }

            return sqlWrapper;
        }
        #endregion

        #region GroupBy
        /// <summary>
        /// GroupBy
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper GroupBy(NewExpression expression, SqlWrapper sqlWrapper)
        {
            foreach (Expression item in expression.Arguments)
            {
                SqlExpressionProvider.GroupBy(item, sqlWrapper);
                sqlWrapper += ",";
            }

            _ = sqlWrapper.RemoveLast(',');

            return sqlWrapper;
        }
        #endregion

        #region OrderBy
        /// <summary>
        /// OrderBy
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <param name="orders">排序方式</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper OrderBy(NewExpression expression, SqlWrapper sqlWrapper, params OrderType[] orders)
        {
            for (int i = 0; i < expression.Arguments.Count; i++)
            {
                SqlExpressionProvider.OrderBy(expression.Arguments[i], sqlWrapper);

                if (i <= orders.Length - 1)
                {
                    sqlWrapper += $" {(orders[i] == OrderType.Descending ? "DESC" : "ASC")},";
                }
                else
                {
                    sqlWrapper += " ASC,";
                }
            }

            _ = sqlWrapper.RemoveLast(',');

            return sqlWrapper;
        }
        #endregion
    }
}