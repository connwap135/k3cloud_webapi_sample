﻿#region License
/***
 * Copyright © 2018-2025, 张强 (943620963@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * without warranties or conditions of any kind, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#endregion

using SQLBuilder.Entry;
using SQLBuilder.Extensions;
using System;
using System.Linq.Expressions;

namespace SQLBuilder.Expressions
{
    /// <summary>
    /// 表示具有二进制运算符的表达式
    /// </summary>
	public class BinaryExpressionResolver : BaseExpression<BinaryExpression>
    {
        #region Select
        /// <summary>
        /// Select
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper Select(BinaryExpression expression, SqlWrapper sqlWrapper)
        {
            object field = expression?.ToObject();
            if (field != null)
            {
                SqlExpressionProvider.Select(Expression.Constant(field), sqlWrapper);
            }

            return sqlWrapper;
        }
        #endregion

        #region Join
        /// <summary>
        /// Join
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper Join(BinaryExpression expression, SqlWrapper sqlWrapper)
        {
            int startIndex = sqlWrapper.Length;

            //嵌套解析
            int operatorIndex = ExpressionNestedResolver(expression, sqlWrapper, nameof(Join));

            //取非解析
            ExpressionNotResolver(expression, sqlWrapper, startIndex, operatorIndex);

            return sqlWrapper;
        }
        #endregion

        #region Where
        /// <summary>
        /// Where
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper Where(BinaryExpression expression, SqlWrapper sqlWrapper)
        {
            int startIndex = sqlWrapper.Length;

            //嵌套解析
            int operatorIndex = ExpressionNestedResolver(expression, sqlWrapper, nameof(Where));

            //取非解析
            ExpressionNotResolver(expression, sqlWrapper, startIndex, operatorIndex);

            return sqlWrapper;
        }
        #endregion

        #region Having
        /// <summary>
        /// Having
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <returns>SqlWrapper</returns>
        public override SqlWrapper Having(BinaryExpression expression, SqlWrapper sqlWrapper)
        {
            int startIndex = sqlWrapper.Length;

            //嵌套解析
            int operatorIndex = ExpressionNestedResolver(expression, sqlWrapper, nameof(Having));

            //取非解析
            ExpressionNotResolver(expression, sqlWrapper, startIndex, operatorIndex);

            return sqlWrapper;
        }
        #endregion

        #region OperatorResolver
        /// <summary>
        /// Expression操作符解析
        /// </summary>
        /// <param name="expressionNodeType">表达式树节点类型</param>
        /// <param name="operatorIndex">操作符索引</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <param name="useIs">是否使用is</param>
        public static void OperatorResolver(ExpressionType expressionNodeType, int operatorIndex, SqlWrapper sqlWrapper, bool useIs = false)
        {
            _ = expressionNodeType switch
            {
                ExpressionType.And or ExpressionType.AndAlso => sqlWrapper.Insert(operatorIndex, " AND "),
                ExpressionType.Equal => useIs ? sqlWrapper.Insert(operatorIndex, " IS ") : sqlWrapper.Insert(operatorIndex, " = "),
                ExpressionType.GreaterThan => sqlWrapper.Insert(operatorIndex, " > "),
                ExpressionType.GreaterThanOrEqual => sqlWrapper.Insert(operatorIndex, " >= "),
                ExpressionType.NotEqual => useIs ? sqlWrapper.Insert(operatorIndex, " IS NOT ") : sqlWrapper.Insert(operatorIndex, " <> "),
                ExpressionType.Or or ExpressionType.OrElse => sqlWrapper.Insert(operatorIndex, " OR "),
                ExpressionType.LessThan => sqlWrapper.Insert(operatorIndex, " < "),
                ExpressionType.LessThanOrEqual => sqlWrapper.Insert(operatorIndex, " <= "),
                ExpressionType.Add => sqlWrapper.Insert(operatorIndex, " + "),
                ExpressionType.Subtract => sqlWrapper.Insert(operatorIndex, " - "),
                ExpressionType.Multiply => sqlWrapper.Insert(operatorIndex, " * "),
                ExpressionType.Divide => sqlWrapper.Insert(operatorIndex, " / "),
                ExpressionType.Modulo => sqlWrapper.Insert(operatorIndex, " % "),
                _ => throw new NotImplementedException("NotImplemented ExpressionType " + expressionNodeType),
            };
        }
        #endregion

        #region ExpressionNestedResolver
        /// <summary>
        /// Expression嵌套解析
        /// </summary>
        /// <param name="expression">表达式树</param>
        /// <param name="sqlWrapper">sql包装器</param>
        /// <param name="method">方法名，可选值：Join、Having、Where</param>
        public static int ExpressionNestedResolver(BinaryExpression expression, SqlWrapper sqlWrapper, string method)
        {
            //左侧嵌套
            BinaryExpression lExpr = expression.Left as BinaryExpression;

            bool llIsBinaryExpr = lExpr?.Left is BinaryExpression;
            bool llIsBoolMethod = typeof(bool) == (lExpr?.Left as MethodCallExpression)?.Method.ReturnType;

            bool lrIsBinaryExpr = lExpr?.Right is BinaryExpression;
            bool lrIsBoolMethod = typeof(bool) == (lExpr?.Right as MethodCallExpression)?.Method.ReturnType;

            bool lNested = (llIsBinaryExpr || llIsBoolMethod) && (lrIsBinaryExpr || lrIsBoolMethod);

            if (lNested)
            {
                _ = sqlWrapper.Append("(");
            }

            if (method.EqualIgnoreCase(nameof(Join)))
            {
                SqlExpressionProvider.Join(expression.Left, sqlWrapper);
            }
            else if (method.EqualIgnoreCase(nameof(Having)))
            {
                SqlExpressionProvider.Having(expression.Left, sqlWrapper);
            }
            else
            {
                SqlExpressionProvider.Where(expression.Left, sqlWrapper);
            }

            if (lNested)
            {
                _ = sqlWrapper.Append(")");
            }

            int operatorIndex = sqlWrapper.Length;

            //右侧嵌套
            BinaryExpression rExpr = expression.Right as BinaryExpression;

            bool rlIsBinaryExpr = rExpr?.Left is BinaryExpression;
            bool rlIsBoolMethod = typeof(bool) == (rExpr?.Left as MethodCallExpression)?.Method.ReturnType;

            bool rrIsBinaryExpr = rExpr?.Right is BinaryExpression;
            bool rrIsBoolMethod = typeof(bool) == (rExpr?.Right as MethodCallExpression)?.Method.ReturnType;

            bool rNested = (rlIsBinaryExpr || rlIsBoolMethod) && (rrIsBinaryExpr || rrIsBoolMethod);

            if (rNested)
            {
                _ = sqlWrapper.Append("(");
            }

            if (method.EqualIgnoreCase(nameof(Having)))
            {
                SqlExpressionProvider.Having(expression.Right, sqlWrapper);
            }
            else
            {
                SqlExpressionProvider.Where(expression.Right, sqlWrapper);
            }

            if (rNested)
            {
                _ = sqlWrapper.Append(")");
            }

            return operatorIndex;
        }
        #endregion

        #region ExpressionNotResolver
        /// <summary>
        /// Expression表达式取非解析
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="sqlWrapper"></param>
        /// <param name="startIndex"></param>
        /// <param name="operatorIndex"></param>
        /// <returns></returns>
        public static void ExpressionNotResolver(BinaryExpression expression, SqlWrapper sqlWrapper, int startIndex, int operatorIndex)
        {
            //表达式左侧为bool类型常量且为true时，不进行sql拼接
            if (!(expression.Left.NodeType == ExpressionType.Constant && expression.Left.ToObject() is bool left && left))
            {
                //若表达式右侧为bool类型，且为false时，条件取非
                if ((ExpressionType.Constant == expression.Right.NodeType ||
                    (ExpressionType.Convert == expression.Right.NodeType &&
                    expression.Right is UnaryExpression unary &&
                    ExpressionType.Constant == unary.Operand.NodeType)) &&
                    expression.Right.ToObject() is bool res)
                {
                    if (expression.NodeType != ExpressionType.NotEqual && !res)
                    {
                        string subString = sqlWrapper.Substring(startIndex, sqlWrapper.Length - startIndex);

                        //IS NOT、IS                      
                        if (subString.Contains(" IS NOT "))
                        {
                            int index = sqlWrapper.LastIndexOf(" IS NOT ");
                            if (index != -1)
                            {
                                _ = sqlWrapper.Replace(" IS NOT ", " IS ", index, 8);
                            }
                        }
                        else if (subString.Contains(" IS "))
                        {
                            if (subString.Contains(" IS TRUE"))
                            {
                                int index = sqlWrapper.LastIndexOf(" IS TRUE");
                                if (index != -1)
                                {
                                    _ = sqlWrapper.Replace(" IS TRUE", " IS FALSE", index, 8);
                                }
                            }
                            else
                            {
                                int index = sqlWrapper.LastIndexOf(" IS ");
                                if (index != -1)
                                {
                                    _ = sqlWrapper.Replace(" IS ", " IS NOT ", index, 4);
                                }
                            }
                        }

                        //NOT LIKE、LIKE
                        if (subString.Contains(" NOT LIKE "))
                        {
                            int index = sqlWrapper.LastIndexOf(" NOT LIKE ");
                            if (index != -1)
                            {
                                _ = sqlWrapper.Replace(" NOT LIKE ", " LIKE ", index, 10);
                            }
                        }
                        else if (subString.Contains(" LIKE "))
                        {
                            int index = sqlWrapper.LastIndexOf(" LIKE ");
                            if (index != -1)
                            {
                                _ = sqlWrapper.Replace(" LIKE ", " NOT LIKE ", index, 6);
                            }
                        }

                        //NOT IN、IN
                        if (subString.Contains(" NOT IN "))
                        {
                            int index = sqlWrapper.LastIndexOf(" NOT IN ");
                            if (index != -1)
                            {
                                _ = sqlWrapper.Replace(" NOT IN ", " IN ", index, 8);
                            }
                        }
                        else if (subString.Contains(" IN "))
                        {
                            int index = sqlWrapper.LastIndexOf(" IN ");
                            if (index != -1)
                            {
                                _ = sqlWrapper.Replace(" IN ", " NOT IN ", index, 4);
                            }
                        }

                        //AND、OR
                        if (subString.Contains(" AND "))
                        {
                            int index = sqlWrapper.LastIndexOf(" AND ");
                            if (index != -1)
                            {
                                _ = sqlWrapper.Replace(" AND ", " OR ", index, 5);
                            }
                        }
                        if (subString.Contains(" OR "))
                        {
                            int index = sqlWrapper.LastIndexOf(" OR ");
                            if (index != -1)
                            {
                                _ = sqlWrapper.Replace(" OR ", " AND ", index, 4);
                            }
                        }

                        //=、<>
                        if (subString.Contains(" = "))
                        {
                            if (subString.Contains(" = 1"))
                            {
                                int index = sqlWrapper.LastIndexOf(" = 1");
                                if (index != -1)
                                {
                                    _ = sqlWrapper.Replace(" = 1", " = 0", index, 4);
                                }
                            }
                            else
                            {
                                int index = sqlWrapper.LastIndexOf(" = ");
                                if (index != -1)
                                {
                                    _ = sqlWrapper.Replace(" = ", " <> ", index, 3);
                                }
                            }
                        }
                        if (subString.Contains(" <> "))
                        {
                            int index = sqlWrapper.LastIndexOf(" <> ");
                            if (index != -1)
                            {
                                _ = sqlWrapper.Replace(" <> ", " = ", index, 4);
                            }
                        }

                        //>、<
                        if (subString.Contains(" > "))
                        {
                            int index = sqlWrapper.LastIndexOf(" > ");
                            if (index != -1)
                            {
                                _ = sqlWrapper.Replace(" > ", " <= ", index, 3);
                            }
                        }
                        if (subString.Contains(" < "))
                        {
                            int index = sqlWrapper.LastIndexOf(" < ");
                            if (index != -1)
                            {
                                _ = sqlWrapper.Replace(" < ", " >= ", index, 3);
                            }
                        }

                        //>=、<=
                        if (subString.Contains(" >= "))
                        {
                            int index = sqlWrapper.LastIndexOf(" >= ");
                            if (index != -1)
                            {
                                _ = sqlWrapper.Replace(" >= ", " < ", index, 4);
                            }
                        }
                        if (subString.Contains(" <= "))
                        {
                            int index = sqlWrapper.LastIndexOf(" <= ");
                            if (index != -1)
                            {
                                _ = sqlWrapper.Replace(" <= ", " > ", index, 4);
                            }
                        }
                    }
                }
                else
                {
                    OperatorResolver(
                        expression.NodeType,
                        operatorIndex,
                        sqlWrapper,
                        sqlWrapper.EndsWith("NULL"));
                }
            }
        }
        #endregion
    }
}