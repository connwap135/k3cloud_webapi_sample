﻿// https://github.com/dotnet/runtime/blob/main/src/libraries/Microsoft.Extensions.Caching.Abstractions/src/IDistributedCache.cs
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace MyRedisLibrary.Microsoft.Extensions.Caching.Distributed
{
    public interface IDistributedCache
    {
        void Add<V>(string key, V value);
        void Add<V>(string key, V value, int cacheDurationInSeconds);
        bool ContainsKey(string key);
        bool ContainsKey<V>(string key);
        byte[] Get(string key);
        V Get<V>(string key);
        IEnumerable<string> GetAllKey();
        IEnumerable<string> GetAllKey<V>();
        Task<byte[]> GetAsync(string key, CancellationToken token = default);
        V GetOrCreate<V>(string cacheKey, Func<V> create, int cacheDurationInSeconds = int.MaxValue);
        void Refresh(string key);
        Task RefreshAsync(string key, CancellationToken token = default);
        void Remove(string key);
        void Remove<V>(string key);
        Task RemoveAsync(string key, CancellationToken token = default);
        void Set(string key, byte[] value, DistributedCacheEntryOptions options);
        Task SetAsync(string key, byte[] value, DistributedCacheEntryOptions options, CancellationToken token = default);
    }
}
