﻿using Microsoft.Extensions.Logging;
using System;

namespace MyRedisLibrary.RedisCaches
{
    public partial class RedisCache
    {
        private static partial class Log
        {
            [LoggerMessage(1, LogLevel.Warning, "Could not determine the Redis server version. Falling back to use HMSET command instead of HSET.", EventName = "CouldNotDetermineServerVersion")]
            public static partial void CouldNotDetermineServerVersion(ILogger logger, Exception exception);

            [LoggerMessage(2, LogLevel.Debug, "Unable to add library name suffix.", EventName = "UnableToAddLibraryNameSuffix")]
            internal static partial void UnableToAddLibraryNameSuffix(ILogger logger, Exception exception);
        }
    }
}
