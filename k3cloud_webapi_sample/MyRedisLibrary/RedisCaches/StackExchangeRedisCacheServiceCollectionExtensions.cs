﻿using MyRedisLibrary.Microsoft.Extensions.Caching.Distributed;
using MyRedisLibrary.RedisCaches;
using MyRedisLibrary.ThrowHelper;
using System;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class StackExchangeRedisCacheServiceCollectionExtensions
    {
        /// <summary>
        /// 配置StackExchange.Redis服务
        /// </summary>
        public static IServiceCollection AddStackExchangeRedisCache(this IServiceCollection services, Action<RedisCacheOptions> setupAction)
        {
            ArgumentNullThrowHelper.ThrowIfNull(nameof(services));
            ArgumentNullThrowHelper.ThrowIfNull(nameof(setupAction));

            services.AddOptions();
            services.Configure(setupAction);
            services.Add(ServiceDescriptor.Singleton<IDistributedCache, RedisCacheImpl>());

            return services;
        }
    }
}
