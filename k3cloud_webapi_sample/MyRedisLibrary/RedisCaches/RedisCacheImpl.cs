﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace MyRedisLibrary.RedisCaches
{
    internal sealed class RedisCacheImpl : RedisCache
    {
        public RedisCacheImpl(IOptions<RedisCacheOptions> optionsAccessor, ILogger<RedisCache> logger) : base(optionsAccessor, logger)
        {
        }
        public RedisCacheImpl(IOptions<RedisCacheOptions> optionsAccessor) : base(optionsAccessor)
        {
        }
    }
}
