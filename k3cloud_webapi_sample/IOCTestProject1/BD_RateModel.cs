﻿using K3Cloud.WebApi.Core.IoC.Attributes;
using K3Cloud.WebApi.Core.IoC.DataEntity;

namespace IOCTestProject1
{
    [Alias("BD_Rate")]
    internal class BD_RateModel
    {
        public int FRateID { get; set; }
        /// <summary>
        /// 汇率类型
        /// </summary>
        public K3Number FRATETYPEID { get; set; }
        /// <summary>
        /// 原币
        /// </summary>
        public K3Number FCyForID { get; set; }
        /// <summary>
        /// 目标币
        /// </summary>
        public K3Number FCyToID { get; set; }
        /// <summary>
        /// 直接汇率
        /// </summary>
        public decimal FExchangeRate { get; set; }
        /// <summary>
        /// 生效日期
        /// </summary>
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime FBegDate { get; set; }
        /// <summary>
        /// 失效日期
        /// </summary>
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime FEndDate { get; set; }
        /// <summary>
        /// 使用组织
        /// </summary>
        public K3Number FUseOrgId { get; set; }
        /// <summary>
        /// 创建组织
        /// </summary>
        public K3Number FCreateOrgId { get; set; }
    }
}