﻿namespace IOCTestProject1
{
    public class BD_Rate
    {
        public int FRateID { get; set; }
        /// <summary>
        /// 汇率类型
        /// </summary>
        public string FRATETYPEID { get; set; }
        /// <summary>
        /// 原币
        /// </summary>
        public string FCyForID { get; set; }
        /// <summary>
        /// 目标币
        /// </summary>
        public string FCyToID { get; set; }
        /// <summary>
        /// 直接汇率
        /// </summary>
        public decimal FExchangeRate { get; set; }
        /// <summary>
        /// 生效日期
        /// </summary>
        public DateTime FBegDate { get; set; }
        /// <summary>
        /// 失效日期
        /// </summary>
        public DateTime FEndDate { get; set; }
        /// <summary>
        /// 使用组织
        /// </summary>
        public string FUseOrgId { get; set; }
        /// <summary>
        /// 创建组织
        /// </summary>
        public string FCreateOrgId { get; set; }
    }
}