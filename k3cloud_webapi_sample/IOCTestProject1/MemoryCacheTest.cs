﻿
using K3Cloud.WebApi.Core.IoC.Cache;
using Microsoft.Extensions.DependencyInjection;

namespace IOCTestProject1
{
    public class MemoryCacheTest
    {
        private ICacheService cache;
        [SetUp]
        public void Setup()
        {
            ServiceCollection services = new();
            _ = services.AddSingleton<ICacheService, MemoryCacheService>();
            cache = services.BuildServiceProvider().GetService<ICacheService>();
        }
        [Test]
        public void Add_ShouldAddValueToCache()
        {
            string cacheKey = "testKey";
            string value = "testValue";
            cache.Add(cacheKey, value);
            string value1 = cache.Get<string>(cacheKey);
            Assert.That(value, Is.EqualTo(value1));
        }

        [Test]
        public void Remove_ShouldRemoveFromCache()
        {
            string cacheKey = "testKey";
            cache.Remove<string>(cacheKey);
            string value1 = cache.Get<string>(cacheKey);
            Assert.That(value1, Is.Null);
        }
    }
}
