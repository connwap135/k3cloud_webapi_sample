﻿using K3Cloud.WebApi.Core.IoC.Attributes;

namespace IOCTestProject1;

public class BD_MATERIAL
{
    /// <summary>
    /// FMATERIALID
    /// </summary>
    [Key]
    [Display(Name = "主键")]
    public int FMATERIALID { get; set; }
    /// <summary>
    /// 数据状态
    /// </summary>
    [Display(Name = "数据状态")]
    public string? FDocumentStatus { get; set; }
    /// <summary>
    /// 禁用状态
    /// </summary>
    [Display(Name = "禁用状态")]
    public string? FForbidStatus { get; set; }
    /// <summary>
    /// 名称
    /// </summary>
    [Display(Name = "名称")]
    public string FName { get; set; }
    /// <summary>
    /// 编码
    /// </summary>
    [Display(Name = "编码")]
    public string? FNumber { get; set; }
    /// <summary>
    /// 描述
    /// </summary>
    [Display(Name = "描述")]
    [Alias("FDescription")]
    public string? FModel { get; set; }
    /// <summary>
    /// 规格型号
    /// </summary>
    [Display(Name = "规格型号")]
    public string? FSpecification { get; set; }
    /// <summary>
    /// 物料属性
    /// </summary>
    [Display(Name = "物料属性")]
    public EnumErpCls FErpClsID { get; set; }
    /// <summary>
    /// 允许库存
    /// </summary>
    [Display(Name = "允许库存")]
    public bool FIsInventory { get; set; }
    /// <summary>
    /// 允许销售
    /// </summary>
    [Display(Name = "允许销售")]
    public bool FIsSale { get; set; }
    /// <summary>
    /// 允许委外
    /// </summary>
    [Display(Name = "允许委外")]
    public bool FIsSubContract { get; set; }
    /// <summary>
    /// 允许生产
    /// </summary>
    [Display(Name = "允许生产")]
    public bool FIsProduce { get; set; }
    /// <summary>
    /// 允许采购
    /// </summary>
    [Display(Name = "允许采购")]
    public bool FIsPurchase { get; set; }
    /// <summary>
    /// 存货类别名称
    /// </summary>
    [Display(Name = "存货类别")]
    public string FCategoryID__FName { get; set; }
}
public enum EnumErpCls
{
    /// <summary>
    /// 外购
    /// </summary>
    [Display(Name = "外购")]
    外购 = 1,

    /// <summary>
    ///  自制
    /// </summary>
    [Display(Name = "自制")]
    自制 = 2,

    /// <summary>
    /// 委外
    /// </summary>
    [Display(Name = "委外")]
    委外 = 3,

    /// <summary>
    /// 特征
    /// </summary>
    [Display(Name = "特征")]
    特征 = 4,

    /// <summary>
    /// 虚拟
    /// </summary>
    [Display(Name = "虚拟")]
    虚拟 = 5,

    /// <summary>
    /// 服务
    /// </summary>
    [Display(Name = "服务")]
    服务 = 6,

    /// <summary>
    /// 一次性
    /// </summary>
    [Display(Name = "一次性")]
    一次性 = 7,
    /// <summary>
    /// 配置
    /// </summary>
    [Display(Name = "配置")]
    配置 = 9,

    /// <summary>
    /// 资产
    /// </summary>
    [Display(Name = "资产")]
    资产 = 10,

    /// <summary>
    /// 费用
    /// </summary>
    [Display(Name = "费用")]
    费用 = 11,

    /// <summary>
    /// 模型
    /// </summary>
    [Display(Name = "模型")]
    模型 = 12,
    /// <summary>
    /// 产品系列
    /// </summary>
    [Display(Name = "产品系列")]
    产品系列 = 13
}
